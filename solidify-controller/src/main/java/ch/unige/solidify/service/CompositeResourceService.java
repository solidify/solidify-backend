/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - CompositeResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import static ch.unige.solidify.util.StringTool.isNullOrEmpty;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.validation.ValidationError;

/**
 * <p>
 * A service for resources that implement RemoteResourceContainer.
 * </p>
 * <p>
 * Sub resources that must be fetched remotely are automatically retrieved by using a RemoteResourceService for each sub resource type
 * and set as property on the RemoteResourceContainer object
 * </p>
 *
 * @param <T> the type of the resource
 */
public abstract class CompositeResourceService<T extends Resource> extends ResourceService<T> {

  private static final Logger log = LoggerFactory.getLogger(CompositeResourceService.class);

  /**
   * Use a LinkedRESTResourceChecker instead of calling directly validateLinkedRESTResources() method
   * in order to be able to disable validation during tests. (during tests
   * TestLinkedRESTResourceChecker is injected, thus bypassing linked resources validation)
   */
  @Autowired
  private LinkedResourceChecker<T> linkedResourceValidator;

  @Override
  public Page<T> findAll(Pageable pageable) {
    Page<T> page = super.findAll(pageable);
    this.completeEmbeddedResources(page);
    return page;
  }

  @Override
  public Page<T> findAll(Specification<T> spec, Pageable pageable) {
    Page<T> page = super.findAll(spec, pageable);
    this.completeEmbeddedResources(page);
    return page;
  }

  @Override
  public T findOne(String resId) {
    final T item = super.findOne(resId);
    this.completeEmbeddedResources(item);
    return item;
  }

  @Override
  public void patchResource(T item, Map<String, Object> updateMap) {
    super.patchResource(item, updateMap);
    this.completeEmbeddedResources(item);
  }

  /**
   * Complete a page of resources by adding all its subresources
   *
   * @param items
   */
  protected void completeEmbeddedResources(Iterable<T> items) {
    for (final T item : items) {
      this.completeEmbeddedResources(item);
    }
  }

  /**
   * Complete a resource by adding all its subresources
   *
   * @param item the resource to be completed
   */
  protected void completeEmbeddedResources(T item) {
    if (item instanceof RemoteResourceContainer remoteResourceContainer) {
      final List<Class<? extends Resource>> embeddedResources = remoteResourceContainer.getEmbeddedResourceTypes();
      for (final Class<? extends Resource> type : embeddedResources) {
        this.completeEmbeddedResources(item, type);
      }
    }
  }

  /**
   * Complete a resource by adding a subresource of a given type
   *
   * @param item the resource to be completed
   * @param subResourceType the subresource type
   * @param <V> The subresource type as a parameter type
   */
  protected <V extends Resource> void completeEmbeddedResources(T item, Class<V> subResourceType) {
    if (item instanceof RemoteResourceContainer) {
      try {
        // Get sub resource Id from item property (i.e: getLanguageId)
        String methodName = "get" + subResourceType.getSimpleName() + "Id";
        final Method getter = item.getClass().getMethod(methodName);
        final String subResourceId = (String) getter.invoke(item);
        if (!isNullOrEmpty(subResourceId)) {
          final Resource child = this.getSubResource(subResourceId, subResourceType);
          if (child == null) {
            log.error(this.messageService.get("resources.embedded.notfound",
                    new Object[] { subResourceId, subResourceType.getName(), item.getResId(), item.getClass().getName() }));
          }
          // Set the item property with fetched object
          methodName = "set" + subResourceType.getSimpleName();
          final Method setter = item.getClass().getMethod(methodName, subResourceType);
          setter.invoke(item, child);
        }
      } catch (final Exception ex) {
        log.error("Unable to retrieve subresource {} : {}", subResourceType.getSimpleName(), item.getResId(), ex);
      }
    } else {
      throw new IllegalArgumentException("item must be a RemoteResourceContainer");
    }
  }

  public <S extends Resource> S getSubResource(String subResourceId, Class<S> subResourceType) {
    final RemoteResourceService<S> service = this.getResourceService(subResourceType);
    return service.findOneWithCache(subResourceId);
  }

  public <S extends Resource, U extends RemoteResourceContainer> RestCollection<S> getSubResources(U parentItem, Class<S> subResourceType,
          Pageable pageable) {

    Page<String> subResourceIds = parentItem.getSubResourceIds(subResourceType, pageable);

    final List<S> items = new ArrayList<>();

    if (subResourceIds != null) {
      for (final String subResourceId : subResourceIds) {
        if (!isNullOrEmpty(subResourceId)) {
          final S item = this.getSubResource(subResourceId, subResourceType);
          if (item != null) {
            items.add(item);
          }
        }
      }
      Page<S> page = new PageImpl<>(items, pageable, subResourceIds.getTotalElements());
      return new RestCollection<>(page, pageable);
    } else {
      return new RestCollection<>();
    }
  }

  protected <S extends Resource> RemoteResourceService<S> getResourceService(Class<S> type) {
    throw new UnsupportedOperationException("Missing resource service for type " + type.getName());
  }

  @Override
  protected void validate(T item) {
    super.validate(item);
    final BindingResult errors = new BeanPropertyBindingResult(item, item.getClass().getName());
    this.linkedResourceValidator.validateLinkedResources(this, item, errors);
    if (errors.hasErrors()) {
      throw new SolidifyValidationException(new ValidationError(errors));
    }
  }
}
