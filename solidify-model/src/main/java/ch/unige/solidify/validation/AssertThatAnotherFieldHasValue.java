/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - AssertThatAnotherFieldHasValue.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ TYPE, ANNOTATION_TYPE, FIELD })
@Retention(RUNTIME)
@Constraint(validatedBy = AssertThatAnotherFieldHasValueValidator.class)
@Documented
public @interface AssertThatAnotherFieldHasValue {

  @Target({ TYPE, ANNOTATION_TYPE, FIELD })
  @Retention(RUNTIME)
  @Documented
  @interface List {
    AssertThatAnotherFieldHasValue[] value();
  }

  String dependFieldName();

  String fieldName();

  String fieldValue();

  Class<?>[] groups() default {};

  String message() default "{message.validator.assert.another.field.has.value}";

  Class<? extends Payload>[] payload() default {};
}
