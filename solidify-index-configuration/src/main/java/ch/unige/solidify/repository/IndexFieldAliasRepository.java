/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Management - IndexFieldAliasRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.repository;

import java.util.List;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.controller.IndexConfigurationController;
import ch.unige.solidify.model.dto.FacetSortDTO;
import ch.unige.solidify.model.index.IndexFieldAlias;

@Repository
@ConditionalOnBean(IndexConfigurationController.class)
public interface IndexFieldAliasRepository extends SolidifyRepository<IndexFieldAlias> {

  IndexFieldAlias findByIndexNameAndAlias(String index, String alias);

  @Query("SELECT NEW ch.unige.solidify.model.dto.FacetSortDTO(ifa.resId, ifa.facetOrder) FROM IndexFieldAlias ifa "
          + "WHERE ifa.indexName = :indexName AND ifa.facetOrder IS NOT NULL "
          + "AND ifa.facetOrder = (SELECT MAX(ifa2.facetOrder) FROM IndexFieldAlias ifa2 WHERE ifa2.indexName = :indexName)")
  List<FacetSortDTO> findByMaxFacetOrderAndIndexName(String indexName);

}
