/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Remote Search - AssociationIndexReadOnlyRemoteController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller.index;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.service.CompositeNoSqlResourceService;
import ch.unige.solidify.service.IndexSearchRemoteService;

@RootPermissions
public abstract class AssociationIndexReadOnlyRemoteController<T extends NoSqlResource, V extends IndexMetadata>
        extends SolidifyController {

  protected CompositeNoSqlResourceService<T> noSqlResourceService;

  protected IndexSearchRemoteService<V> indexSearchService;

  protected AssociationIndexReadOnlyRemoteController(
          CompositeNoSqlResourceService<T> noSqlResourceService,
          IndexSearchRemoteService<V> indexSearchService) {
    this.noSqlResourceService = noSqlResourceService;
    this.indexSearchService = indexSearchService;
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<V> get(@PathVariable String parentid, @PathVariable String id) {
    this.checkIfResourceExists(parentid);
    final V item = this.noSqlResourceService.findByParentIdAndChildId(parentid, this.getMainResourceClass(),
            this.getSubResourceClass(), id);
    if (item != null) {
      this.addLinks(parentid, item);
      return new ResponseEntity<>(item, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping
  public HttpEntity<RestCollection<V>> list(@PathVariable String parentid, @ModelAttribute V filterItem,
          Pageable pageable) {
    final T parent = this.checkIfResourceExists(parentid);
    if (parent == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      final Page<V> list = this.noSqlResourceService.findByParentId(parentid, this.getMainResourceClass(),
              this.getSubResourceClass(), pageable);

      for (final V v : list) {
        this.addLinks(parentid, v);
      }

      final RestCollection<V> collection = this.processPageAndLinks(list, parentid, pageable);
      return new ResponseEntity<>(collection, HttpStatus.OK);
    }
  }

  protected void addLinks(String parentid, V v) {
    v.removeLinks();
    v.addLinks(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass())
            .list(parentid, (IndexMetadata) null, (Pageable) null)));
  }

  protected <W extends RepresentationModel<W>> void addOthersLinks(String parentid, W w) {
    // Do nothing
    // Could be overridden by subclass
  }

  protected T checkIfResourceExists(String id) {
    return this.noSqlResourceService.findOne(id);
  }

  protected abstract String getIndex();

  protected abstract Class<T> getMainResourceClass();

  protected abstract Class<V> getSubResourceClass();

  protected RestCollection<V> processPageAndLinks(Page<V> list, String parentid, Pageable pageable) {
    final RestCollection<V> collection = new RestCollection<>(list, pageable);
    collection.add(WebMvcLinkBuilder.linkTo(this.getClass(), parentid).withSelfRel());
    collection.add(Tool.parentLink(
            WebMvcLinkBuilder.linkTo(this.getClass(), parentid).toUriComponentsBuilder())
            .withRel("parent"));
    this.addSortLinks(linkTo(this.getClass(), parentid, pageable), collection);
    this.addPageLinks(WebMvcLinkBuilder.linkTo(this.getClass(), parentid), collection,
            pageable);
    this.addOthersLinks(parentid, collection);
    return collection;
  }

}
