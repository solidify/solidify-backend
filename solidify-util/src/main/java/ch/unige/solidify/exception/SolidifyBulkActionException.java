/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - SolidifyBulkActionException.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.exception;

import java.util.Map;
import java.util.Map.Entry;

public class SolidifyBulkActionException extends SolidifyRuntimeException {

  private static final long serialVersionUID = 7823180563330529746L;

  private final Map<String, String> errorList;

  public SolidifyBulkActionException(String message, Map<String, String> errorList) {
    super(message);
    this.errorList = errorList;
  }

  public Map<String, String> getErrorList() {
    return this.errorList;
  }

  public String getDetailedMessage() {
    StringBuilder sb = new StringBuilder();
    for (Entry<String, String> error : this.errorList.entrySet()) {
      sb.append(error.getKey()).append(" : ").append(error.getValue()).append(System.lineSeparator());
    }
    return sb.toString();
  }

}
