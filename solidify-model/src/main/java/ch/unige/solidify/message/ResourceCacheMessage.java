/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ResourceCacheMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.message;

import java.io.Serial;

import ch.unige.solidify.rest.Resource;

public final class ResourceCacheMessage extends CacheMessage {

  @Serial
  private static final long serialVersionUID = 1L;

  private final Class<? extends Resource> resourceClass;
  private final String resId;

  @SuppressWarnings("unchecked")
  public ResourceCacheMessage(Class<?> clazz, String resId) {
    this.resourceClass = (Class<? extends Resource>) clazz;
    this.resId = resId;

  }

  public String getResId() {
    return this.resId;
  }

  public Class<? extends Resource> getResourceClass() {
    return this.resourceClass;
  }

  @Override
  public String toString() {
    return "ResourceCacheMessage [resourceClass=" + this.resourceClass.getSimpleName() + ", resId=" + this.resId + "]";
  }
}
