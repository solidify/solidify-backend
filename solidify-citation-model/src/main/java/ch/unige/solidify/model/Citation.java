/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Citation Model - Citation.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.CitationConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "itemId", "mode", "language", "style", "outputFormat" }) })
public class Citation extends ResourceNormalized {

  public enum Mode {
    BIBLIOGRAPHY, CITATION
  }

  public enum OutputFormat {
    TEXT, HTML, ASCII_DOC, FO, RTF
  }

  @NotNull
  private String itemId;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false, length = SolidifyConstants.DB_SHORT_STRING_LENGTH)
  private Mode mode;

  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_SHORT_STRING_LENGTH)
  private String language;

  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_MEDIUM_STRING_LENGTH)
  private String style;

  @Enumerated(EnumType.STRING)
  @Column(nullable = false, length = SolidifyConstants.DB_SHORT_STRING_LENGTH)
  private OutputFormat outputFormat;

  @NotNull
  @Size(max = SolidifyConstants.DB_MAX_STRING_LENGTH)
  private String text;

  @Override
  public void init() {
    // no default value to initialize
  }

  public String getItemId() {
    return this.itemId;
  }

  public void setItemId(String archiveId) {
    this.itemId = archiveId;
  }

  public Mode getMode() {
    return this.mode;
  }

  public void setMode(Mode mode) {
    this.mode = mode;
  }

  public String getLanguage() {
    return this.language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getStyle() {
    return this.style;
  }

  public void setStyle(String style) {
    this.style = style;
  }

  public OutputFormat getOutputFormat() {
    return this.outputFormat;
  }

  public void setOutputFormat(OutputFormat outputFormat) {
    this.outputFormat = outputFormat;
  }

  public String getText() {
    return this.text;
  }

  public void setText(String text) {
    this.text = text;
  }

  @Override
  public String managedBy() {
    return CitationConstants.MODULE_NAME;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + ": resId=" + this.getResId() + ", mode=" + this.mode + ", lang=" + this.language + ", style="
            + this.style + ", outputFormat=" + this.outputFormat;
  }
}
