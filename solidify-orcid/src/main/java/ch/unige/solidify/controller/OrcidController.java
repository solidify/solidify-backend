/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID - OrcidController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import java.util.List;

import org.json.JSONObject;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.OrcidWebsiteDTO;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.service.OrcidOAuthService;
import ch.unige.solidify.service.OrcidWebsitesService;
import ch.unige.solidify.util.StringTool;

@RestController
@Profile("orcid")
@RequestMapping(SolidifyConstants.URL_ORCID)
public class OrcidController {
  private static final String LOCATION_HEADER = "Location";

  private final OrcidOAuthService orcidOAuthService;
  private final OrcidWebsitesService externalWebsiteService;

  public OrcidController(OrcidOAuthService orcidOAuthService, OrcidWebsitesService externalWebsiteService) {
    this.orcidOAuthService = orcidOAuthService;
    this.externalWebsiteService = externalWebsiteService;
  }

  @EveryonePermissions
  @GetMapping("/" + ActionName.LANDING)
  public ResponseEntity<Void> landingUrl(@RequestParam(required = false) String code,
          @RequestParam(required = false) String error,
          @RequestParam(required = false, name = "error_description") String errorDescription,
          @RequestParam String authTransactionId) throws JsonProcessingException {
    String redirectUrl;
    if (!StringTool.isNullOrEmpty(code)) {
      redirectUrl = this.orcidOAuthService.getOrcidAndTokensFromOrcidWebsite(code, authTransactionId);
    } else {
      redirectUrl = this.orcidOAuthService.getAndRemoveOriginUrl(authTransactionId, error, errorDescription);
    }

    return ResponseEntity.status(HttpStatus.FOUND).header(LOCATION_HEADER, redirectUrl).build();
  }

  @UserPermissions
  @GetMapping("/" + ActionName.START_ORCID_AUTH)
  public ResponseEntity<String> startOrcidAuth(@RequestParam String originUrl) {
    final JSONObject jsonObject = this.orcidOAuthService.startOrcidAuth(originUrl);
    return ResponseEntity.ok().body(jsonObject.toString());
  }

  @EveryonePermissions
  @GetMapping("/{orcid}/" + ActionName.ORCID_EXTERNAL_WEBSITES)
  public HttpEntity<List<OrcidWebsiteDTO>> getExternalWebsitesByOrcid(@PathVariable String orcid) {
    List<OrcidWebsiteDTO> orcidWebsites = this.externalWebsiteService.getAvailableWebsitesForOrcid(orcid);
    return new ResponseEntity<>(orcidWebsites, HttpStatus.OK);
  }
}
