/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - PropertiesLogger.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.PropertySource;

public class PropertiesLogger implements ApplicationListener<ApplicationStartedEvent> {
  private static final Logger log = LoggerFactory.getLogger(PropertiesLogger.class);

  private ConfigurableEnvironment environment;
  private boolean isPrinted;

  @Override
  public void onApplicationEvent(ApplicationStartedEvent event) {
    if (!this.isPrinted) {
      this.environment = event.getApplicationContext().getEnvironment();
      this.printProperties();
      this.isPrinted = true;
    }
  }

  public void printProperties() {
    for (final EnumerablePropertySource<?> propertySource : this.findPropertiesPropertySources()) {
      log.info("********** {} **********", propertySource.getName());
      final String[] propertyNames = propertySource.getPropertyNames();
      Arrays.sort(propertyNames);
      for (final String propertyName : propertyNames) {
        if (propertyName.toLowerCase().endsWith("password") ||
                propertyName.toLowerCase().endsWith("secret") ||
                propertyName.toLowerCase().endsWith("secret-key") ||
                propertyName.toLowerCase().endsWith("access-key") ||
                propertyName.toLowerCase().endsWith("apikey") ||
                propertyName.toLowerCase().endsWith("users.root") ||
                propertyName.toLowerCase().endsWith("cloud.config.uri")) {
          log.info("{}={}", propertyName, "********");
          continue;
        }
        final String resolvedProperty = this.environment.getProperty(propertyName);
        final Object sourceProperty = propertySource.getProperty(propertyName);
        if (resolvedProperty != null && sourceProperty != null) {
          if (resolvedProperty.equals(sourceProperty.toString())) {
            log.info("{}={}", propertyName, resolvedProperty);
          } else {
            log.info("{}={} OVERRIDEN to {}", propertyName, sourceProperty, resolvedProperty);
          }
        }
      }
    }
  }

  private List<EnumerablePropertySource<?>> findPropertiesPropertySources() {
    final List<EnumerablePropertySource<?>> propertiesPropertySources = new LinkedList<>();
    for (final PropertySource<?> propertySource : this.environment.getPropertySources()) {
      if (propertySource instanceof EnumerablePropertySource) {
        propertiesPropertySources.add((EnumerablePropertySource<?>) propertySource);
      }
    }
    return propertiesPropertySources;
  }
}
