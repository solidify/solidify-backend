/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - EntityMockSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.specification.dao;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class EntityMockSpecification implements Specification<EntityMock> {
  @Serial
  private static final long serialVersionUID = 8535594096558989092L;
  private final EntityMock criteria;

  public EntityMockSpecification(EntityMock criteria) {
    this.criteria = criteria;
  }

  @Override
  public Predicate toPredicate(Root<EntityMock> root, CriteriaQuery<?> criteriaQuery,
          CriteriaBuilder criteriaBuilder) {
    final List<Predicate> listPredicate = new ArrayList<>();
    if (this.criteria.getLabel() != null) {
      listPredicate.add(criteriaBuilder
              .like(root.get("label"), "%" + this.criteria.getLabel() + "%"));
    }
    return criteriaBuilder.and(listPredicate.toArray(new Predicate[listPredicate.size()]));
  }
}
