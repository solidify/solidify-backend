/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SolidifyDownloadTokenController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import java.security.SecureRandom;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.model.security.SolidifyDownloadTokenType;
import ch.unige.solidify.repository.DownloadTokenRepository;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.util.HashTool;

@UserPermissions
public abstract class SolidifyDownloadTokenController<T extends SolidifyDownloadTokenType> extends SolidifyController {

  // Token length should be a multiple of three to avoid padding with '=' character that could be
  // problematic in cookie value
  private static final int DOWNLOAD_TOKEN_LENGTH = 201;
  private static final int DOWNLOAD_TOKEN_USER_ID = 20;
  private static final String DOWNLOAD_TOKEN_ANONYMOUS = "anonymous";

  private final DownloadTokenRepository downloadTokenRepository;
  private final SecureRandom secureRandom;
  private final int downloadTokenLifeTimeMinutes;
  private final boolean secureCookie;

  protected SolidifyDownloadTokenController(
          SolidifyProperties config,
          DownloadTokenRepository downloadTokenRepository) {
    this.downloadTokenRepository = downloadTokenRepository;
    this.downloadTokenLifeTimeMinutes = config.getSecurity().getDownloadTokenLifeTimeMinutes();
    this.secureCookie = config.getSecurity().getDownloadToken().getCookie().getSecure();
    this.secureRandom = new SecureRandom();
  }

  protected ResponseEntity<DownloadToken> getToken(@PathVariable String resourceId, T resourceType, String cookiePath) {
    final String userId = this.getUserId();
    if (resourceType == null) {
      throw new SolidifyCheckingException("Download Token Type not defined");
    }
    final String resourceTypeLabel = resourceType.getLabel();

    // Create new download token
    final String token = this.getRandomBase64String(DOWNLOAD_TOKEN_LENGTH);
    final String tokenHash = HashTool.hash(token);
    DownloadToken downloadToken = new DownloadToken(userId, resourceTypeLabel, resourceId, tokenHash, token);

    synchronized (this) {
      // If there is already a download token for this resource and this user replace it
      DownloadToken existingToken = this.downloadTokenRepository.findByResourceIdAndUserId(resourceId, userId);
      if (existingToken != null) {
        this.downloadTokenRepository.delete(existingToken);
      }
      this.downloadTokenRepository.save(downloadToken);
    }
    final HttpHeaders headers = new HttpHeaders();
    final OffsetDateTime expiresDate = ZonedDateTime.now().toInstant().atOffset(ZoneOffset.UTC).plusMinutes(this.downloadTokenLifeTimeMinutes);
    final String expiresString = DateTimeFormatter.RFC_1123_DATE_TIME.format(expiresDate);
    final String secureFlag = this.secureCookie ? "; Secure" : "";
    headers.add(HttpHeaders.SET_COOKIE, "downloadToken-" + resourceType + "-" + resourceId + "=" + token + "; Expires=" +
            expiresString + "; Path=" + cookiePath + secureFlag + "; HttpOnly");
    return new ResponseEntity<>(headers, HttpStatus.OK);
  }

  private String getUserId() {
    String userId = this.getAuthenticatedUserExternalUid();
    if (userId == null) {
      userId = DOWNLOAD_TOKEN_ANONYMOUS + this.getRandomString(DOWNLOAD_TOKEN_USER_ID);
    }
    return userId;
  }

  private String getRandomString(int length) {
    final byte[] bytes = new byte[length];
    this.secureRandom.nextBytes(bytes);
    return new String(bytes);
  }

  private String getRandomBase64String(int length) {
    final byte[] bytes = new byte[length];
    this.secureRandom.nextBytes(bytes);
    return Base64.getEncoder().encodeToString(bytes);
  }

}
