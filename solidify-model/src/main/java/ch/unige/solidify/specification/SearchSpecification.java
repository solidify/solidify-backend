/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SearchSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import static ch.unige.solidify.util.SearchOperation.BETWEEN;

import java.io.Serial;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.SearchCriteria;

/**
 * Specification generated with a SearchCriteria.
 *
 * @param <T>
 * @see ch.unige.solidify.util.SearchCriteria
 */
public abstract class SearchSpecification<T> implements Specification<T> {
  @Serial
  private static final long serialVersionUID = 7428274476634034865L;
  private final SearchCriteria criteria;

  protected SearchSpecification(SearchCriteria criteria) {
    this.criteria = criteria;
  }

  public Predicate toPredicate(Path fieldPath, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final Class fieldType = fieldPath.getJavaType();
    Object criteriaValue = null;
    if (!this.criteria.getOperationType().equals(BETWEEN)) {
      criteriaValue = this.convertString2Object(fieldType, this.criteria.getValue());
    }
    switch (this.criteria.getOperationType()) {
      case EQUALITY:
        if (this.criteria.isCaseInsensitive()) {
          if (this.isString(fieldType)) {
            return builder.equal(
                    builder.lower(fieldPath),
                    this.criteria.getValue().toString().toLowerCase());
          } else if (fieldType.isEnum()) {
            return builder.equal(builder.lower(fieldPath), Enum.valueOf(fieldType, this.criteria.getValue().toString()));
          }
        }
        return builder.equal(fieldPath, criteriaValue);

      case NEGATION:
        if (this.criteria.isCaseInsensitive()) {
          if (this.isString(fieldType)) {
            return builder.notEqual(
                    builder.lower(fieldPath),
                    this.criteria.getValue().toString().toLowerCase());
          } else if (fieldType.isEnum()) {
            return builder.notEqual(builder.lower(fieldPath), Enum.valueOf(fieldType, this.criteria.getValue().toString()));
          }
        }
        return builder.notEqual(fieldPath, criteriaValue);

      case GREATER_THAN:
        if (this.criteria.isCaseInsensitive() && this.isString(fieldType)) {
          return builder.greaterThan(
                  builder.lower(fieldPath),
                  this.criteria.getValue().toString().toLowerCase());
        }
        return builder.greaterThan(fieldPath, (Comparable) criteriaValue);

      case LESS_THAN:
        if (this.criteria.isCaseInsensitive() && this.isString(fieldType)) {
          return builder.lessThan(
                  builder.lower(fieldPath),
                  this.criteria.getValue().toString().toLowerCase());
        }
        return builder.lessThan(fieldPath, (Comparable) criteriaValue);

      case STARTS_WITH:
        if (!this.isString(fieldType)) {
          return null;
        }
        if (this.criteria.isCaseInsensitive() && this.isString(fieldType)) {
          return builder.like(
                  builder.lower(fieldPath),
                  this.criteria.getValue().toString().toLowerCase() + "%");
        }
        return builder.like(fieldPath, this.criteria.getValue() + "%");

      case ENDS_WITH:
        if (this.criteria.isCaseInsensitive() && this.isString(fieldType)) {
          return builder.like(
                  builder.lower(fieldPath),
                  "%" + this.criteria.getValue().toString().toLowerCase());
        }
        return builder.like(fieldPath, "%" + this.criteria.getValue());

      case CONTAINS:
        if (this.criteria.isCaseInsensitive() && this.isString(fieldType)) {
          return builder.like(
                  builder.lower(fieldPath),
                  "%" + this.criteria.getValue().toString().toLowerCase() + "%");
        }
        return builder.like(fieldPath, "%" + this.criteria.getValue() + "%");

      case ORACLE_CONTAINS:
        final Expression<Integer> exp = builder.function("CONTAINS", Integer.class, (Expression<String>) fieldPath,
                builder.literal(this.criteria.getValue().toString()),
                builder.literal(1));
        return (builder.gt(exp, 0));

      case GREATER_EQUAL:
        if (this.criteria.isCaseInsensitive() && this.isString(fieldType)) {
          return builder.greaterThanOrEqualTo(
                  builder.lower(fieldPath),
                  this.criteria.getValue().toString().toLowerCase());
        }
        return builder.greaterThanOrEqualTo(fieldPath, (Comparable) criteriaValue);

      case LESS_EQUAL:
        if (this.criteria.isCaseInsensitive() && this.isString(fieldType)) {
          return builder.lessThanOrEqualTo(
                  builder.lower(fieldPath),
                  this.criteria.getValue().toString().toLowerCase());
        }
        return builder.lessThanOrEqualTo((Expression<String>) fieldPath, (Expression) criteriaValue);

      case BETWEEN:
        if (this.isString(fieldType)) {
          throw new SolidifyRuntimeException("BETWEEN operation can be applied only for number or date");
        }
        final Object[] values = this.convertBetweenValueString2Object(fieldType, this.criteria.getValue());
        return builder.between(fieldPath, (Comparable) values[0], (Comparable) values[1]);
      default:
        return null;
    }
  }

  @Override

  /**
   * Creates a WHERE clause for a query of the referenced entity in form of a Predicate for the given
   * Root and instance variable SearchCriteria "criteria". criteria.key is one of entity field name
   * criteria.value is used to create instance of different Object according to the entity field type
   * criteria.operationType is used to choose predicate
   *
   * @see ch.unige.solidify.util.SearchCriteria
   */
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final Path fieldPath = this.findFieldPath(root, this.criteria.getKey());
    return this.toPredicate(fieldPath, query, builder);
  }

  protected SearchCriteria getCriteria() {
    return this.criteria;
  }

  // for between operator, the value will be 20_30 or 2016-08-16_2018-09-25.
  // it treats only non-string field
  private Object[] convertBetweenValueString2Object(Class fieldType, Object inputValue) {
    final String[] values = StringUtils.split(String.valueOf(inputValue), "_");
    if (values == null || values.length != 2) {
      return new Object[0];
    }
    final Object first = this.convertString2Object(fieldType, values[0]);
    final Object second = this.convertString2Object(fieldType, values[1]);
    return new Object[] { first, second };
  }

  private Object convertString2Object(Class fieldType, Object inputValue) {
    // first test number
    if (Number.class.isAssignableFrom(fieldType)) {
      return new BigDecimal(String.valueOf(inputValue));
    }

    // Test boolean
    if (Boolean.class.isAssignableFrom(fieldType) || boolean.class.isAssignableFrom(fieldType)) {
      return Boolean.parseBoolean(String.valueOf(inputValue));
    }

    // Obtains an instance of LocalDate from a text string such as "2016-08-16"
    // The string must represent a valid date-time and is parsed using DateTimeFormatter.ISO_LOCAL_DATE.
    if (LocalDate.class.isAssignableFrom(fieldType)) {
      return LocalDate.parse(String.valueOf(inputValue));
    }
    // Obtains an instance of LocalDateTime from a text string such as 2007-12-03T10:15:30.
    // The string must represent a valid date-time and is parsed using
    // DateTimeFormatter.ISO_LOCAL_DATE_TIME.
    if (LocalDateTime.class.isAssignableFrom(fieldType)) {
      return LocalDateTime.parse(String.valueOf(inputValue));
    }
    // Obtains an instance of OffsetDateTime from a text string such as 2007-12-03T10:15:30+01:00
    if (OffsetDateTime.class.isAssignableFrom(fieldType)) {
      return OffsetDateTime.parse(String.valueOf(inputValue));
    }
    return inputValue;
  }

  private Path<?> findFieldPath(Root<?> root, String fieldName) {
    Path<?> expression;
    if (fieldName.contains(".")) {
      final String[] names = StringUtils.split(fieldName, ".");
      if (names == null || names.length == 0) {
        return null;
      }
      expression = root.get(names[0]);
      for (int i = 1; i < names.length; i++) {
        expression = expression.get(names[i]);
      }
    } else {
      expression = root.get(fieldName);
    }
    return expression;
  }

  private boolean isString(Class fieldType) {
    return String.class.isAssignableFrom(fieldType);
  }

}
