/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - MessageService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
  private static final Logger log = LoggerFactory.getLogger(MessageService.class);

  private final MessageSourceAccessor accessor;

  public MessageService(MessageSource messageSource) {
    this.accessor = new MessageSourceAccessor(messageSource, Locale.ENGLISH);
  }

  public String get(String code) {
    return this.get(code, LocaleContextHolder.getLocale());
  }

  public String get(String code, Locale locale) {
    try {
      return this.accessor.getMessage(code, locale);
    } catch (final NoSuchMessageException e) {
      return this.getErrorMessage(code, e);
    }
  }

  public String get(String code, Object[] args) {
    return this.get(code, args, LocaleContextHolder.getLocale());
  }
  
  public String get(String code, Object[] args, Locale locale) {
    try {
      return this.accessor.getMessage(code, args, locale);
    } catch (final NoSuchMessageException e) {
      return this.getErrorMessage(code, e);
    }
  }

  private String getErrorMessage(String code, NoSuchMessageException e) {
    log.error("Cannot find message", e);
    try {
      return this.accessor.getMessage("message.not.found", new Object[] { code }, LocaleContextHolder.getLocale());
    } catch (final NoSuchMessageException noSuchMessageException) {
      return "Message not found : " + code;
    }
  }
}
