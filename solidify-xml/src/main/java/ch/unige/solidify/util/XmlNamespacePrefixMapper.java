/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify XML - XmlNamespacePrefixMapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import org.glassfish.jaxb.runtime.marshaller.NamespacePrefixMapper;

public class XmlNamespacePrefixMapper extends NamespacePrefixMapper {

  private final Map<String, String> namespaces = new HashMap<>();

  public XmlNamespacePrefixMapper() {
    this.namespaces.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");
    this.namespaces.put("http://www.w3.org/1999/xhtml", "xhtml");
    this.namespaces.put("http://www.w3.org/1999/xlink", "xlink");
  }

  @Override
  public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
    return this.namespaces.getOrDefault(namespaceUri, suggestion);
  }

  protected void addSchema(String nameSpace, String nameSpacePrefix) {
    this.namespaces.put(nameSpace, nameSpacePrefix);
  }

  public Map<String, String> getNamespaces() {
    return this.namespaces;
  }

  public Map<String, String> getNamespacePrefixes() {
    return this.namespaces.entrySet().stream()
            .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
  }

}
