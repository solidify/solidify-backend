/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Remote Search - TrustedIndexFieldAliasRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service.rest.trusted;

import org.springframework.stereotype.Service;

import ch.unige.solidify.IndexApplicationProperties;
import ch.unige.solidify.service.TrustedRestClientService;
import ch.unige.solidify.service.rest.abstractservice.IndexFieldAliasRemoteResourceService;

@Service
public class TrustedIndexFieldAliasRemoteResourceService extends IndexFieldAliasRemoteResourceService {

  public TrustedIndexFieldAliasRemoteResourceService(IndexApplicationProperties config, TrustedRestClientService restClientService) {
    super(config.getIndexUrl(), restClientService);
  }
}
