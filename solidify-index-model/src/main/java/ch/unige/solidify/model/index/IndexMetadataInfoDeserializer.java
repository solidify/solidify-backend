/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - IndexMetadataInfoDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.index;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.solidify.SolidifyConstants;

public abstract class IndexMetadataInfoDeserializer<T extends IndexMetadata> extends JsonDeserializer<T> {

  protected abstract T getNewObject();

  @Override
  public T deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    final JsonNode node = jp.getCodec().readTree(jp);
    final T indexMetadata = this.getNewObject();
    if (node.has("index") && node.has(SolidifyConstants.DB_RES_ID)) {
      // Support remote mode with 'index' & 'resId' fields
      indexMetadata.setResId(node.get(SolidifyConstants.DB_RES_ID).asText());
      indexMetadata.setIndex(node.get("index").asText());
      if (node.has("metadata")) {
        indexMetadata.setMetadata(node.findValue("metadata").toString());
      }
    } else {
      // Support direct mode with only 'metadata' field
      indexMetadata.setMetadata(node.toString());
    }
    return indexMetadata;
  }
}
