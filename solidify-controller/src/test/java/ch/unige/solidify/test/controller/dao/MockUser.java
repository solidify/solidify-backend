/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - MockUser.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.controller.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import org.springframework.data.jpa.domain.Specification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.unige.solidify.rest.ResourceCET;
import ch.unige.solidify.rest.Searchable;
import ch.unige.solidify.util.SearchCriteria;

@Entity
@Table(name = "MockUser")
public class MockUser extends ResourceCET implements Searchable<MockUser> {

  private int age;
  private LocalDate birthday;
  private Long carKilos;

  private Integer childNumber;
  @Id
  @Column(name = "id")
  private String entityId;
  private LocalDateTime marriageTime;
  private OffsetDateTime meetingTime;
  private String name;
  private BigDecimal salary;

  @JsonProperty
  @JsonInclude(JsonInclude.Include.NON_NULL)
  @Transient
  private List<SearchCriteria> searchCriterias;

  public MockUser() {
    super();
  }

  public MockUser(String name, int age, Integer childNumber, Long carKilos, BigDecimal salary,
          LocalDate birthday, LocalDateTime marriageTime, OffsetDateTime meetingTime) {
    this.name = name;
    this.age = age;
    this.childNumber = childNumber;
    this.carKilos = carKilos;
    this.salary = salary;
    this.birthday = birthday;
    this.marriageTime = marriageTime;
    this.meetingTime = meetingTime;
  }

  public MockUser(String id, String name, int age) {
    super();
    this.entityId = id;
    this.name = name;
    this.age = age;
  }

  public MockUser(String id, String name, int age, Integer childNumber, Long carKilos,
          BigDecimal salary, LocalDate birthday, LocalDateTime marriageTime,
          OffsetDateTime meetingTime) {
    this.entityId = id;
    this.name = name;
    this.age = age;
    this.childNumber = childNumber;
    this.carKilos = carKilos;
    this.salary = salary;
    this.birthday = birthday;
    this.marriageTime = marriageTime;
    this.meetingTime = meetingTime;
  }

  public int getAge() {
    return this.age;
  }

  public LocalDate getBirthday() {
    return this.birthday;
  }

  public Long getCarKilos() {
    return this.carKilos;
  }

  public Integer getChildNumber() {
    return this.childNumber;
  }

  @Override
  public String getEntityId() {
    return this.entityId;
  }

  public LocalDateTime getMarriageTime() {
    return this.marriageTime;
  }

  public OffsetDateTime getMeetingTime() {
    return this.meetingTime;
  }

  public String getName() {
    return this.name;
  }

  public BigDecimal getSalary() {
    return this.salary;
  }

  @Override
  @java.beans.Transient
  @JsonIgnore
  public Specification<MockUser> getSearchSpecification(SearchCriteria criteria) {
    return new MockUserSearchSpecification(criteria);
  }

  @Override
  @JsonIgnore
  public List<SearchCriteria> getSearchCriterias() {
    return this.searchCriterias;
  }

  @Override
  @JsonProperty
  public void setSearchCriterias(List<SearchCriteria> searchCriterias) {
    this.searchCriterias = searchCriterias;
  }

  @Override
  public void init() {
  }

  @Override
  public String managedBy() {
    return null;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public void setBirthday(LocalDate birthday) {
    this.birthday = birthday;
  }

  public void setCarKilos(Long carKilos) {
    this.carKilos = carKilos;
  }

  public void setChildNumber(Integer childNumber) {
    this.childNumber = childNumber;
  }

  public void setEntityId(String entityId) {
    this.entityId = entityId;
  }

  public void setMarriageTime(LocalDateTime marriageTime) {
    this.marriageTime = marriageTime;
  }

  public void setMeetingTime(OffsetDateTime meetingTime) {
    this.meetingTime = meetingTime;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSalary(BigDecimal salary) {
    this.salary = salary;
  }

}
