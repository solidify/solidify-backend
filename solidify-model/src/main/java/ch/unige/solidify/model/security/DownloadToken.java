/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - DownloadToken.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.security;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;

@Schema(description = "The download token.")
@Entity
public class DownloadToken extends ResourceNormalized {

  @Schema(description = "The user identifier of the download token.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String userId;

  @Schema(description = "The resource identifier of the download token.")
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String resourceId;

  @Schema(description = "The resource type of the download token.")
  @NotNull
  private String resourceType;

  @Schema(description = "The fingerprint of the download token.")
  @JsonIgnore
  private String tokenHash;

  @Schema(description = "The token of the download token.")
  @Transient
  @JsonInclude
  private String token;

  public DownloadToken() {
    // no-op
  }

  public DownloadToken(String userId, String resourceType, String resourceId, String tokenHash, String token) {
    this.userId = userId;
    this.resourceType = resourceType;
    this.resourceId = resourceId;
    this.tokenHash = tokenHash;
    this.token = token;
  }

  public String getUserId() {
    return this.userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getResourceType() {
    return this.resourceType;
  }

  public void setResourceType(String resourceType) {
    this.resourceType = resourceType;
  }

  public String getResourceId() {
    return this.resourceId;
  }

  public void setResourceId(String resourceId) {
    this.resourceId = resourceId;
  }

  public String getTokenHash() {
    return this.tokenHash;
  }

  public void setTokenHash(String tokenHash) {
    this.tokenHash = tokenHash;
  }

  public String getToken() {
    return this.token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return SolidifyConstants.ADMIN_MODULE;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.resourceId, this.resourceType, this.token, this.tokenHash, this.userId);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    DownloadToken other = (DownloadToken) obj;
    return Objects.equals(this.resourceId, other.resourceId) && Objects.equals(this.resourceType, other.resourceType)
            && Objects.equals(this.token, other.token)
            && Objects.equals(this.tokenHash, other.tokenHash) && Objects.equals(this.userId, other.userId);
  }

}
