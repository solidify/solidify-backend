/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - ZipTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.ZipTool;

class ZipTest {

  private static final String DEST_PATH1 = "dlcm.xml";

  private static final String DEST_PATH2 = "include/datacite-funderIdentifierType-v4.xsd";

  private static final String FILE_NAME_3 = "geo-location.json";
  private static final String INCLUDE = "include";
  private static final String EMPTY_FOLDER = "empty-folder";
  private static final String PATH_FILE1 = "dlcm/dlcm.xml";
  private static final String PATH_FILE2 = "dlcm/include/datacite-funderIdentifierType-v4.xsd";
  private static final String PATH_FILE3 = "json/geo-location.json";
  private static final String PATH_FILE_WITH_INCLUDED_FILE_NAME = "zip/test.txt";
  private static final String PATH_FILE_WITH_INCLUDING_FILE_NAME = "zip/test.txt.orig";
  private static final String UNZIP_DIR = "unzip";
  private static final String ZIP_ARCHIVE = "test.zip";
  private static final String ZIP_DIR = "dlcm";
  private static final int ZIP_FILE_NUMBER = 17;
  private static final int ZIP_INCLUDE_NUMBER = 8;

  @Test
  void createZipByListFilesTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(".").getURI());
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    final List<Path> filesToAdd = new ArrayList<>();
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE1).getURI()));
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE2).getURI()));
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE3).getURI()));
    assertTrue(zipTool.addFilesToZip(zipDir, filesToAdd, null, false, false));
    assertTrue(zipTool.checkZip());
    assertEquals(3, zipTool.getEntryNumber());
    assertTrue(zipTool.contains(PATH_FILE3));
    assertFalse(zipTool.contains(FILE_NAME_3));
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
  }

  @Test
  void createZipByListFilesWithEmptyFolderTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(".").getURI());
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    final List<Path> filesToAdd = new ArrayList<>();
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE1).getURI()).getParent().resolve(EMPTY_FOLDER));
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE1).getURI()));
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE2).getURI()));
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE3).getURI()));
    assertTrue(zipTool.addFilesToZip(zipDir, filesToAdd, null, false, true));
    assertTrue(zipTool.checkZip());
    assertEquals(4, zipTool.getEntryNumber());
    assertEquals(3, zipTool.getFileNumber());
    assertEquals(1, zipTool.getFolderNumber());
    assertTrue(zipTool.contains(PATH_FILE3));
    assertFalse(zipTool.contains(FILE_NAME_3));
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
  }

  @Test
  void createZipSpecifyingListFilesByDifferentPathsTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(".").getURI());
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    final List<Path> filesToAdd = new ArrayList<>();
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE1).getURI()));
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE2).getURI()));
    final List<Path> destPaths = new ArrayList<>();
    destPaths.add(Paths.get(DEST_PATH1));
    destPaths.add(Paths.get(DEST_PATH2));
    assertTrue(zipTool.addFilesToZip(zipDir, filesToAdd, destPaths, true, false));
    assertTrue(zipTool.checkZip());
    assertEquals(2, zipTool.getEntryNumber());
    assertTrue(zipTool.contains(DEST_PATH1));
    assertFalse(zipTool.contains(PATH_FILE1));
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
  }

  @Test
  void createZipSpecifyingListFilesTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(".").getURI());
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    final List<Path> filesToAdd = new ArrayList<>();
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE1).getURI()));
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE2).getURI()));
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE3).getURI()));
    assertTrue(zipTool.addFilesToZip(zipDir, filesToAdd, null, true, false));
    assertTrue(zipTool.checkZip());
    assertEquals(3, zipTool.getEntryNumber());
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
  }

  @Test
  void zipFilesTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(ZIP_DIR).getURI());
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    assertTrue(zipTool.zipFiles(zipDir.toUri()));
    assertTrue(zipTool.checkZip());
    assertEquals(ZIP_FILE_NUMBER, zipTool.getEntryNumber());
    assertEquals(ZIP_FILE_NUMBER, zipTool.getFileNumber());
    assertEquals(0, zipTool.getFolderNumber());
  }

  @Test
  void zipFilesWithEmptyFolderTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(ZIP_DIR).getURI());
    FileTool.ensureFolderExists(zipDir.resolve(EMPTY_FOLDER));
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    assertTrue(zipTool.zipFilesWithEmptyFolders(zipDir.toUri()));
    assertTrue(zipTool.checkZip());
    assertEquals(ZIP_FILE_NUMBER + 1, zipTool.getEntryNumber());
    assertEquals(ZIP_FILE_NUMBER, zipTool.getFileNumber());
    assertEquals(1, zipTool.getFolderNumber());
  }

  @ParameterizedTest
  @ValueSource(strings = { "zip/zip_test.zip", "zip/windows_zip_test.zip" })
  void partialUnzipArchiveTest(String zipFile) throws IOException {
    final Path zipFilePath = Paths.get(new ClassPathResource(zipFile).getURI());
    final Path unzipDir = Paths.get(System.getProperty("java.io.tmpdir") + File.separatorChar + UNZIP_DIR);
    final ZipTool zipTool = new ZipTool(zipFilePath.toString());
    final List<String> extractList = Arrays.asList("Folder with file/**", "root file.txt");
    assertTrue(zipTool.unzipFiles(unzipDir, extractList, true));
    final List<Path> fileList = new ArrayList<>();
    fileList.addAll(FileTool.readFolder(unzipDir));
    fileList.addAll(FileTool.readFolder(unzipDir.resolve("Folder with file")));
    assertEquals(2, fileList.size());
    assertTrue(FileTool.deleteFolder(unzipDir));
  }

  /**
   * Test ant expressions on the following zip file:
   *  ab.txt
   *  abc.txt
   *  dir1/
   *  dir1/ab.txt
   *  dir1/abc.txt
   *  dir1/dir2/
   *  dir1/dir2/ab.txt
   *  dir1/dir2/abc.txt
   */
  @ParameterizedTest
  @CsvSource({
          "*, 2",
          "**, 6",
          "ab*, 2",
          "abc.txt, 1",
          "abc*, 1",
          "a?c*, 1",
          "abc.t?t, 1",
          "dir1/ab.txt, 1",
          "dir1/ab*.txt, 2",
          "dir1/**, 4",
          "di*/*, 2",
          "di*/**, 4",
          "dir1, 0",
          "dir1/, 0",
          "dir1\\, 0",
          "dir1/*, 2",
          "di*/*, 2",
          "di*, 0",
          "dir1/dir2, 0",
          "dir1/dir2/ab.txt, 1",
          "dir1/dir2/ab*, 2"
  })
  void antExpressionUnzipArchiveTest(String antExpression, int expectedNumber) throws IOException {
    final Path zipFilePath = Paths.get(new ClassPathResource("zip/zip_ant_test.zip").getURI());
    final Path unzipDir = Paths
            .get(System.getProperty("java.io.tmpdir") + File.separatorChar + UNZIP_DIR);
    final ZipTool zipTool = new ZipTool(zipFilePath.toString());
    final List<String> extractList = List.of(antExpression);
    assertTrue(zipTool.unzipFiles(unzipDir, extractList, true));
    final List<Path> fileList = new ArrayList<>(FileTool.readFolder(unzipDir));
    final Path dir1 = unzipDir.resolve("dir1");
    final Path dir2 = unzipDir.resolve("dir1/dir2");
    if (Files.exists(dir1)) {
      fileList.addAll(FileTool.readFolder(dir1));
    }
    if (Files.exists(dir2)) {
      fileList.addAll(FileTool.readFolder(dir2));
    }
    assertEquals(expectedNumber, fileList.size());
    assertTrue(FileTool.deleteFolder(unzipDir));
  }

  @Test
  void unzipArchiveTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(ZIP_DIR).getURI());
    final Path unzipDir = Paths.get(System.getProperty("java.io.tmpdir") + File.separatorChar + UNZIP_DIR);
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    assertTrue(zipTool.zipFiles(zipDir.toUri()));
    assertTrue(zipTool.unzipFiles(unzipDir, true));
    assertFalse(zipTool.unzipFiles(unzipDir, true));
    assertTrue(zipTool.unzipFiles(unzipDir, false));
    final List<Path> fileList = new ArrayList<>();
    fileList.addAll(FileTool.readFolder(unzipDir));
    fileList.addAll(FileTool.readFolder(unzipDir.resolve(INCLUDE)));
    assertEquals(ZIP_FILE_NUMBER, fileList.size());
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
    assertTrue(FileTool.deleteFolder(unzipDir));
  }

  @Test
  void unzipArchiveTest1() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(ZIP_DIR).getURI());
    final Path unzipDir = Paths
            .get(System.getProperty("java.io.tmpdir") + File.separatorChar + UNZIP_DIR);
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    assertTrue(zipTool.zipFiles(zipDir));
    assertTrue(zipTool.unzipFiles(unzipDir, true));
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
    assertTrue(FileTool.deleteFolder(unzipDir));
  }

  @Test
  void unzipArchiveTest2() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(ZIP_DIR).getURI());
    final Path unzipDir = Paths
            .get(System.getProperty("java.io.tmpdir") + File.separatorChar + UNZIP_DIR);
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    assertTrue(zipTool.zipFiles(zipDir));
    assertTrue(zipTool.unzipFiles(unzipDir, List.of("**"), true));
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
    assertTrue(FileTool.deleteFolder(unzipDir));
  }

  @Test
  void unzipFolderTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(ZIP_DIR).getURI());
    final Path unzipDir = Paths
            .get(System.getProperty("java.io.tmpdir") + File.separatorChar + UNZIP_DIR);
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    assertTrue(zipTool.zipFiles(zipDir.toUri()));
    assertTrue(zipTool.unzipFolder(unzipDir, INCLUDE + "/", true));
    final List<Path> fileList = FileTool.readFolder(unzipDir);
    assertEquals(ZIP_INCLUDE_NUMBER, fileList.size(), "Wrong file number");
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
    assertTrue(FileTool.deleteFolder(unzipDir), "Cannot delete folder");
  }

  @Test
  void unzipOSDependantFolderTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(ZIP_DIR).getURI());
    final Path unzipDir = Paths
            .get(System.getProperty("java.io.tmpdir") + File.separatorChar + UNZIP_DIR);
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    assertTrue(zipTool.zipFiles(zipDir.toUri()));
    assertTrue(zipTool.unzipFolder(unzipDir, INCLUDE + File.separator, true));
    final List<Path> fileList = FileTool.readFolder(unzipDir);
    assertEquals(ZIP_INCLUDE_NUMBER, fileList.size(), "Wrong file number");
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
    assertTrue(FileTool.deleteFolder(unzipDir), "Wrong file number");
  }

  @Test
  void extractExactFilePath() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(".").getURI());
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    final Path unzipDir = Paths.get(System.getProperty("java.io.tmpdir") + File.separatorChar + UNZIP_DIR);
    final List<Path> filesToAdd = new ArrayList<>();
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE_WITH_INCLUDED_FILE_NAME).getURI()));
    filesToAdd.add(Paths.get(new ClassPathResource(PATH_FILE_WITH_INCLUDING_FILE_NAME).getURI()));
    assertTrue(zipTool.addFilesToZip(zipDir, filesToAdd, null, false, false));
    assertTrue(zipTool.checkZip());
    assertEquals(2, zipTool.getEntryNumber());
    zipTool.unzipFiles(unzipDir, List.of(PATH_FILE_WITH_INCLUDED_FILE_NAME), true);
    List<Path> extractedFiles = FileTool.readFolder(unzipDir.resolve("zip"));
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
    assertTrue(FileTool.deleteFolder(unzipDir));
    assertEquals(1, extractedFiles.size());
  }

  @ParameterizedTest
  @ValueSource(strings = { "zip-slip.zip", "zip-slip-win.zip" })
  void unzipZipSlipTest(String zipFile) throws IOException {
    this.runUnzipTest(zipFile, true);
  }

  @ParameterizedTest
  @ValueSource(strings = { "r.zip", "datafiles-encoded.zip", "windows_zip_test.zip" })
  void unzipTest(String zipFile) throws IOException {
    this.runUnzipTest(zipFile, false);
  }

  @Test
  void zipArchiveTest() throws IOException {
    final Path zipDir = Paths.get(new ClassPathResource(ZIP_DIR).getURI());
    final ZipTool zipTool = new ZipTool(ZIP_ARCHIVE);
    assertNotNull(zipDir);
    assertTrue(zipTool.zipFiles(zipDir));
    assertTrue(zipTool.checkZip());
    assertEquals(ZIP_FILE_NUMBER, zipTool.getEntryNumber());
    FileTool.deleteFile(Paths.get(ZIP_ARCHIVE));
  }

  private void runUnzipTest(String zipFile, boolean unzipError) throws IOException {
    final Path unzipDir = Paths.get(System.getProperty("java.io.tmpdir") + File.separatorChar + UNZIP_DIR);
    final URI zipFileUri = new ClassPathResource("zip" + File.separatorChar + zipFile).getURI();
    final ZipTool zipTool = new ZipTool(zipFileUri);
    assertTrue(zipTool.checkZip());
    if (unzipError) {
      assertFalse(zipTool.unzipFiles(unzipDir, true));
    } else {
      final int entryNumber = zipTool.getEntryNumber();
      final int fileNumber = zipTool.getFileNumber();
      final int folderNumber = zipTool.getFolderNumber();
      assertTrue(zipTool.unzipFiles(unzipDir, true));
      assertEquals(entryNumber, folderNumber + fileNumber);
      assertEquals(fileNumber, FileTool.findFiles(unzipDir).size());
    }
    if (FileTool.ensureFolderExists(unzipDir)) {
      assertTrue(FileTool.deleteFolder(unzipDir));
    }
  }

}
