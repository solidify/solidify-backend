/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - ReflectionToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.util.ReflectionTool;

class ReflectionToolTest {

  private static class Thing {
    private String name;
    protected Integer age;
    private boolean ageSetterCalled = false;

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public Integer getAge() {
      return this.age;
    }

    public void setAge(Integer age) {
      this.age = age;
      this.ageSetterCalled = true;
    }

    public boolean isAgeSetterCalled() {
      return this.ageSetterCalled;
    }
  }

  private enum InstrumentType {
    STRING, WIND, PERCUSSION
  }

  private class Instrument extends Thing {
    private InstrumentType type;

    public InstrumentType getType() {
      return this.type;
    }

    public void setType(InstrumentType type) {
      this.type = type;
    }
  }

  private class StringInstrument extends Instrument {
    public StringInstrument() {
      this.setType(InstrumentType.STRING);
    }
  }

  public class Guitar extends StringInstrument {
    private int strings = 6;
    private String color = "black";

    private boolean stringsSetterCalled = false;

    public int getStrings() {
      return this.strings;
    }

    public void setStrings(int strings) {
      this.strings = strings;
      this.stringsSetterCalled = true;
    }

    public String getColor() {
      return this.color;
    }

    public boolean isStringsSetterCalled() {
      return this.stringsSetterCalled;
    }
  }

  private class PercussionInstrument extends Instrument {
    public PercussionInstrument() {
      this.setType(InstrumentType.PERCUSSION);
    }
  }

  private class Drums extends PercussionInstrument {
    private int toms;

    public int getToms() {
      return this.toms;
    }

    public void setToms(int toms) {
      this.toms = toms;
    }
  }

  private enum Size {
    S, M, L
  }

  public class TShirt extends Thing {
    private Size size;
    private boolean tShirtAgeSetterCalled = false;

    public Size getSize() {
      return this.size;
    }

    public void setSize(Size size) {
      this.size = size;
    }

    @Override
    public Integer getAge() {
      return this.age;
    }

    @Override
    public void setAge(Integer age) {
      this.age = age;
      this.tShirtAgeSetterCalled = true;
    }

    public boolean isTShirtAgeSetterCalled() {
      return this.tShirtAgeSetterCalled;
    }
  }

  @Test
  void getParentClassesTest() {
    Guitar guitar = new Guitar();
    List<Class<? extends Object>> guitarParentClasses = ReflectionTool.getParentClasses(guitar);
    assertEquals(5, guitarParentClasses.size());
    assertEquals(Guitar.class, guitarParentClasses.get(0));
    assertEquals(StringInstrument.class, guitarParentClasses.get(1));
    assertEquals(Instrument.class, guitarParentClasses.get(2));
    assertEquals(Thing.class, guitarParentClasses.get(3));
    assertEquals(Object.class, guitarParentClasses.get(4));

    Drums drums = new Drums();
    List<Class<? extends Object>> drumsParentClasses = ReflectionTool.getParentClasses(drums);
    assertEquals(5, drumsParentClasses.size());
    assertEquals(Drums.class, drumsParentClasses.get(0));
    assertEquals(PercussionInstrument.class, drumsParentClasses.get(1));
    assertEquals(Instrument.class, drumsParentClasses.get(2));
    assertEquals(Thing.class, drumsParentClasses.get(3));
    assertEquals(Object.class, drumsParentClasses.get(4));

    List<Class<? extends Object>> newGuitarParentClasses = ReflectionTool.getParentClasses(guitar, Instrument.class);
    assertEquals(3, newGuitarParentClasses.size());
    assertEquals(Guitar.class, newGuitarParentClasses.get(0));
    assertEquals(StringInstrument.class, newGuitarParentClasses.get(1));
    assertEquals(Instrument.class, newGuitarParentClasses.get(2));
  }

  @Test
  void copyFieldsTest() {
    Guitar guitar = new Guitar();
    guitar.setName("guitar");
    guitar.setAge(12);
    assertEquals(InstrumentType.STRING, guitar.getType());

    // all properties are copied
    Drums drums = new Drums();
    drums.setToms(5);
    assertEquals(InstrumentType.PERCUSSION, drums.getType());
    ReflectionTool.copyFields(drums, guitar, Thing.class);
    assertEquals(5, drums.getToms());
    assertEquals(InstrumentType.STRING, drums.getType());
    assertEquals("guitar", drums.getName());
    assertEquals(12, drums.getAge().intValue());

    // name and age are not copied as they are Thing's members, but type is copied
    drums = new Drums();
    drums.setToms(5);
    assertEquals(InstrumentType.PERCUSSION, drums.getType());
    ReflectionTool.copyFields(drums, guitar, Instrument.class);
    assertEquals(5, drums.getToms());
    assertEquals(InstrumentType.STRING, drums.getType());
    assertNull(drums.getName());
    assertNull(drums.getAge());

    // all properties are copied
    TShirt tShirt = new TShirt();
    tShirt.setSize(Size.L);
    ReflectionTool.copyFields(tShirt, guitar, Thing.class);
    assertEquals(Size.L, tShirt.getSize());
    assertEquals("guitar", tShirt.getName());
    assertEquals(12, tShirt.getAge().intValue());

    // name and age are not copied as they are Thing's members
    tShirt = new TShirt();
    tShirt.setSize(Size.L);
    ReflectionTool.copyFields(tShirt, guitar, Instrument.class);
    assertEquals(Size.L, tShirt.getSize());
    assertNull(tShirt.getName());
    assertNull(tShirt.getAge());
  }

  @Test
  void useSetterTest() {
    Guitar guitar = new Guitar();
    guitar.setName("guitar");
    guitar.setAge(12);
    assertEquals(InstrumentType.STRING, guitar.getType());
    assertEquals(6, guitar.getStrings());
    assertFalse(guitar.isStringsSetterCalled());

    String setterName = ReflectionTool.getSetterName(guitar, "strings");
    assertEquals("setStrings", setterName);

    ReflectionTool.setValueWithSetter(guitar, "strings", 12);
    assertEquals(12, guitar.getStrings());
    assertTrue(guitar.isStringsSetterCalled());
  }

  @Test
  void useParentSetterTest() {
    Guitar guitar = new Guitar();
    guitar.setName("guitar");
    assertEquals(InstrumentType.STRING, guitar.getType());
    assertNull(guitar.getAge());
    assertFalse(guitar.isAgeSetterCalled());

    String setterName = ReflectionTool.getSetterName(guitar, "age");
    assertEquals("setAge", setterName);

    ReflectionTool.setValueWithSetter(guitar, "age", 10);
    assertEquals(10, guitar.getAge());
    assertTrue(guitar.isAgeSetterCalled());
  }

  @Test
  void useOverrideSetterTest() {
    TShirt tShirt = new TShirt();
    assertFalse(tShirt.isAgeSetterCalled());
    assertFalse(tShirt.isTShirtAgeSetterCalled());

    String setterName = ReflectionTool.getSetterName(tShirt, "age");
    assertEquals("setAge", setterName);

    ReflectionTool.setValueWithSetter(tShirt, "age", 10);
    assertEquals(10, tShirt.getAge());
    assertFalse(tShirt.isAgeSetterCalled());
    assertTrue(tShirt.isTShirtAgeSetterCalled());
  }
}
