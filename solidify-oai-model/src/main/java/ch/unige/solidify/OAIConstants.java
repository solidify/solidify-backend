/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

public class OAIConstants {

  private OAIConstants() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  // OAI
  public static final String OAI_MODULE = "oai-info";
  public static final String OAI_PREFIX = "oai";
  public static final String OAI_DOI_REGISTRATION = "doi-registration";
  public static final String OAI_SEP = ":";

  // OAI resources
  public static final String OAI_RESOURCE = "oai";
  public static final String OAI_PROVIDER = "oai-provider";
  public static final String OAI_SETS = "oai-sets";
  public static final String OAI_METADATA_PREFIX = "oai-metadata-prefixes";

  // OAI verbs
  public static final String OAI_IDENTIFY = "Identify";
  public static final String OAI_LIST_METADATA_FORMATS = "ListMetadataFormats";
  public static final String OAI_LIST_IDENTIFIERS = "ListIdentifiers";
  public static final String OAI_GET_RECORD = "GetRecord";
  public static final String OAI_LIST_RECORDS = "ListRecords";
  public static final String OAI_LIST_SETS = "ListSets";

  // OAI parameters
  public static final String OAI_VERB = "verb";
  public static final String OAI_IDENTIFIER = "identifier";
  public static final String OAI_FROM = "from";
  public static final String OAI_TO = "until";
  public static final String OAI_METADATAPREFIX = "metadataPrefix";
  public static final String OAI_SET = "set";
  public static final String OAI_TOKEN = "resumptionToken";
  public static final String OAI_VIEW = "smartView";
  // OAI Provider
  public static final String OAI_PMH = "OAI-PMH";
  public static final String OAI_PMH_SCHEMA = "http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd";
  public static final String OAI_PMH_PREFIX_NAMESPACE = "oai-pmh";
  public static final String OAI_PMH_NAMESPACE = "http://www.openarchives.org/OAI/2.0/";
  // OAI Dublin Core
  public static final String OAI_DC = "oai_dc";
  public static final String OAI_DC_NAME = "Dublin Core for OAI-PMH";
  public static final String OAI_DC_SCHEMA = "http://www.openarchives.org/OAI/2.0/oai_dc.xsd";
  public static final String OAI_DC_NAMESPACE = "http://www.openarchives.org/OAI/2.0/oai_dc/";
  // OAI Identifier
  public static final String OAI_ID = "oai-identifier";
  public static final String OAI_ID_SCHEMA = "http://www.openarchives.org/OAI/2.0/oai-identifier.xsd";
  public static final String OAI_ID_NAMESPACE = "http://www.openarchives.org/OAI/2.0/oai-identifier";
  // Dublin Core
  public static final String DC = "dc";
  public static final String DC_NAMESPACE = "http://purl.org/dc/elements/1.1/";

  public static final String METADATA_CONTENT_TAG = "<metadataContent/>";

  public static final String XSL_PARAM_ARCHIVE_URL = "archiveUrl";
  public static final String XSL_PARAM_RES_ID = "resId";
  public static final String XSL_PARAM_OAI_ID = "oaiId";

  //
  public static final String NAMESPACE_MAPPER_PROPERTY = "org.glassfish.jaxb.namespacePrefixMapper";
}
