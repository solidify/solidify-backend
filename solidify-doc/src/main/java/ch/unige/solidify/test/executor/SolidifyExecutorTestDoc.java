/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Documentation - SolidifyExecutorTestDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.executor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.hamcrest.Matcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.restdocs.hypermedia.HypermediaDocumentation;
import org.springframework.restdocs.hypermedia.LinkDescriptor;
import org.springframework.restdocs.hypermedia.LinksSnippet;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.PayloadDocumentation;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epages.restdocs.apispec.MockMvcRestDocumentationWrapper;

import ch.unige.solidify.test.exception.SolidifyTestRuntimeException;
import ch.unige.solidify.test.helper.FileUtils;

/**
 * Class executor that gives the four cycles for make Spring Rest-docs. Spring has five
 * documentation generator factory: payload, http, header, request and hypermedia.
 */
public class SolidifyExecutorTestDoc {
  private static final String APPLICATION = "application";
  private static final String GENERATED_SNIPPETS = "generated-snippets";
  private static final Logger log = LoggerFactory.getLogger(SolidifyExecutorTestDoc.class);
  private static final String PATH_REQUEST_ADOC = "path-request.adoc";
  private static final String ROOT_CONTEXT = "http(s)://<root context>";
  private static final String SEPARATOR = "/";
  private static final String TARGET = "target";

  /**
   * Creation of the fields snippets
   *
   * @param resultActions The request execution. Usually started in the 1st cycle
   * @param restName the endpoint name
   * @param fieldDescriptors a list containing field descriptors
   */
  public static void makeFieldsDocumentation(ResultActions resultActions, String restName, List<FieldDescriptor> fieldDescriptors,
          boolean readOnly) {
    final String docRestName = getDocPath(restName);
    try {
      log.info("Documentation for {}", docRestName);
      final ResponseFieldsSnippet snippet = PayloadDocumentation.responseFields(fieldDescriptors);
      resultActions.andDo(MockMvcRestDocumentationWrapper.document(docRestName, snippet));
      generatePathInAdocFile(docRestName, readOnly);
    } catch (final Exception e) {
      throw new SolidifyTestRuntimeException("Fail to make field " + restName + " documentation", e);
    }
  }

  /**
   * Creation of the link fields snippets
   *
   * @param resultActions The request execution
   * @param restName the endpoint name
   * @param linkDescriptorList a map containing the pair [xpath=linkDescriptor]
   */
  public static void makeLinksDocumentation(ResultActions resultActions, String restName, boolean list,
          List<LinkDescriptor> linkDescriptorList) {
    String docRestName = getDocPath(restName);
    if (list) {
      docRestName += "/list";
    }
    try {
      final LinksSnippet linksSnippet = HypermediaDocumentation.links(HypermediaDocumentation.halLinks(), linkDescriptorList);
      resultActions.andDo(MockMvcRestDocumentation.document(docRestName, linksSnippet));
    } catch (final Exception e) {
      throw new SolidifyTestRuntimeException("Fail to make links documentation for " + docRestName, e);
    }
  }

  /**
   * The rest integration test cycle. Usually the second cycle of the tests
   *
   * @param resultActions The request execution. Usually started in the 1st cycle
   * @param expectedMap a map containing the pair [xpath=Matcher]
   */
  public static void performExpectationTests(ResultActions resultActions, Map<String, Matcher<?>> expectedMap) {
    expectedMap.forEach((key, expected) -> {
      try {
        resultActions.andExpect(MockMvcResultMatchers.jsonPath(key, expected));
      } catch (final Exception e) {
        throw new SolidifyTestRuntimeException("Fail to perform expectations test", e);
      }
    });
  }

  /**
   * A http mock request. The same than
   * {@link #doPerformRequestCycle(MockMvc, String, MediaType, HttpStatus)}. It uses default media
   * type and default response status.
   */
  public static ResultActions performRequest(MockMvc mockMvc, MockHttpServletRequestBuilder requestBuilder, RequestMethod method,
          String mediaType) {
    HttpStatus httpStatus = HttpStatus.OK;
    if (RequestMethod.POST.equals(method)) {
      httpStatus = HttpStatus.CREATED;
    }
    try {
      return mockMvc.perform(requestBuilder).andDo(MockMvcResultHandlers.print())
              .andExpect(MockMvcResultMatchers.content().contentType(mediaType))
              .andExpect(MockMvcResultMatchers.status().is(httpStatus.value()));
    } catch (final Exception e) {
      throw new SolidifyTestRuntimeException("Fail to perform request", e);
    }
  }

  private static void generatePathInAdocFile(String docRestName, boolean readOnly) {
    final String url = generatePathUrl(docRestName, readOnly);
    try {
      final String outputFilePath = TARGET + SEPARATOR + GENERATED_SNIPPETS + SEPARATOR + docRestName + SEPARATOR + PATH_REQUEST_ADOC;
      FileUtils.generateFile(url, outputFilePath);
    } catch (final IOException e) {
      log.error("In 'generatePathInAdocFile' function", e);
    }
  }

  private static String generatePathUrl(String docRestName, boolean readOnly) {
    final StringBuilder url = new StringBuilder();
    url.append("The URL of the REST resource is :\n");
    url.append("`" + ROOT_CONTEXT);

    final List<String> listPartOfPath = getPartOfPath(docRestName);
    for (final String name : listPartOfPath) {
      url.append(SEPARATOR).append(name);
    }
    url.append("`");
    if (readOnly) {
      url.append(" (_Read-Only REST resource_)");
    }
    return url.toString();
  }

  private static String getDocPath(String path) {
    try {
      return APPLICATION + path.substring(path.indexOf(SEPARATOR, path.indexOf(SEPARATOR) + 1));
    } catch (final StringIndexOutOfBoundsException e) {
      return APPLICATION;
    }
  }

  private static List<String> getPartOfPath(String docRestName) {
    final List<String> listPartOfPath = new ArrayList<>(Arrays.asList(docRestName.split(SEPARATOR)));
    removeApplicationPrefix(listPartOfPath);
    return listPartOfPath;
  }

  private static void removeApplicationPrefix(List<String> listPartOfPath) {
    final int indexApplication = 0;
    if (listPartOfPath.get(indexApplication).equals(APPLICATION)) {
      listPartOfPath.remove(indexApplication);
    }
  }

}
