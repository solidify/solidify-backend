/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH - OAIProviderController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller.oai;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.OAIUrlPath;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.OAIController;
import ch.unige.solidify.exception.OAIException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.oai.OAIMetadataPrefix;
import ch.unige.solidify.model.oai.OAIPMH;
import ch.unige.solidify.model.oai.OAIToken;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHerrorcodeType;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHtype;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.OAIService;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.OAITool;
import ch.unige.solidify.util.StringTool;

@RestController
@EveryonePermissions
@ConditionalOnBean(OAIController.class)
@RequestMapping(OAIUrlPath.OAI_PROVIDER)
public class OAIProviderController implements ControllerWithHateoasHome {
  private static final Logger log = LoggerFactory.getLogger(OAIProviderController.class);

  protected OAIService oaiService;

  public OAIProviderController(OAIService oaiService) {
    this.oaiService = oaiService;
  }

  @GetMapping("/" + OAIConstants.OAI_RESOURCE)
  public HttpEntity<String> process(HttpServletRequest request, @RequestParam(value = OAIConstants.OAI_VERB, required = false) String verb,
          @RequestParam(value = OAIConstants.OAI_IDENTIFIER, required = false) String identifier,
          @RequestParam(value = OAIConstants.OAI_METADATAPREFIX, required = false) String metadataPrefix,
          @RequestParam(value = OAIConstants.OAI_FROM, required = false) String from,
          @RequestParam(value = OAIConstants.OAI_TO, required = false) String until,
          @RequestParam(value = OAIConstants.OAI_SET, required = false) String set,
          @RequestParam(value = OAIConstants.OAI_TOKEN, required = false) String token,
          @RequestParam(value = OAIConstants.OAI_VIEW, required = false) String smartView) {
    final OAIPMHtype oai = new OAIPMHtype();
    final String oaiUrl = this.getBaseUrl(request) + this.getRequestUri(request); // OAI provider base URL
    final Map<String, String> schemaLocations = new HashMap<>();

    if (StringTool.isNullOrEmpty(metadataPrefix) && !StringTool.isNullOrEmpty(token)) {
      metadataPrefix = this.getMetadataPrefixFromToken(token);
    }

    try {
      switch (this.oaiService.getVerb(verb)) {
        case GET_RECORD -> {
          // Specification
          // ------------
          // https://www.openarchives.org/OAI/openarchivesprotocol.html#GetRecord
          oai.setGetRecord(this.oaiService.getRecord(this.getRootUrl(request), identifier, metadataPrefix));
          final OAIMetadataPrefix oaiMetadataPrefixForRecord = this.getOAIMetadataPrefix(metadataPrefix, token, oaiUrl);
          schemaLocations.put(oaiMetadataPrefixForRecord.getSchemaNamespace(), oaiMetadataPrefixForRecord.getSchemaUrl().toString());
        }
        case IDENTIFY -> {
          // Specification
          // ------------
          // https://www.openarchives.org/OAI/openarchivesprotocol.html#Identify
          oai.setIdentify(this.oaiService.getIdentify(oaiUrl));
          // OAI Identifier
          schemaLocations.put(OAIConstants.OAI_ID_NAMESPACE, OAIConstants.OAI_ID_SCHEMA);
          // OAI Repository
          // Force http usage, in order to be compatible with old XML validators
          final String httpOnlyOaiUrl = oaiUrl.replace("https://", "http://");
          schemaLocations.put(this.oaiService.getOaiRepositoryInfo().getRepositoryNamespace(), httpOnlyOaiUrl + "/" + SolidifyConstants.SCHEMA);
        }
        case LIST_IDENTIFIERS ->
          // Specification
          // ------------
          // https://www.openarchives.org/OAI/openarchivesprotocol.html#ListIdentifiers
                oai.setListIdentifiers(this.oaiService.listIdentifiers(metadataPrefix, from, until, set, token));
        case LIST_METADATA_FORMATS ->
          // Specification
          // ------------
          // https://www.openarchives.org/OAI/openarchivesprotocol.html#ListMetadataFormats
                oai.setListMetadataFormats(this.oaiService.listMetadataFormats(identifier));
        case LIST_RECORDS -> {
          // Specification
          // ------------
          // https://www.openarchives.org/OAI/openarchivesprotocol.html#ListRecords
          oai.setListRecords(this.oaiService.listRecords(this.getBaseUrl(request), metadataPrefix, from, until, set, token));
          // OAI metadata prefix
          final OAIMetadataPrefix oaiMetadataPrefixForList = this.getOAIMetadataPrefix(metadataPrefix, token, oaiUrl);
          schemaLocations.put(oaiMetadataPrefixForList.getSchemaNamespace(), oaiMetadataPrefixForList.getSchemaUrl().toString());
        }
        case LIST_SETS ->
          // Specification
          // ------------
          // https://www.openarchives.org/OAI/openarchivesprotocol.html#ListSets
                oai.setListSets(this.oaiService.listSets(token));
      }
      oai.setRequest(this.oaiService.getRequest(oaiUrl, verb, identifier, metadataPrefix, from, until, set, token));
    } catch (final IllegalArgumentException e) {
      log.error("Unknown verb: {}", verb);
      oai.getError().add(this.oaiService.getError(OAIPMHerrorcodeType.BAD_VERB, verb));
      oai.setRequest(this.oaiService.getRequest(oaiUrl, null, identifier, metadataPrefix, from, until, set, token));
    } catch (final OAIException e) {
      log.error("{} : {}", e.getError().getCode(), e.getError().getValue());
      oai.getError().add(e.getError());
      oai.setRequest(this.oaiService.getRequest(oaiUrl, verb, identifier, metadataPrefix, from, until, set, token));
    } catch (final SolidifyResourceNotFoundException e) {
      log.error("ID not found: {}", identifier);
      oai.getError().add(this.oaiService.getError(OAIPMHerrorcodeType.ID_DOES_NOT_EXIST, identifier));
      oai.setRequest(this.oaiService.getRequest(oaiUrl, null, identifier, metadataPrefix, from, until, set, token));
    } catch (final JAXBException e) {
      throw new SolidifyRuntimeException("Error during OAI-PMH request processing", e);
    }
    oai.setResponseDate(Instant.now().atOffset(ZoneOffset.UTC).withNano(0));
    String result = null;
    String xslUrl = null;
    try {
      if (!StringTool.isNullOrEmpty(smartView)) {
        xslUrl = oaiUrl + "/" + SolidifyConstants.XSL + "?" + OAIConstants.OAI_VIEW + "=" + smartView;
      }
      result = this.oaiService.printOAIPMH(oai, xslUrl, metadataPrefix, schemaLocations);
      log.trace(result);
    } catch (final JAXBException e) {
      log.error("In converting OAI in xml", e);
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    final HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML_VALUE);
    return new ResponseEntity<>(result, headers, HttpStatus.OK);
  }

  @PostMapping(value = "/" + OAIConstants.OAI_RESOURCE, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public HttpEntity<String> processPost(HttpServletRequest request, @RequestBody MultiValueMap<String, String> params) {
    log.trace("OAI-PMH parameters: {}", params);
    return this.process(request, this.getParameter(params, OAIConstants.OAI_VERB), this.getParameter(params, OAIConstants.OAI_IDENTIFIER),
            this.getParameter(params, OAIConstants.OAI_METADATAPREFIX), this.getParameter(params, OAIConstants.OAI_FROM),
            this.getParameter(params, OAIConstants.OAI_TO), this.getParameter(params, OAIConstants.OAI_SET),
            this.getParameter(params, OAIConstants.OAI_TOKEN), null);
  }

  @GetMapping
  public HttpEntity<OAIPMH> status() {
    final OAIPMH oaiPmh = new OAIPMH();
    final OAIPMHtype oai = new OAIPMHtype();
    final WebMvcLinkBuilder linkBuilder = linkTo(methodOn(this.getClass()).process(null, null, null, null, null, null, null, null, null));
    oai.setIdentify(this.oaiService.getIdentify(linkBuilder.toUri().toASCIIString()));
    oaiPmh.setOai(oai);
    oaiPmh.add(linkTo(this.getClass()).withSelfRel());
    oaiPmh.add(Tool.link(linkTo(methodOn(this.getClass()).xsl(null)).toUriComponentsBuilder()).withRel(SolidifyConstants.XSL));
    oaiPmh.add(OAITool.oaiLink(linkBuilder.toUriComponentsBuilder(), OAIConstants.OAI_IDENTIFY).withRel(OAIConstants.OAI_IDENTIFY));
    oaiPmh.add(OAITool.oaiLink(linkBuilder.toUriComponentsBuilder(), OAIConstants.OAI_LIST_METADATA_FORMATS)
            .withRel(OAIConstants.OAI_LIST_METADATA_FORMATS));
    oaiPmh.add(OAITool.oaiLink(linkBuilder.toUriComponentsBuilder(), OAIConstants.OAI_LIST_IDENTIFIERS, OAIConstants.OAI_DC)
            .withRel(OAIConstants.OAI_LIST_IDENTIFIERS));
    oaiPmh.add(OAITool.oaiLink(linkBuilder.toUriComponentsBuilder(), OAIConstants.OAI_LIST_SETS).withRel(OAIConstants.OAI_LIST_SETS));
    oaiPmh.add(OAITool.oaiLink(linkBuilder.toUriComponentsBuilder(), OAIConstants.OAI_LIST_RECORDS, OAIConstants.OAI_DC)
            .withRel(OAIConstants.OAI_LIST_RECORDS));
    oaiPmh.add(OAITool.oaiLink(linkBuilder.toUriComponentsBuilder(), OAIConstants.OAI_GET_RECORD).withRel(OAIConstants.OAI_GET_RECORD));
    return new ResponseEntity<>(oaiPmh, HttpStatus.OK);
  }

  @GetMapping("/" + OAIConstants.OAI_RESOURCE + "/" + SolidifyConstants.XSL)
  public HttpEntity<String> xsl(@RequestParam(value = OAIConstants.OAI_VIEW, required = false) String smartView) {
    try {
      String result;
      if (StringTool.isNullOrEmpty(smartView)) {
        StringBuilder xslt = new StringBuilder();
        xslt.append("<xmlTransformation>");
        for (final Resource f : FileTool.scanClassPath("classpath:" + SolidifyConstants.XSL_HOME + "/*.xsl")) {
          xslt.append("<xslFile>").append(f.getFilename()).append("</xslFile>");
        }
        xslt.append("</xmlTransformation>");
        result = xslt.toString();
      } else {
        final ClassPathResource xsl = new ClassPathResource(SolidifyConstants.XSL_HOME + "/" + smartView);
        result = FileTool.toString(xsl.getInputStream());
      }
      final HttpHeaders headers = new HttpHeaders();
      headers.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML_VALUE);
      return new ResponseEntity<>(result, headers, HttpStatus.OK);
    } catch (final IOException e) {
      log.error("XML Transformation error", e);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping("/" + OAIConstants.OAI_RESOURCE + "/" + SolidifyConstants.SCHEMA)
  public HttpEntity<String> xsd() {
    try {
      final ClassPathResource xsl = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" +
              this.oaiService.getOaiRepositoryInfo().getRepositorySchema());
      String result = FileTool.toString(xsl.getInputStream());

      final HttpHeaders headers = new HttpHeaders();
      headers.add(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML_VALUE);
      return new ResponseEntity<>(result, headers, HttpStatus.OK);
    } catch (final Exception e) {
      log.error("XML Schema error", e);
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  /**
   * @param HttpServletRequest
   * @return Base local URL
   */
  private String getBaseUrl(HttpServletRequest request) {
    final String scheme = request.getScheme();
    final int portNum = request.getServerPort();
    final boolean standardHttp = "http".equals(scheme) && portNum == 80;
    final boolean standardHttps = "https".equals(scheme) && portNum == 443;
    String portString = "";
    if (!standardHttp && !standardHttps) {
      portString = ":" + portNum;
    }
    return request.getScheme() + "://" + request.getServerName() + portString;
  }

  /**
   * @param HttpServletRequest
   * @return Real public URL (from all proxy chain)
   */
  private String getRequestUri(HttpServletRequest request) {
    final String forwardedPath = request.getHeader("X-Forwarded-Path");
    if (forwardedPath != null) {
      return forwardedPath;
    } else {
      return request.getRequestURI();
    }
  }

  /**
   * @param HttpServletRequest
   * @return Module Root local URL (without OAI patch)
   */
  private String getRootUrl(HttpServletRequest request) {
    String baseUrl = this.getBaseUrl(request) + request.getRequestURI();
    baseUrl = baseUrl.substring(0, baseUrl.indexOf(OAIConstants.OAI_PROVIDER + "/" + OAIConstants.OAI_RESOURCE));
    return baseUrl;
  }

  private OAIMetadataPrefix getOAIMetadataPrefix(String metadataPrefix, String token, String oaiUrl) {
    if (metadataPrefix != null) {
      return this.oaiService.getOAIMetadataPrefix(metadataPrefix);
    } else if (token != null) {
      OAIToken oaiToken = new OAIToken();
      oaiToken.setToken(token);
      return this.oaiService.getOAIMetadataPrefix(oaiToken.getOaiParameters().getMetadataPrefix());
    } else {
      throw new OAIException(OAIPMHerrorcodeType.NO_METADATA_FORMATS, oaiUrl);
    }
  }

  private String getMetadataPrefixFromToken(String token) {
    OAIToken oaiToken = new OAIToken();
    oaiToken.setToken(token);
    return oaiToken.getOaiParameters().getMetadataPrefix();
  }
}
