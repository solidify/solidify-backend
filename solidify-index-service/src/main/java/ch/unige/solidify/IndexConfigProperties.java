/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Service - IndexProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "solidify.index")
public class IndexConfigProperties {

  private String indexPrefix = "solidify";
  private boolean generateData = false;
  private Config config = new Config();

  public boolean isGenerateData() {
    return this.generateData;
  }

  public void setGenerateData(boolean generateData) {
    this.generateData = generateData;
  }

  public String getIndexPrefix() {
    return this.indexPrefix;
  }

  public void setIndexPrefix(String indexPrefix) {
    this.indexPrefix = indexPrefix;
  }

  public Config getConfig() {
    return this.config;
  }

  public void setConfig(Config config) {
    this.config = config;
  }

  public static class Config {
    private String host = "localhost";
    private String username;
    private String password;
    private int port = 9200;
    private int shardNumber = 1;
    private int replicaNumber = 0;
    private int fieldsLimit = 2000;
    private String clusterName = "elasticsearch";
    private boolean sslEnabled = false;

    private String[] exceptions = {};

    public String getHost() {
      return this.host;
    }

    public String getUsername() {
      return this.username;
    }

    public String getPassword() {
      return this.password;
    }

    public int getPort() {
      return this.port;
    }

    public String getClusterName() {
      return this.clusterName;
    }

    public boolean isSslEnabled() {
      return this.sslEnabled;
    }

    public String[] getExceptions() {
      return this.exceptions;
    }

    public void setHost(String host) {
      this.host = host;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public void setPort(int port) {
      this.port = port;
    }

    public void setClusterName(String clusterName) {
      this.clusterName = clusterName;
    }

    public void setSslEnabled(boolean sslEnabled) {
      this.sslEnabled = sslEnabled;
    }

    public void setExceptions(String[] exceptions) {
      this.exceptions = exceptions;
    }

    public int getShardNumber() {
      return this.shardNumber;
    }

    public void setShardNumber(int shardNumber) {
      this.shardNumber = shardNumber;
    }

    public int getReplicaNumber() {
      return this.replicaNumber;
    }

    public void setReplicaNumber(int replicaNumber) {
      this.replicaNumber = replicaNumber;
    }

    public int getFieldsLimit() {
      return this.fieldsLimit;
    }

    public void setFieldsLimit(int fieldsLimit) {
      this.fieldsLimit = fieldsLimit;
    }
  }
}
