/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID - OrcidSynchronizationRepository.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.solidify.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Repository;

import ch.unige.solidify.controller.OrcidController;
import ch.unige.solidify.model.OrcidSynchronization;

@Repository
@ConditionalOnBean(OrcidController.class)
public interface OrcidSynchronizationRepository extends SolidifyRepository<OrcidSynchronization> {
  List<OrcidSynchronization> findByObjectId(String aipId);

  List<OrcidSynchronization> findByPersonIdAndObjectId(String personId, String aipId);

  Optional<OrcidSynchronization> findByPersonIdAndPutCode(String personId, BigInteger putCode);
}
