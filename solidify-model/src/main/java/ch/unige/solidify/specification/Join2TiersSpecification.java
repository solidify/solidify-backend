/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - Join2TiersSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.JoinResource;

/**
 * Specification allowing to manage a 2-tiers relationship
 *
 * @param <J> Type of the join resource entity between the 2 entity types linked together by a relation table
 */
public abstract class Join2TiersSpecification<J extends JoinResource<?>> extends JoinNTiersSpecification<J> {

  protected Join2TiersSpecification(J joinCriteria, String pathToParent, String pathToChild) {
    super(joinCriteria, pathToParent, pathToChild);
  }

  @Override
  public Predicate toPredicate(Root<J> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final List<Predicate> predicatesList = new ArrayList<>();

    // ensure only children of parent are found
    this.completeParentIdPredicate(root, builder, predicatesList);

    if (this.filterOnJoinResourceId) {
      if (this.joinCriteria.getCompositeKey() != null) {
        predicatesList.add(builder.equal(root.get(SolidifyConstants.COMPOSITE_RES_ID_FIELD), this.joinCriteria.getCompositeKey()));
      }
    }

    if (this.filterOnChildResId) {
      // ensure the child has specific id (used to get one specific child)
      this.completeChildIdPredicate(root, builder, predicatesList);
    } else {

      // allow to filter on join table properties
      this.completeJoinPredicatesList(root, query, builder, predicatesList);

      // allow to filter list of children on child properties
      this.completeChildPredicatesList(root, query, builder, predicatesList);
    }

    return builder.and(predicatesList.toArray(new Predicate[predicatesList.size()]));
  }
}
