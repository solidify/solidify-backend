/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Documentation - TestObjectBuilder.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.helper;

import static ch.unige.solidify.test.helper.AbstractResourceDoc.TEST_STRING_VALUE;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.restdocs.payload.FieldDescriptor;

import ch.unige.solidify.SolidifyConstants;

public class TestObjectBuilder {

  /**
   * Build a JSON object from the list of field descriptors Only field descriptors with TEST_VALUE attribute are taken Embedded objects are
   * constructed from path names with dotted notation
   *
   * @param fieldDescriptorList the list of the field descriptors
   * @return a JSON object
   */
  public static JSONObject buildJsonObject(List<FieldDescriptor> fieldDescriptorList) {
    final Map<String, Object> mainMap = new HashMap<>();
    for (final FieldDescriptor fieldDescriptor : fieldDescriptorList) {
      Map<String, Object> currentMap = mainMap;
      final String[] pathPartList = fieldDescriptor.getPath().split(SolidifyConstants.REGEX_FIELD_PATH_SEP);
      for (int i = 0; i < pathPartList.length; i++) {
        if (currentMap.get(pathPartList[i]) == null) {
          if (i != pathPartList.length - 1) {
            final Map<String, Object> newMap = new HashMap<>();
            currentMap.put(pathPartList[i], newMap);
            currentMap = newMap;
          } else {
            currentMap.put(pathPartList[pathPartList.length - 1], fieldDescriptor.getAttributes().get(TEST_STRING_VALUE));
          }
        } else if (currentMap.get(pathPartList[i]) instanceof Map) {
          currentMap = (Map<String, Object>) currentMap.get(pathPartList[i]);
        }
      }

    }
    return new JSONObject(mainMap);
  }

}
