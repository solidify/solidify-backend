/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - JoinResource2TiersService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.specification.Join2TiersSpecification;

/**
 * Service layer used to manage a 2-tiers relationship
 *
 * @param <T> Type of the entity considered as the parent when managing the relation through the Relation2TiersController
 * @param <V> Type of the entity considered as the children when managing the relation through the Relation2TiersController
 * @param <J> Type of the join resource entity (the relation table) between &lt;T&gt; and &lt;V&gt;
 * @param <K> Type of the composite key of the relation
 */
public abstract class JoinResource2TiersService<T extends Resource, V extends Resource, J extends JoinResource<K>, K> extends
        JoinResourceNTiersService<T, V, J> {

  public Page<J> findAllRelations(Pageable pageable) {
    return this.joinRepository.findAll(pageable);
  }

  public Page<J> findAllRelations(Join2TiersSpecification<J> specification, Pageable pageable) {
    return this.joinRepository.findAll(specification, pageable);
  }

  public List<J> findAllRelations(Join2TiersSpecification<J> specification) {
    return this.joinRepository.findAll(specification);
  }

  public J findRelation(T parentResource, V childResource) {
    Join2TiersSpecification<J> joinSpecification = this.getJoinSpecification(parentResource.getResId(), childResource);
    joinSpecification.setFilterOnChildResId(true);

    return this.joinRepository.findOne(joinSpecification).orElseThrow(() -> new NoSuchElementException(
            "No relation with parentId " + parentResource.getResId() + " and childId " + childResource.getResId()));
  }

  public void deleteRelation(T parentResource, V childResource) {
    Join2TiersSpecification<J> joinSpecification = this.getJoinSpecification(parentResource.getResId(), childResource);
    joinSpecification.setFilterOnChildResId(true);
    for (J relation : this.findAllRelations(joinSpecification)) {
      this.deleteRelation(relation);
    }
  }

  public void deleteRelation(T parentResource, V childResource, K compositeKey) {
    Join2TiersSpecification<J> joinSpecification = this.getJoinSpecification(parentResource.getResId(), childResource);
    joinSpecification.getJoinCriteria().setCompositeKey(compositeKey);
    joinSpecification.setFilterOnJoinResourceId(true);
    List<J> relationList = this.findAllRelations(joinSpecification);
    if (!relationList.isEmpty()) {
      if (relationList.size() == 1) {
        this.deleteRelation(relationList.get(0));
      } else {
        throw new IllegalStateException("Multiple relations found with the same compositeKey");
      }
    }
  }

  public boolean relationExists(T parentResource, V childResource) {
    Join2TiersSpecification<J> joinSpecification = this.getJoinSpecification(parentResource.getResId(), childResource);
    joinSpecification.setFilterOnChildResId(true);
    return this.joinRepository.exists(joinSpecification);
  }

  public J saveRelation(T parentResource, V childResource, J joinResource) {
    return this.saveRelation(this.completeJoinResourceRelations(joinResource, parentResource, childResource));
  }

  public Join2TiersSpecification<J> getJoinSpecification(String parentId, V childCriteria) {

    J joinResource = this.getEmptyJoinResourceObject();

    T parentResource = this.getEmptyParentResourceObject();
    parentResource.setResId(parentId);

    this.setParentResource(joinResource, parentResource);
    this.setChildResource(joinResource, childCriteria);

    return this.getJoinSpecification(joinResource);
  }

  /**
   * Return a new JoinResource that can be saved in database to add a new relation between parent and child, with properties from the join
   * resource
   *
   * @param joinResource
   * @param parentItem
   * @param childItem
   * @return
   */
  public J completeJoinResourceRelations(J joinResource, T parentItem, V childItem) {

    this.setParentResource(joinResource, parentItem);
    this.setChildResource(joinResource, childItem);

    return joinResource;
  }

  /****************************************************************************************************/

  public abstract Join2TiersSpecification<J> getJoinSpecification(J joinResource);

  /**
   * Return the child property of the join resource
   *
   * @param joinResource
   * @return
   */
  public abstract V getChildResource(J joinResource);

  /**
   * Get the path from the specification root (the join resource) to the entity considered as the child resource
   * <p>
   * (e.g: "compositeKey.entityName")
   *
   * @return
   */
  public abstract String getChildPathFromJoinResource();

  /**
   * Return an object used to display the child resource along with the relation's properties
   *
   * @param joinResource
   * @return
   */
  public abstract JoinResourceContainer<J> getChildDTO(J joinResource);
}
