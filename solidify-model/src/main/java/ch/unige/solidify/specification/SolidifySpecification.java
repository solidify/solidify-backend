/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SolidifySpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.Resource;

public abstract class SolidifySpecification<T extends Resource> implements Specification<T> {

  private static final long serialVersionUID = 3675708444036947518L;

  protected final T criteria;

  protected final List<JoinedTableInValues> joinedTableInValues = new ArrayList<>();

  /**
   * Indicates if a criteria must be added for the root entity resId.
   * <p>
   * If 'true' the list returned when using the specification will only contain 1 or 0 element. It can be used to confirm that a specific entity
   * correspond to all given criteria: if the entity matches all of them it is returned in the list, otherwise en empty list is returned.
   */
  protected boolean filterOnRootResId = false;

  protected SolidifySpecification(T criteria) {
    this.criteria = criteria;
  }

  public List<JoinedTableInValues> getJoinedTableInValues() {
    return this.joinedTableInValues;
  }

  public void addJoinTableInValues(JoinedTableInValues joinedTableValue) {
    this.joinedTableInValues.add(joinedTableValue);
  }

  public boolean getFilterOnRootResId() {
    return this.filterOnRootResId;
  }

  public void setFilterOnRootResId(boolean filterOnRootResId) {
    this.filterOnRootResId = filterOnRootResId;
  }

  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final List<Predicate> predicatesList = new ArrayList<>();

    this.completePredicatesList(root, query, builder, predicatesList);
    this.completeJoinedTableInValues(root, query, predicatesList);
    this.completeFilterOnRootResId(root, builder, predicatesList);

    return builder.and(predicatesList.toArray(new Predicate[predicatesList.size()]));
  }

  private void completeJoinedTableInValues(Root<T> root, CriteriaQuery<?> query, List<Predicate> predicatesList) {
    if (!this.joinedTableInValues.isEmpty()) {

      // Add criteria on eventual JOINed table values
      this.joinedTableInValues.forEach(joinedTableValue -> predicatesList
              .add(this.getPathFromExpression(root.join(joinedTableValue.getRelationName()), joinedTableValue.getFieldName())
                      .in(joinedTableValue.getValues())));

      // Set distinct to avoid duplicates if multiple JOIN or multiple matching values
      query.distinct(true);
    }
  }

  private void completeFilterOnRootResId(Root<T> root, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (this.filterOnRootResId) {
      predicatesList.add(builder.equal(root.get(SolidifyConstants.DB_RES_ID), this.criteria.getResId()));
    }
  }

  /**
   * Method allowing to get a Path or a sub Path by using paths names delimited by a colon (like in JPA path expression) Examples of possible
   * values for pathExpression:
   * <p>
   * - "fieldname" - "embeddedId.fieldname"
   *
   * @param join
   * @param pathExpression
   * @return
   */
  private Path<?> getPathFromExpression(Join<?, ?> join, String pathExpression) {
    String[] attributeNames = pathExpression.split(SolidifyConstants.REGEX_FIELD_PATH_SEP);
    Path path = null;
    for (String attributeName : attributeNames) {
      if (path == null) {
        path = join.get(attributeName);
      } else {
        path = path.get(attributeName);
      }
    }
    return path;
  }

  protected abstract void completePredicatesList(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList);
}
