/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - DownloadTokenService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.time.OffsetDateTime;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.model.security.SolidifyDownloadTokenType;
import ch.unige.solidify.repository.DownloadTokenRepository;
import ch.unige.solidify.util.HashTool;

@Service
public class DownloadTokenService {

  private final int downloadTokenLifeTimeMinutes;
  private final DownloadTokenRepository downloadTokenRepository;

  private static final long CLEANING_INTERVAL_MILLISECONDS = 600000;  // ten minutes

  public DownloadTokenService(DownloadTokenRepository downloadTokenRepository, SolidifyProperties config) {
    this.downloadTokenRepository = downloadTokenRepository;
    this.downloadTokenLifeTimeMinutes = config.getSecurity().getDownloadTokenLifeTimeMinutes();
  }

  @Scheduled(fixedDelay = CLEANING_INTERVAL_MILLISECONDS)
  public void removeExpiredDownloadToken() {
    final OffsetDateTime limitTime = OffsetDateTime.now().minusMinutes(this.downloadTokenLifeTimeMinutes);
    for (DownloadToken downloadToken : this.downloadTokenRepository.findExpiredDownloadTokens(limitTime)) {
      this.downloadTokenRepository.delete(downloadToken);
    }
  }

  public String getTokenHashFromRequest(String resourceId, SolidifyDownloadTokenType resourceType) {
    ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    if (servletRequestAttributes == null) {
      throw new SolidifyRuntimeException("Can't get servlet request attributes");
    }

    HttpServletRequest httpServletRequest = servletRequestAttributes.getRequest();
    String tokenString = null;

    if (httpServletRequest.getCookies() == null) {
      return null;
    }
    for (Cookie cookie : httpServletRequest.getCookies()) {
      String cookieName = cookie.getName();
      String cookieTypeAndResourceId = cookieName.substring(cookieName.indexOf('-') + 1);
      if (cookieTypeAndResourceId.equals(resourceType + "-" + resourceId)) {
        tokenString = cookie.getValue();
      }
    }
    if (tokenString == null) {
      return null;
    }
    return HashTool.hash(tokenString);
  }

  public DownloadToken findByTokenHash(String tokenHash) {
    return this.downloadTokenRepository.findByTokenHash(tokenHash);
  }
}
