/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Authorization Model - ApplicationRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "The application role defines the priviledge of the user.")
public interface ApplicationRole {
  @Schema(description = "The identifier of the resource. The default format is a Universally Unique IDentifier (UUID).")
  String getResId();

  @Schema(description = "The name of the application role.")
  String getName();

  @Schema(description = "The level of the application role. The lowest value has more privileges.")
  int getLevel();
}
