/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID - OrcidOAuthService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import reactor.core.publisher.Mono;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.controller.OrcidController;
import ch.unige.solidify.model.OrcidToken;
import ch.unige.solidify.model.PersonWithOrcid;

@Service
@ConditionalOnBean(OrcidController.class)
public class OrcidOAuthService {

  private static final Logger log = LoggerFactory.getLogger(OrcidOAuthService.class);

  private static final String ORCID_PARAMETER = "orcid";

  private static final String ORCID_ERROR_PARAMETER = "orcid_error";
  private static final String ORCID_ERROR_DESCRIPTION_PARAMETER = "orcid_error_description";

  private final PersonWithOrcidService personWithOrcidService;

  private final String grantType;
  private final String oAuthTokenUrl;

  private final String clientId;
  private final String clientSecret;

  private final Map<String, PersonOrigin> mapPersonOrigin = new HashMap<>();

  public OrcidOAuthService(PersonWithOrcidService personWithOrcidService, SolidifyProperties solidifyProperties) {
    this.personWithOrcidService = personWithOrcidService;
    this.clientId = solidifyProperties.getOrcid().getClientId();
    this.clientSecret = solidifyProperties.getOrcid().getClientSecret();
    this.oAuthTokenUrl = solidifyProperties.getOrcid().getTokenUrl();
    this.grantType = solidifyProperties.getOrcid().getGrantType();
  }

  private PersonWithOrcid getCurrentPerson() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return this.personWithOrcidService.getPerson(authentication);
  }

  public JSONObject startOrcidAuth(String originUrl) {
    originUrl = this.cleanOriginUrl(originUrl);
    final PersonWithOrcid person = this.getCurrentPerson();
    final String authTransactionId = UUID.randomUUID().toString();
    this.mapPersonOrigin.put(authTransactionId, new PersonOrigin(person.getResId(), originUrl));
    log.info("{} starting ORCID authentication with authTransactionId {}", person, authTransactionId);
    return new JSONObject().put("authTransactionId", authTransactionId);
  }

  public String getOrcidAndTokensFromOrcidWebsite(String code, String authTransactionId) throws JsonProcessingException {

    final WebClient client = WebClient.create(this.oAuthTokenUrl);
    final Mono<String> response = client.post()
            .header("Accept", "application/json")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(BodyInserters.fromValue("client_id=" + this.clientId
                    + "&client_secret=" + this.clientSecret
                    + "&grant_type=" + this.grantType
                    + "&code=" + code))
            .exchangeToMono(clientResponse -> clientResponse.bodyToMono(String.class));
    final JsonNode jsonResponse = new ObjectMapper().readTree(response.block());

    // Store personWithOrcid's ORCID and set it as "verified"
    final PersonWithOrcid personWithOrcid = this.personWithOrcidService.findOne(this.mapPersonOrigin.get(authTransactionId).getPersonId());
    final String authenticatedOrcid = jsonResponse.get(ORCID_PARAMETER).textValue();
    personWithOrcid.setOrcid(authenticatedOrcid);
    personWithOrcid.setVerifiedOrcid(true);
    log.info("{} has obtained authenticated ORCID {}", personWithOrcid, authenticatedOrcid);

    // If ORCID response contains tokens, store them
    if (jsonResponse.get("access_token") != null) {
      OrcidToken orcidToken = new OrcidToken();
      orcidToken.setAccessToken(jsonResponse.get("access_token").textValue());
      int expiresInSeconds = jsonResponse.get("expires_in").asInt();
      orcidToken.setExpiresIn(OffsetDateTime.now().plusSeconds(expiresInSeconds));
      orcidToken.setRefreshToken(jsonResponse.get("refresh_token").textValue());
      orcidToken.setOrcid(authenticatedOrcid);
      orcidToken.setName(jsonResponse.get("name").textValue());
      orcidToken.setScope(jsonResponse.get("scope").textValue());
      personWithOrcid.setOrcidToken(orcidToken);
    }

    // Save Person (eventually with token)
    this.personWithOrcidService.save(personWithOrcid);

    return this.getAndRemoveOriginUrl(authTransactionId, true, null, null);
  }

  public String getAndRemoveOriginUrl(String authTransactionId, String errorType, String errorDescription) {
    return this.getAndRemoveOriginUrl(authTransactionId, false, errorType, errorDescription);
  }

  private String getAndRemoveOriginUrl(String authTransactionId, boolean grantSuccess, String errorType, String errorDescription) {
    final String originUrl = this.mapPersonOrigin.get(authTransactionId).getOriginUrl();

    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(originUrl);
    this.setUrlParameter(uriBuilder, ORCID_PARAMETER, grantSuccess);
    this.setUrlParameter(uriBuilder, ORCID_ERROR_PARAMETER, errorType);
    this.setUrlParameter(uriBuilder, ORCID_ERROR_DESCRIPTION_PARAMETER, errorDescription);

    this.mapPersonOrigin.remove(authTransactionId);

    return uriBuilder.toUriString();
  }

  private void setUrlParameter(UriComponentsBuilder uriBuilder, String parameterName, Object value) {
    if (value != null) {
      if (uriBuilder.build().getQueryParams().containsKey(parameterName)) {
        uriBuilder.replaceQueryParam(parameterName, value);
      } else {
        uriBuilder.queryParam(parameterName, value);
      }
    }
  }

  private String cleanOriginUrl(String originUrl) {
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(originUrl);
    uriBuilder.replaceQueryParam(ORCID_PARAMETER);
    uriBuilder.replaceQueryParam(ORCID_ERROR_PARAMETER);
    uriBuilder.replaceQueryParam(ORCID_ERROR_DESCRIPTION_PARAMETER);
    return uriBuilder.toUriString();
  }

  private static class PersonOrigin {
    private final String originUrl;
    private final String personId;

    public PersonOrigin(String personId, String originUrl) {
      this.personId = personId;
      this.originUrl = originUrl;
    }

    public String getOriginUrl() {
      return this.originUrl;
    }

    public String getPersonId() {
      return this.personId;
    }
  }
}
