/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Management - InMemorySettingsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.index.settings.inmemory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import ch.unige.solidify.controller.IndexConfigurationController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexDefinition;
import ch.unige.solidify.model.index.IndexDefinitionList;
import ch.unige.solidify.model.index.IndexProperties;

@Profile("index-inmemory")
@Service
@ConditionalOnBean(IndexConfigurationController.class)
public class InMemorySettingsService extends IndexingSettingsService<IndexProperties> {
  private static final Logger log = LoggerFactory.getLogger(InMemorySettingsService.class);

  Map<String, IndexProperties> indexes = new HashMap<>();

  public InMemorySettingsService(IndexDefinitionList indexList) {
    this.init(indexList);
  }

  @Override
  public boolean delete(IndexProperties index) {
    if (!this.indexes.containsKey(index.getResId())) {
      return false;
    }
    return this.indexes.remove(index.getResId(), index);
  }

  @Override
  public List<IndexProperties> findAll(IndexProperties search) {
    final List<IndexProperties> list = new ArrayList<>();
    list.addAll(this.indexes.values());
    return list;
  }

  @Override
  public IndexProperties findOne(String index) {
    if (!this.indexes.containsKey(index)) {
      log.error("Could not find index ({})", index);
      return null;
    }
    return this.indexes.get(index);
  }

  @Override
  public void init(IndexDefinitionList indexList) {
    for (IndexDefinition index : indexList.getIndexList()) {
      this.createIndex(index.getIndexName());
    }
  }

  @Override
  public IndexProperties save(IndexProperties index) {
    if (!this.indexes.containsKey(index.getResId())) {
      this.indexes.put(index.getResId(), index);
      return index;
    } else {
      throw new SolidifyRuntimeException("Cannot create index (" + index + ")");
    }
  }

  @Override
  public IndexProperties update(IndexProperties t) {
    throw new UnsupportedOperationException("Index Setting update");
  }

  private void createIndex(String name) {
    if (this.findOne(name) == null) {
      final IndexProperties index = new IndexProperties();
      index.setResId(name);
      try {
        this.save(index);
        log.info("Index '{}' created", name);
      } catch (final SolidifyRuntimeException e) {
        final String message = "Cannot create '" + name + "' index";
        log.error(message);
        throw new SolidifyRuntimeException(message, e);
      }
    }
  }

}
