/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - ReflectionTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public class ReflectionTool {
  private static final Logger log = LoggerFactory.getLogger(ReflectionTool.class);

  public static String getSetterName(Object object, String propertyName) {

    try {
      Field field = ReflectionTool.getField(object, propertyName);

      Class<?> clazz = object.getClass();
      String setterName = "set" + propertyName.substring(0, 1).toUpperCase() + propertyName.substring(1);
      Method[] methods = clazz.getMethods();
      if (Arrays.stream(methods).anyMatch(method -> method.getName().equals(setterName)
              && Modifier.isPublic(method.getModifiers())
              && method.getReturnType().equals(void.class)
              && method.getParameterTypes().length == 1
              && method.getParameterTypes()[0].equals(field.getType()))) {
        return setterName;
      }
    } catch (NoSuchFieldException e) {
      log.warn("No setter name found for property '" + propertyName + "' on class '" + object.getClass().getSimpleName() + "'", e);
    }
    return null;
  }

  public static Method getSetter(Object object, String propertyName) {
    String setterName = ReflectionTool.getSetterName(object, propertyName);
    if (setterName != null) {
      Class<?> clazz = object.getClass();
      try {
        Field field = ReflectionTool.getField(object, propertyName);
        return clazz.getMethod(setterName, field.getType());
      } catch (NoSuchFieldException | NoSuchMethodException e) {
        log.warn("No setter method found for property '" + propertyName + "' on class '" + clazz.getSimpleName() + "'", e);
      }
    }
    return null;
  }

  public static void setValueWithSetter(Object object, String propertyName, Object value) {
    Method setterMethod = ReflectionTool.getSetter(object, propertyName);
    if (setterMethod != null) {
      try {
        setterMethod.invoke(object, value);
      } catch (IllegalAccessException | InvocationTargetException e) {
        log.warn("setter method cannot be called", e);
      }
    }
  }

  /**
   * Returns the first field in the hierarchy for the specified name
   */
  public static Field getField(Object object, String name) throws NoSuchFieldException {
    Class<?> clazz = object.getClass();
    Field field = null;

    while (clazz != null && field == null) {

      try {
        field = clazz.getDeclaredField(name);
      } catch (final NoSuchFieldException e) {
        log.debug("Field not found", e);
      }

      clazz = clazz.getSuperclass();
    }

    if (field == null) {
      throw new NoSuchFieldException(name);
    }

    return field;
  }

  public static String getMethodName(int stackTraceIndex) {
    return Thread.currentThread().getStackTrace()[stackTraceIndex].getMethodName();
  }

  /**
   * Return the value of the specified property of the specified object as a String
   *
   * @param object Object whose property is to be extracted
   * @param name Name of the property to be extracted
   * @return String The property's value, converted to a String
   * @throws NoSuchFieldException
   * @throws IllegalAccessException
   */
  public static String getProperty(Object object, String name)
          throws NoSuchFieldException, IllegalAccessException {
    final Field field = ReflectionTool.getField(object, name);
    field.setAccessible(true);
    final Object valueObj = field.get(object);
    return (valueObj != null) ? valueObj.toString() : null;
  }

  public static List<Class<?>> getParentClasses(Object object) {
    return ReflectionTool.getParentClasses(object, Object.class);
  }

  public static List<Class<?>> getParentClasses(Object object, Class<?> higherClass) {
    List<Class<?>> classes = new ArrayList<>();
    Class<?> clazz = object.getClass();
    while (clazz != null) {
      classes.add(clazz);
      clazz = clazz.getSuperclass();
      if (clazz == higherClass) {
        classes.add(clazz);
        return classes;
      }
    }
    throw new SolidifyRuntimeException(
            "higherClass '" + higherClass.getSimpleName() + "' not found in the object '" + object.getClass().getSimpleName()
                    + "' parent classes");
  }

  /**
   * Copy fields from an object to another
   *
   * @param targetObject The object to update
   * @param sourceObject The object from which fields must me copied
   * @param higherSourceClass sourceObject's fields are copied only if they are members of this class or below in the sourceObject class
   * hierarchy
   * @param <T>
   * @param <V>
   */
  public static <T, V> void copyFields(T targetObject, V sourceObject, Class<? extends Object> higherSourceClass) {
    final List<Class<?>> sourceClasses = ReflectionTool.getParentClasses(sourceObject, higherSourceClass);
    for (final Class<?> sourceClass : sourceClasses) {

      final Field[] sourceClassFields = sourceClass.getDeclaredFields();

      for (final Field sourceField : sourceClassFields) {
        sourceField.setAccessible(true);
        try {
          final Field targetField = ReflectionTool.getField(targetObject, sourceField.getName());
          if (!Modifier.isFinal(targetField.getModifiers())) {
            final Object fieldValue = sourceField.get(sourceObject);
            sourceField.set(targetObject, fieldValue);
          }
        } catch (final NoSuchFieldException e) {
          log.debug("Field " + sourceField.getName() + " from class " + sourceObject.getClass().getName()
                  + " is not present in target object of class " + targetObject.getClass().getName(), e);
        } catch (final IllegalAccessException | IllegalArgumentException e) {
          log.warn("Could not copy property '" + sourceField.getName() + "' from " + sourceObject.getClass().getName() + " to "
                  + targetObject.getClass().getName(), e);
        }
        sourceField.setAccessible(false);
      }
    }
  }

  /**
   * This constructor should not be called
   */
  private ReflectionTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
