/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID - OrcidWebsitesService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.solidify.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.controller.OrcidController;
import ch.unige.solidify.model.OrcidWebsite;
import ch.unige.solidify.model.OrcidWebsiteDTO;

@Service
@ConditionalOnBean(OrcidController.class)
public class OrcidWebsitesService {

  private static final Logger log = LoggerFactory.getLogger(OrcidWebsitesService.class);

  private final static String CACHE_NAME = "SolidifyOrcidExternalWebsites";

  private final List<OrcidWebsite> externalWebsites;

  public OrcidWebsitesService(SolidifyProperties solidifyProperties) {
    this.externalWebsites = List.of(solidifyProperties.getOrcid().getWebsites());
  }

  @Cacheable(CACHE_NAME)
  public List<OrcidWebsiteDTO> getAvailableWebsitesForOrcid(String orcid) {
    List<OrcidWebsiteDTO> availableOrcidWebsites = new ArrayList<>();
    for (OrcidWebsite orcidWebsite : this.externalWebsites) {
      OrcidWebsiteChecker checker = new OrcidUrlChecker(orcidWebsite.getCheckUrl());
      if (checker.checkOrcidExists(orcid)) {
        availableOrcidWebsites.add(new OrcidWebsiteDTO(orcidWebsite, orcid));
      }
    }
    return availableOrcidWebsites;
  }

  @Scheduled(fixedDelayString = "${solidify.orcid.externalWebsitesRefreshDelay:600000}")
  @CacheEvict(value = CACHE_NAME, allEntries = true)
  public void evictCache() {
    log.debug("All ORCID external websites evicted from cache");
  }
}
