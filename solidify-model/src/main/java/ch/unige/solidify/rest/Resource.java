/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - Resource.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.message.ResourceCacheMessage;

public abstract class Resource extends ResourceBase {

  public void addLinks(WebMvcLinkBuilder linkBuilder, boolean mainRes, boolean subResOnly) {
    this.add(linkBuilder.slash(this.getResId()).withSelfRel());
    if (!subResOnly) {
      this.add(
              (Tool.referenceLink(linkBuilder.slash(this.getResId()).toUriComponentsBuilder(), this.managedBy()))
                      .withSelfRel());
    }
    this.add(linkBuilder.withRel(ActionName.LIST));
    if (mainRes) {
      this.add((Tool.parentLink(linkBuilder.toUriComponentsBuilder())).withRel(ActionName.MODULE));
    } else {
      this.add((Tool.parentLink(linkBuilder.toUriComponentsBuilder())).withRel(ActionName.PARENT));
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof Resource resource)) {
      return false;
    }
    return super.equals(resource) && Objects.equals(this.getResId(), resource.getResId());
  }

  @JsonIgnore
  public CacheMessage getCacheMessage() {
    return new ResourceCacheMessage(this.getClass(), this.getResId());
  }

  @JsonIgnore
  public boolean sendCacheMessageOnCreation() {
    return false;
  }

  @JsonIgnore
  public URI getDownloadUri() {
    throw new UnsupportedOperationException();
  }

  @JsonIgnore
  public String getContentType() {
    throw new UnsupportedOperationException();
  }

  public abstract String getResId();

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.getResId());
  }

  public abstract void init();

  @JsonIgnore
  public boolean isDeletable() {
    return true;
  }

  @JsonIgnore
  public boolean isModifiable() {
    return true;
  }

  public abstract String managedBy();

  public abstract void setResId(String resId);

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + ": resId=" + this.getResId();
  }

  protected <V extends Resource> boolean exist(List<V> list, V item) {
    for (final V i : list) {
      if (i.getResId().equals(item.getResId())) {
        return true;
      }
    }
    return false;
  }

}
