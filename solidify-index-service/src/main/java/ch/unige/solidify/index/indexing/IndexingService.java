/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Service - IndexingService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.index.indexing;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.service.IndexResourceService;
import ch.unige.solidify.util.StringTool;

public abstract class IndexingService<T extends IndexMetadata> extends IndexResourceService<T> {

  private String indexPrefix;

  protected IndexingService(IndexConfigProperties indexConfigProperties) {
    this.indexPrefix = indexConfigProperties.getIndexPrefix();
  }

  protected abstract T getMetadataObject();

  protected void checkIndex() {
    for (String indexName : this.getIndexList())
      if (!indexName.startsWith(this.indexPrefix)) {
        throw new SolidifyCheckingException("Invalid index name '" + indexName + "' must start with '" + this.indexPrefix + "'");
      }
  }

  protected void checkIndex(T indexMetadata) {
    this.checkIndex();
    if (indexMetadata != null
            && !StringTool.isNullOrEmpty(indexMetadata.getIndex())
            && !this.getIndexList().contains(indexMetadata.getIndex())) {
      throw new SolidifyCheckingException("Wrong index name '" + indexMetadata.getIndex() + "' must be '" + this.getIndexName() + "'");
    }
  }
}
