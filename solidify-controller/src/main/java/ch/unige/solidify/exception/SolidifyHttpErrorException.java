/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SolidifyHttpErrorException.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.exception;

import java.io.Serial;
import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

public class SolidifyHttpErrorException extends SolidifyRuntimeException {
  @Serial
  private static final long serialVersionUID = -1095213850780070956L;
  private final SolidifyError solidifyError = new SolidifyError();

  public SolidifyHttpErrorException(HttpStatus httpStatus, String message) {
    super(message);
    this.solidifyError.setError(httpStatus.toString());
    this.solidifyError.setMessage(message);
    this.solidifyError.setStatus(httpStatus);
    this.solidifyError.setTimeStamp(LocalDateTime.now().toString());
  }

  public SolidifyError getSolidifyError() {
    return this.solidifyError;
  }
}
