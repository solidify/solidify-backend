/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify CSV - CSVToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.util.CSVTool;
import ch.unige.solidify.util.FileTool;

@ActiveProfiles({ "triplestore-inmemory", "sec-noauth" })
@ExtendWith(SpringExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CSVToolTest {

  protected Path sourceFolder = Paths.get("src", "test", "resources").toAbsolutePath();

  @Order(10)
  @ParameterizedTest
  @ValueSource(strings = {
          "addresses.csv",
          "addresses.tsv",
          "addresses-semicolon.csv",
          "cities.csv" })
  void wellformedTest(String filename) throws IOException {
    CSVTool.wellformed(this.getFile(filename));
  }

  @Order(20)
  @ParameterizedTest
  @ValueSource(strings = {
          "addresses-missing-column.csv",
          "addresses-empty-line.csv",
          "geneve.jpg",
          "MyUNIGE.pdf" })
  void notWellformedTest(String filename) throws IOException {
    assertThrows(SolidifyCheckingException.class, () -> CSVTool.wellformed(this.getFile(filename)));
  }

  private Path getFile(String filename) {
    final Path file = this.sourceFolder.resolve(filename);
    assertNotNull(file);
    assertTrue(FileTool.checkFile(file));
    return file;
  }
}
