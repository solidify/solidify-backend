/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAITool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import org.springframework.hateoas.Link;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.OAIConstants;

public class OAITool {
  public static Link oaiLink(UriComponentsBuilder ucb, String verb) {
    ucb.queryParam(OAIConstants.OAI_VERB, verb);
    return Link.of(ucb.toUriString());
  }

  public static Link oaiLink(UriComponentsBuilder ucb, String verb, String metadataPrefix) {
    ucb.queryParam(OAIConstants.OAI_VERB, verb);
    ucb.queryParam(OAIConstants.OAI_METADATAPREFIX, metadataPrefix);
    return Link.of(ucb.toUriString());
  }

}
