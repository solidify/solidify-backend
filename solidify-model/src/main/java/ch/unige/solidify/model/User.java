/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - User.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.util.Map;
import java.util.Set;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "The user information identify a physical person with an authentication.")
public interface User {

  Map<String, String> getAllProperties();

  @Schema(description = "The user email which must be a valid address.")
  String getEmail();

  @Schema(description = "The first name of the user.")
  String getFirstName();

  @Schema(description = "The home organiztion or institution of the user.")
  String getHomeOrganization();

  @Schema(description = "The last name of the user.")
  String getLastName();

  @Schema(description = "The roles of the user.")
  Set<String> getRoles();

  @Schema(description = "The unige ID of the user.")
  public String getUniqueId();
}
