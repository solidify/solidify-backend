/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - IIndividuRestService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.oracle.test.rest;

import org.springframework.stereotype.Service;
import org.springframework.web.client.ResponseErrorHandler;

import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.AbstractRestClientTool;

@Service
public class IIndividuRestService extends SolidifyRestClientService {

  public IIndividuRestService(AbstractRestClientTool restClientTool, ResponseErrorHandler responseErrorHandler) {
    super(restClientTool, responseErrorHandler);
  }
}
