/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIMetadataPrefixSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.model.oai.OAIMetadataPrefix;
import ch.unige.solidify.util.StringTool;

public class OAIMetadataPrefixSpecification extends SolidifySpecification<OAIMetadataPrefix> {

  @Serial
  private static final long serialVersionUID = -4072379104800932719L;

  public OAIMetadataPrefixSpecification(OAIMetadataPrefix criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<OAIMetadataPrefix> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {

    if (!StringTool.isNullOrEmpty(this.criteria.getPrefix())) {
      predicatesList.add(builder.like(root.get("prefix"), "%" + this.criteria.getPrefix() + "%"));
    }

    if (!StringTool.isNullOrEmpty(this.criteria.getName())) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }

    if (!StringTool.isNullOrEmpty(this.criteria.getDescription())) {
      predicatesList.add(builder.like(root.get("description"), "%" + this.criteria.getDescription() + "%"));
    }

    if (this.criteria.getEnabled() != null) {
      Predicate p = null;
      if (this.criteria.isEnabled()) {
        p = builder.isTrue(root.get("enabled"));
      } else {
        p = builder.isFalse(root.get("enabled"));
      }
      predicatesList.add(p);
    }

    if (this.criteria.getReference() != null) {
      Predicate p = null;
      if (this.criteria.isReference()) {
        p = builder.isTrue(root.get("reference"));
      } else {
        p = builder.isFalse(root.get("reference"));
      }
      predicatesList.add(p);
    }

  }
}
