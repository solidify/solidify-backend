/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - StringCipherToolsTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.util.StringCipherTools;

class StringCipherToolsTest {

  private static final int MAX_PLAIN_TEXT_LENGTH = 200;
  private static final int MIN_PLAIN_TEXT_LENGTH = 1;
  private static final int NB_TRIES = 10;

  private final CharsetDecoder charsetDecoder = StandardCharsets.UTF_8.newDecoder()
          .onMalformedInput(CodingErrorAction.IGNORE)
          .onUnmappableCharacter(CodingErrorAction.IGNORE);

  @Test
  void cipherTestWithGeneratedSalt() {
    for (int i = 0; i < NB_TRIES; i++) {
      final String plainText = this.generateRandomPlainText();
      final String password = this.generateRandomPassword();
      final String cipherText = StringCipherTools.encrypt(plainText, password, 8);
      final String decryptedText = StringCipherTools.decrypt(cipherText, password, 8);
      assertEquals(plainText, decryptedText, "Plain text (" + plainText + ") differs from decrypted text (" + decryptedText + ")");
      assertNotEquals(plainText, cipherText, "Plain text (" + plainText + ") equals cipher text (" + cipherText + ")");
    }
  }

  @Test
  void cipherTestWithPredefinedSalt() {
    for (int i = 0; i < NB_TRIES; i++) {
      final String plainText = this.generateRandomPlainText();
      final String password = this.generateRandomPassword();
      final byte[] salt = this.generateRandomSalt();
      final String cipherText = StringCipherTools.encrypt(plainText, password, salt);
      final String decryptedText = StringCipherTools.decrypt(cipherText, password, salt.length);
      assertEquals(plainText, decryptedText, "Plain text (" + plainText + ") differs from decrypted text (" + decryptedText + ")");
      assertNotEquals(plainText, cipherText, "Plain text (" + plainText + ") equals cipher text (" + cipherText + ")");
    }
  }

  @Test
  void undeterministCipher() {
    for (int i = 0; i < NB_TRIES; i++) {
      final String plainText = this.generateRandomPlainText();
      final byte[] salt = this.generateRandomSalt();
      final String password = this.generateRandomPassword();
      final String cipherText1 = StringCipherTools.encrypt(plainText, password, salt);
      final String cipherText2 = StringCipherTools.encrypt(plainText, password, salt);
      assertNotEquals(cipherText1, cipherText2, "Two encryption of the plain text (" + plainText + ") gives the same cipher");
    }
  }

  private String generateRandomPassword() {
    return this.generateRandomPlainText();
  }

  private String generateRandomPlainText() {
    final int plainTextLength = ThreadLocalRandom.current().nextInt(MIN_PLAIN_TEXT_LENGTH, MAX_PLAIN_TEXT_LENGTH + 1);
    final byte[] plainTextBytes = new byte[plainTextLength];
    String generatedText = null;
    while(generatedText == null) {
      new Random().nextBytes(plainTextBytes);
      charsetDecoder.reset();
      try {
        generatedText = charsetDecoder.decode(ByteBuffer.wrap(plainTextBytes)).toString();
      } catch (CharacterCodingException e) {
        continue;
      }
    }
    return generatedText;
  }

  private byte[] generateRandomSalt() {
    final byte[] saltBytes = new byte[8];
    new Random().nextBytes(saltBytes);
    return saltBytes;
  }
}
