/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify XML - XMLTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.xml.SimpleNamespaceContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyProcessingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public class XMLTool {

  private static final Logger log = LoggerFactory.getLogger(XMLTool.class);

  public static final String SCHEMA_FULL_VALIDATION = "http://apache.org/xml/features/validation/schema-full-checking";

  public static final String SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
  public static final String SCHEMA_PREFIX = "{XML Schema} ";
  public static final String SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";

  public static final String SCHEMA_VALIDATION = "http://apache.org/xml/features/validation/schema";
  public static final String XML_PREFIX = "{XML Document} ";
  public static final String XML_VALIDATION = "http://xml.org/sax/features/validation";
  public static final String XSL_PREFIX = "{XSL Transformation} ";

  /**
   * This constructor should not be called
   */
  private XMLTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  /********************************************************************************************
   * XSL TRANSFORMATION
   */

  public static void transform(Path xml, Path xsl) {
    transform(xml, xsl, getOutputFile(xml).toString());
  }

  public static void transform(Path xml, Path xsl, String newXml) {
    try {
      final StreamSource xmlSource = new StreamSource(Files.newInputStream(xml));
      final StreamSource xslSource = new StreamSource(Files.newInputStream(xsl));
      transform(xmlSource, xslSource, newXml);
    } catch (TransformerException | IOException e) {
      throw new SolidifyProcessingException(logTransformationError(xml.toString(), xsl.toString()), e);
    }
  }

  public static void transform(Path xml, String xsl) {
    transform(xml, xsl, getOutputFile(xml).toString());
  }

  public static void transform(Path xml, String xsl, String newXml) {
    try {
      final StreamSource xmlSource = new StreamSource(Files.newInputStream(xml));
      final StreamSource xslSource = new StreamSource(new StringReader(xsl));
      transform(xmlSource, xslSource, newXml);
    } catch (TransformerException | IOException e) {
      throw new SolidifyProcessingException(logTransformationError(xml.toString(), xsl), e);
    }
  }

  public static String transform(String xml, String xsl) {
    final StreamSource xmlSource = new StreamSource(new StringReader(xml));
    final StreamSource xslSource = new StreamSource(new StringReader(xsl));

    try {
      final Transformer transformer = getTransformer(xslSource);
      final StringWriter writer = new StringWriter();
      final StreamResult result = new StreamResult(writer);
      transformer.transform(xmlSource, result);
      return writer.toString();
    } catch (final TransformerException e) {
      throw new SolidifyProcessingException(logTransformationError(xml, xsl), e);
    }
  }

  public static String transform(String xml, String xsl, Map<String, String> parameters) {
    final StreamSource xmlSource = new StreamSource(new StringReader(xml));
    final StreamSource xslSource = new StreamSource(new StringReader(xsl));

    try {
      final Transformer transformer = getTransformer(xslSource);
      // Parameters
      for (final Map.Entry<String, String> entry : parameters.entrySet()) {
        transformer.setParameter(entry.getKey(), entry.getValue());
      }
      final StringWriter writer = new StringWriter();
      final StreamResult result = new StreamResult(writer);
      transformer.transform(xmlSource, result);
      return writer.toString();
    } catch (final TransformerException e) {
      throw new SolidifyProcessingException(logTransformationError(xml, xsl), e);
    }
  }

  /********************************************************************************************
   * VALIDATION
   */

  public static void validate(List<URL> xsds, Path xml) {
    validate(xsds, xml, true);
  }

  public static void validate(List<URL> xsds, Path xml, boolean domParser) {

    final SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    Source[] sources;
    Schema schema;

    // Schema factory
    try {
      sources = getSchemaSources(xsds);
      schemaFactory.setFeature(SCHEMA_FULL_VALIDATION, true);
      schemaFactory.setResourceResolver(new SchemaResourceResolver());
      schema = schemaFactory.newSchema(sources);
    } catch (IOException | SAXException e) {
      throw new SolidifyCheckingException(logValidationError(xml.toString(), null), e);
    }

    try {
      if (domParser) {
        parseWithDom(schema, xml);
      } else {
        parseWithSax(schema, xml);
      }
    } catch (ParserConfigurationException | SAXException | IOException e) {
      throw new SolidifyCheckingException("Error while parsing file " + xml + " with schemas " + xsds, e);
    }
  }

  public static void validate(String xsd, String xml) {

    DocumentBuilder parser;
    Document doc;

    try {
      // parse an XML document into a DOM tree
      parser = getDocumentBuilder();
      parser.setErrorHandler(new XMLError());

      doc = parser.parse(new InputSource(new StringReader(xml)));

      // create a SchemaFactory capable of understanding WXS schemas
      final SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

      // load a WXS schema, represented by a Schema instance
      final Schema schema = factory.newSchema(new SAXSource(new InputSource(new StringReader(xsd))));

      // create a Validator instance, which can be used to validate an
      // instance document
      final Validator validator = schema.newValidator();
      // Disable external resources
      validator.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
      validator.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

      // validate the DOM tree
      validator.validate(new DOMSource(doc));

    } catch (ParserConfigurationException | SAXException | IOException e) {
      throw new SolidifyCheckingException(e.getMessage(), e);
    }
  }

  public static void wellformed(Path xml) {
    try {
      wellformed(Files.newInputStream(xml));
    } catch (IOException e) {
      throw new SolidifyCheckingException("Error during the well-formedness verification", e);
    }
  }

  public static void wellformed(String xml) {
    wellformed(new ByteArrayInputStream(xml.getBytes()));
  }

  private static void wellformed(InputStream xmlStream) {

    DocumentBuilderFactory factory;
    try {
      factory = getDocumentBuilderFactory(false);
    } catch (final ParserConfigurationException e) {
      throw new SolidifyCheckingException("Error in DocumentBuilderFactory configuration", e);
    }

    DocumentBuilder parser;
    try {
      parser = getDocumentBuilder(factory);
      parser.setErrorHandler(new XMLError());
      parser.parse(new InputSource(new InputStreamReader(xmlStream, StandardCharsets.UTF_8)));
    } catch (ParserConfigurationException | SAXException | IOException e) {
      throw new SolidifyCheckingException("Error during the well-formedness verification", e);
    }
  }

  /********************************************************************************************
   * DOM
   */

  public static Document parseXML(String xml)
          throws ParserConfigurationException, SAXException, IOException {
    final DocumentBuilder builder = getDocumentBuilder();
    return builder.parse(new InputSource(new StringReader(xml)));
  }

  public static String documentToString(Document document) throws TransformerException {
    Transformer transformer = getTransformer();
    StringWriter sw = new StringWriter();
    transformer.transform(new DOMSource(document), new StreamResult(sw));
    return sw.toString();
  }

  public static Node addChildNode(Node parentNode, String nodeName) {
    return XMLTool.addChildNode(parentNode, nodeName, null);
  }

  public static Node addChildNode(Node parentNode, String nodeName, String textContent) {
    Node newNode;
    if (parentNode instanceof Document document) {
      newNode = document.createElement(nodeName);
    } else {
      newNode = parentNode.getOwnerDocument().createElement(nodeName);
    }
    if (textContent != null) {
      newNode.setTextContent(textContent);
    }
    return parentNode.appendChild(newNode);
  }

  public static Node addChildNodeNS(Node parentNode, String namespace, String nodeName, String textContent) {
    Node newNode = parentNode.getOwnerDocument().createElementNS(namespace, nodeName);
    newNode.setTextContent(textContent);
    return parentNode.appendChild(newNode);
  }

  /********************************************************************************************
   * XPATH
   */

  public static XPath buildXPath() {
    return XMLTool.buildXPath(new HashMap<>());
  }

  public static XPath buildXPath(XmlNamespacePrefixMapper namespacePrefixMapper) {
    return XMLTool.buildXPath(namespacePrefixMapper.getNamespacePrefixes());
  }

  public static XPath buildXPath(Map<String, String> namespacePrefixes) {
    final XPath xpath = XPathFactory.newInstance().newXPath();

    if (namespacePrefixes != null && !namespacePrefixes.isEmpty()) {
      final SimpleNamespaceContext nsc = new SimpleNamespaceContext();
      for (final Map.Entry<String, String> namespacePrefix : namespacePrefixes.entrySet()) {
        nsc.bindNamespaceUri(namespacePrefix.getKey(), namespacePrefix.getValue());
      }
      xpath.setNamespaceContext(nsc);
    }

    return xpath;
  }

  public static Node getFirstNode(Node node, String xpathQuery) {
    return XMLTool.getFirstNode(node, xpathQuery, new HashMap<>());
  }

  public static Node getFirstNode(Node node, String xpathQuery, Map<String, String> namespaces) {
    final XPath xpath = XMLTool.buildXPath(namespaces);
    try {
      return (Node) xpath.compile(xpathQuery).evaluate(node, XPathConstants.NODE);
    } catch (XPathExpressionException e) {
      throw new SolidifyRuntimeException("unable to get first xml node", e);
    }
  }

  public static String getFirstTextContent(Node node, String xpathQuery) {
    return XMLTool.getFirstTextContent(node, xpathQuery, new HashMap<>());
  }

  public static String getFirstTextContent(Node node, String xpathQuery, Map<String, String> namespaces) {
    Node foundNode = XMLTool.getFirstNode(node, xpathQuery, namespaces);
    if (foundNode == null) {
      return null;
    } else {
      return foundNode.getTextContent();
    }
  }

  public static String getFirstNodeInnerRawContent(Node node, String xpathQuery) {
    return XMLTool.getFirstNodeInnerRawContent(node, xpathQuery, false);
  }

  public static String getFirstNodeInnerRawContent(Node node, String xpathQuery, boolean keepIndentation) {
    return XMLTool.getFirstNodeInnerRawContent(node, xpathQuery, keepIndentation, new HashMap<>());
  }

  public static String getFirstNodeInnerRawContent(Node node, String xpathQuery, Map<String, String> namespaces) {
    return XMLTool.getFirstNodeInnerRawContent(node, xpathQuery, false, namespaces);
  }

  public static String getFirstNodeInnerRawContent(Node node, String xpathQuery, boolean keepIndentation, Map<String, String> namespaces) {
    Node foundNode = XMLTool.getFirstNode(node, xpathQuery, namespaces);
    if (foundNode == null) {
      return null;
    } else {
      NodeList nodeList = foundNode.getChildNodes();
      if (nodeList == null || nodeList.getLength() == 0) {
        return null;
      }
      try {
        return XMLTool.concatenateNodesRawContent(nodeList, keepIndentation);
      } catch (Exception e) {
        return null;
      }
    }
  }

  private static String concatenateNodesRawContent(NodeList nodeList, boolean keepIndentation) throws TransformerException {
    StringWriter sw = new StringWriter();
    for (int i = 0; i < nodeList.getLength(); i++) {
      Node node = nodeList.item(i);
      sw.append(XMLTool.nodeRawContentToString(node, keepIndentation));
    }
    return sw.toString().trim();
  }

  public static String getRawContent(Node node, String xpathQuery) {
    return XMLTool.getRawContent(node, xpathQuery, new HashMap<>());
  }

  public static String getRawContent(Node node, String xpathQuery, Map<String, String> namespaces) {
    Node foundNode = XMLTool.getFirstNode(node, xpathQuery, namespaces);
    if (foundNode == null) {
      return null;
    } else {
      try {
        return XMLTool.nodeRawContentToString(foundNode, false);
      } catch (Exception e) {
        return null;
      }
    }
  }

  private static String nodeRawContentToString(Node node, boolean keepIndentation) throws TransformerException {
    StringWriter sw = new StringWriter();

    Transformer t = getTransformerFactory().newTransformer();
    t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    t.setOutputProperty(OutputKeys.INDENT, (keepIndentation ? "yes" : "no"));
    t.transform(new DOMSource(node), new StreamResult(sw));

    String value = sw.toString();
    if (!keepIndentation) {
      value = value.trim();
    }
    return value;
  }

  /********************************************************************************************
   * MISC
   */

  public static JSONObject xml2Json(String xml) {
    return XML.toJSONObject(xml);
  }

  public static void xml2Stream(Marshaller m, OutputStream out, Object obj) throws JAXBException, TransformerException {
    final DOMResult domResult = new DOMResult();

    m.marshal(obj, domResult);

    final Transformer transformer = getTransformer();
    transformer.transform(new DOMSource(domResult.getNode()), new StreamResult(out));
  }

  public static Path getOutputFile(Path xml) {
    return xml.getParent().resolve(xml.getFileName().toString()
            .replaceFirst(SolidifyConstants.XML_EXT, SolidifyConstants.XML_OUTPUT_EXT));
  }

  /**
   * @throws TransformerConfigurationException
   ******************************************************************************************/
  private static TransformerFactory getTransformerFactory() throws TransformerConfigurationException {
    final TransformerFactory transformerFactory = TransformerFactory.newInstance();
    // Disable external resources
    transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
    transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
    // Secure processing
    transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
    return transformerFactory;
  }

  private static Transformer getTransformer() throws TransformerConfigurationException {
    final Transformer transformer = getTransformerFactory().newTransformer();
    enableIndentation(transformer);
    return transformer;
  }

  private static Transformer getTransformer(Source source) throws TransformerConfigurationException {
    final Transformer transformer = getTransformerFactory().newTransformer(source);
    enableIndentation(transformer);
    return transformer;
  }

  private static void enableIndentation(Transformer transformer) {
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
  }

  @SuppressWarnings("java:S2755")
  private static DocumentBuilderFactory getDocumentBuilderFactory(boolean validating) throws ParserConfigurationException {
    final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    setSecureParameters(factory);
    // Namespace awareness
    factory.setNamespaceAware(true);
    // Validation
    factory.setValidating(validating);
    return factory;
  }

  private static void setSecureParameters(DocumentBuilderFactory factory) throws ParserConfigurationException {
    // Disable external resources
    try {
      // to be compliant, completely disable DOCTYPE declaration:
      factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
      // or completely disable external entities declarations:
      factory.setFeature("http://xml.org/sax/features/external-general-entities", false);
      factory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
      // or prohibit the use of all protocols by external entities:
      factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
      factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
      // or disable entity expansion but keep in mind that this doesn't prevent fetching external entities
      // and this solution is not correct for OpenJDK < 13 due to a bug: https://bugs.openjdk.java.net/browse/JDK-8206132
      factory.setExpandEntityReferences(false);
    } catch (IllegalArgumentException e) {
      log.warn("Cannot disable external access: {}", e.getMessage());
    }
    // Secure processing
    factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
  }

  private static DocumentBuilder getDocumentBuilder(DocumentBuilderFactory factory) throws ParserConfigurationException {
    setSecureParameters(factory);
    return factory.newDocumentBuilder();
  }

  private static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
    return getDocumentBuilderFactory(false).newDocumentBuilder();
  }

  private static Source[] getSchemaSources(List<URL> xsds) throws IOException {
    final Source[] sources = new Source[xsds.size()];
    int i = 0;
    for (final URL xsd : xsds) {
      sources[i] = new StreamSource(xsd.openStream());
      i++;
    }
    return sources;
  }

  private static String logTransformationError(String xml, String xsl) {
    final StringBuilder builder = new StringBuilder();
    builder.append("Error while transforming file " + xml);
    builder.append(" with " + xsl);
    return builder.toString();
  }

  private static String logValidationError(String xml, String xsd) {
    final StringBuilder builder = new StringBuilder();
    builder.append("Error while validating file " + xml);
    if (xsd != null) {
      builder.append(" with " + xsd);
    }
    return builder.toString();
  }

  private static void parseWithDom(Schema schema, Path xml)
          throws ParserConfigurationException, SAXException, IOException {
    final DocumentBuilderFactory factory = getDocumentBuilderFactory(false);
    // Doc Factory
    factory.setSchema(schema);

    // Parse document
    final DocumentBuilder parser = getDocumentBuilder(factory);
    parser.setErrorHandler(new XMLError());
    parser.parse(Files.newInputStream(xml));
  }

  private static void parseWithSax(Schema schema, Path xml)
          throws ParserConfigurationException, SAXException, IOException {
    final SAXParserFactory factory = SAXParserFactory.newInstance();
    // Disable external resources
    factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
    // Doc Factory
    factory.setSchema(schema);
    factory.setValidating(false);
    factory.setNamespaceAware(true);

    // Parser
    final SAXParser parser = factory.newSAXParser();
    // Disable external resources
    parser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
    parser.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

    // Parse document
    final XMLReader reader = parser.getXMLReader();
    reader.setErrorHandler(new XMLError());
    reader.parse(new InputSource(Files.newInputStream(xml)));
  }

  private static void transform(StreamSource xmlSource, StreamSource xslSource, String newXml) throws TransformerException {
    final Transformer transformer = getTransformer(xslSource);
    final StreamResult xmlResult = new StreamResult(new File(newXml));
    xmlResult.setSystemId(java.net.URLDecoder.decode(xmlResult.getSystemId(), StandardCharsets.UTF_8));
    transformer.transform(xmlSource, xmlResult);
  }
}
