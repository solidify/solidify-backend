/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - AbstractCacheService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;

import ch.unige.solidify.rest.CacheNames;
import ch.unige.solidify.rest.CacheSubResourceContainer;
import ch.unige.solidify.rest.Resource;

public abstract class AbstractCacheService {

  private static final Logger logger = LoggerFactory.getLogger(AbstractCacheService.class);

  // Key : subResourceId, Value: the list of resources containing this subResource
  protected final Map<String, Set<String>> remoteResourceContainersMap = new HashMap<>();

  public Map<String, Set<String>> getRemoteResourceContainersMap() {
    return this.remoteResourceContainersMap;
  }

  /**
   * Fill the remoteResourceContainersMap that can then be used to clean resources stored in cache
   * with sub resources of containerResource recursively.
   *
   * @param containerResource
   */
  public void addSubResourcesIds(Resource containerResource) {
    this.addSubResourcesIds(containerResource, new HashSet<>());
  }

  /**
   * @param containerResource
   * @param treatedResourceIds used to prevent infinite loop during recursion
   */
  private void addSubResourcesIds(Resource containerResource, Set<String> treatedResourceIds) {
    String resourceId = containerResource.getResId();
    if (!treatedResourceIds.contains(resourceId)) {
      treatedResourceIds.add(resourceId);

      List<Resource> subResources = this.getSubResources(containerResource);

      List<String> subResourceIds = subResources.stream().map(Resource::getResId).toList();
      this.fillRemoteResourceContainersMap(resourceId, subResourceIds);

      // Repeat recursively for each sub resource
      for (Resource resource : subResources) {
        this.addSubResourcesIds(resource, treatedResourceIds);
      }

      // If the resource had already been cached before, remoteResourceContainersMap may contain obsolete information if its sub resources are not the same anymore
      // --> clean the remoteResourceContainersMap
      this.cleanFromContainersList(resourceId, subResourceIds);
    }
  }

  private void fillRemoteResourceContainersMap(String resourceId, List<String> subResourcesIds) {
    for (String subResourceId : subResourcesIds) {
      final Set<String> containersList = this.remoteResourceContainersMap.computeIfAbsent(subResourceId, key -> new HashSet<>());
      containersList.add(resourceId);
    }
  }

  /**
   * Check if containerId is declared as a container for a sub resource while it is not. This may happen if :
   * 1) A resource has been put in cache with its sub resource
   * 2) Then the resource is updated and a link to one of its sub resource is removed
   */
  private void cleanFromContainersList(String containerId, List<String> subResourceIds) {
    List<String> keysToRemove = new ArrayList<>();

    for (Map.Entry<String, Set<String>> entry : this.remoteResourceContainersMap.entrySet()) {
      String subResourceId = entry.getKey();
      Set<String> cachedContainerIds = entry.getValue();
      if (!subResourceIds.contains(subResourceId) && cachedContainerIds.contains(containerId)) {
        cachedContainerIds.remove(containerId);
        if (cachedContainerIds.isEmpty()) {
          keysToRemove.add(subResourceId);
        }
      }
    }

    // If remoteResourceContainersMap now contains entries with empty Set, they are not useful anymore -> remove them
    for (String keyToRemove : keysToRemove) {
      this.remoteResourceContainersMap.remove(keyToRemove);
    }
  }

  /**
   * <p>Return a list of resource ids that must be cleaned from cache when a resource is removed from cache.</p>
   * <p>The list is built recursively and contains resources that contain the given resource and their parents.</p>
   *
   * @param resourceId
   * @return
   */
  public Set<String> getResourceIdsToClean(String resourceId) {
    return this.getResourceIdsToClean(resourceId, new HashSet<>());
  }

  /**
   * @param resourceId
   * @param treatedResourceIds used to prevent infinite loop during recursion
   * @return
   */
  public Set<String> getResourceIdsToClean(String resourceId, Set<String> treatedResourceIds) {
    Set<String> resourceIdsToRemoveFromCache = new HashSet<>();
    resourceIdsToRemoveFromCache.add(resourceId);

    for (String containerId : this.remoteResourceContainersMap.getOrDefault(resourceId, Set.of())) {
      resourceIdsToRemoveFromCache.add(containerId);

      // Is the container itself in another container ?
      if (this.remoteResourceContainersMap.containsKey(containerId) && !treatedResourceIds.contains(containerId)) {
        treatedResourceIds.add(containerId);
        Set<String> parentContainersIds = this.getResourceIdsToClean(containerId, treatedResourceIds);
        resourceIdsToRemoveFromCache.addAll(parentContainersIds);
      }
    }

    return resourceIdsToRemoveFromCache;
  }

  /**
   * <p>Find Resources contained in the given containerResource.</p>
   * <p>If containerResource implements CacheSubResourceContainer, it uses its getSubResourcesForCache() method to return sub resources</p>
   * <p>Else it uses reflection to get sub resources</p>
   *
   * @param containerResource
   * @return
   */
  public List<Resource> getSubResources(Resource containerResource) {
    List<Resource> subResources;
    if (containerResource instanceof CacheSubResourceContainer cacheSubResourceContainer) {
      subResources = cacheSubResourceContainer.getSubResourcesForCache();
    } else {
      subResources = this.getResourcesFromPropertiesAndCollections(containerResource);
    }
    return subResources;
  }

  /**
   * <p>Find Resources contained in the given Resource.</p>
   * <p>It returns Resources that are container's properties and resources that are contained in container's collections properties.</p>
   * <p>Getting sub resources is done by using reflection. If reflection doesn't give correct results, resource may implement the CacheSubResourceContainer interface instead.</p>
   *
   * @param container
   * @return
   */
  public List<Resource> getResourcesFromPropertiesAndCollections(Resource container) {
    List<Resource> subResources = new ArrayList<>();
    for (Field field : container.getClass().getDeclaredFields()) {
      try {

        if (Resource.class.isAssignableFrom(field.getType())) {
          field.setAccessible(true);
          Resource subResource = (Resource) field.get(container);
          if (subResource != null) {
            subResources.add(subResource);
          }
        } else if (java.util.Collection.class.isAssignableFrom(field.getType())) {
          field.setAccessible(true);
          Collection<Resource> resourcesList = (Collection<Resource>) field.get(container);
          if (resourcesList != null && !resourcesList.isEmpty()) {
            // Try to cast first element into a Resource (work around for type erasure)
            Resource resource = resourcesList.iterator().next();
            subResources.addAll(resourcesList);
          }
        }

      } catch (IllegalAccessException e) {
        // do nothing
        logger.warn("Unable to get Resource from field '" + field.getName() + "' for class '" + container.getClass().getSimpleName() + "'", e);
      } catch (ClassCastException e) {
        // Collection is not a collection of Resource. Nothing to do.
      }
    }
    return subResources;
  }

  @CacheEvict(cacheNames = { CacheNames.RESOURCES })
  public void clearCachedResource(String resId) {
    AbstractCacheService.logger.trace("'{}' cache cleared for resId: {}", CacheNames.RESOURCES, resId);
  }
}
