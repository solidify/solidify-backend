/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - StringTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URISyntaxException;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.StringTool;

class StringTest {

  @Test
  void base64Test() {
    final String value = "Parameter with space and , and + and &";
    final String encodedValue = StringTool.encode64(value);
    final String encodedParam = SolidifyConstants.BASE64_PREFIX + encodedValue;

    assertEquals(value, StringTool.decode64(encodedValue));
    assertEquals(value, StringTool.decode64IfEncoded(encodedParam));
    assertEquals(value, StringTool.decode64IfEncoded(value));
  }

  @ParameterizedTest
  @MethodSource("urlList")
  void normalizeUriTest(String uri) throws URISyntaxException {
    final String normalizedUri = StringTool.normalizeUri(uri);
    assertFalse(normalizedUri.endsWith("/"));
  }

  @ParameterizedTest
  @MethodSource("urlList")
  void normalizeUrlTest(String url) throws URISyntaxException {
    final String normalizedUrl = StringTool.normalizeUrl(url);
    assertFalse(normalizedUrl.endsWith("/"));
  }

  @ParameterizedTest
  @MethodSource("urlList")
  void normalizeBaseUriTest(String uri) throws URISyntaxException {
    final String normalizedBaseUri = StringTool.normalizeBaseUri(uri);
    assertTrue(normalizedBaseUri.endsWith("/"));
  }

  @ParameterizedTest
  @MethodSource("urlList")
  void normalizeBaseUrlTest(String url) throws URISyntaxException {
    final String normalizedBaseUrl = StringTool.normalizeBaseUrl(url);
    assertTrue(normalizedBaseUrl.endsWith("/"));
  }

  @ParameterizedTest
  @MethodSource("wrongUriList")
  void normalizeWrongUriTest(String uri) {
    assertThrows(SolidifyRuntimeException.class, () -> StringTool.normalizeUri(uri));
    assertThrows(SolidifyRuntimeException.class, () -> StringTool.normalizeBaseUri(uri));
  }

  @ParameterizedTest
  @MethodSource("wrongUrlList")
  void normalizeWrongUrlTest(String url) {
    assertThrows(SolidifyRuntimeException.class, () -> StringTool.normalizeUrl(url));
    assertThrows(SolidifyRuntimeException.class, () -> StringTool.normalizeBaseUrl(url));
  }

  @ParameterizedTest
  @ValueSource(strings = { "2018-01-24", "2018-01-24T14:06:28Z", "2018-01-24T14:06:28.000Z" })
  void checkDateTest(String date) {
    assertTrue(StringTool.checkDate(date));
  }

  @ParameterizedTest
  @ValueSource(strings = { "2018-14-24T14:06:28.000Z", "2018", "abdce" })
  void checkWrongDateTest(String wrongDate) {
    assertFalse(StringTool.checkDate(wrongDate));
  }

  @Test
  void formatSmartSizeTest() {

    // @formatter:off
    final long b1       = 1;
    final long b10      = 10;
    final long b100     = 100;
    final long b1000    = 1000;

    final long kb1      = 1l    * 1024;
    final long kb10     = 10l   * 1024;
    final long kb100    = 100l  * 1024;
    final long kb1000   = 1000l * 1024;

    final long mega1    = 1l    * 1024 * 1024;
    final long mega10   = 10l   * 1024 * 1024;
    final long mega100  = 100l  * 1024 * 1024;
    final long mega1000 = 1000l * 1024 * 1024;

    final long gb1      = 1     * 1024 * 1024 * 1024;
    final long gb1_5    = 15    * 100  * 1024 * 1024;
    final long gb10     = 10l   * 1024 * 1024 * 1024;
    final long gb100    = 100l  * 1024 * 1024 * 1024;
    final long gb1000   = 1000l * 1024 * 1024 * 1024;

    final long tera1    = 1l    * 1024 * 1024 * 1024 * 1024;
    final long tera1_7  = 17l   * 100  * 1024 * 1024 * 1024;
    final long tera5    = 5l    * 1024 * 1024 * 1024 * 1024;
    final long tera10   = 10l   * 1024 * 1024 * 1024 * 1024;
    final long tera100  = 100l  * 1024 * 1024 * 1024 * 1024;
    final long tera1000 = 1000l * 1024 * 1024 * 1024 * 1024;
    // @formatter:on

    assertEquals("1.0 B", StringTool.formatSmartSize(b1));
    assertEquals("10.0 B", StringTool.formatSmartSize(b10));
    assertEquals("100.0 B", StringTool.formatSmartSize(b100));
    assertEquals("1000.0 B", StringTool.formatSmartSize(b1000));

    assertEquals("1.0 KB", StringTool.formatSmartSize(kb1));
    assertEquals("10.0 KB", StringTool.formatSmartSize(kb10));
    assertEquals("100.0 KB", StringTool.formatSmartSize(kb100));
    assertEquals("1000.0 KB", StringTool.formatSmartSize(kb1000));

    assertEquals("1.0 MB", StringTool.formatSmartSize(mega1));
    assertEquals("10.0 MB", StringTool.formatSmartSize(mega10));
    assertEquals("100.0 MB", StringTool.formatSmartSize(mega100));
    assertEquals("1000.0 MB", StringTool.formatSmartSize(mega1000));

    assertEquals("1.0 GB", StringTool.formatSmartSize(gb1));
    assertEquals("1.5 GB", StringTool.formatSmartSize(gb1_5));
    assertEquals("10.0 GB", StringTool.formatSmartSize(gb10));
    assertEquals("100.0 GB", StringTool.formatSmartSize(gb100));
    assertEquals("1000.0 GB", StringTool.formatSmartSize(gb1000));

    assertEquals("1.0 TB", StringTool.formatSmartSize(tera1));
    assertEquals("1.7 TB", StringTool.formatSmartSize(tera1_7));
    assertEquals("5.0 TB", StringTool.formatSmartSize(tera5));
    assertEquals("10.0 TB", StringTool.formatSmartSize(tera10));
    assertEquals("100.0 TB", StringTool.formatSmartSize(tera100));
    assertEquals("1000.0 TB", StringTool.formatSmartSize(tera1000));
  }

  @Test
  void getSizeFromSmartSizeTest() {

    // @formatter:off
    final long b1       = 1;
    final long b10      = 10;
    final long b100     = 100;
    final long b1000    = 1000;

    final long kb1      = 1l    * 1024;
    final long kb10     = 10l   * 1024;
    final long kb100    = 100l  * 1024;
    final long kb1000   = 1000l * 1024;

    final long mega1    = 1l    * 1024 * 1024;
    final long mega10   = 10l   * 1024 * 1024;
    final long mega100  = 100l  * 1024 * 1024;
    final long mega1000 = 1000l * 1024 * 1024;
    // @formatter:on

    assertEquals(b1, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(b1)));
    assertEquals(b10, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(b10)));
    assertEquals(b100, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(b100)));
    assertEquals(b1000, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(b1000)));

    assertEquals(kb1, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(kb1)));
    assertEquals(kb10, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(kb10)));
    assertEquals(kb100, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(kb100)));
    assertEquals(kb1000, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(kb1000)));

    assertEquals(mega1, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(mega1)));
    assertEquals(mega10, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(mega10)));
    assertEquals(mega100, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(mega100)));
    assertEquals(mega1000, StringTool.getSizeFromSmartSize(StringTool.formatSmartSize(mega1000)));
  }

  @Test
  void truncateTest() {

    // truncate if too long
    final String longStr = "long string that we need to truncate";
    String truncatedStr = StringTool.truncate(longStr, 20);
    assertEquals(20, truncatedStr.length());
    assertEquals(longStr.substring(0, 20), truncatedStr);

    // do not truncate if shorter
    truncatedStr = StringTool.truncate(longStr, 100);
    assertEquals(longStr, truncatedStr);

    // do not do anything if length is wrong
    truncatedStr = StringTool.truncate(longStr, -10);
    assertEquals(longStr, truncatedStr);
  }

  @Test
  void truncateWithCustomEllipsisTest() {

    final String longStr = "long string that we need to truncate";
    final String shortStr = "abcdef";
    final String ellipsis = "****";

    // truncate if too long
    String truncatedStr = StringTool.truncateWithEllipsis(longStr, 20, ellipsis);
    assertEquals(20, truncatedStr.length());
    assertNotEquals(longStr.substring(0, 20), truncatedStr);
    assertEquals(longStr.substring(0, 20 - ellipsis.length()) + ellipsis, truncatedStr);

    // do not truncate if shorter than allowed
    truncatedStr = StringTool.truncateWithEllipsis(longStr, 100, ellipsis);
    assertEquals(longStr, truncatedStr);

    // do not do anything if length is wrong
    truncatedStr = StringTool.truncateWithEllipsis(longStr, -10, ellipsis);
    assertEquals(longStr, truncatedStr);

    // truncate if too long and some original string is left
    truncatedStr = StringTool.truncateWithEllipsis(shortStr, 5, ellipsis);
    assertEquals("a" + ellipsis, truncatedStr);

    // do not use ellipsis if no original string would remain
    truncatedStr = StringTool.truncateWithEllipsis(shortStr, 3, ellipsis);
    assertEquals("abc", truncatedStr);

    // do not use ellipsis if no original string would remain
    truncatedStr = StringTool.truncateWithEllipsis(shortStr, 2, ellipsis);
    assertEquals("ab", truncatedStr);
  }

  @Test
  void truncateWithEllipsisTest() {

    final String longStr = "long string that we need to truncate";
    final String shortStr = "abcde";

    // truncate if too long
    String truncatedStr = StringTool.truncateWithEllipsis(longStr, 20);
    assertEquals(20, truncatedStr.length());
    assertNotEquals(longStr.substring(0, 20), truncatedStr);
    assertEquals(longStr.substring(0, 17) + "...", truncatedStr);

    // do not truncate if shorter than allowed
    truncatedStr = StringTool.truncateWithEllipsis(longStr, 100);
    assertEquals(longStr, truncatedStr);

    // do not do anything if length is wrong
    truncatedStr = StringTool.truncateWithEllipsis(longStr, -10);
    assertEquals(longStr, truncatedStr);

    // truncate if too long and some original string is left
    truncatedStr = StringTool.truncateWithEllipsis(shortStr, 4);
    assertEquals("a...", truncatedStr);

    // do not use ellipsis if no original string would remain
    truncatedStr = StringTool.truncateWithEllipsis(shortStr, 3);
    assertEquals("abc", truncatedStr);

    // do not use ellipsis if no original string would remain
    truncatedStr = StringTool.truncateWithEllipsis(shortStr, 2);
    assertEquals("ab", truncatedStr);
  }

  @Test
  void truncateOnSpaceWithEllipsis() {
    final String longStr = "long string that we need to truncate"; // 36 chars
    final String longStrWithTrailingSpace = "long string that we need to truncate                   "; // 55 chars
    final String longStrWithoutSpace = "loremipsumdoloresamenest"; // 24 chars
    final String shortStr = "abcde"; // 5 chars
    final String shortStrWithSpace = "a b c d e"; // 9 chars

    // nothing to truncate
    String truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStr, 40);
    assertEquals(longStr, truncatedStr);

    // max size is in a word
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStr, 34);
    assertTrue(truncatedStr.length() <= 34);
    assertEquals("long string that we need to...", truncatedStr);

    // "long string that we need to |truncate"
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStr, 28);
    assertTrue(truncatedStr.length() <= 28);
    assertEquals("long string that we need...", truncatedStr);

    // "long string that we need to| truncate"
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStr, 27);
    assertTrue(truncatedStr.length() <= 27);
    assertEquals("long string that we need...", truncatedStr);

    // "long string that we need t|o truncate"
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStr, 26);
    assertTrue(truncatedStr.length() <= 26);
    assertEquals("long string that we...", truncatedStr);

    // "long string that we need| to truncate"
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStr, 24);
    assertTrue(truncatedStr.length() <= 24);
    assertEquals("long string that we...", truncatedStr);

    // "long string that we nee|d to truncate"
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStr, 23);
    assertTrue(truncatedStr.length() <= 23);
    assertEquals("long string that we...", truncatedStr);

    // nothing to truncate
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStrWithTrailingSpace, 100);
    assertTrue(truncatedStr.length() <= 100);
    assertEquals(longStrWithTrailingSpace, truncatedStr);

    // max size is a space, but trailing spaces
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStrWithTrailingSpace, 50);
    assertTrue(truncatedStr.length() <= 50);
    assertEquals("long string that we need to truncate...", truncatedStr);

    // do not do anything if length is wrong
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStr, -10);
    assertEquals(longStr, truncatedStr);

    // do not cut on space if there isn't any left
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(longStrWithoutSpace, 15);
    assertEquals(15, truncatedStr.length());
    assertEquals("loremipsumdo...", truncatedStr);

    // do not use ellipsis if no original string would remain
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(shortStr, 3);
    assertEquals(3, truncatedStr.length());
    assertEquals("abc", truncatedStr);

    // do not use ellipsis if no original string would remain
    truncatedStr = StringTool.truncateOnSpaceWithEllipsis(shortStrWithSpace, 3);
    assertEquals(3, truncatedStr.length());
    assertEquals("a b", truncatedStr);
  }

  @Test
  void substringBetweenTest() {
    final String longStr = "a string from which [start]we want to extract[end] a substring";
    final String startStr = "[start]";
    final String endStr = "[end]";

    String subStr = StringTool.substringBetween(longStr, startStr, endStr);
    assertEquals("we want to extract", subStr);

    subStr = StringTool.substringBetween(longStr, startStr, "[xxx]");
    assertEquals("we want to extract[end] a substring", subStr);

    subStr = StringTool.substringBetween(longStr, endStr, startStr);
    assertEquals(" a substring", subStr);

    subStr = StringTool.substringBetween(longStr, "[xxx]", endStr);
    assertEquals(longStr, subStr);

    subStr = StringTool.substringBetween(longStr, "[xxx]", "[yyy]");
    assertEquals(longStr, subStr);
  }

  @Test
  void countSubstringOccurrences() {
    String sub = "zzz";
    assertEquals(0, StringTool.countSubstringOccurrences("Lorem ipsum dolor sit amet,  elit, sed do eiusmod", sub));
    assertEquals(1, StringTool.countSubstringOccurrences("Lorem sit " + sub + " amet, consectetur adipisicing elit,", sub));
    assertEquals(2, StringTool.countSubstringOccurrences("dolor sit " + sub + " amet, adipisi" + sub + "cing elit", sub));
    assertEquals(1, StringTool.countSubstringOccurrences(sub + "Lorem ipsum dolor sit amet, sed do eiusmod", sub));
    assertEquals(1, StringTool.countSubstringOccurrences("Lorem ipsum dolor sit amet,, sed do eiusmod" + sub, sub));
    assertEquals(3, StringTool.countSubstringOccurrences(sub + "Lorem " + sub + "sit amet" + "elit, " + sub + "sed do", sub));
    assertEquals(3, StringTool.countSubstringOccurrences("Lorem " + sub + " ipsum dolor " + sub + "sit amet, eiusmod" + sub, sub));
    assertEquals(4, StringTool.countSubstringOccurrences(sub + "Lorem " + sub + " ipsum dolor " + sub + "sit eiusmod" + sub, sub));
  }

  @Test
  void removeTrailingString() {
    String trailingDiv = "</div>";
    assertEquals("Some text", StringTool.removeTrailing("Some text</div>", trailingDiv));
    assertEquals("Some text</div>", StringTool.removeTrailing("Some text</div></div>", trailingDiv));
    assertEquals("</div>Some text</div>", StringTool.removeTrailing("</div>Some text</div></div>", trailingDiv));
    assertEquals("Some text</div> ", StringTool.removeTrailing("Some text</div> ", trailingDiv));
    assertEquals("</div>Some text", StringTool.removeTrailing("</div>Some text", trailingDiv));
    assertNull(StringTool.removeTrailing(null, trailingDiv));
    assertThrows(NullPointerException.class, () -> StringTool.removeTrailing("Some text", null));
  }

  @Test
  void removePrefixString() {
    String prefix = "prefix:";
    assertEquals("lorem ipsum", StringTool.removePrefix(prefix + "lorem ipsum", prefix));
    assertEquals("lorem " + prefix + " ipsum", StringTool.removePrefix("lorem " + prefix + " ipsum", prefix));
    assertEquals("lorem ipsum" + prefix, StringTool.removePrefix("lorem ipsum" + prefix, prefix));
    assertEquals("lorem ipsum" + prefix, StringTool.removePrefix(prefix + "lorem ipsum" + prefix, prefix));
    assertEquals("", StringTool.removePrefix("", prefix));
    assertEquals("lorem ipsum", StringTool.removePrefix("媒介语lorem ipsum", "媒介语"));

    assertNull(StringTool.removePrefix(null, prefix));
    assertThrows(NullPointerException.class, () -> StringTool.removeTrailing("lorem ipsum", null));
  }

  @Test
  void encodeNonAsciiCharsIntoHtmlHexadecimalEntities() {
    String str = "<a href=\"http://www.unige.ch\">媒介语 éà test # ; '</a>";
    String result = "<a href=\"http://www.unige.ch\">&#x5a92;&#x4ecb;&#x8bed; &#xe9;&#xe0; test # ; '</a>";

    assertNull(StringTool.encodeNonAsciiCharsIntoHtmlHexadecimalEntities(null));
    assertEquals("", StringTool.encodeNonAsciiCharsIntoHtmlHexadecimalEntities(""));
    assertEquals(result, StringTool.encodeNonAsciiCharsIntoHtmlHexadecimalEntities(str));
  }

  @Test
  void replaceIgnoreCase() {
    final String sourceText = "Une partie du texte doit être remplacée";
    final String sourceTextCase = "UNE partie du TEXTe doit être remPLACÉE";
    final String newText = "[replaced]";

    // text to replace found
    assertEquals("[replaced] du texte doit être remplacée", StringTool.replaceIgnoreCase(sourceText, "une partie", newText));
    assertEquals("[replaced] du texte doit être remplacée", StringTool.replaceIgnoreCase(sourceText, "Une partie", newText));
    assertEquals("[replaced] du texte doit être remplacée", StringTool.replaceIgnoreCase(sourceText, "UNE PArtie", newText));
    assertEquals("Une partie du [replaced] doit être remplacée", StringTool.replaceIgnoreCase(sourceText, "texte", newText));
    assertEquals("Une partie du [replaced] doit être remplacée", StringTool.replaceIgnoreCase(sourceText, "TEXTE", newText));
    assertEquals("Une partie du texte [replaced] remplacée", StringTool.replaceIgnoreCase(sourceText, "doit être", newText));
    assertEquals("Une partie du texte [replaced] remplacée", StringTool.replaceIgnoreCase(sourceText, "DOIT ÊTRE", newText));
    assertEquals("Une partie du texte doit être [replaced]", StringTool.replaceIgnoreCase(sourceText, "remplacée", newText));
    assertEquals("Une partie du texte doit être [replaced]", StringTool.replaceIgnoreCase(sourceText, "REMPLACÉE", newText));

    assertEquals("[replaced] du TEXTe doit être remPLACÉE", StringTool.replaceIgnoreCase(sourceTextCase, "une partie", newText));
    assertEquals("[replaced] du TEXTe doit être remPLACÉE", StringTool.replaceIgnoreCase(sourceTextCase, "Une partie", newText));
    assertEquals("[replaced] du TEXTe doit être remPLACÉE", StringTool.replaceIgnoreCase(sourceTextCase, "UNE PArtie", newText));
    assertEquals("UNE partie du [replaced] doit être remPLACÉE", StringTool.replaceIgnoreCase(sourceTextCase, "texte", newText));
    assertEquals("UNE partie du [replaced] doit être remPLACÉE", StringTool.replaceIgnoreCase(sourceTextCase, "TEXTE", newText));
    assertEquals("UNE partie du TEXTe [replaced] remPLACÉE", StringTool.replaceIgnoreCase(sourceTextCase, "doit être", newText));
    assertEquals("UNE partie du TEXTe [replaced] remPLACÉE", StringTool.replaceIgnoreCase(sourceTextCase, "DOIT ÊTRE", newText));
    assertEquals("UNE partie du TEXTe doit être [replaced]", StringTool.replaceIgnoreCase(sourceTextCase, "remplacée", newText));
    assertEquals("UNE partie du TEXTe doit être [replaced]", StringTool.replaceIgnoreCase(sourceTextCase, "REMPLACÉE", newText));

    // text to replace not found
    assertEquals(sourceText, StringTool.replaceIgnoreCase(sourceText, "word not found", newText));
    assertEquals(sourceText, StringTool.replaceIgnoreCase(sourceText, "remplacee", newText));
    assertEquals(sourceText, StringTool.replaceIgnoreCase(sourceText, "REMPLACEE", newText));
    assertEquals("", StringTool.replaceIgnoreCase("", "word not found", newText));
    assertEquals("Une partie du  doit être remplacée", StringTool.replaceIgnoreCase(sourceText, "texte", ""));

    // multiple replacements
    String sourceText2 = "Du texte avec plusieurs répétitions répétitions répétitions";
    assertEquals("Du texte avec plusieurs [replaced] [replaced] [replaced]", StringTool.replaceIgnoreCase(sourceText2, "répétitions", newText));
    assertEquals("Du texte avec plusieurs [replaced] [replaced] [replaced]", StringTool.replaceIgnoreCase(sourceText2, "RÉPÉTITIONS", newText));

    // Exceptions
    assertThrows(NullPointerException.class, () -> StringTool.replaceIgnoreCase(sourceText, "partie", null));
    assertThrows(NullPointerException.class, () -> StringTool.replaceIgnoreCase(sourceText, null, newText));
    assertThrows(NullPointerException.class, () -> StringTool.replaceIgnoreCase(null, "partie", newText));
  }

  @Test
  void findIgnoreCase() {
    final String sourceText = "Text that serves as a test for searching in";

    // Text found
    assertEquals("Text that serves", StringTool.findIgnoreCase(sourceText, "Text that serves"));
    assertEquals("Text that serves", StringTool.findIgnoreCase(sourceText, "Text that serves"));
    assertEquals("Text that serves", StringTool.findIgnoreCase(sourceText, "TEXT THAT SERVES"));
    assertEquals("searching in", StringTool.findIgnoreCase(sourceText, "searching in"));
    assertEquals("searching in", StringTool.findIgnoreCase(sourceText, "SEARCHING IN"));

    // Text not found
    assertEquals(null, StringTool.findIgnoreCase(sourceText, "not found"));
    assertEquals(null, StringTool.findIgnoreCase(sourceText, "searching in not found"));

    // Exceptions
    assertThrows(NullPointerException.class, () -> StringTool.findIgnoreCase(null, "text"));
    assertThrows(NullPointerException.class, () -> StringTool.findIgnoreCase(sourceText, null));
  }

  private static Stream<Arguments> urlList() {
    return Stream.of(
            Arguments.of("http://solidify.unige.ch"),
            Arguments.of("http://solidify.unige.ch "),
            Arguments.of("http://solidify.unige.ch/"),
            Arguments.of("http://solidify.unige.ch/ "),
            Arguments.of("https://solidify.unige.ch"),
            Arguments.of("https://solidify.unige.ch"),
            Arguments.of("file://solidify.unige.ch/file.jpg"),
            Arguments.of("file:///solidify.unige.ch/file.jpg"),
            Arguments.of("http://solidify.unige.ch/context"),
            Arguments.of("http://solidify.unige.ch/context/"));
  }

  private static Stream<Arguments> wrongUriList() {
    return Stream.of(
            null,
            Arguments.of(""),
            Arguments.of("http://solidify.unige.ch/file with space.jpg"),
            Arguments.of("http://solidify.unige.ch/sub folder/"),
            Arguments.of("file:///solidify.unige.ch/file with space.jpg"));
  }

  private static Stream<Arguments> wrongUrlList() {
    return Stream.of(
            null,
            Arguments.of(""),
            Arguments.of("htt://solidify.unige.ch"),
            Arguments.of("http//solidify.unige.ch/"),
            Arguments.of("http://solidify.unige.ch/file with space.jpg"),
            Arguments.of("solidify.unige.ch"),
            Arguments.of("solidify.unige.ch/context"),
            Arguments.of("file://solidify.unige.ch/file with space.jpg"),
            Arguments.of("http://solidify.unige.ch/file with space.jpg"));
  }

}
