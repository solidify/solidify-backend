/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - ResourceWithFileController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static ch.unige.solidify.SolidifyConstants.MIME_TYPE_PARAM;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.OffsetDateTime;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.ResourceFile;
import ch.unige.solidify.model.ResourceFileInterface;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.security.RootPermissions;

@RootPermissions
public abstract class ResourceWithFileController<T extends Resource & ResourceFileInterface> extends ResourceController<T> {

  @ResponseBody
  public HttpEntity<StreamingResponseBody> downloadFile(@PathVariable String id) {
    final T item = this.itemService.findOne(id);
    if (item.getResourceFile() == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // get content type from the byte[]
    InputStream is = new BufferedInputStream(new ByteArrayInputStream(item.getResourceFile().getFileContent()));
    String mimeType = item.getResourceFile().getMimeType();
    if(mimeType == null) {
      mimeType = MediaType.APPLICATION_OCTET_STREAM_VALUE;
    }

    return this.buildDownloadResponseEntity(is, item.getResourceFile().getFileName(), mimeType, item.getResourceFile().getFileSize());
  }

  public HttpEntity<T> uploadFile(@PathVariable String id, @RequestParam(SolidifyConstants.FILE_PARAM) MultipartFile file,
          @RequestParam(value = MIME_TYPE_PARAM, required = false) String mimeType) {
    T item = this.itemService.findOne(id);
    if (mimeType == null) {
      mimeType = file.getContentType();
    }
    try {
      // create or update the file
      item = this.updateOrCreateFile(item, file, mimeType);
      this.itemService.setAuthenticatedUserProperties(item.getResourceFile());
    } catch (final IOException e) {
      final String message = "Error uploading file " + file;
      throw new SolidifyRuntimeException(message, e);
    }

    this.itemService.save(item);
    return new ResponseEntity<>(item, HttpStatus.OK);
  }

  protected HttpEntity<T> createItemAndUploadFile(T item, MultipartFile file, String mimeType) {
    if (item.getResId() != null && this.itemService.existsById(item.getResId())) {
      throw new SolidifyRuntimeException(this.messageService.get("validation.resource.alreadyexist",
              new Object[] { item.getResId() }));
    }
    if (mimeType == null) {
       mimeType = file.getContentType();
    }
    try {
      // add file to Resource
      this.updateOrCreateFile(item, file, mimeType);
      this.itemService.setAuthenticatedUserProperties(item.getResourceFile());
    } catch (final IOException e) {
      final String message = "Error uploading file " + file;
      throw new SolidifyRuntimeException(message, e);
    }

    this.itemService.save(item);
    this.addLinks(item);
    return new ResponseEntity<>(item, HttpStatus.CREATED);
  }

  public HttpEntity<T> deleteFile(@PathVariable String id) {
    T item = this.itemService.findOne(id);
    item.setResourceFile(null);

    this.itemService.save(item);
    return new ResponseEntity<>(item, HttpStatus.OK);
  }


  private T updateOrCreateFile(T item, MultipartFile file, String mimeType) throws IOException {
    ResourceFile resourceFile = item.getResourceFile();
    if (resourceFile == null) {
      resourceFile = item.setNewResourceFile();
      resourceFile.getCreation().setWhen(OffsetDateTime.now());
    }
    resourceFile.getLastUpdate().setWhen(OffsetDateTime.now());
    resourceFile.setFileContent(file.getBytes());
    resourceFile.setFileSize(file.getSize());
    resourceFile.setFileName(file.getOriginalFilename());
    resourceFile.setMimeType(mimeType);
    return item;
  }

}
