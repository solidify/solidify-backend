/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - NoSqlResource.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

public abstract class NoSqlResource extends RepresentationModel<NoSqlResource> {
  private String resId;

  public void addLinks(WebMvcLinkBuilder linkBuilder) {
    this.add(linkBuilder.slash(this.getResId()).withSelfRel());
    this.add(linkBuilder.withRel(ActionName.LIST));
    this.add((Tool.parentLink(linkBuilder.toUriComponentsBuilder())).withRel(ActionName.MODULE));
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof NoSqlResource that)) {
      return false;
    }
    if (!super.equals(obj)) {
      return false;
    }
    return Objects.equals(this.resId, that.resId);
  }

  public String getResId() {
    return this.resId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.resId);
  }

  public void setResId(String resId) {
    this.resId = resId;
  }

  public abstract boolean update(NoSqlResource item);
}
