/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - JSONTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public class JSONTool {
  private static final Logger log = LoggerFactory.getLogger(JSONTool.class);
  private static final String INVALID_JSON_EXCEPTION_MESSAGE = "value is neither a valid JSONObject (%s)) nor a valid JSONArray (%s))";

  public static JSONObject convert2Json(Object object) {
    try {
      return new JSONObject(convert2JsonString(object));
    } catch (final JSONException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  public static JSONArray convert2JsonArray(Object object) {
    try {
      return new JSONArray(convert2JsonString(object));
    } catch (final JSONException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  public static String convert2JsonString(Object object) {
    try {
      final ObjectMapper mapper = new ObjectMapper();
      mapper.enable(SerializationFeature.INDENT_OUTPUT);
      mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
      final StringWriter sw = new StringWriter();
      mapper.writeValue(sw, object);
      return sw.toString();
    } catch (final IOException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  public static String convert2SortedJsonString(JSONObject json) {
    try {
      final ObjectMapper mapper = new ObjectMapper().copy()
              .configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true)
              .configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true)
              .configure(SerializationFeature.INDENT_OUTPUT, true);
      final Object sortedJson = mapper.readValue(json.toString(2), Object.class);
      return mapper.writeValueAsString(sortedJson);
    } catch (JSONException | IOException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  @SuppressWarnings("unchecked")
  public static Map<String, Object> convertJson2Map(String s) {
    try {
      return new ObjectMapper().readValue(s, HashMap.class);
    } catch (final IOException e) {
      log.error("Error while converting Json to Map {}", s);
    }
    return new HashMap<>();
  }

  public static void validate(Path schema, Path json) {
    try {
      validate(FileTool.toString(schema), FileTool.toString(json));
    } catch (final IOException e) {
      throw new SolidifyCheckingException(e.getMessage(), e);
    }
  }

  public static void validate(String schemaString, String jsonString) {
    try {
      final JSONObject jsonSchema = new JSONObject(schemaString);
      final JSONObject jsonObject = new JSONObject(jsonString);
      final Schema schema = SchemaLoader.load(jsonSchema);
      schema.validate(jsonObject);
    } catch (final ValidationException e) {
      throw new SolidifyCheckingException(e.getMessage(), e);
    }
  }

  public static void wellformed(Path json) {
    try {
      wellformed(FileTool.toString(json));
    } catch (final IOException e) {
      throw new SolidifyCheckingException(e.getMessage(), e);
    }
  }

  public static void wellformed(String json) {
    try {
      JSONTool.wellformedObject(json);
    } catch (SolidifyCheckingException objException) {
      try {
        JSONTool.wellformedArray(json);
      } catch (SolidifyCheckingException arrayException) {
        throw new SolidifyCheckingException(
                String.format(INVALID_JSON_EXCEPTION_MESSAGE, objException.getMessage(), arrayException.getMessage()));
      }
    }
  }

  public static void wellformedObject(String json) {
    try {
      new JSONObject(json);
    } catch (final JSONException e) {
      throw new SolidifyCheckingException(e.getMessage(), e);
    }
  }

  public static void wellformedArray(String json) {
    try {
      new JSONArray(json);
    } catch (final JSONException e) {
      throw new SolidifyCheckingException(e.getMessage(), e);
    }
  }

  /**
   * This constructor should not be called
   */
  private JSONTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
