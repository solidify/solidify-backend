/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "solidify.oai-pmh")
public class OAIProperties {
  private String publicUrl;
  private int pageSize = 100;
  private int tokenLifeTimeMinutes = 5;

  // Parameter that defines if metadata contents must be serialized with their XML namespaces
  private boolean serializeMetadataWithNamespaces = false;

  public String getPublicUrl() {
    return this.publicUrl;
  }

  public void setPublicUrl(String publicUrl) {
    this.publicUrl = publicUrl;
  }

  public int getPageSize() {
    return this.pageSize;
  }

  public void setPageSize(int pageSize) {
    this.pageSize = pageSize;
  }

  public int getTokenLifeTimeMinutes() {
    return this.tokenLifeTimeMinutes;
  }

  public void setTokenLifeTimeMinutes(int tokenLifeTimeMinutes) {
    this.tokenLifeTimeMinutes = tokenLifeTimeMinutes;
  }

  public boolean isSerializeMetadataWithNamespaces() {
    return this.serializeMetadataWithNamespaces;
  }

  public void setSerializeMetadataWithNamespaces(boolean serializeMetadataWithNamespaces) {
    this.serializeMetadataWithNamespaces = serializeMetadataWithNamespaces;
  }
}
