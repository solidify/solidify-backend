/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - WeakManyToManyConverter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.converter;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.AttributeConverter;

import ch.unige.solidify.util.StringTool;

public class WeakManyToManyConverter implements AttributeConverter<List<String>, String> {

  public static final String SEPARATOR = ";";

  @Override
  public String convertToDatabaseColumn(List<String> ids) {

    if (ids == null || ids.isEmpty()) {
      return null;
    } else {
      /*
       * Clean values to not store any eventual empty String or duplicate value
       */
      final List<String> cleanedValues = new ArrayList<>();
      for (final String id : ids) {
        if (!StringTool.isNullOrEmpty(id) && !cleanedValues.contains(id)) {
          cleanedValues.add(id);
        }
      }

      return String.join(SEPARATOR, cleanedValues);
    }
  }

  @Override
  public List<String> convertToEntityAttribute(String databaseValue) {

    if (StringTool.isNullOrEmpty(databaseValue)) {
      return new ArrayList<>();
    } else {

      final String[] ids = databaseValue.split(SEPARATOR);

      final List<String> list = new ArrayList<>();
      for (final String id : ids) {
        if (!StringTool.isNullOrEmpty(id) && !list.contains(id)) {
          list.add(id);
        }
      }

      return list;
    }

  }
}
