/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - AbstractResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import static ch.unige.solidify.util.StringTool.isNullOrEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.model.AuthApplicationRole;
import ch.unige.solidify.auth.model.UserInfo;
import ch.unige.solidify.rest.ResourceBase;

public abstract class AbstractResourceService {

  @Autowired
  private UserInfoService userInfoService;

  public void setChangeInfoFullName(ResourceBase resourceBase) {
    this.setChangeInfoFullName(resourceBase, ChangeInfoType.CREATION);
    this.setChangeInfoFullName(resourceBase, ChangeInfoType.LAST_UPDATE);
  }

  private enum ChangeInfoType {CREATION, LAST_UPDATE}

  private void setChangeInfoFullName(ResourceBase resourceBase, ChangeInfoType changeInfoType) {
    String externalUid = null;

    if (changeInfoType == ChangeInfoType.CREATION) {
      externalUid = resourceBase.getCreatedBy();
    } else if (changeInfoType == ChangeInfoType.LAST_UPDATE) {
      externalUid = resourceBase.getUpdatedBy();
    }

    if (externalUid != null) {
      UserInfo user = this.userInfoService.findByExternalUid(externalUid);

      if (user != null && user.getFullName() != null) {
        String userFullName = user.getFullName();
        if (changeInfoType == ChangeInfoType.CREATION) {
          resourceBase.setCreatorName(userFullName);
        } else if (changeInfoType == ChangeInfoType.LAST_UPDATE) {
          resourceBase.setEditorName(userFullName);
        }
      }
    }
  }

  public void setAuthenticatedUserProperties(ResourceBase resourceBase) {
    String externalUid = this.getAuthenticatedUserExternalUid();
    if (externalUid == null) {
      externalUid = SolidifyConstants.NO_CREDENTIAL;
    }
    if (this.isHumanUser() || isNullOrEmpty(resourceBase.getUpdatedBy())) {
      resourceBase.setUpdatedBy(externalUid);
    }
    if (isNullOrEmpty(resourceBase.getCreatedBy())) {
      resourceBase.setCreatedBy(externalUid);
    }
  }

  public String getAuthenticatedUserExternalUid() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
      return authentication.getName();
    } else {
      return null;
    }
  }

  public boolean isHumanUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null) {
      return false;
    }
    for (GrantedAuthority authority : authentication.getAuthorities()) {
      if (authority.getAuthority().equals(AuthApplicationRole.TRUSTED_CLIENT_ID) ||
              authority.getAuthority().equals(SolidifyConstants.MESSAGE_PROCESSOR_ROLE)) {
        return false;
      }
    }
    return true;
  }
}
