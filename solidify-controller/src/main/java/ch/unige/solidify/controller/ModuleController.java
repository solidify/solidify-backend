/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - ModuleController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import ch.unige.solidify.model.Module;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.service.MessageService;

public abstract class ModuleController {

  protected List<ControllerWithHateoasHome> resourceList;

  protected String root;
  private final String name;
  @Autowired
  MessageService messageService;

  protected ModuleController(String name) {
    this.name = name;
    this.root = name.toLowerCase();
    this.resourceList = new ArrayList<>();
  }

  public void addResource(ControllerWithHateoasHome r) {
    this.resourceList.add(r);
  }

  public String getName() {
    return this.name;
  }

  public String getRoot() {
    return this.root;
  }

  public List<ControllerWithHateoasHome> getResourceList() {
    return this.resourceList;
  }

  @GetMapping
  public HttpEntity<Module> home() {
    final Module mod = new Module(this.getName());
    mod.add(linkTo(methodOn(this.getClass()).home()).withSelfRel());
    for (final ControllerWithHateoasHome r : this.resourceList) {
      mod.add(linkTo(methodOn(r.getClass()).home()).withRel(ActionName.RESOURCES));
    }
    return new ResponseEntity<>(mod, HttpStatus.OK);
  }

}
