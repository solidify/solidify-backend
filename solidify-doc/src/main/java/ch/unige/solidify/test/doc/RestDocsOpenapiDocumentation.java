/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Documentation - RestDocsOpenapiDocumentation.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.doc;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.WebApplicationContext;

import ch.unige.solidify.test.exception.SolidifyTestRuntimeException;
import ch.unige.solidify.test.executor.SolidifyExecutorTestDoc;

/**
 * Generic Rest-Docs context configuration for Solidify applications
 */
@ExtendWith(RestDocumentationExtension.class)
public abstract class RestDocsOpenapiDocumentation {

  @Value("${server.servlet.context-path}")
  private String contextPath;

  private static final Logger log = LoggerFactory.getLogger(RestDocsOpenapiDocumentation.class);

  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  protected abstract String getFileName();

  protected List<String> getModuleList() {
    return Collections.emptyList();
  }

  /**
   * The main REST generate Openapi specification
   */
  @Test
  public void generateOpenapiDocs() {
    // All API
    this.generateOpenApiDoc(this.contextPath + "/api-docs", "target/openapi-spec/" + this.getFileName());
    // API by module
    for (String module : this.getModuleList()) {
      this.generateOpenApiDoc(this.contextPath + "/api-docs/" + module.toLowerCase(), "target/openapi-spec/" + this.getFileName(module));
    }
  }

  private String getFileName(String module) {
    String fileName = this.getFileName();
    int dot = fileName.lastIndexOf('.');
    return fileName.substring(0, dot) + "-" + module.toLowerCase() + fileName.substring(dot);
  }

  @BeforeEach
  public void setUp(RestDocumentationContextProvider restDocumentation) {
    this.mockMvc = MockMvcBuilders
            .webAppContextSetup(this.webApplicationContext)
            .defaultRequest(RestDocumentationRequestBuilders.get(this.contextPath).contextPath(this.contextPath))
            .apply(documentationConfiguration(restDocumentation)).build();
  }

  private void generateOpenApiDoc(String inputUrl, String outputFile) {
    this.createDirectory(outputFile);
    try (FileWriter fileWriter = new FileWriter(Paths.get(outputFile).toFile())) {
      final ResultActions result = SolidifyExecutorTestDoc.performRequest(this.mockMvc, this.getOpenapiRequestBuilder(inputUrl), RequestMethod.GET,
              MediaType.APPLICATION_JSON.toString());
      fileWriter.write(result.andReturn().getResponse().getContentAsString());
      log.info("Generating Openapi specifications [{}]", outputFile);
    } catch (Exception e) {
      throw new SolidifyTestRuntimeException("Fail to make OpenApi documentation", e);
    }

  }

  /**
   * Return a request builder for openapi spec
   *
   * @param url
   */
  private MockHttpServletRequestBuilder getOpenapiRequestBuilder(String url) {
    log.info("Building Openapi level request [{}]", url);
    final MockHttpServletRequestBuilder mockHttpServletRequestBuilder = RestDocumentationRequestBuilders.get(url).contentType(MediaType.APPLICATION_JSON);
    mockHttpServletRequestBuilder.contextPath(this.contextPath);
    return mockHttpServletRequestBuilder;
  }

  private void createDirectory(String outputFilePath) {
    final File directory = new File(this.getDirectoryPath(outputFilePath));
    if (!directory.exists()) {
      directory.mkdirs();
    }
  }

  private String getDirectoryPath(String outputFilePath) {
    if (outputFilePath.endsWith("/")) {
      return outputFilePath;
    }
    final int lastIndexOfSeparator = outputFilePath.lastIndexOf("/");
    return outputFilePath.substring(0, lastIndexOfSeparator);
  }

}
