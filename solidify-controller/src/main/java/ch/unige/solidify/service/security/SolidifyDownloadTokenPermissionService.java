/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SolidifyDownloadTokenPermissionService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service.security;

import ch.unige.solidify.model.security.DownloadToken;
import ch.unige.solidify.model.security.SolidifyDownloadTokenType;
import ch.unige.solidify.service.DownloadTokenService;

public abstract class SolidifyDownloadTokenPermissionService<T extends SolidifyDownloadTokenType> implements SolidifyPermissionService {
  private final DownloadTokenService downloadTokenService;

  protected SolidifyDownloadTokenPermissionService(DownloadTokenService downloadTokenService) {
    super();
    this.downloadTokenService = downloadTokenService;
  }

  public boolean isAllowed(String resourceId, T resourceType) {
    if (this.isRootOrTrustedRole()) {
      return true;
    }
    String tokenHash = this.downloadTokenService.getTokenHashFromRequest(resourceId, resourceType);
    if (tokenHash == null) {
      return false;
    }
    DownloadToken downloadToken = this.downloadTokenService.findByTokenHash(tokenHash);
    return downloadToken != null
            && downloadToken.getResourceType().equals(resourceType.getLabel())
            && downloadToken.getResourceId().equals(resourceId);
  }

}
