/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SolidifyApplicationRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import static ch.unige.solidify.SolidifyConstants.RES_ID_FIELD;
import static org.springframework.util.Assert.hasText;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.auth.model.ApplicationRole;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.rest.SolidifyModuleName;
import ch.unige.solidify.util.StringTool;

@Schema(description = "A role at the application level, one of USER, ADMIN, ROOT.")
@Table(name = "ApplicationRole")
@Entity
public class SolidifyApplicationRole extends ResourceNormalized implements ApplicationRole {

  private static final String LEVEL_FIELD = "level";

  @Schema(description = "The name of the application role.")
  @NotNull
  @Column(unique = true)
  private String name;

  @Schema(description = "The level of the application role. The lowest value has more privileges.")
  @NotNull
  @Column(unique = true)
  private int level;

  public SolidifyApplicationRole() {}

  public SolidifyApplicationRole(String name, int level) {
    hasText(name, "A role representation is required");
    this.setResId(name.toUpperCase());
    this.name = StringTool.toCamelCase(name);
    this.level = level;
  }

  public SolidifyApplicationRole(ApplicationRole applicationRole) {
    this.setResId(applicationRole.getResId());
    this.setName(applicationRole.getName());
    this.level = applicationRole.getLevel();
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public int getLevel() {
    return this.level;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return SolidifyModuleName.ADMIN;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("Application Role [name=").append(this.name).append("]").append("[resId=")
            .append(this.getResId()).append("]");
    return builder.toString();
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o)
      return true;
    if (!(o instanceof SolidifyApplicationRole that))
      return false;
    return this.getResId().equals(that.getResId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.name, this.level);
  }

  @Override
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(RES_ID_FIELD, LEVEL_FIELD);
  }
}
