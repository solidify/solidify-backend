/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - IndexFieldAlias.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.index;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.Label;
import ch.unige.solidify.model.LargeLabel;
import ch.unige.solidify.rest.ResourceNormalized;

/**
 *  Represents an index field alias which defines a facet/category for indexing and searching.
 *
 * <p>This class provides various metadata about the indexing field, such as its name, alias, field path,
 * and properties like whether it's a facet or system field. It also includes information about facets
 * like minimum count, limit, order, and default visible values in the user interface.</p>
 *
 * <p>The {@code IndexFieldAlias} class uses annotations from Jakarta Persistence API for ORM mapping
 * and from Jakarta Validation API for validation constraints.</p>
 */
@Schema(description = "A index field alias defines a facet/category for indexing and searching.")
@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "indexName", "alias" }) })
public class IndexFieldAlias extends ResourceNormalized {

  @Schema(description = "The name of index field alias.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String indexName;

  @Schema(description = "The name that will be exposed to clients (as a field or a facet.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String alias;

  @Schema(description = "The field path in the index")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  private String field;

  @Schema(description = "If the index field alias is a facet.")
  @NotNull
  private Boolean facet;

  @Schema(description = "If the index field alias is mandatory for the system.")
  @NotNull
  private Boolean system;

  @Schema(description = "The minimum number of occurrences of a facet value to be returned in facet results if the index Field alias is a facet.")
  private Integer facetMinCount;

  @Schema(description = "The maximum number of facets values to return if the index field alias is a facet.")
  private Integer facetLimit;

  @Schema(description = "The order determines the position of a facet in the user interface.")
  private Integer facetOrder;

  @Schema(description = "The number of facet values to display by default in the user interface.")
  private Integer facetDefaultVisibleValues;

  @Schema(description = "The translated labels by languages for the index field alias.")
  @Valid
  @ElementCollection
  @Column(name = IndexConstants.DB_INDEX_FIELD_ALIAS_ID)
  @CollectionTable(joinColumns = {
          @JoinColumn(name = IndexConstants.DB_INDEX_FIELD_ALIAS_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = {
          IndexConstants.DB_INDEX_FIELD_ALIAS_ID, SolidifyConstants.DB_LANGUAGE_ID }))
  private List<Label> labels = new ArrayList<>();

  @Schema(description = "The translated descriptions of the index field alias.")
  @ElementCollection
  @Column(name = IndexConstants.DB_INDEX_FIELD_ALIAS_ID)
  @CollectionTable(joinColumns = {
          @JoinColumn(name = IndexConstants.DB_INDEX_FIELD_ALIAS_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = {
          IndexConstants.DB_INDEX_FIELD_ALIAS_ID, SolidifyConstants.DB_LANGUAGE_ID }))
  private List<LargeLabel> descriptionLabels = new ArrayList<>();

  public String getIndexName() {
    return this.indexName;
  }

  public void setIndexName(String indexName) {
    this.indexName = indexName;
  }

  public String getAlias() {
    return this.alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getField() {
    return this.field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public Boolean isFacet() {
    return this.facet;
  }

  public void setFacet(Boolean facet) {
    this.facet = facet;
  }

  public Boolean isSystem() {
    return this.system;
  }

  public void setSystem(Boolean system) {
    this.system = system;
  }

  public Integer getFacetMinCount() {
    return this.facetMinCount;
  }

  public void setFacetMinCount(Integer facetMinCount) {
    this.facetMinCount = facetMinCount;
  }

  public Integer getFacetLimit() {
    return this.facetLimit;
  }

  public void setFacetLimit(Integer facetLimit) {
    this.facetLimit = facetLimit;
  }

  public Integer getFacetOrder() {
    return this.facetOrder;
  }

  public void setFacetOrder(Integer facetOrder) {
    this.facetOrder = facetOrder;
  }

  public Integer getFacetDefaultVisibleValues() {
    return this.facetDefaultVisibleValues;
  }

  public void setFacetDefaultVisibleValues(Integer facetDefaultVisibleValues) {
    this.facetDefaultVisibleValues = facetDefaultVisibleValues;
  }

  public List<Label> getLabels() {
    return this.labels;
  }

  public void setLabels(List<Label> labels) {
    this.labels = labels;
  }

  public List<LargeLabel> getDescriptionLabels() {
    return this.descriptionLabels;
  }

  public void setDescriptionLabels(List<LargeLabel> descriptionLabels) {
    this.descriptionLabels = descriptionLabels;
  }

  @Schema(description = "If the index field alias has a description.", accessMode = Schema.AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public Boolean isWithDescription() {
    return !this.getDescriptionLabels().isEmpty();
  }

  @Override
  public void init() {
    if (this.isFacet() == null) {
      this.setFacet(false);
    }
    if (this.isSystem() == null) {
      this.setSystem(false);
    }
  }

  @Override
  public String managedBy() {
    return IndexConstants.INDEX_MODULE;
  }

  @Override
  public boolean isDeletable() {
    return !this.isSystem();
  }

  @Override
  public boolean sendCacheMessageOnCreation() {
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result
            + Objects.hash(this.alias, this.facet, this.facetDefaultVisibleValues, this.facetLimit, this.facetMinCount, this.facetOrder,
            this.field, this.indexName, this.labels, this.descriptionLabels, this.system);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    IndexFieldAlias other = (IndexFieldAlias) obj;
    return Objects.equals(this.alias, other.alias) && Objects.equals(this.facet, other.facet)
            && Objects.equals(this.facetDefaultVisibleValues, other.facetDefaultVisibleValues)
            && Objects.equals(this.facetLimit, other.facetLimit)
            && Objects.equals(this.facetMinCount, other.facetMinCount) && Objects.equals(this.facetOrder, other.facetOrder)
            && Objects.equals(this.field, other.field) && Objects.equals(this.indexName, other.indexName)
            && Objects.equals(this.labels, other.labels)
            && Objects.equals(this.descriptionLabels, other.descriptionLabels)
            && Objects.equals(this.system, other.system);
  }

}
