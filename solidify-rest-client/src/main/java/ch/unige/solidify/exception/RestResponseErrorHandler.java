/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - RestResponseErrorHandler.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.exception;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.client.DefaultResponseErrorHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class RestResponseErrorHandler extends DefaultResponseErrorHandler {
  private static final Logger log = LoggerFactory.getLogger(RestResponseErrorHandler.class);

  @Override
  public void handleError(ClientHttpResponse response) throws IOException {
    final HttpStatus statusCode = HttpStatus.resolve(response.getRawStatusCode());
    if (statusCode == BAD_REQUEST) {
      final String body = this.getResponseBodyString(response);
      // second, try to convert to SolidifyRestError
      final SolidifyError error = this.convertResponseBody2Exception(body, SolidifyError.class);
      if (error != null) {
        throw new SolidifyRestException("Validation Exception", error);
      }
    }
    super.handleError(response);
  }

  protected <T> T convertResponseBody2Exception(String body, Class<T> clazz) {
    try {
      final ObjectMapper objectMapper = new ObjectMapper();
      return objectMapper.readValue(body, clazz);
    } catch (final Exception e) {
      log.error("Deserialization error: target class =" + clazz + "JsonString=" + body + "", e);
      return null;
    }

  }

  protected String getResponseBodyString(ClientHttpResponse response) {
    final byte[] bodyRawDate = this.getResponseBody(response);
    final Charset charset = this.getCharset(response);
    return new String(bodyRawDate, Objects.requireNonNullElse(charset, StandardCharsets.UTF_8));
  }
}
