/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - SearchRequest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import org.springframework.data.domain.Sort;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import ch.unige.solidify.rest.RestCollectionPage;

// Replaced by SearchCondition
@Deprecated(forRemoval = true)
public class SearchRequest {

  private String conditions;
  private String query;
  private int size = RestCollectionPage.DEFAULT_SIZE_PAGE;
  private int page = 0;

  @JsonSerialize(using = SortJsonSerializer.class)
  @JsonDeserialize(using = SortJsonDeserializer.class)
  private Sort sort;

  public SearchRequest() {

  }

  public SearchRequest(String conditions, String query, int size, int page, Sort sort) {
    this.conditions = conditions;
    this.query = query;
    this.size = size;
    this.page = page;
    if (sort != null && !sort.isEmpty() && sort.isSorted()) {
      this.sort = sort;
    }
  }

  public String getConditions() {
    return this.conditions;
  }

  public void setConditions(String conditions) {
    this.conditions = conditions;
  }

  public String getQuery() {
    return this.query;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public int getSize() {
    return this.size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public int getPage() {
    return this.page;
  }

  public void setPage(int page) {
    this.page = page;
  }

  public Sort getSort() {
    return this.sort;
  }

  public void setSort(Sort sort) {
    this.sort = sort;
  }

  @Override
  public String toString() {
    return "SearchRequest{" +
            "conditions='" + this.conditions + '\'' +
            ", query='" + this.query + '\'' +
            ", size=" + this.size +
            ", page=" + this.page +
            ", sort=" + this.sort.toString() +
            '}';
  }
}
