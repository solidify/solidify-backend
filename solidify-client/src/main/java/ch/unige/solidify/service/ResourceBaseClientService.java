/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Client - ResourceBaseClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URI;
import java.nio.file.Path;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.ReflectionTool;

public class ResourceBaseClientService<T> {
  private static final Logger logger = LoggerFactory.getLogger(ResourceBaseClientService.class);

  protected static final String SIZE_LABEL = ActionName.SIZE;
  protected static final String SIZE_PARAM = "?" + SIZE_LABEL + "=";

  private final String application;
  private final String module;
  private final String resourcePath;

  protected AbstractRestClientTool restClientTool;

  private final Environment env;
  private boolean isResourceServer = false;
  private final ObjectMapper mapper = new ObjectMapper();
  private final Class<T> resourceClass;

  public ResourceBaseClientService(
          String application,
          String module,
          String resourcePath,
          Class<T> resourceClass,
          AbstractRestClientTool restClientTool,
          Environment env) {
    this.application = application;
    this.module = module;
    this.resourcePath = resourcePath;
    this.resourceClass = resourceClass;
    this.restClientTool = restClientTool;
    this.env = env;
    this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    this.mapper.registerModule(new JavaTimeModule());
    this.mapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));
  }

  /**
   * Setter
   **/
  public void setIsResourceServer(boolean isResourceServer) {
    this.isResourceServer = isResourceServer;
  }

  /**
   * Getter
   **/
  protected String getResourceUrl() {
    return this.getBaseUrl() + "/" + this.resourcePath;
  }

  protected String getResourceUrl(int moduleIndex) {
    return this.getBaseUrl(moduleIndex) + "/" + this.resourcePath;
  }

  protected String getBaseUrl() {
    return this.getBaseUrl(0);
  }

  private String getBaseUrl(int moduleIndex) {
    String propertyName = this.getModuleUrlPropertyName();
    String url = this.env.getProperty(propertyName);
    if (url == null) {
      throw new IllegalStateException("Missing property: " + propertyName);
    }
    // Manage if there is a list of URLs
    if (url.contains(",")) {
      url = url.split(",")[moduleIndex];
    }
    if (this.isResourceServer) {
      final int index = url.lastIndexOf('/');
      url = url.substring(0, index + 1).concat(SolidifyConstants.RES_SRV);
    }
    return url;
  }

  public AbstractRestClientTool getRestClientTool() {
    return this.restClientTool;
  }

  protected Environment getEnv() {
    return this.env;
  }

  protected String getModule() {
    return this.module;
  }

  protected Class<T> getResourceClass() {
    return this.resourceClass;
  }

  protected String getModuleUrlPropertyName() {
    return this.application + ".module." + this.module + ".url";
  }

  /**
   * GET
   **/

  protected T getObjectFromUrl(String url) {
    return this.getObjectFromUrl(url, Collections.emptyMap(), this.getResourceClass());
  }

  protected T getObjectFromUrl(String url, Map<String, String> params) {
    return this.getObjectFromUrl(url, params, this.getResourceClass());
  }

  protected <V> V getObjectFromUrl(String url, Map<String, String> params, Class<V> clazz) {
    V result = null;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    try {
      final String jsonString = restTemplate.getForObject(url, String.class, params);
      if (jsonString == null) {
        return null;
      }
      result = this.mapper.readValue(jsonString, clazz);
    } catch (final IOException e) {
      logger.error("Error getting object from " + url, e);
    }
    return result;
  }

  /**
   * DOWNLOAD
   **/
  protected void download(String url, Path path) {
    this.getRestClientTool().downloadContent(URI.create(url), path, TokenUsage.WITH_TOKEN);
  }

  /**
   * UPLOAD
   **/
  protected <U> U uploadFile(String url, Resource file, Class<U> itemClass) {
    final MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<>();
    parameters.add(SolidifyConstants.FILE_PARAM, file);
    return this.uploadFile(url, file, itemClass, parameters);
  }

  protected <U> U uploadFile(String url, Resource file, Class<U> itemClass, MultiValueMap<String, Object> parameters) {
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    parameters.add(SolidifyConstants.FILE_PARAM, file);

    final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(parameters, headers);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(url, requestEntity, itemClass);
  }

  protected Map<String, Object> objectToMap(Object object, List<String> propertiesToMap) {
    if (object != null) {
      try {
        Map<String, Object> propertiesMap = new HashMap<>();
        for (String propertyName : propertiesToMap) {
          if (propertyName.contains(".")) {
            String parentPropertyName = propertyName.substring(0, propertyName.indexOf("."));
            String childPropertyName = propertyName.substring(propertyName.indexOf(".") + 1);

            Field field = ReflectionTool.getField(object, parentPropertyName);
            field.setAccessible(true);
            Object value = field.get(object);
            Map<String, Object> onePropertyMap = this.objectToMap(value, List.of(childPropertyName));

            if (!propertiesMap.containsKey(parentPropertyName)) {
              propertiesMap.put(parentPropertyName, new HashMap<String, Object>());
            }
            HashMap<String, Object> propertyMap = (HashMap<String, Object>) propertiesMap.get(parentPropertyName);
            propertyMap.putAll(onePropertyMap);
          } else {
            Field field = ReflectionTool.getField(object, propertyName);
            field.setAccessible(true);
            Object value = field.get(object);
            propertiesMap.put(propertyName, value);
          }
        }
        return propertiesMap;
      } catch (NoSuchFieldException | IllegalAccessException e) {
        throw new SolidifyRuntimeException("Cannot get a Map from the given object", e);
      }
    } else {
      throw new SolidifyRuntimeException("Cannot get a Map from a null object");
    }
  }
}
