/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SolidifyIdentifierType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "The identifier type identifies the type of unique identifier.")
public interface SolidifyIdentifierType {
  // Interface for list of identifier type

  @Schema(description = "The name of identifier type.")
  String getName();

  @Schema(description = "The description of identifier type.")
  String getDescription();

}
