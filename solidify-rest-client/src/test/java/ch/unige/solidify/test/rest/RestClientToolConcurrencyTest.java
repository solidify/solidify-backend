/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - RestClientToolConcurrencyTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.rest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.util.TrustedRestClientTool;

class RestClientToolConcurrencyTest {

  private static final int NB_THREADS = 100;
  private static final int MAX_WAIT_MS = 10;
  private static final String FAKE_TOKEN = "fake-token";


  @Test
  // Test that there is always a client associated to the TrustedRestClientTool even when parallel threads are setting a new token
  void multipleCallTest() throws ExecutionException, InterruptedException {
    final TrustedRestClientTool trustedRestClientTool = new TrustedRestClientTool();
    final ExecutorService executorService = Executors.newFixedThreadPool(NB_THREADS);
    Future<?> future = executorService.submit(() -> {
      randomWait();
      trustedRestClientTool.setTrustedToken(FAKE_TOKEN);
      randomWait();
      assertNotNull(trustedRestClientTool.getClient());
    });
    future.get(); // This will rethrow Exceptions and Errors as ExecutionException
  }

  private void randomWait() {
    try {
      TimeUnit.MILLISECONDS.sleep(ThreadLocalRandom.current().nextInt(MAX_WAIT_MS + 1));
    } catch (InterruptedException e) {
      Thread.currentThread().interrupt();  //set the flag back to rue
    }
  }

}
