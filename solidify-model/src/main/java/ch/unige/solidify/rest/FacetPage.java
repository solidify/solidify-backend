/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - FacetPage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public class FacetPage<T> extends PageImpl<T> {

  private final List<FacetResult> facetResults = new ArrayList<>();

  public FacetPage(List<T> content, List<FacetResult> facetResults, Pageable pageable, long total) {
    super(content, pageable, total);
    if (facetResults != null) {
      this.facetResults.addAll(facetResults);
    }
  }

  public FacetPage(List<T> content, Pageable pageable, long total) {
    super(content, pageable, total);
  }

  public FacetPage(List<T> content) {
    super(content);
  }

  public List<FacetResult> getFacetResults() {
    return this.facetResults;
  }
}
