/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Authorization Model - UserInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

/**
 * User interface of User entities implemented at project level
 */
@Schema(description = "The user information identify a physical person with an authentication.")
public interface UserInfo {

  @Schema(description = "The identifier of the resource. The default format is a Universally Unique IDentifier (UUID).")
  String getResId();

  @Schema(description = "The external unique ID of the user.")
  String getExternalUid();

  @Schema(description = "The application role of the user.")
  ApplicationRole getApplicationRole();

  @Schema(description = "The first name of the user.")
  String getFirstName();

  @Schema(description = "The last name of the user.")
  String getLastName();

  @Schema(description = "The full name of the user.", accessMode = AccessMode.READ_ONLY)
  String getFullName();

  @Schema(description = "The user email which must be a valid address.")
  String getEmail();

  @Schema(description = "If the user is enable.")
  boolean isEnabled();

  @Schema(description = "The linked person of the user.")
  PersonInfo getPerson();
}
