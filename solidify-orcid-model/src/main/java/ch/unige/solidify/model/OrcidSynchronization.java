/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID Model - OrcidSynchronization.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.solidify.model;

import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;
import static ch.unige.solidify.model.OrcidSynchronization.OBJECT_ID_DB_FIELD;
import static ch.unige.solidify.model.OrcidSynchronization.PERSON_ID_DB_FIELD;

import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "orcid_synchronization", uniqueConstraints = {
        @UniqueConstraint(columnNames = { OBJECT_ID_DB_FIELD, PERSON_ID_DB_FIELD, "put_code" }) })
public abstract class OrcidSynchronization extends ResourceNormalized {

  public static final String OBJECT_ID_DB_FIELD = "object_id";
  public static final String OBJECT_ID_FIELD = "objectId";
  public static final String PERSON_ID_DB_FIELD = "person_id";
  public static final String PERSON_ID_FIELD = "personId";

  @Schema(description = "The resId of the object synchronized with ORCID")
  @NotNull
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  protected String objectId;

  @Schema(description = "The identifier of the person for whom the object is synchronised")
  @NotNull
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  protected String personId;

  @Schema(description = "Date on which the item was exported to ORCID profile")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime uploadDate;

  @Schema(description = "Date on which the item was imported from ORCID profile")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  private OffsetDateTime downloadDate;

  @Schema(description = "The ORCID's work put code")
  @NotNull
  private BigInteger putCode;

  public abstract String getPersonId();

  public abstract String getObjectId();

  public OffsetDateTime getUploadDate() {
    return this.uploadDate;
  }

  public void setUploadDate(OffsetDateTime uploadDate) {
    this.uploadDate = uploadDate;
  }

  public OffsetDateTime getDownloadDate() {
    return this.downloadDate;
  }

  public void setDownloadDate(OffsetDateTime downloadDate) {
    this.downloadDate = downloadDate;
  }

  public BigInteger getPutCode() {
    return this.putCode;
  }

  public void setPutCode(BigInteger putCode) {
    this.putCode = putCode;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || this.getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    OrcidSynchronization that = (OrcidSynchronization) o;
    return Objects.equals(this.objectId, that.objectId) && Objects.equals(this.personId, that.personId) && Objects.equals(
            this.uploadDate, that.uploadDate) && Objects.equals(this.downloadDate, that.downloadDate) && Objects.equals(this.putCode,
            that.putCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.objectId, this.personId, this.uploadDate, this.downloadDate, this.putCode);
  }
}
