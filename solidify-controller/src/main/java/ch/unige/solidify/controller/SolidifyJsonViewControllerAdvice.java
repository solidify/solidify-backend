/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SolidifyJsonViewControllerAdvice.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static ch.unige.solidify.SolidifyConstants.USER_PROPERTY_FILTER_NAME;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractMappingJacksonResponseBodyAdvice;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import ch.unige.solidify.auth.model.RoleView;
import ch.unige.solidify.auth.service.ApplicationRoleListService;
import ch.unige.solidify.rest.UserPropertyFilter;

@RestControllerAdvice
public class SolidifyJsonViewControllerAdvice extends AbstractMappingJacksonResponseBodyAdvice implements ApplicationRoleListService {

  @Override
  protected void beforeBodyWriteInternal(MappingJacksonValue bodyContainer, MediaType contentType, MethodParameter returnType,
          ServerHttpRequest req, ServerHttpResponse res) {
    Class<?> jsonViewClass = null;

    if (this.isRootOrTrustedRole()) {
      jsonViewClass = RoleView.Root.class;
    } else if (this.isAdminRole()) {
      jsonViewClass = RoleView.Admin.class;
    } else if (this.isUserRole()) {
      jsonViewClass = RoleView.User.class;
    }
    if (jsonViewClass != null) {
      bodyContainer.setSerializationView(jsonViewClass);
    }
    final String externalUid = this.getExternalUid();
    final UserPropertyFilter userPropertyFilter = new UserPropertyFilter(externalUid, jsonViewClass);
    final FilterProvider filters = new SimpleFilterProvider().addFilter(USER_PROPERTY_FILTER_NAME, userPropertyFilter);
    bodyContainer.setFilters(filters);
  }

  private String getExternalUid() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
      return null;
    }
    return authentication.getName();
  }

}
