/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Authorization Client - TrustedTokenManager.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.client.service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.interceptor.HeaderRequestInterceptor;
import ch.unige.solidify.util.TrustedRestClientTool;

@Profile("!test & !sec-noauth & client-backend")
@Service
public class TrustedTokenManager {
  private static final Logger log = LoggerFactory.getLogger(TrustedTokenManager.class);
  private static class NoRedirectHttpRequestFactory extends SimpleClientHttpRequestFactory {
    @Override
    protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
      super.prepareConnection(connection, httpMethod);
      connection.setInstanceFollowRedirects(false);
    }
  }

  private static final String APPROVAL_PROMPT = "auto";

  private static final String AUTHORIZE_URI_TEMPLATE = "/oauth/authorize?state={STATE}" + "&scope=" + "&response_type={RESPONSE_TYPE}"
          + "&approval_prompt={APPROVAL_PROMPT}"
          + "&redirect_uri={REDIRECT_URI}" + "&client_id={CLIENT_ID}";
  private static final String GRANT_TYPE = "authorization_code";
  private static final String RESPONSE_TYPE = "code";
  private static final String STATE = UUID.randomUUID().toString();
  private static final String TOKEN_URI_TEMPLATE = "/oauth/token?code={CODE}" + "&state={STATE}" + "&grant_type={GRANT_TYPE}"
          + "&redirect_uri={REDIRECT_URI}";
  private final String authorizationUrl;
  private final String oAuth2ClientCredentials;
  private final String oAuth2ClientId;
  private final String redirectUri;
  private final RestTemplateBuilder restTemplateBuilder;
  private final String trustedUserCredentials;
  private final TrustedRestClientTool trustedRestClientTool;
  public TrustedTokenManager(AuthorizationClientProperties properties, TrustedRestClientTool trustedRestClientTool) {
    this.trustedRestClientTool = trustedRestClientTool;

    // OAuth2 client
    this.oAuth2ClientId = properties.getClientId();
    final String oAuth2ClientSecret = properties.getClientSecret();
    this.oAuth2ClientCredentials = Base64Utils.encodeToString((this.oAuth2ClientId + ":" + oAuth2ClientSecret).getBytes());
    this.redirectUri = properties.getRedirectUri();

    // Trusted basic user
    final String trustedUserName = properties.getTrustedUserName();
    final String trustedUserPassword = properties.getTrustedUserPassword();
    this.trustedUserCredentials = Base64Utils.encodeToString((trustedUserName + ":" + trustedUserPassword).getBytes());

    this.authorizationUrl = properties.getAuthorizationServerUrl();
    this.restTemplateBuilder = new RestTemplateBuilder().requestFactory(NoRedirectHttpRequestFactory.class);
  }

  private String getAuthorizationCode() {
    final RestTemplate restTemplate1 = this.restTemplateBuilder.build();
    restTemplate1.setInterceptors(List.of(new HeaderRequestInterceptor("Authorization", "Basic " + this.trustedUserCredentials)));
    final HttpEntity<String> response = restTemplate1
            .exchange(this.authorizationUrl + AUTHORIZE_URI_TEMPLATE, HttpMethod.GET, null, String.class, STATE, RESPONSE_TYPE, APPROVAL_PROMPT,
                    this.redirectUri, this.oAuth2ClientId);
    final List<String> location = response.getHeaders().get("Location");
    if (location != null && !location.isEmpty()) {
      final String clientRedirect = location.get(0);
      final int startAuthorizationCode = clientRedirect.indexOf('=') + 1;
      final int endAuthorizationCode = clientRedirect.indexOf('&');
      return clientRedirect.substring(startAuthorizationCode, endAuthorizationCode);
    } else {
      throw new SolidifyRuntimeException("No location header found");
    }
  }

  @Scheduled(fixedDelayString = "${auth.client.trustedTokenRefreshDelay:60000}")
  private void getTokens() {
    final String authorizationCode = this.getAuthorizationCode();
    final RestTemplate restTemplate = this.restTemplateBuilder.build();
    restTemplate.setInterceptors(List.of(new HeaderRequestInterceptor("Authorization", "Basic " + this.oAuth2ClientCredentials)));
    try {
      final String jsonString = restTemplate
              .postForObject(this.authorizationUrl + TOKEN_URI_TEMPLATE, null, String.class, authorizationCode, STATE, GRANT_TYPE,
                      this.redirectUri);
      this.updateTokens(jsonString);
    } catch (final HttpServerErrorException e) {
      log.error("Couldn't get tokens from Authorization server", e);
    }
  }

  private void updateTokens(String jsonString) {
    try {
      final JsonNode jwt = new ObjectMapper().readTree(jsonString);
      final String trustedAccessToken = jwt.get("access_token").textValue();
      this.trustedRestClientTool.setTrustedToken(trustedAccessToken);
    } catch (JsonProcessingException e) {
      log.error("Unable to update trusted token", e);
    }
  }

}
