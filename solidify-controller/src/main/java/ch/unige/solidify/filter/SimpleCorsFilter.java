/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SimpleCorsFilter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.filter;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.rest.ActionName;

/**
 * Spring OAUTH2 and CORS Configuration
 * https://medium.com/@muiruri/spring-oauth2-and-cors-configuration-3529337525b4
 */
@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SimpleCorsFilter implements Filter {

  private String allowedDomains;
  private String allowedHeaders;
  private String allowedMethods;

  public SimpleCorsFilter(SolidifyProperties config) {
    this.allowedDomains = config.getSecurity().getCors().getAllowedDomains();
    this.allowedHeaders = config.getSecurity().getCors().getAllowedHeaders();
    this.allowedMethods = config.getSecurity().getCors().getAllowedMethods();
  }

  @Override
  public void destroy() {
    // Nothing to do
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
    final HttpServletResponse response = (HttpServletResponse) res;
    final HttpServletRequest request = (HttpServletRequest) req;
    final String origin = request.getHeader("Origin");
    for (final String allowedOrigin : this.allowedDomains.split(" ")) {
      if (allowedOrigin.equals(origin)) {
        response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN, allowedOrigin);
      }
    }
    response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS, this.allowedMethods);
    response.setHeader(HttpHeaders.ACCESS_CONTROL_MAX_AGE, "3600");
    response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS, this.allowedHeaders);

    if (((HttpServletRequest) req).getRequestURL().toString().endsWith("/" + ActionName.DOWNLOAD_TOKEN)) {
      response.setHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");
    }

    if (HttpMethod.OPTIONS.name().equalsIgnoreCase(request.getMethod())) {
      response.setStatus(HttpServletResponse.SC_OK);
    } else {
      chain.doFilter(req, res);
    }
  }

  @Override
  public void init(FilterConfig filterConfig) {
    // Nothing to do
  }
}
