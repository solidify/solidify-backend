/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Remote Search - IndexFieldAliasRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service.rest.abstractservice;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.IndexCacheNames;
import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.dto.LabelDTO;
import ch.unige.solidify.model.Label;
import ch.unige.solidify.model.index.FacetProperties;
import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.IndexFieldAliasInfoProvider;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;

public abstract class IndexFieldAliasRemoteResourceService extends RemoteResourceService<IndexFieldAlias>
        implements IndexFieldAliasInfoProvider {

  private final String indexUrl;

  protected IndexFieldAliasRemoteResourceService(String rootContext, SolidifyRestClientService restClientService) {
    super(restClientService);
    this.indexUrl = rootContext + "/" + IndexConstants.INDEX_MODULE;
  }

  @Override
  protected Class<IndexFieldAlias> getResourceClass() {
    return IndexFieldAlias.class;
  }

  @Override
  protected String getResourceUrl() {
    return this.indexUrl + "/" + IndexConstants.INDEX_FIELD_ALIAS;
  }

  public List<FacetProperties> getFacetProperties(String index) {
    List<FacetProperties> list = new ArrayList<>();
    for (final IndexFieldAlias field : this
            .findAll(PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(IndexConstants.FACET_ORDER_FIELD))).getData()) {
      if (field.getIndexName().equals(index)) {
        FacetProperties facetProperties = new FacetProperties();
        facetProperties.setName(field.getAlias());
        facetProperties.setDefaultVisibleValues(field.getFacetDefaultVisibleValues());
        for (Label label : field.getLabels()) {
          String text = label.getText();
          if (StringTool.isNullOrEmpty(text)) {
            text = field.getAlias();
          }
          facetProperties.getLabels().add(new LabelDTO(text, label.getLanguage().getCode()));
        }
        list.add(facetProperties);
      }
    }
    return list;
  }

  @Override
  @Cacheable(IndexCacheNames.FACET_REQUESTS)
  public List<FacetRequest> getFacetRequestsListForIndex(String indexName) {
    String url = this.getResourceUrl() + "/" + ActionName.LIST_FACET_REQUESTS + "/" + indexName;
    return this.restClientService.getAllResources(url, FacetRequest.class);
  }

  @Cacheable(IndexCacheNames.INDEX_FIELD_ALIASES)
  public List<IndexFieldAlias> findByIndexName(String indexName) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    String url = this.getResourceUrl();
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    params.set("indexName", indexName);
    uriBuilder.queryParams(params);
    url = uriBuilder.toUriString();
    RestCollection<IndexFieldAlias> indexFieldAliasCollection = this.restClientService.getResourceList(url, IndexFieldAlias.class, pageable);
    return indexFieldAliasCollection.getData();
  }

  @Override
  public IndexFieldAlias findByIndexNameAndAlias(String indexName, String alias) {
    List<IndexFieldAlias> indexFieldAliasesForIndex = this.findByIndexName(indexName);
    return indexFieldAliasesForIndex.stream().filter(ifa -> ifa.getAlias().equals(alias)).findFirst().orElse(null);
  }

}
