/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - CommandTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.unige.solidify.SolidifyConstants;

public class CommandTool {

  private static final Logger log = LoggerFactory.getLogger(CommandTool.class);

  public static int run(String... commandWithParameters) throws IOException, InterruptedException {
    // Double check that commandWithParameters is sanitized (no injection is possible by user) when using this function
    if (log.isInfoEnabled()) {
      log.info("Command execution: {}", Arrays.deepToString(commandWithParameters));
      log.info("Working folder: {}", System.getProperty("user.dir"));
    }
    ProcessBuilder processBuilder = new ProcessBuilder(commandWithParameters);
    processBuilder.redirectErrorStream(true);
    Process process = processBuilder.start();
    logOutput(process.getInputStream());
    return process.waitFor();
  }

  public static int runWithWorkingFolder(String workingFolder, String... commandWithParameters) throws IOException, InterruptedException {
    // Double check that commandWithParameters is sanitized (no injection is possible by user) when using this function
    if (log.isInfoEnabled()) {
      log.info("Command execution: {}", Arrays.deepToString(commandWithParameters));
      log.info("Working folder: {}", workingFolder);
    }
    ProcessBuilder processBuilder = new ProcessBuilder(commandWithParameters);
    processBuilder.directory(new File(workingFolder));
    processBuilder.redirectErrorStream(true);
    Process process = processBuilder.start();
    logOutput(process.getInputStream());
    return process.waitFor();
  }

  private static void logOutput(InputStream inputStream) throws IOException {
    try (BufferedReader output = new BufferedReader(new InputStreamReader(inputStream))) {
      output.lines().forEach(log::info);
    }
  }

  /**
   * This constructor should not be called
   */
  private CommandTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}