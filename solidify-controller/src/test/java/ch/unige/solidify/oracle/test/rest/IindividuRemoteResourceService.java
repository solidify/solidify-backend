/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - IindividuRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.oracle.test.rest;

import org.springframework.stereotype.Service;

import ch.unige.solidify.oracle.test.dao.IindividuEntity;
import ch.unige.solidify.service.RemoteResourceService;
import ch.unige.solidify.service.SolidifyRestClientService;

@Service
public class IindividuRemoteResourceService extends RemoteResourceService<IindividuEntity> {

  public IindividuRemoteResourceService(SolidifyRestClientService restClientService) {
    super(restClientService);
  }

  @Override
  protected Class<IindividuEntity> getResourceClass() {
    return null;
  }
}
