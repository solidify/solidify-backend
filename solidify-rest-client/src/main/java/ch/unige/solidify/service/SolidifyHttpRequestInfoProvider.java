/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - SolidifyHttpRequestInfoProvider.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.security.Principal;
import java.text.ParseException;

import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.request.RequestContextHolder;

import com.nimbusds.jwt.SignedJWT;

@Profile({ "client-backend", "test" })
@Component
public class SolidifyHttpRequestInfoProvider implements HttpRequestInfoProvider {
  private static final Logger log = LoggerFactory.getLogger(SolidifyHttpRequestInfoProvider.class);

  private HttpServletRequest incomingRequest;

  public SolidifyHttpRequestInfoProvider(HttpServletRequest incomingRequest) {
    this.incomingRequest = incomingRequest;
  }

  @Override
  public MultiValueMap<String, String> getAuthorizationHeader() {
    final MultiValueMap<String, String> authorizationHeader = new LinkedMultiValueMap<>();
    authorizationHeader.add(HttpHeaders.AUTHORIZATION, "Bearer " + this.getIncomingToken());
    return authorizationHeader;
  }

  @Override
  public String getIncomingToken() {
    String token = null;
    if (RequestContextHolder.getRequestAttributes() != null) {
      log.debug("Request with attributes");
      if (this.incomingRequest.getHeader(HttpHeaders.AUTHORIZATION) != null) {
        token = this.incomingRequest.getHeader(HttpHeaders.AUTHORIZATION).substring("Bearer ".length());
        if (log.isDebugEnabled()) {
          try {
            log.debug("Authorization header present {}", SignedJWT.parse(token).getJWTClaimsSet());
          } catch (ParseException e) {
            log.debug("Invalid token, cannot parse token");
          }
        }
      } else {
        log.debug("Authorization header not present");
      }
    } else {
      log.debug("Request attributes is null");
    }
    return token;
  }

  @Override
  public Principal getPrincipal() {
    if (RequestContextHolder.getRequestAttributes() != null) {
      return this.incomingRequest.getUserPrincipal();
    }
    return null;
  }
}
