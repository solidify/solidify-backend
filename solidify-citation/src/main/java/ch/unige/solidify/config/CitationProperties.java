/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Citation - CitationProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "solidify.citation")
public class CitationProperties {

  private FormatConfig bibliographies = new FormatConfig();
  private FormatConfig citations = new FormatConfig();

  public FormatConfig getBibliographies() {
    return this.bibliographies;
  }

  public void setBibliographies(FormatConfig bibliographies) {
    this.bibliographies = bibliographies;
  }

  public FormatConfig getCitations() {
    return this.citations;
  }

  public void setCitations(FormatConfig citations) {
    this.citations = citations;
  }

  public static class FormatConfig {
    private String[] styles = {};
    private String[] languages = {};
    private String[] outputFormats = {};

    public String[] getStyles() {
      return this.styles;
    }

    public void setStyles(String[] styles) {
      this.styles = styles;
    }

    public String[] getLanguages() {
      return this.languages;
    }

    public void setLanguages(String[] languages) {
      this.languages = languages;
    }

    public String[] getOutputFormats() {
      return this.outputFormats;
    }

    public void setOutputFormats(String[] outputFormats) {
      this.outputFormats = outputFormats;
    }
  }
}
