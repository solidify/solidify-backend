/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - LogRequestFilter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.filter;

import java.io.IOException;
import java.text.ParseException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class LogRequestFilter implements Filter {
  private static final Logger log = LoggerFactory.getLogger(LogRequestFilter.class);

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
    final long startTime = System.currentTimeMillis();
    chain.doFilter(req, res);
    if (log.isDebugEnabled()) {
      final long elapsedTime = System.currentTimeMillis() - startTime;
      final HttpServletRequest request = (HttpServletRequest) req;
      final String authorizationHeader = request.getHeader("Authorization");
      final String headerXFF = request.getHeader("X-FORWARDED-FOR");
      final String ipAddress = headerXFF == null ? request.getRemoteAddr() : request.getRemoteAddr() + " XFF(" + headerXFF + ")";
      final String userId = this.getExternalUid(authorizationHeader);
      log.debug("Incoming REST call: [" + elapsedTime + "ms]" + " " + request.getMethod() + " "
              + this.getFullURL(request) + " - " + userId + " from " + ipAddress);
    }
  }

  private String getFullURL(HttpServletRequest request) {
    StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
    String queryString = request.getQueryString();

    if (queryString == null) {
      return requestURL.toString();
    } else {
      return requestURL.append('?').append(queryString).toString();
    }
  }

  private String getExternalUid(String authorizationHeader) {
    if (authorizationHeader != null) {
      try {
        final int tokenStart = authorizationHeader.indexOf(' ') + 1;
        final String token = authorizationHeader.substring(tokenStart);
        if (tokenStart == -1) {
          throw new ParseException("No space found", 0);
        }
        JWT jwtToken = JWTParser.parse(token);
        Object userClaim = jwtToken.getJWTClaimsSet().getClaim("user_name");
        if (userClaim instanceof String userClainString) {
          return userClainString;
        } else {
          throw new ParseException("User claim is not a String", 0);
        }
      } catch (ParseException e) {
        return "Bad token";
      }
    } else {
      return "No token";
    }
  }
}
