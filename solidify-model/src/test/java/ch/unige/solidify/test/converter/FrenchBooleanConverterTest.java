/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - FrenchBooleanConverterTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.converter.FrenchBooleanConverter;

class FrenchBooleanConverterTest {

  FrenchBooleanConverter testConv = new FrenchBooleanConverter();

  @Test
  void convertFalseToEntAtt_returnN() {
    assertEquals(false, this.testConv.convertToEntityAttribute("N"));
  }

  @Test
  void convertNToDBColumn_returnTrue() {
    assertEquals("N", this.testConv.convertToDatabaseColumn(false));
  }

  @Test
  void convertNullToDBColumn_returnNull() {
    assertEquals(null, this.testConv.convertToDatabaseColumn(null));
  }

  @Test
  void convertNullToEntAtt_returnNull() {
    assertEquals(null, this.testConv.convertToEntityAttribute(null));
  }

  @Test
  void convertOToDBColumn_returnTrue() {
    assertEquals("O", this.testConv.convertToDatabaseColumn(true));
  }

  @Test
  void convertTrueToEntAtt_returnO() {
    assertEquals(true, this.testConv.convertToEntityAttribute("O"));
  }
}
