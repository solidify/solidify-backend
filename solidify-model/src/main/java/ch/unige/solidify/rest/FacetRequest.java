/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - FacetRequest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.io.Serializable;

public class FacetRequest implements Serializable {

  /**
   * Facet name in search results
   */
  private String name;

  /**
   * Minimum number of occurrences to be returned as facet value
   */
  private Integer minCount;

  /**
   * Maximum number of facet values to return
   */
  private Integer limit;

  /**
   * Name of the facet index field
   */
  private String field;

  public FacetRequest() {
  }

  public FacetRequest(String name, Integer minCount, Integer limit, String field) {
    this.name = name;
    this.minCount = minCount;
    this.limit = limit;
    this.field = field;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getMinCount() {
    return this.minCount;
  }

  public void setMinCount(Integer minCount) {
    this.minCount = minCount;
  }

  public Integer getLimit() {
    return this.limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public String getField() {
    return this.field;
  }

  public void setField(String field) {
    this.field = field;
  }
}
