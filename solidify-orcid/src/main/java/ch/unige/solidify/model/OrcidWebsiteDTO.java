/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID - OrcidWebsiteDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.solidify.model;

import java.util.ArrayList;
import java.util.List;

public class OrcidWebsiteDTO {

  private List<I18nLink> i18nWebsites = new ArrayList<>();

  public OrcidWebsiteDTO(OrcidWebsite orcidWebsite, String orcid) {
    for (OrcidWebsiteLabel orcidWebsiteLabel : orcidWebsite.getLabels()) {
      String url = orcidWebsiteLabel.getUrl().replace("{orcid}", orcid);
      String languageCode = orcidWebsiteLabel.getLanguage().getCode();
      String text = orcidWebsiteLabel.getText();
      this.i18nWebsites.add(new I18nLink(url, languageCode, text));
    }
  }

  public List<I18nLink> getLinks() {
    return this.i18nWebsites;
  }

  private class I18nLink {
    private String url;
    private String languageCode;
    private String text;

    public I18nLink(String url, String languageCode, String text) {
      this.url = url;
      this.languageCode = languageCode;
      this.text = text;
    }

    public String getUrl() {
      return this.url;
    }

    public String getLanguageCode() {
      return this.languageCode;
    }

    public String getText() {
      return this.text;
    }
  }
}
