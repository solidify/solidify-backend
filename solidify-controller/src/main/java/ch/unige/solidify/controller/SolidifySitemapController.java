/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SolidifySitemapController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.xml.siteindex.v0_9.Sitemapindex;
import ch.unige.solidify.model.xml.siteindex.v0_9.TSitemap;
import ch.unige.solidify.model.xml.sitemap.v0_9.TChangeFreq;
import ch.unige.solidify.model.xml.sitemap.v0_9.TUrl;
import ch.unige.solidify.model.xml.sitemap.v0_9.Urlset;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.sitemap.SolidifySitemapService;
import ch.unige.solidify.util.StringTool;

public abstract class SolidifySitemapController {

  private static final Logger log = LoggerFactory.getLogger(SolidifySitemapController.class);

  private final boolean enable;

  private final int pageSize;

  private final String sitemapLocationPrefix;

  private final String portalUrl;

  private final List<SolidifySitemapService> sitemapServicesList = new ArrayList<>();

  protected SolidifySitemapController(SolidifyProperties solidifyProperties, String portalUrl) {
    this.enable = solidifyProperties.getSitemap().isEnable();
    this.pageSize = solidifyProperties.getSitemap().getPageSize();
    if (!StringTool.isNullOrEmpty(solidifyProperties.getSitemap().getLocationPrefix())) {
      this.sitemapLocationPrefix = solidifyProperties.getSitemap().getLocationPrefix();
    } else {
      this.sitemapLocationPrefix = "";
    }
    this.portalUrl = portalUrl;
  }

  protected List<SolidifySitemapService> getSitemapServiceList() {
    return this.sitemapServicesList;
  }

  protected abstract List<String> getExtraUrls();

  @EveryonePermissions
  @GetMapping(value = ActionName.SITEMAP_XML, produces = MediaType.APPLICATION_XML_VALUE)
  public HttpEntity<Sitemapindex> getSitemapIndex() {
    // Sitemap disable
    if (!this.enable) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    Sitemapindex sitemapIndex = new Sitemapindex();

    // List all pages of items
    this.getSitemapServiceList().forEach(sitemap -> {
      long total = sitemap.getItemsTotal();
      for (int i = 0; i < total; i = i + this.pageSize) {
        sitemapIndex.getSitemap().add(
                this.getNewSitemap(
                        this.sitemapLocationPrefix + "/" + ActionName.SITEMAP + "-" + sitemap.getName() + ActionName.XML_EXTENSION + "?"
                                + SolidifyConstants.FROM_PARAM + "="
                                + i));
      }
    });

    // Add extra links
    if (!this.getExtraUrls().isEmpty()) {
      sitemapIndex.getSitemap().add(
              this.getNewSitemap(this.sitemapLocationPrefix + "/" + ActionName.SITEMAP_XML + "?"
                      + SolidifyConstants.EXTRA_PARAM + "=" + SolidifyConstants.URL_PARAM));
    }
    return new ResponseEntity<>(sitemapIndex, HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping(value = ActionName.SITEMAP + "-{name}"
          + ActionName.XML_EXTENSION, params = SolidifyConstants.FROM_PARAM, produces = MediaType.APPLICATION_XML_VALUE)
  public HttpEntity<Urlset> getSitemap(@PathVariable String name, @RequestParam(name = SolidifyConstants.FROM_PARAM) String from) {
    if (!this.enable) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    int startItem;
    try {
      startItem = Integer.parseInt(from);
    } catch (NumberFormatException e) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    if (startItem < 0) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    final SolidifySitemapService sitemapService = this.getSitemapServiceList().stream().filter(s -> name.equals(s.getName()))
            .findFirst()
            .orElse(null);

    Urlset urlSet = new Urlset();
    if (sitemapService == null) {
      new ResponseEntity<>(HttpStatus.NOT_FOUND);
    } else {
      List<String> items = sitemapService.getItemsFrom(startItem);
      if (items.isEmpty()) {
        log.warn("No item for site map");
      }
      for (String item : items) {
        urlSet.getUrl().add(this.getNewUrl(this.portalUrl + "/" + item, TChangeFreq.NEVER));
      }
    }
    return new ResponseEntity<>(urlSet, HttpStatus.OK);
  }

  @EveryonePermissions
  @GetMapping(value = ActionName.SITEMAP_XML, params = SolidifyConstants.EXTRA_PARAM, produces = MediaType.APPLICATION_XML_VALUE)
  public HttpEntity<Urlset> getExtraUrls(@RequestParam(name = SolidifyConstants.EXTRA_PARAM) String extra) {
    // Sitemap disable
    if (!this.enable) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    // Check parameter
    if (!extra.equals(SolidifyConstants.URL_PARAM)) {
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    Urlset urlSet = new Urlset();

    List<String> urls = this.getExtraUrls();
    if (urls.isEmpty()) {
      log.warn("No extra URL");
    }
    for (String url : urls) {
      urlSet.getUrl().add(this.getNewUrl(this.sitemapLocationPrefix + "/" + url, TChangeFreq.NEVER));
    }

    return new ResponseEntity<>(urlSet, HttpStatus.OK);
  }

  private String getNowDateInString() {
    return LocalDateTime.now().format(DateTimeFormatter.ISO_DATE);
  }

  protected int getPageSize() {
    return this.pageSize;
  }

  private TSitemap getNewSitemap(String url) {
    TSitemap sitemap = new TSitemap();
    sitemap.setLoc(url);
    sitemap.setLastmod(this.getNowDateInString());
    return sitemap;
  }

  private TUrl getNewUrl(String urlString, TChangeFreq frequency) {
    TUrl url = new TUrl();
    url.setLoc(urlString);
    url.setLastmod(this.getNowDateInString());
    url.setChangefreq(frequency);
    url.setPriority(BigDecimal.ONE); // High priority
    return url;
  }
}
