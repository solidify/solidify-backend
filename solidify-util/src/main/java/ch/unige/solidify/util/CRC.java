/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - CRC.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.security.MessageDigest;
import java.util.zip.CRC32;

@SuppressWarnings("java:S2257")
public class CRC extends MessageDigest {

  private final CRC32 crcValue;

  public CRC() {
    super("CRC32");
    this.crcValue = new CRC32();
  }

  @Override
  protected byte[] engineDigest() {
    final long l = this.crcValue.getValue();
    final byte[] bytes = new byte[4];
    bytes[0] = (byte) ((l & 0xFF000000) >> 24);
    bytes[1] = (byte) ((l & 0x00FF0000) >> 16);
    bytes[2] = (byte) ((l & 0x0000FF00) >> 8);
    bytes[3] = (byte) (l & 0x000000FF);
    return bytes;
  }

  @Override
  protected void engineReset() {
    this.crcValue.reset();
  }

  @Override
  protected void engineUpdate(byte input) {
    this.crcValue.update(input);
  }

  @Override
  protected void engineUpdate(byte[] input, int offset, int len) {
    this.crcValue.update(input, offset, len);
  }

}
