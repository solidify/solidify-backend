/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - IndexFieldAliasSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.specification.SolidifySpecification;

public class IndexFieldAliasSpecification extends SolidifySpecification<IndexFieldAlias> {

  @Serial
  private static final long serialVersionUID = 6166149531574712100L;

  public IndexFieldAliasSpecification(IndexFieldAlias criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<IndexFieldAlias> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getIndexName() != null) {
      predicatesList.add(builder.equal(root.get("indexName"), this.criteria.getIndexName()));
    }

    if (this.criteria.getAlias() != null) {
      predicatesList.add(builder.like(root.get("alias"), "%" + this.criteria.getAlias() + "%"));
    }

    if (this.criteria.getField() != null) {
      predicatesList.add(builder.like(root.get("field"), "%" + this.criteria.getField() + "%"));
    }

    if (this.criteria.isFacet() != null) {
      predicatesList.add(builder.equal(root.get("facet"), this.criteria.isFacet()));
    }

    if (this.criteria.isSystem() != null) {
      predicatesList.add(builder.equal(root.get("system"), this.criteria.isSystem()));
    }

    if (this.criteria.getFacetMinCount() != null) {
      predicatesList.add(builder.equal(root.get("facetMinCount"), this.criteria.getFacetMinCount()));
    }

    if (this.criteria.getFacetLimit() != null) {
      predicatesList.add(builder.equal(root.get("facetLimit"), this.criteria.getFacetLimit()));
    }

    if (this.criteria.getFacetOrder() != null) {
      predicatesList.add(builder.equal(root.get("facetOrder"), this.criteria.getFacetOrder()));
    }

    if (this.criteria.getFacetDefaultVisibleValues() != null) {
      predicatesList.add(builder.equal(root.get("facetDefaultVisibleValues"), this.criteria.getFacetDefaultVisibleValues()));
    }
  }
}
