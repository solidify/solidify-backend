/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify XML - XMLTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.XMLTool;

@ExtendWith(SpringExtension.class)
class XMLTest {
  private enum XML {
    GOOD(1), GOOD_WO_SCHEMA(2), GOOD_WRONG_SCHEMA(3), NOT_VALID(4), NOT_WELLFORMED(5), SCHEMA(0), WITH_NAMESPACE(6), WITH_HTML(7);

    private final int value;

    private XML(int value) {
      this.value = value;
    }

    public int getValue() {
      return this.value;
    }
  }

  private static String DLCM = "dlcm/dlcm";
  private static final Logger log = LoggerFactory.getLogger(XMLTest.class);

  private static String PREFIX = "test/test";

  private final static String TEST_NAMESPACE = "http://www.unige.ch/test";

  @Test
  void invalidTestDLCM() throws SolidifyCheckingException {
    final List<Path> fileList = this.loadFilesTest(DLCM);
    final List<URL> xsdList = this.loadSchemaTest(DLCM);
    final Path fileNotValid = fileList.get(XML.NOT_VALID.getValue());
    final Path fileNotWellFormed = fileList.get(XML.NOT_WELLFORMED.getValue());

    assertThrows(SolidifyCheckingException.class, () -> XMLTool.validate(xsdList, fileNotValid));
    assertThrows(SolidifyCheckingException.class, () -> XMLTool.validate(xsdList, fileNotWellFormed));
  }

  @Test
  void noValidationTest() throws SolidifyCheckingException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    final List<URL> xsdList = this.loadSchemaTest(PREFIX);
    final Path fileNotValid = fileList.get(XML.NOT_VALID.getValue());
    final Path fileNotWellFormed = fileList.get(XML.NOT_WELLFORMED.getValue());
    assertThrows(SolidifyCheckingException.class, () -> XMLTool.validate(xsdList, fileNotValid));
    assertThrows(SolidifyCheckingException.class, () -> XMLTool.validate(xsdList, fileNotWellFormed));
  }

  @Test
  void noWellFormTest() throws SolidifyCheckingException {
    // Temporarily redirect output stream to avoid stack traces in build log
    final PrintStream systemOutStream = System.out;
    System.setOut(new java.io.PrintStream(new java.io.OutputStream() {
      @Override
      public void write(int i) {
      }
    }));
    final Path filePrefixNotWellFormed = this.loadFilesTest(PREFIX).get(XML.NOT_WELLFORMED.getValue());
    final Path fileDlcmNotWellFormed = this.loadFilesTest(DLCM).get(XML.NOT_WELLFORMED.getValue());

    assertThrows(SolidifyCheckingException.class, () -> XMLTool.wellformed(filePrefixNotWellFormed));
    assertThrows(SolidifyCheckingException.class, () -> XMLTool.wellformed(FileTool.toString(filePrefixNotWellFormed)));
    assertThrows(SolidifyCheckingException.class, () -> XMLTool.wellformed(fileDlcmNotWellFormed));
    assertThrows(SolidifyCheckingException.class, () -> XMLTool.wellformed(FileTool.toString(fileDlcmNotWellFormed)));
    System.setOut(systemOutStream);
  }

  @Test
  void transformStringTest() {
    try {
      final Path xmlSource = this.getPath("dlcm/dlcm_datacite-1.0.xml");
      final Path xslSource = this.getPath("xslt/datacite2oai_dc.xsl");
      final String xml = FileTool.toString(xmlSource);
      final String xsl = FileTool.toString(xslSource);
      final String newXml = xmlSource.getParent().toString() + File.separatorChar
              + xmlSource.getFileName().toString().replaceFirst(".xml", "-out.xml");

      XMLTool.wellformed(xmlSource);
      XMLTool.wellformed(xslSource);
      final String xmlResult = XMLTool.transform(xml, xsl);
      try (PrintWriter out = new PrintWriter(newXml)) {
        out.println(xmlResult);
      }
      XMLTool.wellformed(this.getPath(newXml));
      FileTool.deleteFile(Paths.get(newXml));
    } catch (RuntimeException | IOException e) {
      fail("Exception should not happen : ", e);
    }
  }

  @Test
  void transformStringWithParamTest() {
    try {
      final Path xmlSource = this.getPath("dlcm/dlcm_datacite-1.0.xml");
      final Path xslSource = this.getPath("xslt/datacite2oai_dc_w_param.xsl");
      final Map<String, String> params = new HashMap<>();
      params.put("param1", "value1");
      params.put("param2", "value2");
      params.put("param3", "value3");

      final String xml = FileTool.toString(xmlSource);
      final String xsl = FileTool.toString(xslSource);
      final String newXml = xmlSource.getParent().toString() + File.separatorChar
              + xmlSource.getFileName().toString().replaceFirst(".xml", "-out.xml");

      XMLTool.wellformed(xmlSource);
      XMLTool.wellformed(xslSource);
      final String xmlResult = XMLTool.transform(xml, xsl, params);
      try (PrintWriter out = new PrintWriter(newXml)) {
        out.println(xmlResult);
      }
      XMLTool.wellformed(this.getPath(newXml));
      FileTool.deleteFile(Paths.get(newXml));
    } catch (RuntimeException | IOException e) {
      fail("Exception should not happen : ", e);
    }
  }

  @Test
  void transformTest() {
    try {
      final Path xml = this.getPath("dlcm/dlcm_datacite-1.0.xml");
      final Path xsl = this.getPath("xslt/datacite2oai_dc.xsl");
      final String newXml = xml.getParent().toString() + File.separatorChar
              + xml.getFileName().toString().replaceFirst(".xml", "-out.xml");
      XMLTool.wellformed(xml);
      XMLTool.wellformed(xsl);
      XMLTool.transform(xml, xsl, newXml);
      XMLTool.wellformed(this.getPath(newXml));
      FileTool.deleteFile(Paths.get(newXml));
    } catch (RuntimeException | IOException e) {
      fail("Exception should not happen : ", e);
    }
  }

  @Test
  void validationTest() throws SolidifyCheckingException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    final List<URL> xsdList = this.loadSchemaTest(PREFIX);
    XMLTool.validate(xsdList, fileList.get(XML.GOOD.getValue()));
    XMLTool.validate(xsdList, fileList.get(XML.GOOD_WO_SCHEMA.getValue()));
    XMLTool.validate(xsdList, fileList.get(XML.GOOD_WRONG_SCHEMA.getValue()));
  }

  @Test
  void validationWithSaxTest() throws SolidifyCheckingException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    final List<URL> xsdList = this.loadSchemaTest(PREFIX);
    XMLTool.validate(xsdList, fileList.get(XML.GOOD.getValue()), false);
    XMLTool.validate(xsdList, fileList.get(XML.GOOD_WO_SCHEMA.getValue()), false);
    XMLTool.validate(xsdList, fileList.get(XML.GOOD_WRONG_SCHEMA.getValue()), false);
  }

  @Test
  void validationStringTest() throws SolidifyCheckingException, IOException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    final String xsd = FileTool.toString(new FileInputStream(fileList.get(XML.SCHEMA.getValue()).toString()));
    XMLTool.validate(xsd, FileTool.toString(new FileInputStream(fileList.get(XML.GOOD.getValue()).toString())));
    XMLTool.validate(xsd, FileTool.toString(new FileInputStream(fileList.get(XML.GOOD_WO_SCHEMA.getValue()).toString())));
    XMLTool.validate(xsd, FileTool.toString(new FileInputStream(fileList.get(XML.GOOD_WRONG_SCHEMA.getValue()).toString())));
  }

  @Disabled("Depending on external XML schemas")
  void validationTestStandards() throws IOException, SolidifyCheckingException {
    final String[] dirList = { "DataCite-AllIn1-4.0", "DataCite-4.0", "METS-1.11", "PREMIS-3.0" };
    int count = 0;
    for (final String dir : dirList) {
      log.info(dir);
      final List<Path> xmlList = new ArrayList<>();
      final List<URL> xsdList = new ArrayList<>();
      for (final Path p : FileTool.readFolder(this.getPath(dir))) {
        if (p.toString().endsWith(".xml")) {
          xmlList.add(p);
        }
      }
      for (final Path p : FileTool.readFolder(this.getPath(SolidifyConstants.SCHEMA_HOME))) {
        final String f = p.getFileName().toString();
        if (f.startsWith(dir) && f.endsWith(".xsd")) {
          log.info("*Schema:" + f);
          xsdList.add(p.toUri().toURL());
        }
      }

      for (final Path xml : xmlList) {
        count++;
        log.info("-" + xml.getFileName());
        XMLTool.validate(xsdList, xml);
      }
    }
    log.info(count + " executed tests");
  }

  // @Test
  void valideTestDLCM() throws IOException, SolidifyCheckingException {
    final List<Path> fileList = this.loadFilesTest(DLCM);
    final List<URL> xsdList = this.loadSchemaTest(DLCM);

    XMLTool.validate(xsdList, fileList.get(XML.GOOD.getValue()));
    XMLTool.validate(xsdList, this.getPath("dlcm/dlcm.xml"));
    XMLTool.validate(xsdList, this.getPath("dlcm/dlcm_datacite.xml"));
    XMLTool.validate(xsdList, fileList.get(XML.GOOD.getValue()));
    XMLTool.validate(xsdList, fileList.get(XML.GOOD_WO_SCHEMA.getValue()));
    XMLTool.validate(xsdList, fileList.get(XML.GOOD_WRONG_SCHEMA.getValue()));
  }

  @Test
  void wellFormedForPathTest() throws SolidifyCheckingException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    XMLTool.wellformed(fileList.get(XML.SCHEMA.getValue()));
    XMLTool.wellformed(fileList.get(XML.GOOD.getValue()));
    XMLTool.wellformed(fileList.get(XML.NOT_VALID.getValue()));
  }

  @Test
  void wellFormedForStringTest() throws IOException, SolidifyCheckingException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    XMLTool.wellformed(FileTool.toString(fileList.get(XML.SCHEMA.getValue())));
    XMLTool.wellformed(FileTool.toString(fileList.get(XML.GOOD.getValue())));
    XMLTool.wellformed(FileTool.toString(fileList.get(XML.NOT_VALID.getValue())));
  }

  @Test
  void wellformedTestDLCM() throws SolidifyCheckingException {
    final List<Path> fileList = this.loadFilesTest(DLCM);
    XMLTool.wellformed(fileList.get(XML.SCHEMA.getValue()));
    XMLTool.wellformed(fileList.get(XML.SCHEMA.getValue()));
    XMLTool.wellformed(fileList.get(XML.GOOD.getValue()));
    XMLTool.wellformed(fileList.get(XML.NOT_VALID.getValue()));
  }

  @Test
  void wellformedTestDLCMSchemas() throws URISyntaxException, SolidifyCheckingException {
    final List<URL> xsdList = this.loadSchemaTest(DLCM);
    for (final URL xsd : xsdList) {
      XMLTool.wellformed(Paths.get(xsd.toURI()));
    }
  }

  @Test
  void getFirstNodeWithoutNamespaceTest() throws IOException, ParserConfigurationException, SAXException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    Path xmlPath = fileList.get(XML.GOOD_WO_SCHEMA.getValue());
    String xml = FileTool.toString(xmlPath);
    Document doc = XMLTool.parseXML(xml);

    Node firstNode = XMLTool.getFirstNode(doc, "/shiporder/item/title");
    assertNotNull(firstNode);
    assertEquals("Empire Burlesque", firstNode.getTextContent());

    firstNode = XMLTool.getFirstNode(doc, "/shiporder/item/nothing");
    assertNull(firstNode);

    Map<String, String> namespaces = new HashMap<>();
    namespaces.put("test", TEST_NAMESPACE);
    firstNode = XMLTool.getFirstNode(doc, "/test:shiporder/test:item/test:title", namespaces);
    assertNull(firstNode);
  }

  @Test
  void getFirstNodeWithNamespaceTest() throws IOException, ParserConfigurationException, SAXException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    Path xmlPath = fileList.get(XML.WITH_NAMESPACE.getValue());
    String xml = FileTool.toString(xmlPath);
    Document doc = XMLTool.parseXML(xml);

    Node firstNode = XMLTool.getFirstNode(doc, "/shiporder/item/title");
    assertNull(firstNode);

    Map<String, String> namespaces = new HashMap<>();
    namespaces.put("test", TEST_NAMESPACE);

    firstNode = XMLTool.getFirstNode(doc, "/test:shiporder/test:item/test:title", namespaces);
    assertNotNull(firstNode);
    assertEquals("Empire Burlesque", firstNode.getTextContent());

    firstNode = XMLTool.getFirstNode(doc, "/test:shiporder/test:item/test:nothing", namespaces);
    assertNull(firstNode);
  }

  @Test
  void getFirstStringValueWithoutNamespaceTest() throws IOException, ParserConfigurationException, SAXException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    Path xmlPath = fileList.get(XML.GOOD_WO_SCHEMA.getValue());
    String xml = FileTool.toString(xmlPath);
    Document doc = XMLTool.parseXML(xml);

    String value = XMLTool.getFirstTextContent(doc, "/shiporder/item/title");
    assertNotNull(value);
    assertEquals("Empire Burlesque", value);

    value = XMLTool.getFirstTextContent(doc, "/shiporder/item/nothing");
    assertNull(value);

    Map<String, String> namespaces = new HashMap<>();
    namespaces.put("test", TEST_NAMESPACE);
    value = XMLTool.getFirstTextContent(doc, "/test:shiporder/test:item/test:title", namespaces);
    assertNull(value);
  }

  @Test
  void getFirstStringValueWithNamespaceTest() throws IOException, ParserConfigurationException, SAXException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    Path xmlPath = fileList.get(XML.WITH_NAMESPACE.getValue());
    String xml = FileTool.toString(xmlPath);
    Document doc = XMLTool.parseXML(xml);

    String value = XMLTool.getFirstTextContent(doc, "/shiporder/item/title");
    assertNull(value);

    Map<String, String> namespaces = new HashMap<>();
    namespaces.put("test", TEST_NAMESPACE);

    value = XMLTool.getFirstTextContent(doc, "/test:shiporder/test:item/test:title", namespaces);
    assertNotNull(value);
    assertEquals("Empire Burlesque", value);

    value = XMLTool.getFirstTextContent(doc, "/test:shiporder/test:item/test:nothing", namespaces);
    assertNull(value);
  }

  @Test
  void addChildNodeWithoutNamespaceTest() throws IOException, ParserConfigurationException, SAXException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    Path xmlPath = fileList.get(XML.GOOD_WO_SCHEMA.getValue());
    String xml = FileTool.toString(xmlPath);
    Document doc = XMLTool.parseXML(xml);

    Node shiptoNode = XMLTool.getFirstNode(doc, "/shiporder/shipto");
    XMLTool.addChildNode(shiptoNode, "phone", "12345");

    String value = XMLTool.getFirstTextContent(doc, "/shiporder/shipto/phone");
    assertNotNull(value);
    assertEquals("12345", value);
  }

  @Test
  void addChildNodeWithNamespaceTest() throws IOException, ParserConfigurationException, SAXException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    Path xmlPath = fileList.get(XML.WITH_NAMESPACE.getValue());
    String xml = FileTool.toString(xmlPath);
    Document doc = XMLTool.parseXML(xml);

    Map<String, String> namespaces = new HashMap<>();
    namespaces.put("test", TEST_NAMESPACE);

    Node shiptoNode = XMLTool.getFirstNode(doc, "/test:shiporder/test:shipto", namespaces);
    XMLTool.addChildNodeNS(shiptoNode, TEST_NAMESPACE, "phone", "12345");

    String value = XMLTool.getFirstTextContent(doc, "/test:shiporder/test:shipto/test:phone", namespaces);
    assertNotNull(value);
    assertEquals("12345", value);
  }

  @Test
  void documentToStringTest() throws ParserConfigurationException {
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    Document document = dbf.newDocumentBuilder().newDocument();

    Node rootNode = XMLTool.addChildNode(document, "root");
    XMLTool.addChildNode(rootNode, "child1", "value1");
    XMLTool.addChildNode(rootNode, "child2", "value2");

    try {
      String expectedXml = """
              <?xml version="1.0" encoding="UTF-8"?>
              <root>
                 <child1>value1</child1>
                 <child2>value2</child2>
              </root>
              """;

      String xml = XMLTool.documentToString(document);
      assertEquals(expectedXml, xml);
    } catch (TransformerException e) {
      fail("unable to serialize DOM document");
    }
  }

  @Test
  void getRawContentWithoutNamespaceTest() throws IOException, ParserConfigurationException, SAXException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    Path xmlPath = fileList.get(XML.WITH_HTML.getValue());
    String xml = FileTool.toString(xmlPath);
    Document doc = XMLTool.parseXML(xml);

    String value = XMLTool.getRawContent(doc, "/shiporder/item/title");
    assertNotNull(value);
    assertEquals("<title><h1>Empire <b>Burlesque</b></h1></title>", value);

    value = XMLTool.getRawContent(doc, "/shiporder/item");
    assertNotNull(value);
    assertEquals("""
            <item>
            \t\t<title><h1>Empire <b>Burlesque</b></h1></title>
            \t\t<note>Special Edition</note>
            \t\t<quantity>1</quantity>
            \t\t<price>10.90</price>
            \t</item>""", value);
  }

  @Test
  void getFirstNodeInnerRawContentWithoutNamespaceTest() throws IOException, ParserConfigurationException, SAXException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    Path xmlPath = fileList.get(XML.WITH_HTML.getValue());
    String xml = FileTool.toString(xmlPath);
    Document doc = XMLTool.parseXML(xml);

    String value = XMLTool.getFirstNodeInnerRawContent(doc, "/shiporder/item/title");
    assertNotNull(value);
    assertEquals("<h1>Empire <b>Burlesque</b></h1>", value);

    value = XMLTool.getFirstNodeInnerRawContent(doc, "/shiporder/item");
    assertNotNull(value);
    assertEquals("<title><h1>Empire <b>Burlesque</b></h1></title><note>Special Edition</note><quantity>1</quantity><price>10.90</price>", value);
  }

  @Test
  void getFirstNodeInnerRawContentWithoutNamespaceKeepIndentationTest() throws IOException, ParserConfigurationException, SAXException {
    final List<Path> fileList = this.loadFilesTest(PREFIX);
    Path xmlPath = fileList.get(XML.WITH_HTML.getValue());
    String xml = FileTool.toString(xmlPath);
    Document doc = XMLTool.parseXML(xml);

    String value = XMLTool.getFirstNodeInnerRawContent(doc, "/shiporder/item/title", true);
    assertNotNull(value);
    assertEquals("<h1>Empire <b>Burlesque</b>\n</h1>", value);

    value = XMLTool.getFirstNodeInnerRawContent(doc, "/shiporder/item", true);
    assertNotNull(value);
    assertEquals(
            "<title>\n   <h1>Empire <b>Burlesque</b>\n   </h1>\n</title>\n<note>Special Edition</note>\n<quantity>1</quantity>\n<price>10.90</price>",
            value);
  }

  private Path getPath(String f) {
    try {
      return new ClassPathResource(f).getFile().toPath();
    } catch (final IOException e) {
      return new FileSystemResource(f).getFile().toPath();
    }
  }

  private List<Path> loadFilesTest(String prefix) {
    final List<Path> list = new ArrayList<>();
    list.add(this.getPath(prefix + ".xsd")); // 0 = XML Schema
    list.add(this.getPath(prefix + "_w_schema.xml")); // 1 = Good XML
    list.add(this.getPath(prefix + "_wo_schema.xml")); // 2 = Good XML without schema
    list.add(this.getPath(prefix + "_w_wrong_schema.xml")); // 3 = Good XML without schema
    list.add(this.getPath(prefix + "_not_valid.xml")); // 4 = Not Valid XML
    list.add(this.getPath(prefix + "_not_well_formed.xml")); // 5 = Not well-formed XML
    list.add(this.getPath(prefix + "_w_namespace.xml")); // 6 = XML with namespace
    list.add(this.getPath(prefix + "_w_html.xml")); // 7 = Good XML with html
    return list;
  }

  private List<URL> loadSchemaTest(String prefix) {
    final List<URL> list = new ArrayList<>();
    try {
      list.add(this.getPath(prefix + ".xsd").toUri().toURL());
    } catch (final IOException e) {
      fail(e.getMessage());
    }

    return list;
  }

}
