/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIPMH.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.oai;

import org.springframework.hateoas.RepresentationModel;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.model.xml.oai.v2.OAIPMHtype;

@Schema(description = """
        OAI-PMH Provider
        Specifications: http://www.openarchives.org/OAI/openarchivesprotocol.html
        """)
public class OAIPMH extends RepresentationModel<OAIPMH> {

  private OAIPMHtype oai = new OAIPMHtype();

  public OAIPMHtype getOai() {
    return this.oai;
  }

  public void setOai(OAIPMHtype oai) {
    this.oai = oai;
  }

}
