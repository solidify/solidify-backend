/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SearchCondition.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        The search conditions describes the query to search in the indexes.
        """)
public class SearchCondition implements Serializable {

  @Serial
  private static final long serialVersionUID = 5537263935227896080L;

  /**
   * Indicate how the value must be compared to the value in the index
   */
  private SearchConditionType type = SearchConditionType.MATCH;

  private SearchOperator searchOperator = SearchOperator.AND;

  /**
   * Indicate whether the condition is used to find documents that must, must not or should contain its value
   */
  private BooleanClauseType booleanClauseType = BooleanClauseType.MUST;

  /**
   * Name of the field to query
   */
  private String field;

  /**
   * Names of the fields used in a multimatch query
   */
  private final List<String> multiMatchFields = new ArrayList<>();

  /**
   * Value if we search a single value
   */
  private String value;

  /**
   * Values if we search on multiple term values
   */
  private final List<String> terms = new ArrayList<>();

  /**
   * Upper value with we search a range of possible values
   */
  private String upperValue;

  /**
   * Lower if we search a range of possible values
   */
  private String lowerValue;

  /**
   * Contains the list of nested search conditions for a NESTED_BOOLEAN search condition. It is typically used to build conditions with logical
   * OR between criteria. (See for instance https://stackoverflow.com/questions/28538760/elasticsearch-bool-query-combine-must-with-or)
   */
  private List<SearchCondition> nestedConditions = new ArrayList<>();

  public SearchCondition() {
  }

  public SearchCondition(SearchConditionType type) {
    this.type = type;
  }

  public SearchCondition(SearchConditionType type, String field) {
    this.type = type;
    this.field = field;
  }

  public SearchCondition(SearchConditionType type, String field, String value) {
    this.type = type;
    this.field = field;
    this.value = value;
  }

  public SearchCondition(SearchConditionType type, String field, List<String> terms) {
    this.type = type;
    this.field = field;
    if (terms != null) {
      this.terms.addAll(terms);
    }
  }

  public SearchConditionType getType() {
    return this.type;
  }

  public void setType(SearchConditionType type) {
    this.type = type;
  }

  public BooleanClauseType getBooleanClauseType() {
    return this.booleanClauseType;
  }

  public void setBooleanClauseType(BooleanClauseType booleanClauseType) {
    this.booleanClauseType = booleanClauseType;
  }

  public String getField() {
    return this.field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public List<String> getMultiMatchFields() {
    return this.multiMatchFields;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public List<String> getTerms() {
    return this.terms;
  }

  public List<SearchCondition> getNestedConditions() {
    return this.nestedConditions;
  }

  public String getUpperValue() {
    return this.upperValue;
  }

  public void setUpperValue(String upperValue) {
    this.upperValue = upperValue;
  }

  public String getLowerValue() {
    return this.lowerValue;
  }

  public void setLowerValue(String lowerValue) {
    this.lowerValue = lowerValue;
  }

  public SearchOperator getSearchOperator() {
    return this.searchOperator;
  }

  public void setSearchOperator(SearchOperator searchOperator) {
    this.searchOperator = searchOperator;
  }
}
