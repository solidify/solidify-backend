/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - OSTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

public class OSTool {
  public static class OSEnum {
    public static final String OSX = "mac";
    public static final String SOLARIS = "sunos";
    public static final String UNDEFINED = "undefined";
    public static final String UNIX = "uni";
    public static final String UNIX_AIX = "aix";
    public static final String UNIX_NIX = "nix";
    public static final String UNIX_NUX = "nux";
    public static final String WINDOWS = "win";
  }

  private static final String OS_NAME_PROPERTY = "os.name";

  private static String OS = System.getProperty(OS_NAME_PROPERTY).toLowerCase();

  public static String getOS() {
    if (isWindows()) {
      return OSEnum.WINDOWS;
    } else if (isMac()) {
      return OSEnum.OSX;
    } else if (isUnix()) {
      return OSEnum.UNIX;
    } else if (isSolaris()) {
      return OSEnum.SOLARIS;
    } else {
      return OSEnum.UNDEFINED;
    }
  }

  public static boolean isMac() {
    return OS.contains(OSEnum.OSX);
  }

  public static boolean isSolaris() {
    return OS.contains(OSEnum.SOLARIS);
  }

  public static boolean isUnix() {
    return (OS.contains(OSEnum.UNIX_NIX) || OS.contains(OSEnum.UNIX_NUX)
            || OS.contains(OSEnum.UNIX_AIX));
  }

  public static boolean isWindows() {
    return OS.contains(OSEnum.WINDOWS);
  }

}
