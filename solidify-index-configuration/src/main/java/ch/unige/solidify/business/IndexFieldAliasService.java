/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Management - IndexFieldAliasService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.business;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.IndexConfigurationController;
import ch.unige.solidify.dto.LabelDTO;
import ch.unige.solidify.model.Label;
import ch.unige.solidify.model.LargeLabel;
import ch.unige.solidify.model.dto.FacetSortDTO;
import ch.unige.solidify.model.index.FacetProperties;
import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.model.specification.IndexFieldAliasSpecification;
import ch.unige.solidify.repository.IndexFieldAliasRepository;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.service.IndexFieldAliasInfoProvider;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.StringTool;

@Service
@Primary
@ConditionalOnBean(IndexConfigurationController.class)
public class IndexFieldAliasService extends ResourceService<IndexFieldAlias> implements IndexFieldAliasInfoProvider {

  private static final Logger log = LoggerFactory.getLogger(IndexFieldAliasService.class);

  private static final String FACET_VALUE_MANDATORY_ERROR = "validation.indexFieldAlias.facetValue.mandatory";
  private static final String VALUE_GREATER_THAN_ZERO_ERROR = "validation.value.mustBeGreaterThanZero";
  private static final String FACET_VISIBLE_VALUES_LARGER_THAN_LIMIT_ERROR = "validation.indexFieldAlias.facetDefaultVisibleValues.largerThanLimit";

  /**
   * Return a list of IndexFieldAlias that are facets for the given index name Note: index name may be
   * a list of multiple indices separated by a semicolon.
   *
   * @param index Index name
   * @return
   */
  public List<IndexFieldAlias> findAllFacetsForIndexNames(String index) {
    List<IndexFieldAlias> deduplicatedIndexFieldAliases = new ArrayList<>();
    List<String> indexFieldAliases = new ArrayList<>();
    String[] indices = index.split(SolidifyConstants.FIELD_SEP);
    for (String indexName : indices) {
      Page<IndexFieldAlias> fieldsPage = this.findAllFacetsForIndex(indexName);
      fieldsPage.get().forEach(field -> {
        // If the list of facets is requested for many indices, we check that the same facet name is only
        // added once to the list
        if (!indexFieldAliases.contains(field.getAlias())) {
          deduplicatedIndexFieldAliases.add(field);
          indexFieldAliases.add(field.getAlias());
        } else {
          log.warn("Multiple field definitions with the alias '{}' exist for index '{}'", field.getIndexName(), index);
        }
      });
    }
    return deduplicatedIndexFieldAliases;
  }

  public Page<IndexFieldAlias> findAllFacetsForIndex(String indexName) {
    return this.findAllFacetsForIndex(indexName, PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by(IndexConstants.FACET_ORDER_FIELD)));
  }

  public Page<IndexFieldAlias> findAllFacetsForIndex(String indexName, Pageable pageable) {
    IndexFieldAlias criteria = new IndexFieldAlias();
    criteria.setIndexName(indexName);
    criteria.setFacet(true);
    IndexFieldAliasSpecification specification = new IndexFieldAliasSpecification(criteria);
    return this.findAll(specification, pageable);
  }

  /**
   * Return a list of facets for the given index name. Note: index name may be a list of multiple
   * indices separated by a semicolon.
   *
   * @param index Index name
   * @return
   */
  @Override
  public List<FacetRequest> getFacetRequestsListForIndex(String index) {
    return this.findAllFacetsForIndexNames(index).stream().map(field -> {
      FacetRequest facetRequest = new FacetRequest();
      facetRequest.setName(field.getAlias());
      facetRequest.setField(field.getField());
      if (field.getFacetLimit() != null) {
        facetRequest.setLimit(field.getFacetLimit());
      }
      if (field.getFacetMinCount() != null) {
        facetRequest.setMinCount(field.getFacetMinCount());
      }
      return facetRequest;
    }).collect(Collectors.toList());
  }

  /**
   * Return facets properties for the given index.
   *
   * @param index
   * @return Map of facets properties indexed by the facet names
   */
  public List<FacetProperties> getFacetProperties(String index) {
    return this.findAllFacetsForIndexNames(index).stream().map(field -> {
      FacetProperties facetProperties = new FacetProperties();
      facetProperties.setName(field.getAlias());
      facetProperties.setDefaultVisibleValues(field.getFacetDefaultVisibleValues());
      for (Label label : field.getLabels()) {
        String text = label.getText();
        if (StringTool.isNullOrEmpty(text)) {
          text = field.getAlias();
        }
        facetProperties.getLabels().add(new LabelDTO(text, label.getLanguage().getCode()));
      }
      for (LargeLabel label : field.getDescriptionLabels()) {
        String text = label.getText();
        if (!StringTool.isNullOrEmpty(text)) {
          facetProperties.getDescriptionLabels().add(new LabelDTO(text, label.getLanguage().getCode()));
        }
      }
      return facetProperties;
    }).collect(Collectors.toList());
  }

  @Override
  public IndexFieldAlias findByIndexNameAndAlias(String index, String alias) {
    return ((IndexFieldAliasRepository) this.itemRepository).findByIndexNameAndAlias(index, alias);
  }

  private Optional<FacetSortDTO> getMaxFacetOrder(String indexName) {
    List<FacetSortDTO> maxSorts = ((IndexFieldAliasRepository) this.itemRepository).findByMaxFacetOrderAndIndexName(indexName);
    if (maxSorts.isEmpty()) {
      return Optional.empty();
    } else {
      return Optional.of(maxSorts.get(0));
    }
  }

  @Override
  protected void beforeValidate(IndexFieldAlias item) {

    // Calculates a default facet order if the object is a facet and no order is given
    if (!this.itemRepository.existsById(item.getResId()) && item.isFacet() && item.getFacetOrder() == null &&
            !StringTool.isNullOrEmpty(item.getIndexName())) {
      Optional<FacetSortDTO> maxValueOpt = this.getMaxFacetOrder(item.getIndexName());
      int newOrder = maxValueOpt.isPresent() ? maxValueOpt.get().getSort() + IndexConstants.ORDER_INCREMENT : IndexConstants.ORDER_INCREMENT;
      item.setFacetOrder(newOrder);
    }

    // If the object is not a facet, clear facet related values
    if (!item.isFacet()) {
      item.setFacetOrder(null);
      item.setFacetDefaultVisibleValues(null);
      item.setFacetLimit(null);
      item.setFacetMinCount(null);
    }
  }

  @Override
  protected void validateItemSpecificRules(IndexFieldAlias item, BindingResult errors) {

    if (item.isFacet()) {

      // facetMinCount is mandatory if the object is a facet and must be greater than 0
      if (item.getFacetMinCount() == null) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "facetMinCount", this.messageService.get(FACET_VALUE_MANDATORY_ERROR)));
      } else if (item.getFacetMinCount() < 1) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "facetMinCount", this.messageService.get(VALUE_GREATER_THAN_ZERO_ERROR)));
      }

      // facetLimit is mandatory if the object is a facet and must be greater than 0
      if (item.getFacetLimit() == null) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "facetLimit", this.messageService.get(FACET_VALUE_MANDATORY_ERROR)));
      } else if (item.getFacetLimit() < 1) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "facetLimit", this.messageService.get(VALUE_GREATER_THAN_ZERO_ERROR)));
      }

      // order is mandatory if the object is a facet and must be greater than 0
      if (item.getFacetOrder() == null) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), IndexConstants.FACET_ORDER_FIELD,
                        this.messageService.get(FACET_VALUE_MANDATORY_ERROR)));
      } else if (item.getFacetOrder() < 1) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), IndexConstants.FACET_ORDER_FIELD,
                        this.messageService.get(VALUE_GREATER_THAN_ZERO_ERROR)));
      }

      // facetDefaultVisibleValues must be greater than 0 if it is set
      if (item.getFacetDefaultVisibleValues() != null && item.getFacetDefaultVisibleValues() < 1) {
        errors.addError(
                new FieldError(item.getClass().getSimpleName(), "facetDefaultVisibleValues",
                        this.messageService.get(VALUE_GREATER_THAN_ZERO_ERROR)));
      }

      // facetDefaultVisibleValues cannot be greater than facetLimit
      if (item.getFacetLimit() != null && item.getFacetDefaultVisibleValues() != null
              && item.getFacetLimit() < item.getFacetDefaultVisibleValues()) {
        errors.addError(new FieldError(item.getClass().getSimpleName(), "facetDefaultVisibleValues",
                this.messageService.get(FACET_VISIBLE_VALUES_LARGER_THAN_LIMIT_ERROR)));
      }
    }
  }

  @Override
  public IndexFieldAliasSpecification getSpecification(IndexFieldAlias resource) {
    return new IndexFieldAliasSpecification(resource);
  }

  public void createIfNotExists(String indexName, Integer facetPosition, String alias, String field, boolean isFacet, boolean isSystem,
          Integer facetMinCount,
          Integer facetLimit) {
    IndexFieldAlias indexFieldAlias = new IndexFieldAlias();
    indexFieldAlias.setIndexName(indexName);
    indexFieldAlias.setField(field);
    indexFieldAlias.setAlias(alias);
    indexFieldAlias.setFacet(isFacet);
    indexFieldAlias.setSystem(isSystem);
    indexFieldAlias.setFacetMinCount(facetMinCount);
    indexFieldAlias.setFacetLimit(facetLimit);
    indexFieldAlias.setFacetOrder(facetPosition);
    this.createIfNotExists(indexFieldAlias);
  }

  private void createIfNotExists(IndexFieldAlias indexFieldAlias) {
    if (this.findByIndexNameAndAlias(indexFieldAlias.getIndexName(), indexFieldAlias.getAlias()) == null) {
      this.save(indexFieldAlias);
      log.info("IndexFieldAlias '{}' created", indexFieldAlias.getIndexName() + " - " + indexFieldAlias.getAlias());
    }
  }
}
