/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SchedulerService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ScheduledFuture;

import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.scheduler.AbstractTaskRunnable;
import ch.unige.solidify.scheduler.ScheduledTaskInterface;

@Service
public class SchedulerService<T extends ScheduledTaskInterface> {
  public enum SchedulingCancelType {
    INTERRUPT_RUNNING_TASK, DONT_INTERRUPT_RUNNING_TASK
  }

  private ThreadPoolTaskScheduler taskScheduler;

  /**
   * List containing the scheduled tasks runnables (= the tasks implementations)
   * <p>
   * Each AbstractTaskRunnable contains a reference to its corresponding the resource that implements ScheduledTaskInterface and ScheduledFuture
   */
  private List<AbstractTaskRunnable<T>> scheduledTasks = new ArrayList<>();

  private int additionalThreadsSize = 10;

  public SchedulerService(ThreadPoolTaskScheduler taskScheduler) {
    this.taskScheduler = taskScheduler;
  }

  public int getAdditionalThreadsSize() {
    return this.additionalThreadsSize;
  }

  public void setAdditionalThreadsSize(int additionalThreadsSize) {
    this.additionalThreadsSize = additionalThreadsSize;
  }

  /****************************************************************/

  /**
   * Check if a task is already scheduled
   *
   * @param T
   * @return
   */
  public boolean isTaskScheduled(T item) {
    return this.getScheduledAbstractTaskRunnable(item).isPresent();
  }

  /**
   * Cancel the scheduling of a task
   *
   * @param scheduledTask
   * @param schedulingCancelType The way to interrupt the running task
   */
  public void cancelTaskScheduling(T scheduledTask, SchedulingCancelType schedulingCancelType) {
    Optional<AbstractTaskRunnable<T>> abstractTaskRunnableOptional = this.getScheduledAbstractTaskRunnable(scheduledTask);
    if (abstractTaskRunnableOptional.isPresent()) {
      ScheduledFuture<AbstractTaskRunnable<T>> scheduledFuture = abstractTaskRunnableOptional.get().getScheduledFuture();
      if (!scheduledFuture.isDone() && !scheduledFuture.isCancelled()) {
        boolean interruptRunningTask = schedulingCancelType == SchedulingCancelType.INTERRUPT_RUNNING_TASK;
        if (scheduledFuture.cancel(interruptRunningTask)) {
          this.removeScheduledAbstractTaskRunnable(scheduledTask);
        } else {
          throw new SolidifyRuntimeException("unable to cancel task '" + scheduledTask.getResId() + "' scheduling");
        }
      } else {
        throw new SolidifyRuntimeException("task '" + scheduledTask.getResId() + "' is already done or cancelled");
      }
    } else {
      throw new SolidifyRuntimeException("task '" + scheduledTask.getResId() + "' is not scheduled");
    }
  }

  /**
   * Start the scheduling of a task.
   * <p>
   * It uses a CronTrigger that ensure the same task is not started before its last execution is over. Different tasks can run in parallel as the
   * the ThreadPoolTaskScheduler pool size is automatically resized according to the number of scheduled tasks.
   *
   * @param runnableTask
   */
  public void startTaskScheduling(AbstractTaskRunnable<T> runnableTask) {
    if (Boolean.TRUE.equals(runnableTask.getScheduledTask().isEnabled())) {

      // scheduled a task by using a cron expression
      // Note: If a task run is not over when the next run should start, the CronTrigger skips this next run
      ScheduledFuture<AbstractTaskRunnable<T>> scheduledFuture = (ScheduledFuture<AbstractTaskRunnable<T>>) this.taskScheduler
              .schedule(runnableTask, new CronTrigger(runnableTask.getScheduledTask().getCronExpression()));
      runnableTask.setScheduledFuture(scheduledFuture);

      this.scheduledTasks.add(runnableTask);

      // set the scheduler pool size to allow different scheduled tasks to run in parallel.
      // Add some margin to allow applications's @Scheduled methods to run in parallel as well
      this.taskScheduler.setPoolSize(this.scheduledTasks.size() + this.additionalThreadsSize);
    } else {
      throw new SolidifyRuntimeException("A disabled task cannot be scheduled ('" + runnableTask.getScheduledTask().getResId() + "')");
    }
  }

  /**
   * Cancel all scheduled tasks started by this service
   */
  public void cancelAllTasksScheduling() {
    for (AbstractTaskRunnable<T> abstractTaskRunnable : this.scheduledTasks) {
      ScheduledFuture<AbstractTaskRunnable<T>> scheduledFuture = abstractTaskRunnable.getScheduledFuture();
      if (!scheduledFuture.isDone() && !scheduledFuture.isCancelled()) {
        scheduledFuture.cancel(false);
      }
    }
    this.scheduledTasks.clear();
  }

  /****************************************************************/

  private Optional<AbstractTaskRunnable<T>> getScheduledAbstractTaskRunnable(T item) {
    return this.scheduledTasks.stream().filter(taskRunnable -> taskRunnable.getScheduledTask().getResId().equals(item.getResId()))
            .findFirst();
  }

  private boolean removeScheduledAbstractTaskRunnable(T scheduledTask) {
    Optional<AbstractTaskRunnable<T>> abstractTaskRunnableOptional = this.getScheduledAbstractTaskRunnable(scheduledTask);
    if (abstractTaskRunnableOptional.isPresent()) {
      return this.scheduledTasks.remove(abstractTaskRunnableOptional.get());
    }
    return false;
  }
}
