/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH - OAIMetadataPrefixService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.business;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.controller.OAIController;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.message.oai.RefreshOaiPmhMessage;
import ch.unige.solidify.model.oai.MetadataContent;
import ch.unige.solidify.model.oai.OAIMetadataPrefix;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHtype;
import ch.unige.solidify.model.xml.oai.v2.oai_identifier.OaiIdentifierType;
import ch.unige.solidify.repository.OAIMetadataPrefixRepository;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.OAIMetadataPrefixSpecification;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

@Service
@ConditionalOnBean(OAIController.class)
@DependsOn("solidifyEventPublisher")
public class OAIMetadataPrefixService extends ResourceService<OAIMetadataPrefix> {
  private static final Logger log = LoggerFactory.getLogger(OAIMetadataPrefixService.class);

  private final OAIMetadataPrefixRepository oaiMetadataPrefixRepository;

  public OAIMetadataPrefixService(OAIMetadataPrefixRepository oaiMetadataPrefixRepository) {
    this.oaiMetadataPrefixRepository = oaiMetadataPrefixRepository;
  }

  public OAIMetadataPrefix findByName(String name) {
    return this.oaiMetadataPrefixRepository.findByName(name);
  }

  public OAIMetadataPrefix findByPrefix(String prefix) {
    return this.oaiMetadataPrefixRepository.findByPrefix(prefix);
  }

  @Override
  public OAIMetadataPrefixSpecification getSpecification(OAIMetadataPrefix resource) {
    return new OAIMetadataPrefixSpecification(resource);
  }

  public void initDefaultData() throws IOException {
    this.createIfNotExists(
            OAIConstants.OAI_DC,
            false,
            OAIConstants.OAI_DC_NAME,
            "Schema imports the Dublin Core elements from the DCMI schema for unqualified Dublin Core.\nAdjusted for usage in the OAI-PMH.",
            "oai-dc.xsd",
            null,
            OAIConstants.OAI_DC_SCHEMA,
            OAIConstants.OAI_DC_NAMESPACE,
            null);
  }

  public void updateOaiDc(String metadataTransformation) throws IOException {
    OAIMetadataPrefix oaiDcPrefix = this.findByPrefix(OAIConstants.OAI_DC);
    if (oaiDcPrefix == null) {
      throw new SolidifyRuntimeException("Missing OAI Dublin-Core prefix");
    }
    ClassPathResource xslResource = new ClassPathResource(SolidifyConstants.XSL_HOME + "/" + metadataTransformation);
    oaiDcPrefix.setMetadataXmlTransformation(FileTool.toString(xslResource.getInputStream()));
    this.save(oaiDcPrefix);
    log.info("OAIMetadataPrefix '{}' updated", OAIConstants.OAI_DC);
  }

  public void createIfNotExists(String prefix, boolean reference, String name, String description, String metadataSchema,
          String metadataTransformation, String schemaUrl, String schemaNamesapce, String xmlClass)
          throws IOException {
    if (this.findByPrefix(prefix) == null) {
      OAIMetadataPrefix oaiMetadataPrefix = new OAIMetadataPrefix();
      oaiMetadataPrefix.setPrefix(prefix);
      oaiMetadataPrefix.setName(name);
      oaiMetadataPrefix.setSchemaUrl(new URL(schemaUrl));
      oaiMetadataPrefix.setSchemaNamespace(schemaNamesapce);
      oaiMetadataPrefix.setEnabled(Boolean.TRUE);
      oaiMetadataPrefix.setReference(reference);
      if (!StringTool.isNullOrEmpty(description)) {
        oaiMetadataPrefix.setDescription(description);
      }
      if (!StringTool.isNullOrEmpty(metadataSchema)) {
        ClassPathResource schemaResource = new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + metadataSchema);
        oaiMetadataPrefix.setMetadataSchema(FileTool.toString(schemaResource.getInputStream()));
      }
      if (!StringTool.isNullOrEmpty(metadataTransformation)) {
        ClassPathResource xslResource = new ClassPathResource(SolidifyConstants.XSL_HOME + "/" + metadataTransformation);
        oaiMetadataPrefix.setMetadataXmlTransformation(FileTool.toString(xslResource.getInputStream()));
      }
      if (!StringTool.isNullOrEmpty(xmlClass)) {
        oaiMetadataPrefix.setXmlClass(xmlClass);
      }
      this.save(oaiMetadataPrefix);
      log.info("OAIMetadataPrefix '{}' created", prefix);
    }
  }

  @Override
  public OAIMetadataPrefix save(OAIMetadataPrefix metadataPrefix) {
    final OAIMetadataPrefix updatedMetadataPrefix = super.save(metadataPrefix);
    SolidifyEventPublisher.getPublisher().publishEvent(new RefreshOaiPmhMessage());
    return updatedMetadataPrefix;
  }

  @Override
  protected void validateItemSpecificRules(OAIMetadataPrefix metadataPrefix, BindingResult errors) {
    // Check XML schema
    if (!StringTool.isNullOrEmpty(metadataPrefix.getMetadataSchema())) {
      try {
        XMLTool.wellformed(metadataPrefix.getMetadataSchema());
      } catch (SolidifyCheckingException e) {
        errors.addError(new FieldError(metadataPrefix.getClass().getSimpleName(), "metadataSchema", e.getMessage()));
      }
    }
    // Check XML transformation
    if (!StringTool.isNullOrEmpty(metadataPrefix.getMetadataXmlTransformation())) {
      try {
        XMLTool.wellformed(metadataPrefix.getMetadataXmlTransformation());
      } catch (SolidifyCheckingException e) {
        errors.addError(new FieldError(metadataPrefix.getClass().getSimpleName(), "metadataXmlTransformation", e.getMessage()));
      }
    }
    // Check XML class
    if (!StringTool.isNullOrEmpty(metadataPrefix.getXmlClass())) {
      try {
        final List<Class<?>> classList = new ArrayList<>(Arrays.asList(OAIPMHtype.class, OaiIdentifierType.class, MetadataContent.class,
                Class.forName(metadataPrefix.getXmlClass())));
        JAXBContext.newInstance(classList.toArray(Class<?>[]::new));
      } catch (JAXBException e) {
        errors.addError(new FieldError(metadataPrefix.getClass().getSimpleName(), "xmlClass", e.getMessage()));
      } catch (ClassNotFoundException e) {
        errors.addError(new FieldError(metadataPrefix.getClass().getSimpleName(), "xmlClass", "The class does not exist."));
      }
    }
  }
}
