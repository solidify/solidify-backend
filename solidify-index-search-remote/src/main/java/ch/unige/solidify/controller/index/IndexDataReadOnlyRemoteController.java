/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Remote Search - IndexDataReadOnlyRemoteController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller.index;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.SolidifyController;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.service.rest.abstractservice.IndexMetadataRemoteResourceService;

@RootPermissions
public abstract class IndexDataReadOnlyRemoteController<T extends IndexMetadata> extends SolidifyController
        implements ControllerWithHateoasHome {

  private IndexMetadataRemoteResourceService<T> indexResourceRemoteService;

  protected IndexDataReadOnlyRemoteController(IndexMetadataRemoteResourceService<T> indexResourceRemoteService) {
    this.indexResourceRemoteService = indexResourceRemoteService;
  }

  protected IndexMetadataRemoteResourceService<T> getIndexResourceRemoteService() {
    return this.indexResourceRemoteService;
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<T> get(@PathVariable String id) {
    final T t = this.getIndexResourceRemoteService().getIndexMetadata(id);
    if (t != null) {
      this.addLinks(t);
      return new ResponseEntity<>(t, HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }

  @GetMapping
  public HttpEntity<RestCollection<T>> list(@ModelAttribute T search, Pageable pageable) {
    return new ResponseEntity<>(this.getIndexResourceRemoteService().getIndexMetadataList(pageable), HttpStatus.OK);
  }

  protected void addLinks(T t) {
    t.removeLinks();
    t.addLinks(linkTo(this.getClass()));
  }

  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    // Do nothing
    // Could be overridden by subclass
  }

  protected HttpEntity<RestCollection<T>> page2Collection(Page<T> listItem, Pageable pageable) {
    for (final T t : listItem) {
      this.addLinks(t);
    }
    final RestCollection<T> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(this.getClass(), pageable).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass(), pageable)).toUriComponentsBuilder())
            .withRel(ActionName.MODULE));
    this.addSortLinks(linkTo(this.getClass(), pageable), collection);
    this.addPageLinks(linkTo(this.getClass(), pageable), collection, pageable);
    this.addOthersLinks(collection);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }
}
