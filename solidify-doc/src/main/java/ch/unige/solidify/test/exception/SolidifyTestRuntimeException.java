/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Documentation - SolidifyTestRuntimeException.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.exception;

import java.io.Serial;

import ch.unige.solidify.exception.SolidifyRuntimeException;

/**
 * Common runtime exception class.
 */
public class SolidifyTestRuntimeException extends SolidifyRuntimeException {
  @Serial
  private static final long serialVersionUID = 8084471674660736273L;

  public SolidifyTestRuntimeException(String message) {
    super(message);
  }

  public SolidifyTestRuntimeException(String message, Throwable exception) {
    super(message, exception);
  }

}
