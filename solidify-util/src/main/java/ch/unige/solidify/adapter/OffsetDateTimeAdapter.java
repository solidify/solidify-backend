/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - OffsetDateTimeAdapter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.adapter;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

import ch.unige.solidify.SolidifyConstants;

public class OffsetDateTimeAdapter {

  public static OffsetDateTime parseDateTime(String value) {
    if (value != null) {
      return OffsetDateTime.parse(value);
    }
    return null;
  }

  public static String printDateTime(OffsetDateTime value) {
    if (value != null) {
      return value.format(DateTimeFormatter.ISO_INSTANT);
    }
    return null;
  }

  /**
   * This constructor should not be called
   */
  private OffsetDateTimeAdapter() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
