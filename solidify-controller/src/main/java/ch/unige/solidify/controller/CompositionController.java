/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - CompositionController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jakarta.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.StringTool;

@RootPermissions
public abstract class CompositionController<T extends Resource, V extends Resource> extends SubResourceController<T, V> {

  protected static final String UNMODIFIABLE_RESOURCE = "validation.resource.unmodifiable";
  protected static final String UNDELETABLE_RESOURCE = "validation.resource.undeletable";

  @GetMapping
  public HttpEntity<RestCollection<V>> list(@PathVariable String parentid, @ModelAttribute V filterItem, Pageable pageable) {
    final T parentResource = this.resourceService.findOne(parentid);
    this.setParentResourceProperty(filterItem, parentResource);
    SolidifySpecification<V> specification = this.subResourceService.getSpecification(filterItem);
    return this.getSubResourcesCollectionAsResponseEntity(parentid, specification, pageable);
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<V> get(@PathVariable String parentid, @PathVariable String id) {
    V subResource = this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, false);
    this.addLinks(parentid, subResource);
    return new ResponseEntity<>(subResource, HttpStatus.OK);
  }

  @PostMapping
  public HttpEntity<V> create(@PathVariable final String parentid, final @Valid @RequestBody V childResource) {
    if (childResource.getResId() != null && this.subResourceService.existsById(childResource.getResId())) {
      throw new SolidifyRuntimeException(this.messageService.get("validation.resource.alreadyexist", new Object[] { childResource.getResId() }));
    }

    this.subResourceService.setAuthenticatedUserProperties(childResource);

    final T parentResource = this.resourceService.findOne(parentid);
    if (!parentResource.isModifiable()) {
      throw new SolidifyUnmodifiableException(
              this.messageService.get(UNMODIFIABLE_RESOURCE, new Object[] { parentResource.getClass().getSimpleName() + ": " + parentid }));
    }
    this.setParentResourceProperty(childResource, parentResource);

    this.subResourceService.save(childResource);
    return new ResponseEntity<>(childResource, HttpStatus.CREATED);
  }

  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<V> update(@PathVariable final String parentid, @PathVariable final String id,
          @RequestBody final Map<String, Object> newChildResource) {
    V subResourceToUpdate = this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, true);

    // Check resource
    if (!subResourceToUpdate.isModifiable()) {
      throw new SolidifyUnmodifiableException(
              this.messageService.get(UNMODIFIABLE_RESOURCE, new Object[] { subResourceToUpdate.getClass().getSimpleName() + ": " + id }));
    }

    this.subResourceService.patchResource(subResourceToUpdate, newChildResource);
    this.subResourceService.setAuthenticatedUserProperties(subResourceToUpdate);
    this.subResourceService.save(subResourceToUpdate);
    return new ResponseEntity<>(subResourceToUpdate, HttpStatus.OK);
  }

  @DeleteMapping(SolidifyConstants.URL_ID)
  public ResponseEntity<Void> delete(@PathVariable final String parentid, @PathVariable final String id) {
    V subResourceToDelete = this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, true);
    // Check resource
    if (!subResourceToDelete.isDeletable()) {
      throw new SolidifyUndeletableException(
              this.messageService.get(UNMODIFIABLE_RESOURCE, new Object[] { subResourceToDelete.getClass().getSimpleName() + ": " + id }));
    }
    this.subResourceService.delete(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @DeleteMapping
  public ResponseEntity<Void> deleteList(@PathVariable final String parentid, @RequestBody String[] ids) {
    List<V> subResourcesToDelete = new ArrayList<>();
    for (final String id : ids) {
      V subResourceToDelete = this.checkSubresourceExistsAndIsLinkedToParentAndModifiable(id, parentid, true);
      if (!subResourceToDelete.isDeletable()) {
        throw new SolidifyUndeletableException(
                this.messageService.get(UNDELETABLE_RESOURCE, new Object[] { subResourceToDelete.getClass().getSimpleName() + ": " + id }));
      }
      subResourcesToDelete.add(subResourceToDelete);
    }
    this.subResourceService.deleteAll(subResourcesToDelete);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @Override
  protected void addLinks(final String parentId, final V childResource) {
    childResource.removeLinks();
    childResource.addLinks(linkTo(methodOn(this.getClass()).list(parentId, null, null)), false, true);
  }

  protected V checkSubresourceExistsAndIsLinkedToParentAndModifiable(String childId, String parentId, boolean checkIfParentModifiable) {
    V subResource = this.subResourceService.findOne(childId);
    T parentResource = this.getParentResourceProperty(subResource);

    if ((parentResource == null || StringTool.isNullOrEmpty(parentResource.getResId()) || !parentResource.getResId().equals(parentId))) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND, "");
    }

    // Check parent resource is modifiable
    if (checkIfParentModifiable && !parentResource.isModifiable()) {
      throw new SolidifyUnmodifiableException(
              this.messageService.get(UNMODIFIABLE_RESOURCE, new Object[] { parentResource.getClass().getSimpleName() + ": " + parentId }));
    }
    return subResource;
  }

  protected abstract T getParentResourceProperty(V subResource);

  protected abstract void setParentResourceProperty(V subResource, T parentResource);
}
