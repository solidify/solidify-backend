/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - JoinNTiersSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.JoinResource;

public abstract class JoinNTiersSpecification<J extends JoinResource<?>> implements Specification<J> {

  protected J joinCriteria;

  protected final String pathToParent;
  protected final String pathToChild;

  protected boolean filterOnChildResId = false;
  protected boolean filterOnJoinResourceId = false;

  protected JoinNTiersSpecification(J joinCriteria, String pathToParent, String pathToChild) {
    this.joinCriteria = joinCriteria;
    this.pathToParent = pathToParent;
    this.pathToChild = pathToChild;
  }

  protected void completeParentIdPredicate(Root<J> root, CriteriaBuilder builder, List<Predicate> predicatesList) {
    final String parentId = this.getParentId();
    if (parentId != null) {
      predicatesList.add(builder.equal(this.getParentPath(root).get(SolidifyConstants.DB_RES_ID), parentId));
    }
  }

  protected void completeChildIdPredicate(Root<J> root, CriteriaBuilder builder, List<Predicate> predicatesList) {
    final String childId = this.getChildId();
    if (childId != null) {
      predicatesList.add(builder.equal(this.getChildPath(root).get(SolidifyConstants.DB_RES_ID), childId));
    }
  }

  public void setFilterOnChildResId(boolean filterOnChildResId) {
    this.filterOnChildResId = filterOnChildResId;
  }

  public void setFilterOnJoinResourceId(boolean filterOnJoinResourceId) {
    this.filterOnJoinResourceId = filterOnJoinResourceId;
  }

  protected Path<?> getParentPath(Root<J> root) {
    return this.getPathFromExpression(root, this.pathToParent);
  }

  protected Path<?> getChildPath(Root<J> root) {
    return this.getPathFromExpression(root, this.pathToChild);
  }

  public String getPathToParentId() {
    return this.pathToParent + "." + SolidifyConstants.DB_RES_ID;
  }

  protected Path<?> getPathFromExpression(Path<J> startPath, String pathExpression) {
    String[] attributeNames = pathExpression.split(SolidifyConstants.REGEX_FIELD_PATH_SEP);
    Path<?> path = null;
    for (String attributeName : attributeNames) {
      if (path == null) {
        path = startPath.get(attributeName);
      } else {
        path = path.get(attributeName);
      }
    }
    return path;
  }

  public J getJoinCriteria() {
    return this.joinCriteria;
  }

  /**
   * Filter on relation properties
   *
   * @param root
   * @param query
   * @param builder
   * @param predicatesList
   */
  protected abstract void completeJoinPredicatesList(Root<J> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList);

  /**
   * Filter on child properties
   *
   * @param root
   * @param query
   * @param builder
   * @param predicatesList
   */
  protected abstract void completeChildPredicatesList(Root<J> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList);

  protected abstract String getParentId();

  protected abstract String getChildId();
}
