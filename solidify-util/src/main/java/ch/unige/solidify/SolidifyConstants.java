/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - SolidifyConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

public class SolidifyConstants {

  private SolidifyConstants() {
    throw new IllegalStateException(TOOL_CLASS);
  }

  public static final String TOOL_CLASS = "Utility class";
  public static final String MANIFEST = "/META-INF/MANIFEST.MF";

  // Module
  public static final String ADMIN_MODULE = "admin";
  public static final String RES_SRV = "resource-srv";

  // Default buffer size
  public static final int BUFFER_SIZE = 4 * 1024 * 1024; // 4MB

  // Separators
  public static final String VALUE_SEP = "=";
  public static final String FIELD_SEP = ";";
  public static final String ID_SEP = "-";
  public static final String REGEX_FIELD_PATH_SEP = "\\.";
  public static final String ID_CONCAT = "+";

  public static final String YES = "yes";

  // URL
  public static final String URL_PARENT_ID_FIELD = "parentid";
  public static final String URL_ID_FIELD = "id";
  public static final String URL_GRAND_CHILD_ID_FIELD = "grandChildId";
  public static final String URL_ID = "/{" + URL_ID_FIELD + "}";
  public static final String URL_ID_PLUS_SEP = URL_ID + "/";
  public static final String URL_PARENT_ID = "/{" + URL_PARENT_ID_FIELD + "}/";
  public static final String URL_DASHBOARD = "/dashboard";
  public static final String URL_GRAND_CHILD_ID = "/{" + URL_GRAND_CHILD_ID_FIELD + "}";
  public static final String URL_ADMIN_MODULE = "/admin";
  public static final String ORCID = "orcid";
  public static final String URL_ORCID = URL_ADMIN_MODULE + "/" + ORCID;
  public static final String URL_EXTERNAL_UID = "/{externalUid}";

  // Web service Parameter
  public static final String IDENTIFIER_TYPE_PARAM = "identifierType";
  public static final String FILE_PARAM = "file";
  public static final String MIME_TYPE_PARAM = "mimeType";
  public static final String JOIN_RESOURCE_ID_PARAM = "joinResourceId";

  public static final String FROM_PARAM = "from";
  public static final String EXTRA_PARAM = "extra";
  public static final String URL_PARAM = "url";

  // Prefix
  public static final String BASE64_PREFIX = "{base64}";

  // Fields
  public static final String RES_ID_FIELD = "resId";
  public static final String COMPOSITE_RES_ID_FIELD = "compositeResId";

  // Security
  public static final String NO_CREDENTIAL = "Anonymous";
  public static final String ACCESS_CONTROL_ALLOW_ORIGIN_HEADER = "Access-Control-Allow-Origin";

  // Extension
  public static final String XML_MIME_TYPE = "application/xml";
  public static final String ZIP_MIME_TYPE = "application/zip";
  public static final String JSON_MIME_TYPE = "application/json";
  public static final String OCTET_STREAM_MIME_TYPE = "application/octet-stream";
  public static final String ZIP_EXT = ".zip";
  public static final String XML_EXT = ".xml";
  public static final String JSON_EXT = ".json";
  public static final String XSD_EXT = ".xsd";
  public static final String XSL_EXT = ".xsl";
  public static final String XML_OUTPUT_EXT = "-out.xml";
  public static final String HTML_EXT = ".html";

  // Message processor
  public static final String MESSAGE_PROCESSOR_ROLE = "MESSAGE_PROCESSOR";

  // XML
  public static final String SCHEMA_HOME = "schemas";
  public static final String SCHEMA = "schema";
  public static final String XSL_HOME = "xslt";
  public static final String XSL = "xsl";

  // Database constants
  public static final int DB_ID_LENGTH = 50;
  public static final int DB_BOOLEAN_LENGTH = 5;
  public static final int DB_SHORT_STRING_LENGTH = 30;
  public static final int DB_MEDIUM_STRING_LENGTH = 100;
  public static final int DB_DEFAULT_STRING_LENGTH = 255;
  public static final int DB_LONG_STRING_LENGTH = 1024;
  public static final int DB_LARGE_STRING_LENGTH = 4096;
  public static final int DB_MAX_STRING_LENGTH = 2147483646; // allows to create LONGTEXT or LONGBLOB columns in MariaDB

  public static final int DB_DATE_LENGTH = 3;

  // Database column name
  public static final String DB_RES_ID = "resId";
  public static final String DB_LANGUAGE_ID = "languageId";
  public static final String DB_GLOBAL_BANNER_ID = "bannerId";

  // Test data
  public static final String TEST_RES_ID = "test";

  // Scheduler Duration
  public static final int MILLISECONDS_IN_HOUR = 60 * 60 * 1000;
  public static final int MILLISECONDS_IN_MINUTE = 60 * 1000;

  // Filter names
  public static final String USER_PROPERTY_FILTER_NAME = "userPropertyFilter";

  // Documentation description
  public static final String RES_ID_DESCRIPTION = "The identifier of the resource. The default format is a Universally Unique IDentifier (UUID).";

  // For cache system
  public static final String SEARCH_KEY = "search";
  public static final String EXTERNAL_UID_KEY = "externalUid";

}
