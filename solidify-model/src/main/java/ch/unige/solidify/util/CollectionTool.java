/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - CollectionTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.FacetResult;

public class CollectionTool {
  private static final Logger logger = LoggerFactory.getLogger(CollectionTool.class);

  public static <T> List<T> getList(String jsonString, Class<T> itemClass) {
    List<T> result = new ArrayList<>();
    final ObjectMapper mapper = getObjectMapper(false);

    try {
      final JsonNode jsonPostResult = mapper.readTree(jsonString);
      final String resourceArrayString = jsonPostResult.get("_data").toString();
      final JavaType listItemType = mapper.getTypeFactory().constructParametricType(List.class,
              itemClass);
      result = mapper.readValue(resourceArrayString, listItemType);
    } catch (final IOException e) {
      logger.error("Error in building list", e);
    }
    return result;
  }

  public static RestCollectionPage getPage(String jsonString) {
    RestCollectionPage result = null;
    final ObjectMapper mapper = getObjectMapper(true);

    try {
      final JsonNode jsonPostResult = mapper.readTree(jsonString);
      final String pageString = jsonPostResult.get("_page").toString();
      result = mapper.readValue(pageString, RestCollectionPage.class);
    } catch (final IOException e) {
      logger.error("Error in building list", e);
    }
    return result;
  }

  public static List<FacetResult> getFacetResults(String jsonString) {
    List<FacetResult> result = null;
    final ObjectMapper mapper = getObjectMapper(true);

    try {
      final JsonNode jsonPostResult = mapper.readTree(jsonString);
      final JsonNode facetsNode = jsonPostResult.get("_facets");
      if (facetsNode != null) {
        final String facetsString = facetsNode.toString();
        result = mapper.readValue(facetsString, new TypeReference<List<FacetResult>>() {
        });
      }
    } catch (final IOException e) {
      logger.error("Error in getting facets list", e);
    }
    return result;
  }

  public static <T> List<T> initList(T firstValue) {
    return new ArrayList<>(Arrays.asList(firstValue));
  }

  private static ObjectMapper getObjectMapper(boolean failOnUnknown) {
    final ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new com.fasterxml.jackson.datatype.jsr310.JavaTimeModule());
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, failOnUnknown);
    return mapper;
  }

  private CollectionTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
