/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - FileTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.net.URI;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.util.FileSystemUtils;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public class FileTool {
  private static final Logger log = LoggerFactory.getLogger(FileTool.class);

  /**
   * @param path the path to the file
   * @return true if it's a normal file or a directory
   */
  public static boolean checkFile(Path path) {
    return path.toFile().exists();
  }

  /**
   * @param uri the uri corresponding to the path to the file
   * @return true if it's a normal file or a directory
   */
  public static boolean checkFile(URI uri) {
    return checkFile(Paths.get(uri));
  }

  /**
   * Copy a file
   *
   * @param source the source stream
   * @param destination the destination path
   * @return true if operation succeed
   */
  public static boolean copyFile(InputStream source, Path destination) throws IOException {

    if (!FileTool.ensureFolderExists(destination.getParent())) {
      log.error("Cannot create directory: {}", destination.getParent());
      return false;
    }
    try (BufferedInputStream inStream = new BufferedInputStream(source, SolidifyConstants.BUFFER_SIZE);
            BufferedOutputStream outStream = new BufferedOutputStream(new FileOutputStream(destination.toFile()),
                    SolidifyConstants.BUFFER_SIZE)) {
      byte[] buffer = new byte[SolidifyConstants.BUFFER_SIZE];
      int length;

      while ((length = inStream.read(buffer)) > 0) {
        outStream.write(buffer, 0, length);
      }
    }
    return true;
  }

  /**
   * Copy a file
   *
   * @param source the source path
   * @param destination the destination path
   * @return true if operation succeed
   */
  public static boolean copyFile(Path source, Path destination) throws IOException {
    try (FileInputStream inStream = new FileInputStream(source.toFile())) {
      return copyFile(inStream, destination);
    }
  }

  /**
   * Copy a file
   *
   * @param source the source URI
   * @param destination the destination URI
   * @return true if operation succeed
   */
  public static boolean copyFile(URI source, URI destination) throws IOException {
    return copyFile(Paths.get(source), Paths.get(destination));
  }

  /**
   * Delete a file
   *
   * @param path the path to the file to delete
   */
  public static void deleteFile(Path path) throws IOException {
    Files.delete(path);
    log.info("File {} deleted", path);
  }

  /**
   * Delete a file
   *
   * @param uri the URI to the file to delete
   */
  public static void deleteFile(URI uri) throws IOException {
    deleteFile(Paths.get(uri));
  }

  /**
   * Delete a folder
   *
   * @param folder The path of the folder
   * @return true on success
   */
  public static boolean deleteFolder(Path folder) {
    if (FileSystemUtils.deleteRecursively(folder.toFile())) {
      log.info("Folder {} deleted", folder);
      return true;
    }
    log.info("Folder {} cannot be deleted", folder);
    return false;
  }

  public static void deleteFolderContent(Path rootFolder) throws IOException {
    if (!Files.exists(rootFolder) || !Files.isDirectory(rootFolder)) {
      return;
    }

    try (Stream<Path> walk = Files.walk(rootFolder)) {
      walk.sorted(Comparator.reverseOrder())
              .filter(path -> !path.equals(rootFolder))
              .forEach(path -> {
                try {
                  Files.delete(path);
                } catch (IOException e) {
                  throw new UncheckedIOException(e);
                }
              });
    }
  }


  /**
   * Ensure that folder exists. Create it if not exists.
   *
   * @param path the path to the supposed folder
   * @return true folder exists after execution of the method
   */
  public static boolean ensureFolderExists(Path path) {
    path.toFile().mkdirs();
    return checkFile(path) && isFolder(path);
  }

  /**
   * Get all regular files paths recursively in a folder
   *
   * @param dir the path to the folder
   * @return the list of regular files
   */
  public static List<Path> findFiles(Path dir) {
    try (Stream<Path> filesStream = Files.find(dir, Integer.MAX_VALUE,
            (path, basicFileAttributes) -> basicFileAttributes.isRegularFile())) {
      return filesStream.toList();
    } catch (final IOException e) {
      log.error("IOException when finding files in {}", dir, e);
    }

    return new ArrayList<>();
  }

  /**
   * @param path the path to the file
   * @return the media type of the file
   */
  public static MediaType getContentType(Path path) {
    try {
      return MediaType.valueOf(Files.probeContentType(path));
    } catch (IOException | InvalidMediaTypeException e) {
      return MediaType.APPLICATION_OCTET_STREAM;
    }
  }

  /**
   * @param uri the uri corresponding to the path to the file
   * @return the media type
   */
  public static MediaType getContentType(URI uri) {
    return getContentType(Paths.get(uri));
  }

  public static InputStream getInputStream(Path path) throws IOException {
    return Files.newInputStream(path);
  }

  /**
   * @param path the path to the file
   * @return the file size as a long type
   */
  public static long getSize(Path path) {
    try {
      return Files.size(path);
    } catch (final IOException e) {
      throw new SolidifyRuntimeException("Unable to get size of path " + path, e);
    }
  }

  /**
   * @param uri the uri corresponding to the path to the file
   * @return the file size as a long type
   */
  public static long getSize(URI uri) {
    return getSize(Paths.get(uri));
  }

  /**
   * @param path the path to the file
   * @return true if the it's a normal file
   */
  public static boolean isFile(Path path) {
    return path.toFile().isFile();
  }

  /**
   * @param uri the uri corresponding to the path to the file
   * @return true if the it's a normal file
   */
  public static boolean isFile(URI uri) {
    return isFile(Paths.get(uri));
  }

  /**
   * Return true if the path corresponds to a folder
   *
   * @param path the path to the supposed folder
   * @return true if it's a folder
   */
  public static boolean isFolder(Path path) {
    return path.toFile().isDirectory();
  }

  /**
   * Return true if the path is a empty folder
   *
   * @param path the path to the supposed folder
   * @return true if it's a empty folder
   */
  public static boolean isEmptyFolder(Path folder) {
    if (isFolder(folder)) {
      try (Stream<Path> entries = Files.list(folder)) {
        return !entries.findFirst().isPresent();
      } catch (final IOException e) {
        log.error("IOException when reading folder {} content ", folder, e);
      }
    }
    return false;
  }

  /**
   * @param src source path
   * @param dest destination path
   * @return true if operation succeed
   */
  public static boolean moveFile(Path src, Path dest) throws IOException {
    if (FileTool.copyFile(src, dest)) {
      return FileTool.deleteFolder(src);
    }
    return false;
  }

  /**
   * Return the list of paths of the regular files in a folder
   *
   * @param folder the path of the folder
   * @return the list of regular files Return an empty list if an IOException occurs.
   */
  public static List<Path> readFolder(Path folder) {
    try (Stream<Path> directory = Files.list(folder)) {
      return directory.filter(f -> f.toFile().isFile()).toList();
    } catch (final IOException e) {
      log.error("IOException when reading folder {}", folder, e);
    }
    return new ArrayList<>();
  }

  /**
   * Rename a file
   *
   * @param source the source path
   * @param destination the destination path
   * @param force if true replace if destination exists
   * @throws IOException if an I/O error occurs
   */
  public static void renameFile(Path source, Path destination, boolean force) throws IOException {
    if (force) {
      Files.move(source, destination, StandardCopyOption.REPLACE_EXISTING);
    } else {
      Files.move(source, destination);
    }
  }

  /**
   * Rename a file
   *
   * @param source the source URI
   * @param destination the destination URI
   * @param force if true replace if destination exists
   * @throws IOException if an I/O error occurs
   */
  public static void renameFile(URI source, URI destination, boolean force) throws IOException {
    renameFile(Paths.get(source), Paths.get(destination), force);
  }

  /**
   * Scan the classpath and return the list of all paths satisfying a pattern.
   *
   * @param pattern The pattern used to filter the list of paths.
   * @return the list of all paths satisfying the pattern. If an IOException occurs stop the scanning
   * and return the paths already found.
   */
  public static List<Resource> scanClassPath(String pattern) {
    final List<Resource> fileList = new ArrayList<>();
    final ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
    try {
      fileList.addAll(Arrays.asList(resolver.getResources(pattern)));
    } catch (final IOException e) {
      log.error("IOException when scanning class path with pattern {}", pattern, e);
    }
    return fileList;
  }

  /**
   * Scan a folder and return the list of all paths satisfying a predicate.
   *
   * @param folder The path of the folder to scan.
   * @param predicate The predicate used to filter the list of paths.
   * @return the list of all paths satisfying the predicate. Return an empty list if an IOException
   * occurs.
   */
  public static List<Path> scanFolder(Path folder, Predicate<? super Path> predicate) {
    try (Stream<Path> pathStream = Files.list(folder)) {
      return pathStream.filter(predicate).toList();
    } catch (final IOException e) {
      log.error("IOException when scanning folder {}", folder, e);
    }
    return new ArrayList<>();
  }

  /**
   * Delete recursively all directories containing no files, including the root directory
   * Log a warning if a directory is not empty.
   *
   * @throws SolidifyRuntimeException if an IO error occurs during the traversing or deletion of directories
   * @param rootDirectory the root directory
   */
  public static void deleteRecursivelyEmptyDirectories(Path rootDirectory) {
    if (!Files.isDirectory(rootDirectory)) {
      throw new IllegalArgumentException(rootDirectory + " must be a directory");
    }
    final List<Path> directoryToDeleteList = new ArrayList<>();
    try (Stream<Path> stream = Files.walk(rootDirectory)) {
      directoryToDeleteList.addAll(stream.filter(Files::isDirectory).toList());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("IO Error when traversing directory" + rootDirectory, e);
    }
    Collections.reverse(directoryToDeleteList);
    for (Path directoryToDelete : directoryToDeleteList) {
      try {
        Files.delete(directoryToDelete);
      } catch (DirectoryNotEmptyException e) {
        log.warn("Directory {} no empty. Leaving it as it.", directoryToDelete);
      } catch (IOException e) {
        throw new SolidifyRuntimeException("Unable to delete directory", e);
      }
    }
  }

  public static String toString(InputStream inputStream) throws IOException {
    final StringBuilder resultStringBuilder = new StringBuilder();
    try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
      String line;
      while ((line = br.readLine()) != null) {
        resultStringBuilder.append(line).append("\n");
      }
    }
    return resultStringBuilder.toString();
  }

  public static String toString(Path path) throws IOException {
    return FileTool.toString(FileTool.getInputStream(path));
  }

  /**
   * This constructor should not be called
   */
  private FileTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
