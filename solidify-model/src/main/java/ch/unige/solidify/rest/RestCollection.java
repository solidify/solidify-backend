/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - RestCollection.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.CollectionTool;

@Schema(description = """
        The structure of the REST response:
        - data: the list of objects
        - page: the pagination information
        - links: the HATEOAS links
        - facets: the search information
        """)
public class RestCollection<T> extends RepresentationModel<RestCollection<T>> {
  private final List<T> data;
  private final RestCollectionPage pageable;
  private final List<FacetResult> facets = new ArrayList<>();

  public RestCollection() {
    this.data = Collections.emptyList();
    this.pageable = new RestCollectionPage();
  }

  public RestCollection(List<T> list) {
    this.data = list;
    this.pageable = new RestCollectionPage(list.size());
    this.checkDataAndPageConsistency();
  }

  public RestCollection(List<T> list, RestCollectionPage page) {
    this.data = list;
    this.pageable = page;
    this.checkDataAndPageConsistency();
  }

  public RestCollection(List<T> list, RestCollectionPage page, List<FacetResult> facets) {
    this.data = list;
    this.pageable = page;
    if (facets != null) {
      this.facets.addAll(facets);
    }
    this.checkDataAndPageConsistency();
  }

  public RestCollection(Page<T> page, Pageable pageable) {
    this.data = page.getContent();
    this.pageable = new RestCollectionPage(page, pageable);
    if (page instanceof FacetPage) {
      this.facets.addAll(((FacetPage<T>) page).getFacetResults());
    }
    this.checkDataAndPageConsistency();
  }

  public RestCollection(String jsonString, Class<T> itemClass) {
    this.data = CollectionTool.getList(jsonString, itemClass);
    this.pageable = CollectionTool.getPage(jsonString);
    List<FacetResult> facets = CollectionTool.getFacetResults(jsonString);
    if (facets != null) {
      this.facets.addAll(facets);
    }
    this.checkDataAndPageConsistency();
  }

  private void checkDataAndPageConsistency() {
    if (this.data.size() > this.pageable.getSizePage()) {
      throw new SolidifyRuntimeException("List of data is larger than page size");
    }
  }

  @JsonProperty("_data")
  public List<T> getData() {
    return this.data;
  }

  @JsonProperty("_page")
  public RestCollectionPage getPage() {
    return this.pageable;
  }

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  @JsonProperty("_facets")
  public List<FacetResult> getFacets() {
    return this.facets;
  }

  @Override
  public String toString() {
    return "RestCollection: [" + this.getPage().getTotalItems() + " element(s)]";
  }
}
