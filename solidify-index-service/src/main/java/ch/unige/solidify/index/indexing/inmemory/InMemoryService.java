/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Service - InMemoryService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.index.indexing.inmemory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import jakarta.annotation.PostConstruct;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.model.index.IndexProperties;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.SearchCondition;

public abstract class InMemoryService<T extends IndexMetadata> extends IndexingService<T> {
  private static final Logger log = LoggerFactory.getLogger(InMemoryService.class);

  private final boolean generatedData;
  private final IndexingSettingsService<IndexProperties> settingsService;
  private final IndexData indexData;

  protected InMemoryService(
          IndexConfigProperties indexConfigProperties,
          IndexingSettingsService<IndexProperties> settingsService,
          IndexData indexData) {
    super(indexConfigProperties);
    this.settingsService = settingsService;
    this.indexData = indexData;
    // Create indexes
    for (final IndexProperties archiveIndex : this.settingsService.findAll(null)) {
      if (!this.indexData.getIndexes().containsKey(archiveIndex.getResId())) {
        final Map<String, IndexMetadata> metadata = new HashMap<>();
        this.indexData.getIndexes().put(archiveIndex.getResId(), metadata);
      }
    }
    // Generate data
    this.generatedData = indexConfigProperties.isGenerateData();
  }

  @PostConstruct
  private void loadData() {
    if (this.generatedData) {
      T objectMetadata = this.getMetadataObject();
      objectMetadata.setResId(SolidifyConstants.TEST_RES_ID);
      // Set metadata section
      final StringBuilder metadata = new StringBuilder();
      // @formatter:off
      metadata.append("{")
                .append("\"field\":\"value\",")
                .append("\"").append("aip-organizational-unit").append("\":\"").append(SolidifyConstants.TEST_RES_ID).append("\",")
                .append("\"").append("aip-access-level").append("\":\"").append("PUBLIC").append("\"")
              .append("}");
      // @formatter:on
      objectMetadata.setMetadata(metadata.toString());
      // Save for each index
      for (String index : this.getIndexList()) {
        objectMetadata.setIndex(index);
        this.save(objectMetadata);
        log.info("IndexMetadata create {}", objectMetadata);
      }
    }

  }

  @Override
  public boolean delete(T metadata) {
    this.checkIndex(metadata);
    final String metadataId = metadata.getResId();
    boolean result = true;
    for (String indexName : this.getIndexList()) {
      Map<String, IndexMetadata> index = this.indexData.getIndexValues(indexName);
      if (index.containsKey(metadataId)) {
        index.remove(metadataId, metadata);
      } else {
        result = false;
      }
    }
    return result;
  }

  @Override
  public void deleteList(List<String> ids) {
    for (String indexName : this.getIndexList()) {
      Map<String, IndexMetadata> index = this.indexData.getIndexValues(indexName);
      for (String id : ids) {
        index.remove(id);
      }
    }
  }

  @Override
  public long deleteByQuery(List<SearchCondition> conditions) {
    throw new NotImplementedException();
  }

  @Override
  public List<T> findAll(T search) {
    List<T> list = new ArrayList<>();
    for (String indexName : this.getIndexList()) {
      list.addAll(new ArrayList<>(((Map<String, T>) this.indexData.getIndexValues(indexName)).values()));
    }
    return list;
  }

  @Override
  public T findOne(String id) {
    T result = null;
    for (String indexName : this.getIndexList()) {
      result = ((Map<String, T>) this.indexData.getIndexValues(indexName)).get(id);
    }
    return result;
  }

  @Override
  public T findOne(String id, FieldsRequest fieldsRequest) {
    return this.findOne(id);
  }

  @Override
  public T save(T metadata) {
    this.checkIndex(metadata);
    final Map<String, T> index = ((Map<String, T>) this.indexData.getIndexValues(metadata.getIndex()));
    final String metadataId = metadata.getResId();
    if (!index.containsKey(metadataId)) {
      index.put(metadataId, metadata);
      return metadata;
    } else {
      return null;
    }
  }

  @Override
  public FacetPage<T> search(List<SearchCondition> conditions, List<FacetRequest> facetRequests, Pageable pageable,
          FieldsRequest fieldsRequest) {
    Page<T> page = this.findAll(null, pageable);
    return new FacetPage<>(page.getContent(), null, page.getPageable(), page.getTotalElements());
  }

  @Override
  public T update(T metadata) {
    this.checkIndex(metadata);
    final Map<String, T> index = ((Map<String, T>) this.indexData.getIndexValues(metadata.getIndex()));
    if (index.containsKey(metadata.getResId())) {
      index.replace(metadata.getResId(), metadata);
      return metadata;
    } else {
      return null;
    }
  }

  @Override
  protected void checkIndex() {
    super.checkIndex();
    for (String index : this.getIndexName().split(IndexConstants.INDEX_SEPARATOR)) {
      if (this.settingsService.findOne(index) == null) {
        final String msg = "Undefined index " + index;
        log.error(msg);
        throw new SolidifyRuntimeException(msg);
      }
    }
  }

}
