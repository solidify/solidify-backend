/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH - OAIController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.OAIUrlPath;
import ch.unige.solidify.business.OAIMetadataPrefixService;
import ch.unige.solidify.business.OAISetService;
import ch.unige.solidify.exception.SolidifyRuntimeException;

@RestController
@ConditionalOnProperty(prefix = "solidify.oai-pmh", name = "enable")
@RequestMapping(OAIUrlPath.OAI)
public class OAIController extends ModuleController {

  OAIController(OAIMetadataPrefixService oaiMetadataPrefixService, OAISetService oaiSetService) {
    super(OAIConstants.OAI_MODULE);
    // Module initialization
    try {
      oaiMetadataPrefixService.initDefaultData();
      oaiSetService.initDefaultData();
    } catch (final Exception e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

}
