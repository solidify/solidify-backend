/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - AssociationRemoteController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RemoteResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.service.CompositeResourceService;

@RootPermissions
public abstract class AssociationRemoteController<T extends Resource & RemoteResourceContainer, V extends Resource>
        extends SolidifyController {

  protected final CompositeResourceService<T> itemService;
  private final Class<V> subResourceType;

  protected AssociationRemoteController(CompositeResourceService<T> itemService, Class<V> subResourceType) {
    this.itemService = itemService;
    this.subResourceType = subResourceType;
  }

  @PostMapping
  public HttpEntity<List<V>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    boolean noError = true;
    final T parentItem = this.itemService.findOne(parentid);
    final List<V> list = new ArrayList<>();

    for (final String id : ids) {

      final V childItem = this.itemService.getSubResource(id, this.subResourceType);

      if (childItem == null) {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }

      noError &= parentItem.addItem(childItem);
      list.add(childItem);
    }

    // Save entity if no error
    if (noError) {
      this.itemService.save(parentItem);
      return new ResponseEntity<>(list, HttpStatus.CREATED);
    }
    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @DeleteMapping(SolidifyConstants.URL_ID)
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return this.deleteList(parentid, new String[] { id });
  }

  @DeleteMapping
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    boolean noError = true;
    final T parentItem = this.itemService.findOne(parentid);

    for (final String id : ids) {
      final V childItem = this.itemService.getSubResource(id, this.subResourceType);

      /*
       * Check the child Resource still exists
       */
      // TODO: if Resource does not exist anymore, an old resId reference can persist in parent item
      // property
      // Should we update RESTResource logic to clear the list of ids instead of passing a
      // RESTResource to removeItem() ?
      if (childItem == null) {
        throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND, "Child item not found");
      }

      /*
       * Check the child Resource is linked to the parent item
       */
      final List<String> idsList = parentItem.getSubResourceIds(this.subResourceType);
      if (idsList == null || !idsList.contains(id)) {
        throw new SolidifyHttpErrorException(HttpStatus.GONE, "Child item " + id + " gone");
      }

      /*
       * Remove link from parent Resource to child Resource
       */
      noError &= parentItem.removeItem(childItem);

    }

    if (noError) {
      this.itemService.save(parentItem);
      return new ResponseEntity<>(HttpStatus.OK);
    }

    throw new SolidifyHttpErrorException(HttpStatus.GONE, "");
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<V> get(@PathVariable String parentid, @PathVariable String id) {

    final T parentItem = this.itemService.findOne(parentid);

    /*
     * Check the requested child Resource is linked to the parent Resource before fetching it (we do not fetch Resources that are not linked to
     * the parent just to be coherent)
     */
    final List<String> ids = parentItem.getSubResourceIds(this.subResourceType);
    if (ids != null && ids.contains(id)) {

      final V item = this.itemService.getSubResource(id, this.subResourceType);

      if (item != null) {
        this.addLinks(parentid, item);
        return new ResponseEntity<>(item, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(HttpStatus.GONE);
      }
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  /**********************************************/

  @GetMapping
  public HttpEntity<RestCollection<V>> list(@PathVariable String parentid, Pageable pageable) {

    final T parentItem = this.itemService.findOne(parentid);

    final RestCollection<V> collection = this.itemService.getSubResources(parentItem, this.subResourceType, pageable);

    if (collection != null) {
      for (final V v : collection.getData()) {
        this.addLinks(parentid, v);
      }
      collection.add(linkTo(this.getClass(), parentid).withSelfRel());
      collection.add((Tool.parentLink((linkTo(this.getClass(), parentid)).toUriComponentsBuilder())).withRel(ActionName.PARENT));
      this.addPageLinks(linkTo(this.getClass(), parentid), collection, pageable);

      return new ResponseEntity<>(collection, HttpStatus.OK);

    } else {
      return new ResponseEntity<>(HttpStatus.GONE);
    }
  }

  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<V> update(@PathVariable String parentid, @PathVariable String id,
          @RequestBody V v2) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  protected void addLinks(String parentid, V v) {
    v.removeLinks();
    v.addLinks(linkTo(methodOn(this.getClass()).list(parentid, null)), false, false);
  }

}
