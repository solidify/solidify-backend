/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify BagIt - BagItTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.MultiValueMap;

import gov.loc.repository.bagit.conformance.BagProfileChecker;
import gov.loc.repository.bagit.creator.BagCreator;
import gov.loc.repository.bagit.domain.Bag;
import gov.loc.repository.bagit.exceptions.CorruptChecksumException;
import gov.loc.repository.bagit.exceptions.FileNotInPayloadDirectoryException;
import gov.loc.repository.bagit.exceptions.InvalidBagitFileFormatException;
import gov.loc.repository.bagit.exceptions.InvalidPayloadOxumException;
import gov.loc.repository.bagit.exceptions.MaliciousPathException;
import gov.loc.repository.bagit.exceptions.MissingBagitFileException;
import gov.loc.repository.bagit.exceptions.MissingPayloadDirectoryException;
import gov.loc.repository.bagit.exceptions.MissingPayloadManifestException;
import gov.loc.repository.bagit.exceptions.UnparsableVersionException;
import gov.loc.repository.bagit.exceptions.UnsupportedAlgorithmException;
import gov.loc.repository.bagit.exceptions.VerificationException;
import gov.loc.repository.bagit.exceptions.conformance.BagitVersionIsNotAcceptableException;
import gov.loc.repository.bagit.exceptions.conformance.FetchFileNotAllowedException;
import gov.loc.repository.bagit.exceptions.conformance.MetatdataValueIsNotAcceptableException;
import gov.loc.repository.bagit.exceptions.conformance.MetatdataValueIsNotRepeatableException;
import gov.loc.repository.bagit.exceptions.conformance.RequiredManifestNotPresentException;
import gov.loc.repository.bagit.exceptions.conformance.RequiredMetadataFieldNotPresentException;
import gov.loc.repository.bagit.exceptions.conformance.RequiredTagFileNotPresentException;
import gov.loc.repository.bagit.hash.StandardSupportedAlgorithms;
import gov.loc.repository.bagit.hash.SupportedAlgorithm;
import gov.loc.repository.bagit.reader.BagReader;
import gov.loc.repository.bagit.verify.BagVerifier;
import gov.loc.repository.bagit.writer.BagWriter;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyBagItCheckingException;
import ch.unige.solidify.exception.SolidifyBagItException;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public class BagItTool {
  private static final Logger log = LoggerFactory.getLogger(BagItTool.class);

  public static final String BAGIT_SEPRATOR = ": ";
  public static final String BAGIT_PAYLOAD = "Payload-Oxum";
  public static final String BAGIT_SIZE = "Bag-Size";
  public static final String BAGIT_FILE = "bagit.txt";
  public static final String BAGIT_INFO_FILE = "bag-info.txt";

  // Check conformance of BagIt package
  public static void check(String bagItProfile, String bagItPackage) {
    check(bagItProfile, read(bagItPackage));
  }

  public static void check(String bagItProfile, Bag bag) {
    final boolean ignoreHiddenFiles = true;
    final ExecutorService exeService = new ThreadPoolExecutor(0, 3000, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
    try (BagVerifier bagVerifier = new BagVerifier(exeService)) {
      bagVerifier.isValid(bag, ignoreHiddenFiles);
      if (bagItProfile != null) {
        BagProfileChecker.bagConformsToProfile(new ClassPathResource(bagItProfile).getInputStream(), bag);
      }
    } catch (final InterruptedException e) {
      // Restore interrupted state...
      Thread.currentThread().interrupt();
      throw new SolidifyRuntimeException("BagItTool#check was interrupted!", e);
    } catch (IOException | MissingPayloadManifestException | MissingBagitFileException
            | MissingPayloadDirectoryException | FileNotInPayloadDirectoryException
            | MaliciousPathException | UnsupportedAlgorithmException
            | InvalidBagitFileFormatException | CorruptChecksumException | VerificationException
            | FetchFileNotAllowedException | RequiredMetadataFieldNotPresentException
            | MetatdataValueIsNotAcceptableException | RequiredManifestNotPresentException
            | BagitVersionIsNotAcceptableException | RequiredTagFileNotPresentException
            | MetatdataValueIsNotRepeatableException e) {
      throw BagItTool.generateBagItCheckingException(bag, "Error while checking BagIt package: ", e);
    }

  }

  // Quick check conformance of BagIt package
  public static void quickCheck(String bagItProfile, String bagItPackage) {
    BagItTool.quickCheck(bagItProfile, read(bagItPackage));
  }

  public static void quickCheck(String bagItProfile, Bag bag) {
    try {
      if (BagVerifier.canQuickVerify(bag)) {
        BagVerifier.quicklyVerify(bag);
      } else {
        log.warn("Cannot proceed a quick check for BagIt package {}", bag);
      }
      if (bagItProfile != null) {
        BagProfileChecker.bagConformsToProfile(new ClassPathResource(bagItProfile).getInputStream(), bag);
      }
    } catch (IOException | FetchFileNotAllowedException
            | RequiredMetadataFieldNotPresentException | MetatdataValueIsNotAcceptableException
            | RequiredManifestNotPresentException | BagitVersionIsNotAcceptableException
            | RequiredTagFileNotPresentException | InvalidPayloadOxumException
            | MetatdataValueIsNotRepeatableException e) {
      throw BagItTool.generateBagItCheckingException(bag, "Error while quick checking BagIt package: ", e);
    }
  }

  // Read BagIt package
  public static Bag read(String bagItPackage) {
    Bag bag = null;
    final BagReader bagReader = new BagReader();
    try {
      bag = bagReader.read(Paths.get(bagItPackage));
    } catch (IOException | UnparsableVersionException | MaliciousPathException
            | UnsupportedAlgorithmException | InvalidBagitFileFormatException e) {
      throw BagItTool.generateBagItException("Error in reading BagIt package: ", e);
    }
    return bag;
  }

  // Bag size
  public static long getBagSize(Bag bag) {
    final String payloadOxum = getPayloadOxum(bag);
    return Long.parseLong(payloadOxum.substring(0, payloadOxum.indexOf('.')));
  }

  // Bag size
  public static long getBagFileNumber(Bag bag) {
    final String payloadOxum = getPayloadOxum(bag);
    return Long.parseLong(payloadOxum.substring(payloadOxum.indexOf('.') + 1));
  }

  // Create BagIt package
  public static Bag create(Path folderToBag, MultiValueMap<String, String> bagMetadata) {
    return create(folderToBag,
            Arrays.asList(StandardSupportedAlgorithms.MD5, StandardSupportedAlgorithms.SHA1,
                    StandardSupportedAlgorithms.SHA256, StandardSupportedAlgorithms.SHA512), true,
            bagMetadata);
  }

  public static Bag create(Path folderToBag, List<SupportedAlgorithm> algorithms, boolean includeHiddenFiles,
          MultiValueMap<String, String> bagMetadata) {
    try {
      // Create BagIt
      final Bag bag = BagCreator.bagInPlace(folderToBag, algorithms, includeHiddenFiles);
      bag.getMetadata().add(BAGIT_PAYLOAD, "");
      BagWriter.write(bag, folderToBag);
      // Add custom metadata to BagIt package
      if (bagMetadata != null) {
        for (Entry<String, List<String>> metadata : bagMetadata.entrySet()) {
          for (String value : metadata.getValue()) {
            bag.getMetadata().add(metadata.getKey(), value);
          }
        }
      }
      // Add BagIt Size
      final String payloadOxum = getPayloadOxum(bag);
      bag.getMetadata().add(BAGIT_SIZE, StringTool.formatSmartSize(Long.parseLong(payloadOxum.substring(0, payloadOxum.indexOf('.')))));
      // Update Bag
      BagWriter.write(bag, folderToBag);
      return bag;
    } catch (NoSuchAlgorithmException | IOException e) {
      throw BagItTool.generateBagItException("Error in creating BagIt package: ", e);
    }
  }

  public static Map<String, String> readBagItFile(Path bagInfofile) {
    final Map<String, String> infoList = new HashMap<>();
    try (Stream<String> lines = Files.lines(bagInfofile)) {
      lines.forEach(
              l -> infoList.put(
                      l.substring(0, l.indexOf(BAGIT_SEPRATOR)),
                      l.substring(l.indexOf(BAGIT_SEPRATOR) + BAGIT_SEPRATOR.length())));
    } catch (IOException e) {
      throw BagItTool.generateBagItException("Error in reading BagIt file: ", e);
    }
    return infoList;
  }

  private static String getPayloadOxum(Bag bag) {
    return bag.getMetadata().get(BAGIT_PAYLOAD).get(0);
  }

  /**
   * This constructor should not be called
   */
  private BagItTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  private static SolidifyBagItException generateBagItException(String errorLabel, Exception e) {
    return new SolidifyBagItException(errorLabel + e.getMessage(), e);
  }

  private static SolidifyBagItCheckingException generateBagItCheckingException(Bag bag, String errorLabel, Exception e) {
    final StringBuilder errorMessage = new StringBuilder(errorLabel + e.getMessage());
    if (bag != null) {
      errorMessage.append("\n");
      errorMessage.append(bag);
    }
    return new SolidifyBagItCheckingException(errorMessage.toString(), e);
  }

}
