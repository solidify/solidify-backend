/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ResourceNormalized.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;
import static ch.unige.solidify.SolidifyConstants.DB_ID_LENGTH;

import java.time.OffsetDateTime;
import java.util.Objects;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Size;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.listener.CacheListener;
import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.model.CreatedChangeInfo;
import ch.unige.solidify.model.LastModifiedChangeInfo;
import ch.unige.solidify.util.StringTool;

@MappedSuperclass
@EntityListeners({ CacheListener.class, AuditingEntityListener.class })
public abstract class ResourceNormalized extends Resource {

  @AttributeOverride(name = "when", column = @Column(name = "creationWhen", length = DB_DATE_LENGTH))
  @AttributeOverride(name = "who", column = @Column(name = "creationWho"))
  @Embedded
  @Valid
  protected CreatedChangeInfo creation = new CreatedChangeInfo();

  @AttributeOverride(name = "when", column = @Column(name = "lastUpdateWhen", length = DB_DATE_LENGTH))
  @AttributeOverride(name = "who", column = @Column(name = "lastUpdateWho"))
  @Embedded
  @Valid
  protected LastModifiedChangeInfo lastUpdate = new LastModifiedChangeInfo();

  @Schema(description = SolidifyConstants.RES_ID_DESCRIPTION)
  @Id
  @Size(max = DB_ID_LENGTH)
  @Column(length = DB_ID_LENGTH)
  private String resId;

  protected ResourceNormalized() {
    this.resId = StringTool.generateResId();
  }

  // *************************************************************************************

  @Override
  @JsonIgnore
  public String getCreatedBy() {
    return this.getCreation().getWho();
  }

  @Override
  public ChangeInfo getCreation() {
    return this.creation;
  }

  @Override
  @JsonIgnore
  public OffsetDateTime getCreationTime() {
    return this.getCreation().getWhen();
  }

  @Override
  public ChangeInfo getLastUpdate() {
    return this.lastUpdate;
  }

  @Override
  public String getResId() {
    return this.resId;
  }

  @Override
  @JsonIgnore
  public String getUpdatedBy() {
    return this.getLastUpdate().getWho();
  }

  @Override
  @JsonIgnore
  public OffsetDateTime getUpdateTime() {
    return this.getLastUpdate().getWhen();
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.getCreation().setWho(createdBy);
  }

  @Override
  public void setResId(String resId) {
    this.resId = resId;
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    this.getLastUpdate().setWho(updatedBy);
  }

  @Override
  public void updateTime() {
    // Let the @LastModifiedDate annotation do the job
  }

  @Override
  public String getCreatorName() {
    return this.getCreation().getFullName();
  }

  @Override
  public String getEditorName() {
    return this.getLastUpdate().getFullName();
  }

  @Override
  public void setCreatorName(String creatorName) {
    this.getCreation().setFullName(creatorName);
  }

  @Override
  public void setEditorName(String editorName) {
    this.getLastUpdate().setFullName(editorName);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.creation, this.lastUpdate, this.resId);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    ResourceNormalized other = (ResourceNormalized) obj;
    return Objects.equals(this.creation, other.creation) && Objects.equals(this.lastUpdate, other.lastUpdate)
            && Objects.equals(this.resId, other.resId);
  }

}
