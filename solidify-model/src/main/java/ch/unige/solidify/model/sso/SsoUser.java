/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SsoUser.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.sso;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonFormat;

import ch.unige.solidify.model.User;
import ch.unige.solidify.util.StringTool;

/**
 * The user of the application.
 */
public class SsoUser implements User, Serializable {
  // if current application is a micro-service, the src name (for example header name) used to set
  // userContext attribute
  public static final String CONNECTED_USER_CONTEXT_SOURCE_NAME = "solidify_ms_connected_user_context";

  public static final String CONNECTED_USER_UNIQUE_ID = "solidify_ms_connected_user_id";
  @Serial
  private static final long serialVersionUID = 9067304178593585691L;

  @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
  protected Set<String> affiliation = Collections.synchronizedSet(new HashSet<>());
  protected Map<String, String> allProperties;
  protected String cnindividu;
  protected String email;
  @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
  protected Set<String> employeeType = Collections.synchronizedSet(new HashSet<>());
  protected String firstName;
  protected String homeorganization;
  protected String homeorganizationtype;

  @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
  protected Set<String> ismemberOf = Collections.synchronizedSet(new HashSet<>());
  protected String lastName;
  protected String matriculationnumber;

  protected String name = null;
  protected String oauthToken;
  protected long oauthTokenExpiresTime;
  protected String orgunitdn;

  @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
  protected Set<String> roles = Collections.synchronizedSet(new HashSet<>());
  protected long systemId = 0;
  protected String unigechemployeeoucode;
  protected String unigechoucode;
  @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
  protected Set<String> unigeChStudentCategory = Collections.synchronizedSet(new HashSet<>());
  protected String unigechstudentoucode;
  protected String uniqueId;

  protected String userContext;

  public SsoUser() {
  }

  public SsoUser(final String name) {
    this();
    this.name = name;
  }

  public Set<String> getAffiliation() {
    return this.affiliation;
  }

  @Override
  public Map<String, String> getAllProperties() {
    return this.allProperties;
  }

  public String getCnindividu() {
    return this.cnindividu;
  }

  @Override
  public String getEmail() {
    return this.email;
  }

  public Set<String> getEmployeeType() {
    return this.employeeType;
  }

  @Override
  public String getFirstName() {
    return this.firstName;
  }

  @Override
  public String getHomeOrganization() {
    return this.homeorganization;
  }

  public String getHomeorganizationtype() {
    return this.homeorganizationtype;
  }

  public Set<String> getIsmemberOf() {
    return this.ismemberOf;
  }

  @Override
  public String getLastName() {
    return this.lastName;
  }

  public String getMatriculationnumber() {
    return this.matriculationnumber;
  }

  public String getName() {
    if (!StringTool.isNullOrEmpty(this.name)) {
      return this.name;
    }

    if (StringTool.isNullOrEmpty(this.firstName) && StringTool.isNullOrEmpty(this.lastName)) {
      return "";
    }

    if (StringTool.isNullOrEmpty(this.firstName)) {
      return this.lastName;
    }

    if (StringTool.isNullOrEmpty(this.lastName)) {
      return this.firstName;
    }

    return this.firstName + " " + this.lastName;
  }

  public String getOrgunitdn() {
    return this.orgunitdn;
  }

  @Override
  public Set<String> getRoles() {
    return this.roles;
  }

  public long getSystemId() {
    final String uid = this.getUniqueId();
    if (!StringTool.isNullOrEmpty(uid) && uid.contains("@")) {
      final String[] words = uid.split("@");
      final String id = words[0];
      try {
        return Long.parseLong(id);

      } catch (final Exception ex) {
        return 0;
      }
    }

    return 0;
  }

  public String getUnigechemployeeoucode() {
    return this.unigechemployeeoucode;
  }

  public String getUnigechoucode() {
    return this.unigechoucode;
  }

  public Set<String> getUnigeChStudentCategory() {
    return this.unigeChStudentCategory;
  }

  public String getUnigechstudentoucode() {
    return this.unigechstudentoucode;
  }

  @Override
  public String getUniqueId() {
    return this.uniqueId;
  }

  public String getUserContext() {
    return this.userContext;
  }

  public boolean hasRole(String role) {
    return this.roles.contains(role);
  }

  public boolean isEmployeeType(String employeeType) {
    return this.employeeType.contains(employeeType);
  }

  public boolean isEmpty() {
    return false;
  }

  public boolean isUnigeChStudentCategory(String unigeChStudentCategory) {
    return this.unigeChStudentCategory.contains(unigeChStudentCategory);
  }

  @Override
  public String toString() {
    return "SsoUser{" + "userContext='" + this.userContext + '\'' + ", firstName='" + this.firstName + '\''
            + ", lastName='" + this.lastName + '\'' + ", email='" + this.email + '\'' + ", homeorganization='"
            + this.homeorganization + '\'' + ", homeorganizationtype='" + this.homeorganizationtype + '\''
            + ", affiliation=" + this.affiliation + ", ismemberOf=" + this.ismemberOf + ", employeeType="
            + this.employeeType + ", unigechoucode='" + this.unigechoucode + '\'' + ", unigechstudentoucode='"
            + this.unigechstudentoucode + '\'' + ", unigechemployeeoucode='" + this.unigechemployeeoucode
            + '\'' + ", uniqueId='" + this.uniqueId + '\'' + ", unigeChStudentCategory="
            + this.unigeChStudentCategory + ", orgunitdn='" + this.orgunitdn + '\''
            + ", matriculationnumber='" + this.matriculationnumber + '\'' + ", name='" + this.name + '\''
            + ", roles=" + this.roles + ", systemId=" + this.systemId + ", oauthToken=" + this.oauthToken
            + ", oauthTokenExpiresTime='" + this.oauthTokenExpiresTime + '\'' + ", cnindividu="
            + this.cnindividu + '}';
  }

}
