/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ResourceCETTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.specification;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import jakarta.annotation.Resource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import ch.unige.solidify.test.config.MockUserJpaConfig;
import ch.unige.solidify.test.specification.dao.EntityCETMock;
import ch.unige.solidify.test.specification.dao.EntityCetMockRepository;

@DataJpaTest
@ContextConfiguration(classes = { MockUserJpaConfig.class })
class ResourceCETTest {

  @Resource
  private EntityCetMockRepository entityDefaultMockRepository;

  @BeforeEach
  void setup() {
    final EntityCETMock entity = new EntityCETMock();
    entity.setEntityId("1");
    entity.setLabel("init");
    entity.setUpdatedBy("firstUser");
    entity.setCreatedBy("unitTEST");
    this.entityDefaultMockRepository.save(entity);
  }

  public void Test_insert_metaData() {

    final Optional<EntityCETMock> entity2 = this.entityDefaultMockRepository.findById("1");
    assertNotNull(entity2.get().getCreationTime());
    assertNotNull(entity2.get().getUpdateTime());

  }

  @Test
  void Test_update() {
    final Optional<EntityCETMock> entity = this.entityDefaultMockRepository.findById("1");
    final EntityCETMock entityMock = entity.get();
    entityMock.setLabel("changed");
    entityMock.setUpdatedBy("new");
    this.entityDefaultMockRepository.save(entityMock);
    final Optional<EntityCETMock> entity2 = this.entityDefaultMockRepository.findById("1");
    assertEquals("changed", entity2.get().getLabel());
  }

}
