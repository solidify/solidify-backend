/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ResourceCET.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.listener.CacheListener;
import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.model.RegularChangeInfo;
import ch.unige.solidify.util.DateTool;

/**
 * This class is used for oracle tables with new naming convention for historical columns In this class, Date type History columns will use
 * LocalDateTime because in sql type in DB is timestamp.
 */
@MappedSuperclass
@EntityListeners({ CacheListener.class })
public abstract class ResourceCET extends Resource {
  @Transient
  protected ChangeInfo creation;
  @Transient
  protected ChangeInfo lastUpdate;
  @Column(name = "CREATED_AT")
  private LocalDateTime createdAt;
  @Column(name = "CREATED_BY")
  private String createdBy;
  @Column(name = "UPDATED_AT")
  private LocalDateTime updatedAt;
  @Column(name = "UPDATED_BY")
  private String updatedBy;

  @JsonIgnore
  private String editorName;

  @JsonIgnore
  private String creatorName;

  protected ResourceCET() {
    super();
    this.updatedAt = LocalDateTime.now();
    this.updatedBy = "";
    this.editorName = "";
    this.createdAt = LocalDateTime.now();
    this.createdBy = "";
    this.creatorName = "";
    this.creation = new RegularChangeInfo(DateTool.swissDateTime2offSetDate(this.createdAt),
            this.createdBy);
    this.lastUpdate = new RegularChangeInfo(DateTool.swissDateTime2offSetDate(this.updatedAt),
            this.updatedBy);

  }

  public LocalDateTime getCreatedAt() {
    return this.createdAt;
  }

  @Override
  @JsonIgnore(false)
  public String getCreatedBy() {
    if (this.getCreation().getWho() == null || this.getCreation().getWho().isEmpty()) {
      this.resetCreation();
    }
    return this.createdBy;
  }

  @Override
  @JsonIgnore(false)
  public String getCreatorName() {
    if (this.getCreation().getFullName() == null || this.getCreation().getFullName().isEmpty()) {
      this.resetCreation();
    }
    return this.creatorName;
  }

  @Override
  @JsonIgnore(false)
  public String getEditorName() {
    if (this.getLastUpdate().getFullName() == null || this.getLastUpdate().getFullName().isEmpty()) {
      this.resetCreation();
    }
    return this.creatorName;
  }

  @Transient
  @Override
  @JsonIgnore
  public ChangeInfo getCreation() {
    return this.creation;
  }

  @Override

  public OffsetDateTime getCreationTime() {
    return this.getCreation().getWhen();
  }

  public abstract String getEntityId();

  @Transient
  @Override
  @JsonIgnore
  public ChangeInfo getLastUpdate() {
    return this.lastUpdate;
  }

  @Override
  public String getResId() {
    return this.getEntityId();
  }

  public LocalDateTime getUpdatedAt() {
    return this.updatedAt;
  }

  @Override
  @JsonIgnore(false)
  public String getUpdatedBy() {
    if (this.getLastUpdate().getWho() == null || this.getLastUpdate().getWho().isEmpty()) {
      this.getLastUpdate().setWho(this.updatedBy);
      this.getLastUpdate().setWhen(DateTool.swissDateTime2offSetDate(this.updatedAt));
    }
    return this.updatedBy;
  }

  @Override
  public OffsetDateTime getUpdateTime() {
    return this.getLastUpdate().getWhen();
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.resetCreation();
    this.createdBy = createdBy;
    this.updateTime();
  }

  @Override
  public void setCreatorName(String creatorName) {
    this.resetCreation();
    this.creatorName = creatorName;
    this.updateTime();
  }

  /**
   * ResId is always the entityId. So setter will do nothing
   *
   * @param resId
   */
  @Override
  public void setResId(String resId) {
  }

  public void setUpdatedAt(LocalDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    this.updatedBy = updatedBy;
    this.updateTime();
  }

  @Override
  public void setEditorName(String editorName) {
    this.editorName = editorName;
    this.updateTime();
  }

  @Override
  public void updateTime() {
    this.setUpdatedAt(LocalDateTime.now());
    this.getLastUpdate().setWhen(OffsetDateTime.now(ZoneOffset.UTC));
    this.getLastUpdate().setWho(this.updatedBy);
  }

  private void resetCreation() {
    this.getCreation().setWho(this.createdBy);
    this.getCreation().setWhen(DateTool.swissDateTime2offSetDate(this.createdAt));
  }
}
