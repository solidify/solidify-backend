/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Service - ElasticsearchService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.index.indexing.elasticsearch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.ElasticsearchException;
import co.elastic.clients.elasticsearch._types.FieldValue;
import co.elastic.clients.elasticsearch._types.Result;
import co.elastic.clients.elasticsearch._types.SortOptions;
import co.elastic.clients.elasticsearch._types.SortOrder;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregate;
import co.elastic.clients.elasticsearch._types.aggregations.Aggregation;
import co.elastic.clients.elasticsearch._types.aggregations.StringTermsBucket;
import co.elastic.clients.elasticsearch._types.query_dsl.BoolQuery;
import co.elastic.clients.elasticsearch._types.query_dsl.Operator;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.TermsQueryField;
import co.elastic.clients.elasticsearch.core.BulkRequest;
import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.DeleteByQueryRequest;
import co.elastic.clients.elasticsearch.core.DeleteByQueryResponse;
import co.elastic.clients.elasticsearch.core.DeleteRequest;
import co.elastic.clients.elasticsearch.core.GetRequest;
import co.elastic.clients.elasticsearch.core.GetResponse;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.UpdateRequest;
import co.elastic.clients.elasticsearch.core.UpdateResponse;
import co.elastic.clients.elasticsearch.core.bulk.DeleteOperation;
import co.elastic.clients.elasticsearch.core.search.Hit;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.index.ElasticsearchClientProvider;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.FacetResult;
import ch.unige.solidify.rest.FacetResultValue;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchOperator;
import ch.unige.solidify.util.StringTool;

public abstract class ElasticsearchService<T extends IndexMetadata> extends IndexingService<T> {
  private static final Logger log = LoggerFactory.getLogger(ElasticsearchService.class);

  private final ElasticsearchClient esClient;

  private final Class<T> indexEntryClass;

  protected ElasticsearchService(IndexConfigProperties indexConfigProperties, ElasticsearchClientProvider elasticsearchClientProvider,
          Class<T> indexEntryClass) {
    super(indexConfigProperties);
    this.esClient = elasticsearchClientProvider.getElasticsearchClient();
    this.indexEntryClass = indexEntryClass;
  }

  @Override
  public T findOne(String id) {
    return this.findOne(id, null);
  }

  @Override
  public T findOne(String id, FieldsRequest fieldsRequest) {
    try {
      GetRequest.Builder builder = new GetRequest.Builder()
              .index(this.getIndexName())
              .id(id);
      this.configureFieldsRequest(builder, fieldsRequest);

      GetResponse<T> response = this.esClient.get(builder.build(), this.getIndexEntryClass());
      if (response.found()) {
        T indexEntry = response.source();
        indexEntry.setIndex(this.getIndexName());
        indexEntry.setResId(id);
        log.debug("{} {} found in index {}", this.getIndexEntryClass(), id, this.getIndexName());
        return indexEntry;
      } else {
        log.debug("{} {} not found in index {}", this.getIndexEntryClass(), id, this.getIndexName());
        return null;
      }
    } catch (ElasticsearchException e) {
      throw new SolidifyHttpErrorException(HttpStatus.valueOf(e.httpResponse().statusCode()), e.getMessage());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Error when searching " + id + " from index " + this.getIndexName(), e);
    }
  }

  @Override
  public List<T> findAll(T search) {
    try {
      SearchRequest.Builder builder = new SearchRequest.Builder()
              .index(Arrays.asList(this.getIndexName().split(IndexConstants.INDEX_SEPARATOR)))
              .query(q -> q.matchAll(m -> m))
              .size(RestCollectionPage.DEFAULT_SIZE_PAGE);

      SearchResponse<T> searchResponse = this.esClient.search(builder.build(), this.getIndexEntryClass());
      return this.getResults(searchResponse);
    } catch (ElasticsearchException e) {
      throw new SolidifyHttpErrorException(HttpStatus.valueOf(e.httpResponse().statusCode()), e.getMessage());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Error when searching all from index " + this.getIndexName(), e);
    }
  }

  @Override
  public Page<T> findAll(T search, Pageable pageable) {
    try {
      SearchRequest.Builder builder = new SearchRequest.Builder()
              .index(Arrays.asList(this.getIndexName().split(IndexConstants.INDEX_SEPARATOR)))
              .query(q -> q.matchAll(m -> m))
              .from(pageable.getPageSize() * pageable.getPageNumber())
              .size(pageable.getPageSize());

      this.configureSortAndPagination(builder, pageable);

      SearchResponse<T> searchResponse = this.esClient.search(builder.build(), this.getIndexEntryClass());
      return this.getPage(searchResponse, pageable);
    } catch (ElasticsearchException e) {
      throw new SolidifyHttpErrorException(HttpStatus.valueOf(e.httpResponse().statusCode()), e.getMessage());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Error when searching all from index " + this.getIndexName(), e);
    }
  }

  @Override
  public FacetPage<T> search(List<SearchCondition> conditions, List<FacetRequest> facetRequests, Pageable pageable,
          FieldsRequest fieldsRequest) {
    try {
      BoolQuery.Builder boolQueryBuilder = this.getBooleanQueryBuilder(conditions);

      SearchRequest.Builder builder = new SearchRequest.Builder()
              .index(Arrays.asList(this.getIndexName().split(IndexConstants.INDEX_SEPARATOR)))
              .query(Query.of(q -> q.bool(boolQueryBuilder.build())))
              .from(pageable.getPageSize() * pageable.getPageNumber())
              .size(pageable.getPageSize());

      this.configureFacets(builder, facetRequests);
      this.configureFieldsRequest(builder, fieldsRequest);
      this.configureSortAndPagination(builder, pageable);

      SearchResponse<T> searchResponse = this.esClient.search(builder.build(), this.getIndexEntryClass());

      FacetPage<T> page = this.getPage(searchResponse, pageable);
      this.sortFacetResults(page.getFacetResults(), facetRequests);
      return page;

    } catch (ElasticsearchException e) {
      throw new SolidifyHttpErrorException(HttpStatus.valueOf(e.httpResponse().statusCode()), e.getMessage());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Error when searching all from index " + this.getIndexName(), e);
    }
  }

  private FacetPage<T> getPage(SearchResponse<T> searchResponse, Pageable pageable) {
    final List<T> results = this.getResults(searchResponse);
    List<FacetResult> facetResults = this.getFacetResults(searchResponse);
    return new FacetPage<>(results, facetResults, pageable, searchResponse.hits().total().value());
  }

  private void configureFacets(SearchRequest.Builder builder, List<FacetRequest> facetRequests) {
    if (facetRequests != null && !facetRequests.isEmpty()) {
      for (FacetRequest facetRequest : facetRequests) {
        Aggregation aggregation = new Aggregation.Builder()
                .terms(t -> {
                  t.field(facetRequest.getField());
                  if (facetRequest.getMinCount() != null) {
                    t.minDocCount(facetRequest.getMinCount());
                  }
                  if (facetRequest.getLimit() != null) {
                    t.size(facetRequest.getLimit());
                  }
                  return t;
                }).build();
        builder.aggregations(facetRequest.getName(), aggregation);
      }
    }
  }

  private BoolQuery.Builder getBooleanQueryBuilder(List<SearchCondition> conditions) {
    final BoolQuery.Builder boolQuery = new BoolQuery.Builder();

    for (SearchCondition condition : conditions) {
      switch (condition.getBooleanClauseType()) {
        case MUST -> boolQuery.must(this.getQuery(condition));
        case FILTER -> boolQuery.filter(this.getQuery(condition));
        case MUST_NOT -> boolQuery.mustNot(this.getQuery(condition));
        case SHOULD -> boolQuery.should(this.getQuery(condition));
      }
    }
    return boolQuery;
  }

  private Query getQuery(SearchCondition searchCondition) {
    return switch (searchCondition.getType()) {
      case TERM -> this.getTermQuery(searchCondition);
      case MATCH -> this.getMatchQuery(searchCondition);
      case QUERY -> this.getQueryQuery(searchCondition);
      case SIMPLE_QUERY -> this.getSimpleQueryQuery(searchCondition);
      case WILDCARD -> this.getWildcardQuery(searchCondition);
      case RANGE -> this.getRangeQuery(searchCondition);
      case NESTED_BOOLEAN -> this.getNestedBooleanQuery(searchCondition);
    };
  }

  private Query getTermQuery(SearchCondition condition) {
    Query.Builder builder = new Query.Builder();
    if (!StringTool.isNullOrEmpty(condition.getField()) && !StringTool.isNullOrEmpty(condition.getValue())) {
      // 'term' query
      builder.term(b -> b.field(condition.getField()).value(condition.getValue()));
    } else if (!StringTool.isNullOrEmpty(condition.getField()) && !condition.getTerms().isEmpty()) {
      // 'terms' query
      builder.terms(b -> b.field(condition.getField())
              .terms(TermsQueryField.of(t -> t.value(condition.getTerms().stream().map(FieldValue::of).toList()))));
    } else {
      throw new SolidifyRuntimeException("Invalid 'term' search condition");
    }
    return builder.build();
  }

  private Query getMatchQuery(SearchCondition searchCondition) {
    Query.Builder builder = new Query.Builder();
    if (!StringTool.isNullOrEmpty(searchCondition.getField()) && !StringTool.isNullOrEmpty(searchCondition.getValue())) {
      // 'match' query
      builder.match(b -> {
        b.field(searchCondition.getField()).query(searchCondition.getValue());
        if (searchCondition.getSearchOperator() != null) {
          b.operator(this.searchOperatorToOperator(searchCondition.getSearchOperator()));
        }
        return b;
      });

    } else if (!searchCondition.getMultiMatchFields().isEmpty() && !StringTool.isNullOrEmpty(searchCondition.getValue())) {
      // 'multi_match' query
      builder.multiMatch(b -> {
        b.fields(searchCondition.getMultiMatchFields()).query(searchCondition.getValue());
        if (searchCondition.getSearchOperator() != null) {
          b.operator(Operator.valueOf(searchCondition.getSearchOperator().name()));
        }
        return b;
      });
    } else {
      throw new SolidifyRuntimeException("Invalid 'match' or 'multi_match' search condition");
    }
    return builder.build();
  }

  private Query getWildcardQuery(SearchCondition searchCondition) {
    if (StringTool.isNullOrEmpty(searchCondition.getField()) || StringTool.isNullOrEmpty(searchCondition.getValue())) {
      throw new SolidifyRuntimeException("Invalid 'wildcard' search condition");
    }

    // 'wildcard' query
    Query.Builder builder = new Query.Builder();
    builder.wildcard(b -> b.field(searchCondition.getField()).value(searchCondition.getValue()));
    return builder.build();
  }

  private Query getRangeQuery(SearchCondition condition) {
    if (StringTool.isNullOrEmpty(condition.getLowerValue()) && StringTool.isNullOrEmpty(condition.getUpperValue())) {
      throw new SolidifyRuntimeException("Invalid 'range' search condition");
    }

    Query.Builder builder = new Query.Builder();
    builder.range(b -> b.number(m -> {
      m.field(condition.getField());
      if (!StringTool.isNullOrEmpty(condition.getLowerValue())) {
        m.gte(Double.valueOf(condition.getLowerValue()));
      }
      if (!StringTool.isNullOrEmpty(condition.getUpperValue())) {
        m.lte(Double.valueOf(condition.getUpperValue()));
      }
      return m;
    }));

    return builder.build();
  }

  private Query getQueryQuery(SearchCondition condition) {
    if (StringTool.isNullOrEmpty(condition.getValue())) {
      throw new SolidifyRuntimeException("Invalid 'query' search condition");
    }

    Query.Builder builder = new Query.Builder();
    builder.queryString(b -> b.query(condition.getValue()));
    return builder.build();
  }

  private Query getSimpleQueryQuery(SearchCondition condition) {
    if (StringTool.isNullOrEmpty(condition.getValue())) {
      throw new SolidifyRuntimeException("Invalid 'query' search condition");
    }

    Query.Builder builder = new Query.Builder();
    builder.simpleQueryString(b -> b.query(condition.getValue()));
    return builder.build();
  }

  private Query getNestedBooleanQuery(SearchCondition condition) {
    if (condition.getNestedConditions().isEmpty()) {
      throw new SolidifyRuntimeException("Invalid nested boolean condition");
    }
    BoolQuery.Builder nestedBoolQueryBuilder = this.getBooleanQueryBuilder(condition.getNestedConditions());
    return new Query.Builder().bool(nestedBoolQueryBuilder.build()).build();
  }

  @Override
  public T save(T indexEntry) {
    this.checkMetadata(indexEntry);
    final IndexRequest.Builder<Map<String, Object>> builder = new IndexRequest.Builder<Map<String, Object>>()
            .index(this.getIndexName())
            .id(indexEntry.getResId())
            .document(indexEntry.getMetadata());
    try {
      final IndexResponse response = this.esClient.index(builder.build());
      if (response.result() == Result.Created || response.result() == Result.Updated) {
        return indexEntry;
      } else {
        throw new SolidifyRuntimeException(
                "The following metadata could not be indexed in index '" + this.getIndexName() + "': \n\n" + indexEntry.getMetadata()
                        + "\nResult: " + response.result());
      }
    } catch (ElasticsearchException e) {
      throw new SolidifyHttpErrorException(HttpStatus.valueOf(e.httpResponse().statusCode()), e.getMessage());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Error when saving " + indexEntry.getResId() + " in index " + this.getIndexName(), e);
    }
  }

  @Override
  public T update(T indexEntry) {
    UpdateRequest.Builder<T, Map<String, Object>> updateBuilder = new UpdateRequest.Builder<T, Map<String, Object>>()
            .index(this.getIndexName())
            .id(indexEntry.getResId())
            .doc(indexEntry.getMetadata());
    try {
      UpdateResponse<T> updateResponse = this.esClient.update(updateBuilder.build(), this.getIndexEntryClass());
      if (updateResponse.result() == Result.Updated || updateResponse.result() == Result.NoOp) {
        return indexEntry;
      } else {
        throw new SolidifyRuntimeException(
                "The following metadata could not be updated in index '" + this.getIndexName() + "': \n\n" + indexEntry.getMetadata()
                        + "\nResult: " + updateResponse.result());
      }
    } catch (ElasticsearchException e) {
      throw new SolidifyHttpErrorException(HttpStatus.valueOf(e.httpResponse().statusCode()), e.getMessage());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("IOException when deleting metadata '" + indexEntry.getResId() + "' in " + this.getIndexName(), e);
    }
  }

  @Override
  public boolean delete(T indexEntry) {
    DeleteRequest.Builder deleteBuilder = new DeleteRequest.Builder();
    deleteBuilder.index(this.getIndexName());
    deleteBuilder.id(indexEntry.getResId());
    try {
      this.esClient.delete(deleteBuilder.build());
      return true;
    } catch (ElasticsearchException e) {
      throw new SolidifyHttpErrorException(HttpStatus.valueOf(e.httpResponse().statusCode()), e.getMessage());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("IOException when deleting metadata '" + indexEntry.getResId() + "' in " + this.getIndexName(), e);
    }
  }

  @Override
  public void deleteList(List<String> ids) {
    BulkRequest.Builder bulkRequestBuilder = new BulkRequest.Builder();

    for (String id : ids) {
      bulkRequestBuilder.operations(op -> op
              .delete(new DeleteOperation.Builder()
                      .index(this.getIndexName())
                      .id(id)
                      .build()));
    }

    try {
      BulkResponse bulkResponse = this.esClient.bulk(bulkRequestBuilder.build());

      if (bulkResponse.errors()) {
        StringBuilder sb = new StringBuilder("Some delete failed: ");
        bulkResponse.items().forEach(item -> {
          if (item.error() != null) {
            sb.append("Could not delete ID" + item.id() + ": " + item.error().reason());
          }
        });
        throw new SolidifyRuntimeException(sb.toString());
      }
    } catch (ElasticsearchException e) {
      throw new SolidifyHttpErrorException(HttpStatus.valueOf(e.httpResponse().statusCode()), e.getMessage());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("IOException when deleting list of metadata", e);
    }
  }

  @Override
  public long deleteByQuery(List<SearchCondition> conditions) {
    BoolQuery.Builder boolQueryBuilder = this.getBooleanQueryBuilder(conditions);
    Query query = new Query.Builder().bool(boolQueryBuilder.build()).build();

    DeleteByQueryRequest.Builder deleteBuilder = new DeleteByQueryRequest.Builder()
            .index(this.getIndexName())
            .query(query);

    try {
      DeleteByQueryResponse response = this.esClient.deleteByQuery(deleteBuilder.build());
      if (!response.failures().isEmpty()) {
        throw new SolidifyRuntimeException("Failures when deleting index entries by query: " + response.failures());
      }
      return response.deleted();
    } catch (ElasticsearchException e) {
      throw new SolidifyHttpErrorException(HttpStatus.valueOf(e.httpResponse().statusCode()), e.getMessage());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("IOException when deleting index entries by query", e);
    }
  }

  /**
   * Configure which fields must be included in ES response
   *
   * @param builder
   * @param fieldsRequest
   */
  private void configureFieldsRequest(GetRequest.Builder builder, FieldsRequest fieldsRequest) {
    if (fieldsRequest != null && (!fieldsRequest.getIncludes().isEmpty() || !fieldsRequest.getExcludes().isEmpty())) {
      builder.sourceIncludes(fieldsRequest.getIncludes());
      builder.sourceExcludes(fieldsRequest.getExcludes());
    }
  }

  private void configureFieldsRequest(SearchRequest.Builder builder, FieldsRequest fieldsRequest) {
    if (fieldsRequest != null && (!fieldsRequest.getIncludes().isEmpty() || !fieldsRequest.getExcludes().isEmpty())) {
      builder.source(src -> src.filter(f -> f.excludes(fieldsRequest.getExcludes()).includes(fieldsRequest.getIncludes())));
    }
  }

  private void configureSortAndPagination(SearchRequest.Builder builder, Pageable pageable) {
    if (pageable.getSort().isSorted()) {
      List<SortOptions> sortOptionsList = new ArrayList<>();
      pageable.getSort().forEach(sort -> sortOptionsList.add(SortOptions.of(s -> s
              .field(f -> f
                      .field(sort.getProperty())
                      .order(this.directionToSortOrder(sort.getDirection()))))));
      builder.sort(sortOptionsList);
    }
  }

  private SortOrder directionToSortOrder(Sort.Direction direction) {
    return switch (direction) {
      case ASC -> SortOrder.Asc;
      case DESC -> SortOrder.Desc;
    };
  }

  private Operator searchOperatorToOperator(SearchOperator searchOperator) {
    return switch (searchOperator) {
      case OR -> Operator.Or;
      case AND -> Operator.And;
    };
  }

  private List<T> getResults(SearchResponse<T> searchResponse) {
    List<Hit<T>> hits = searchResponse.hits().hits();
    final List<T> results = new ArrayList<>();
    for (Hit<T> hit : hits) {
      T document = hit.source();
      document.setIndex(hit.index());
      document.setResId(hit.id());
      results.add(document);
    }
    return results;
  }

  private List<FacetResult> getFacetResults(SearchResponse<T> searchResponse) {
    Map<String, Aggregate> aggregations = searchResponse.aggregations();
    if (aggregations != null) {
      List<FacetResult> facetResults = new ArrayList<>();
      for (Map.Entry<String, Aggregate> entry : aggregations.entrySet()) {
        FacetResult facetResult = new FacetResult(entry.getKey());
        Aggregate aggregate = entry.getValue();
        for (StringTermsBucket bucket : aggregate.sterms().buckets().array()) {
          facetResult.getValues().add(new FacetResultValue(bucket.key().stringValue(), bucket.docCount()));
          log.trace("Facet '{}' : {} -> {}", facetResult.getName(), bucket.key().stringValue(), bucket.docCount());
        }
        facetResults.add(facetResult);
      }
      return facetResults;
    }
    return Collections.emptyList();
  }

  /**
   * Sort facets results as they were requested (Elasticsearch does not necessarily return facets in the same order as requested)
   *
   * @param facetResults
   * @param sortedFacetRequests
   */
  private void sortFacetResults(List<FacetResult> facetResults, List<FacetRequest> sortedFacetRequests) {
    if (!facetResults.isEmpty() && sortedFacetRequests != null) {
      List<String> sortedFacetNames = sortedFacetRequests.stream().map(FacetRequest::getName).toList();
      facetResults.sort((left, right) -> {
        int pos1 = sortedFacetNames.indexOf(left.getName());
        int pos2 = sortedFacetNames.indexOf(right.getName());
        return Integer.compare(pos1, pos2);
      });
    }
  }

  private void checkMetadata(T t) {
    if (!t.check()) {
      final String msg = "Missing fields in " + t;
      log.error(msg);
      throw new SolidifyRuntimeException(msg);
    }
  }

  protected Class<T> getIndexEntryClass() {
    return this.indexEntryClass;
  }
}
