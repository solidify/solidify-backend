/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - KeywordsConverter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.converter;

import static java.util.Collections.emptyList;

import java.util.Arrays;
import java.util.List;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.StringTool;

@Converter
public class KeywordsConverter implements AttributeConverter<List<String>, String> {

  @Override
  public String convertToDatabaseColumn(List<String> keywords) {
    if (keywords == null || keywords.isEmpty()) {
      return null;
    }
    final StringBuilder keywordBuilder = new StringBuilder();
    for (final String keyword : keywords) {
      keywordBuilder.append(keyword);
      keywordBuilder.append(SolidifyConstants.FIELD_SEP);
    }
    return keywordBuilder.toString();
  }

  @Override
  public List<String> convertToEntityAttribute(String keywords) {
    if (StringTool.isNullOrEmpty(keywords)) {
      return emptyList();
    }
    return Arrays.asList(keywords.split(SolidifyConstants.FIELD_SEP));
  }
}
