/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Service - ElasticsearchClientProvider.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.index;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.util.StringTool;

@Profile("index-elasticsearch")
@Service
public class ElasticsearchClientProvider {

  private final ElasticsearchClient elasticsearchClient;

  public ElasticsearchClientProvider(IndexConfigProperties indexConfigProperties) {
    final String userName = indexConfigProperties.getConfig().getUsername();
    final String userPassword = indexConfigProperties.getConfig().getPassword();

    // Create the low-level client
    final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
    if (!StringTool.isNullOrEmpty(userName)) {
      credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(userName, userPassword));
    }
    String scheme = "http";
    if (indexConfigProperties.getConfig().isSslEnabled()) {
      scheme = "https";
    }
    HttpHost host = new HttpHost(indexConfigProperties.getConfig().getHost(), indexConfigProperties.getConfig().getPort(), scheme);
    RestClientBuilder restClientBuilder = RestClient.builder(host);
    restClientBuilder.setHttpClientConfigCallback(httpClientBuilder -> httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider));
    RestClient restClient = restClientBuilder.build();

    // Create the transport with a Jackson mapper
    JacksonJsonpMapper mapper = new JacksonJsonpMapper();
    mapper.objectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.objectMapper().registerModule(new JavaTimeModule());
    mapper.objectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    ElasticsearchTransport transport = new RestClientTransport(restClient, mapper);

    this.elasticsearchClient = new ElasticsearchClient(transport);
  }

  public ElasticsearchClient getElasticsearchClient() {
    return this.elasticsearchClient;
  }

}
