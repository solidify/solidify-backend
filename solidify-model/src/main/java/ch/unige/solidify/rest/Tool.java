/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - Tool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import org.springframework.hateoas.Link;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.SolidifyConstants;

public class Tool {

  public static Link filter(UriComponentsBuilder ucb, String field, String value) {
    ucb.queryParam(field, value);
    return Link.of(ucb.toUriString());
  }

  public static Link link(UriComponentsBuilder ucb) {
    return Link.of(ucb.toUriString());
  }

  public static Link pageLink(UriComponentsBuilder ucb, long size, long page) {
    ucb.queryParam(ActionName.SIZE, size);
    ucb.queryParam(ActionName.PAGE, page);
    return Link.of(ucb.toUriString());
  }

  public static Link parentLink(UriComponentsBuilder ucb) {
    final String uri = ucb.toUriString();
    final String parenturi = uri.substring(0, uri.lastIndexOf('/'));
    return Link.of(parenturi);
  }

  public static Link referenceLink(UriComponentsBuilder ucb, String module) {
    final String uri = ucb.toUriString();
    final String[] list = uri.split("/");
    list[list.length - 5] = module;
    list[list.length - 4] = list[list.length - 2];
    list[list.length - 3] = list[list.length - 1];
    final StringBuilder refUri = new StringBuilder(list[0]);
    for (int i = 1; i < list.length - 2; i++) {
      refUri.append("/").append(list[i]);
    }
    return Link.of(refUri.toString());
  }

  public static Link sort(UriComponentsBuilder ucb, String criterion) {
    ucb.queryParam(ActionName.SORT, criterion + ",desc");
    return Link.of(ucb.toUriString());
  }

  private Tool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
