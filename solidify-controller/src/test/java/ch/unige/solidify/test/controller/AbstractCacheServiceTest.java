/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - AbstractCacheServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.rest.CacheSubResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.test.controller.service.rest.TestAbstractCacheService;

class AbstractCacheServiceTest {

  private class Container extends ResourceNormalized {

    private List<Container> childrenList = new ArrayList<>();

    private Set<Container> childrenSet = new HashSet<>();

    private Container subContainer1;

    private Container subContainer2;

    private List<String> otherList = List.of("123", "456", "789");

    private Set<Integer> otherSet = Set.of(1, 2, 3);

    private List<String> nullList;

    public Container(String resId) {
      this.setResId(resId);
    }

    public Container getSubContainer1() {
      return this.subContainer1;
    }

    public void setSubContainer1(Container subContainer1) {
      this.subContainer1 = subContainer1;
    }

    public Container getSubContainer2() {
      return this.subContainer2;
    }

    public void setSubContainer2(Container subContainer2) {
      this.subContainer2 = subContainer2;
    }

    public List<Container> getChildrenList() {
      return this.childrenList;
    }

    public Container getChildFromList(String resId) {
      return this.childrenList.stream().filter(c -> c.getResId().equals(resId)).findFirst().get();
    }

    public Set<Container> getChildrenSet() {
      return this.childrenSet;
    }

    public Container getChildFromSet(String resId) {
      return this.childrenSet.stream().filter(c -> c.getResId().equals(resId)).findFirst().get();
    }

    public List<String> getOtherList() {
      return this.otherList;
    }

    public Set<Integer> getOtherSet() {
      return this.otherSet;
    }

    public List<String> getNullList() {
      return this.nullList;
    }

    @Override
    public void init() {

    }

    @Override
    public String managedBy() {
      return null;
    }
  }

  private class TestContainer extends ResourceNormalized {
    protected Container subContainerA = new Container("a");

    protected Container subContainerB = new Container("b");

    public Container getSubContainerA() {
      return this.subContainerA;
    }

    public void setSubContainerA(Container subContainerA) {
      this.subContainerA = subContainerA;
    }

    public Container getSubContainerB() {
      return this.subContainerB;
    }

    public void setSubContainerB(Container subContainerB) {
      this.subContainerB = subContainerB;
    }

    @Override
    public void init() {

    }

    @Override
    public String managedBy() {
      return null;
    }

  }

  private class TestContainerChild extends TestContainer implements CacheSubResourceContainer {
    @Override
    public List<Resource> getSubResourcesForCache() {
      return List.of(this.subContainerB);
    }
  }

  /**
   * 1
   * |-- list --|-- 2
   * |          |   |--list--|------ 4
   * |          |            |-------5
   * |          |
   * |          |-- 3
   * |              |--list--|------ 6
   * |                       |------ 7
   * |                       |------ 11
   * |                                |-- set -- | -- 1
   * |
   * |-- subContainer1 -- 8
   * |                    |-- subContainer2 -- 10
   * |                    |
   * |                    |-- set -- |-- 13
   * |                               |-- 14
   * |
   * |-- subContainer2 -- 9
   * |                    |-- subContainer1 -- 11
   * |                    |-- subContainer2 -- 12
   * |
   * |
   * |
   *
   * @return
   */
  private Container getTopContainer() {
    Container container1 = new Container("1");
    Container container2 = new Container("2");
    Container container3 = new Container("3");
    Container container4 = new Container("4");
    Container container5 = new Container("5");
    Container container6 = new Container("6");
    Container container7 = new Container("7");
    Container container8 = new Container("8");
    Container container9 = new Container("9");
    Container container10 = new Container("10");
    Container container11 = new Container("11");
    Container container12 = new Container("12");
    Container container13 = new Container("13");
    Container container14 = new Container("14");

    container1.setSubContainer1(container8);
    container1.setSubContainer2(container9);
    container1.getChildrenList().add(container2);
    container1.getChildrenList().add(container3);

    container2.getChildrenList().add(container4);
    container2.getChildrenList().add(container5);

    container3.getChildrenList().add(container6);
    container3.getChildrenList().add(container7);
    container3.getChildrenList().add(container11);

    container8.setSubContainer2(container10);
    container8.getChildrenSet().add(container13);
    container8.getChildrenSet().add(container14);

    container9.setSubContainer1(container11);
    container9.setSubContainer2(container12);

    container11.getChildrenSet().add(container1);

    return container1;
  }

  @Test
  void testContainers() {
    Container container1 = this.getTopContainer();
    assertEquals(2, container1.getChildrenList().size());
    assertTrue(container1.getChildrenList().stream().anyMatch(c -> c.getResId().equals("2")));
    assertTrue(container1.getChildrenList().stream().anyMatch(c -> c.getResId().equals("3")));
    Container container2 = container1.getChildrenList().stream().filter(c -> c.getResId().equals("2")).findFirst().get();
    Container container3 = container1.getChildrenList().stream().filter(c -> c.getResId().equals("3")).findFirst().get();

    assertTrue(container2.getChildrenList().stream().anyMatch(c -> c.getResId().equals("4")));
    assertTrue(container2.getChildrenList().stream().anyMatch(c -> c.getResId().equals("5")));
    assertEquals(3, container3.getChildrenList().size());
    assertTrue(container3.getChildrenList().stream().anyMatch(c -> c.getResId().equals("6")));
    assertTrue(container3.getChildrenList().stream().anyMatch(c -> c.getResId().equals("7")));

    Container container8 = container1.getSubContainer1();
    Container container9 = container1.getSubContainer2();
    assertEquals("8", container8.getResId());
    assertEquals("9", container9.getResId());

    assertEquals(2, container8.getChildrenSet().size());
    assertTrue(container8.getChildrenSet().stream().anyMatch(c -> c.getResId().equals("13")));
    assertTrue(container8.getChildrenSet().stream().anyMatch(c -> c.getResId().equals("14")));

    assertTrue(container9.getChildrenList().isEmpty());

    assertNull(container1.getSubContainer1().getSubContainer1());
    assertEquals("10", container1.getSubContainer1().getSubContainer2().getResId());

    assertEquals("11", container1.getSubContainer2().getSubContainer1().getResId());
    assertEquals("12", container1.getSubContainer2().getSubContainer2().getResId());
  }

  @Test
  void testGetResourcesFromPropertiesAndCollections() {
    TestAbstractCacheService cacheService = new TestAbstractCacheService();

    Container container1 = this.getTopContainer();
    List<Resource> resource1Properties = cacheService.getResourcesFromPropertiesAndCollections(container1);
    List<String> expectedResIds = List.of("2", "3", "8", "9");
    assertEquals(expectedResIds.size(), resource1Properties.size());
    for (String id : expectedResIds) {
      assertTrue(resource1Properties.stream().anyMatch(r -> r.getResId().equals(id)));
    }

    List<Resource> resource8Properties = cacheService.getResourcesFromPropertiesAndCollections(container1.getSubContainer1());
    expectedResIds = List.of("10", "13", "14");
    assertEquals(expectedResIds.size(), resource8Properties.size());
    for (String id : expectedResIds) {
      assertTrue(resource8Properties.stream().anyMatch(r -> r.getResId().equals(id)));
    }

    List<Resource> resource9Properties = cacheService.getResourcesFromPropertiesAndCollections(container1.getSubContainer2());
    expectedResIds = List.of("11", "12");
    assertEquals(expectedResIds.size(), resource9Properties.size());
    for (String id : expectedResIds) {
      assertTrue(resource9Properties.stream().anyMatch(r -> r.getResId().equals(id)));
    }
  }

  @Test
  void testGetSubResources() {
    TestAbstractCacheService cacheService = new TestAbstractCacheService();

    TestContainer testContainer = new TestContainer();
    List<Resource> subResources = cacheService.getSubResources(testContainer);
    List<String> expectedResIds = List.of("a", "b");
    assertEquals(expectedResIds.size(), subResources.size());
    for (String id : expectedResIds) {
      assertTrue(subResources.stream().anyMatch(r -> r.getResId().equals(id)));
    }

    TestContainerChild testContainerChild = new TestContainerChild();
    subResources = cacheService.getSubResources(testContainerChild);
    expectedResIds = List.of("b");
    assertEquals(expectedResIds.size(), subResources.size());
    for (String id : expectedResIds) {
      assertTrue(subResources.stream().anyMatch(r -> r.getResId().equals(id)));
    }
  }

  @Test
  void testAddSubResourcesIds() {
    TestAbstractCacheService cacheService = new TestAbstractCacheService();

    Container container1 = this.getTopContainer();
    cacheService.addSubResourcesIds(container1);
    Map<String, Set<String>> cacheMap = cacheService.getRemoteResourceContainersMap();
    List<String> expectedResIds = List.of("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14");
    assertEquals(expectedResIds.size(), cacheMap.keySet().size());
    for (String id : expectedResIds) {
      assertTrue(cacheMap.containsKey(id));
    }
    assertEquals(1, cacheMap.get("1").size());
    assertTrue(cacheMap.get("1").contains("11"));

    assertEquals(1, cacheMap.get("2").size());
    assertTrue(cacheMap.get("2").contains("1"));

    assertEquals(1, cacheMap.get("3").size());
    assertTrue(cacheMap.get("3").contains("1"));

    assertEquals(1, cacheMap.get("4").size());
    assertTrue(cacheMap.get("4").contains("2"));

    assertEquals(1, cacheMap.get("5").size());
    assertTrue(cacheMap.get("5").contains("2"));

    assertEquals(1, cacheMap.get("6").size());
    assertTrue(cacheMap.get("6").contains("3"));

    assertEquals(1, cacheMap.get("7").size());
    assertTrue(cacheMap.get("7").contains("3"));

    assertEquals(1, cacheMap.get("8").size());
    assertTrue(cacheMap.get("8").contains("1"));

    assertEquals(1, cacheMap.get("9").size());
    assertTrue(cacheMap.get("9").contains("1"));

    assertEquals(1, cacheMap.get("10").size());
    assertTrue(cacheMap.get("10").contains("8"));

    assertEquals(2, cacheMap.get("11").size());
    assertTrue(cacheMap.get("11").contains("3"));
    assertTrue(cacheMap.get("11").contains("9"));

    assertEquals(1, cacheMap.get("12").size());
    assertTrue(cacheMap.get("12").contains("9"));

    assertEquals(1, cacheMap.get("13").size());
    assertTrue(cacheMap.get("13").contains("8"));

    assertEquals(1, cacheMap.get("14").size());
    assertTrue(cacheMap.get("14").contains("8"));

    /***************************************************************
     * Update tree of resources
     * (replace id=14 by id=11)
     */
    container1.getSubContainer1().getChildrenSet().clear();
    container1.getSubContainer1().getChildrenSet().add(new Container("13"));
    container1.getSubContainer1().getChildrenSet().add(new Container("11"));

    cacheService.addSubResourcesIds(container1);
    cacheMap = cacheService.getRemoteResourceContainersMap();

    expectedResIds = List.of("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13");
    assertEquals(expectedResIds.size(), cacheMap.keySet().size());
    for (String id : expectedResIds) {
      assertTrue(cacheMap.containsKey(id));
    }
    // 14 is not in any container anymore --> it is removed from cache map
    assertFalse(cacheMap.containsKey("14"));

    // 11 has 3 containers now
    assertEquals(3, cacheMap.get("11").size());
    assertTrue(cacheMap.get("11").contains("3"));
    assertTrue(cacheMap.get("11").contains("8"));
    assertTrue(cacheMap.get("11").contains("9"));
  }

  @Test
  void testGetResourceIdsToClean() {
    TestAbstractCacheService cacheService = new TestAbstractCacheService();
    Container container1 = this.getTopContainer();
    cacheService.addSubResourcesIds(container1);

    Set<String> idsToClean = cacheService.getResourceIdsToClean("1");
    List<String> expectedResIds = List.of("1", "11", "3", "9");
    assertEquals(expectedResIds.size(), idsToClean.size());
    for (String id : expectedResIds) {
      assertTrue(idsToClean.contains(id));
    }

    idsToClean = cacheService.getResourceIdsToClean("8");
    expectedResIds = List.of("8", "1", "11", "3", "9");
    assertEquals(expectedResIds.size(), idsToClean.size());
    for (String id : expectedResIds) {
      assertTrue(idsToClean.contains(id));
    }

    idsToClean = cacheService.getResourceIdsToClean("10");
    expectedResIds = List.of("10", "8", "1", "11", "3", "9");
    assertEquals(expectedResIds.size(), idsToClean.size());
    for (String id : expectedResIds) {
      assertTrue(idsToClean.contains(id));
    }

    /***************************************************************
     * Update tree of resources
     * (remove 1 from 11 to break infinite loop)
     */
    assertEquals(1, container1.getChildFromList("3").getChildFromList("11").getChildrenSet().size());
    container1.getChildFromList("3").getChildFromList("11").getChildrenSet().remove(container1);
    assertTrue(container1.getChildFromList("3").getChildFromList("11").getChildrenSet().isEmpty());

    cacheService.addSubResourcesIds(container1);

    idsToClean = cacheService.getResourceIdsToClean("1");
    expectedResIds = List.of("1");
    assertEquals(expectedResIds.size(), idsToClean.size());
    for (String id : expectedResIds) {
      assertTrue(idsToClean.contains(id));
    }

    idsToClean = cacheService.getResourceIdsToClean("8");
    expectedResIds = List.of("8", "1");
    assertEquals(expectedResIds.size(), idsToClean.size());
    for (String id : expectedResIds) {
      assertTrue(idsToClean.contains(id));
    }

    idsToClean = cacheService.getResourceIdsToClean("10");
    expectedResIds = List.of("10", "8", "1");
    assertEquals(expectedResIds.size(), idsToClean.size());
    for (String id : expectedResIds) {
      assertTrue(idsToClean.contains(id));
    }
  }

}
