/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID - OrcidClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.solidify.service;

import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.controller.OrcidController;
import ch.unige.solidify.exception.SolidifyOrcidWorkMissingInProfileException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.OrcidToken;
import ch.unige.solidify.model.OrcidWorkUpdateMode;
import ch.unige.solidify.model.xml.orcid.v3_0.activities.Works;
import ch.unige.solidify.model.xml.orcid.v3_0.bulk.Bulk;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Work;

@Service
@ConditionalOnBean(OrcidController.class)
public class OrcidClientService {

  private static final Logger log = LoggerFactory.getLogger(OrcidClientService.class);

  private static final String BEARER = "Bearer";
  private static final String PUT_CODE_PARAMETER = "put_code";
  private static final String ORCID_PARAMETER = "orcid";

  private final String apiUrl;

  public OrcidClientService(SolidifyProperties solidifyProperties) {
    this.apiUrl = solidifyProperties.getOrcid().getApiBaseUrl();
  }

  public Works getWorks(OrcidToken orcidToken) {
    try {
      RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
      RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();

      Map<String, String> parameters = new HashMap<>();
      parameters.put(ORCID_PARAMETER, orcidToken.getOrcid());

      HttpHeaders headers = new HttpHeaders();
      headers.add(HttpHeaders.AUTHORIZATION, BEARER + " " + orcidToken.getAccessToken());
      HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(headers);

      return client.exchange(this.apiUrl + "{orcid}/works", HttpMethod.GET, entity, Works.class, parameters).getBody();
    } catch (RuntimeException e) {
      log.error("An error occurred while getting list of Works from ORCID", e);
      return null;
    }
  }

  public Work getWork(BigInteger putCode, OrcidToken orcidToken) {
    try {
      RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
      RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();

      Map<String, String> parameters = new HashMap<>();
      parameters.put(ORCID_PARAMETER, orcidToken.getOrcid());
      parameters.put(PUT_CODE_PARAMETER, putCode.toString());

      HttpHeaders headers = new HttpHeaders();
      headers.add(HttpHeaders.AUTHORIZATION, BEARER + " " + orcidToken.getAccessToken());
      HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(headers);

      final Bulk bulk = client.exchange(this.apiUrl + "{orcid}/works/{put_code}", HttpMethod.GET, entity, Bulk.class,
              parameters).getBody();
      if (bulk != null && !bulk.getWorkOrError().isEmpty() && bulk.getWorkOrError().get(0) instanceof Work work) {
        return work;
      }
      throw new SolidifyResourceNotFoundException("Work with putCode " + putCode + " could not be found on ORCID API");
    } catch (SolidifyResourceNotFoundException e) {
      throw e;
    } catch (Exception e) {
      log.error("An error occurred while getting a Work from ORCID", e);
      return null;
    }
  }

  public BigInteger uploadWork(Work work, OrcidToken orcidToken) {
    return this.uploadWork(work, orcidToken, null, null);
  }

  public BigInteger uploadWork(Work work, OrcidToken orcidToken, BigInteger putCode, OrcidWorkUpdateMode updateMode) {
    final RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    final RestTemplate client = restTemplateBuilder.rootUri(this.apiUrl).build();
    final HttpHeaders headers = new HttpHeaders();
    headers.add(HttpHeaders.AUTHORIZATION, BEARER + " " + orcidToken.getAccessToken());
    headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_XML + ";charset=UTF-8");
    if (putCode != null) {
      // ORCID Work has already been synchronized once -> update it in ORCID profile
      if (OrcidWorkUpdateMode.CREATE_IF_MISSING == updateMode) {
        return this.updateOrCreateOrcidRecord(work, putCode, client, headers, orcidToken);
      } else {
        return this.updateOrcidRecord(work, putCode, client, headers, orcidToken);
      }
    } else {
      // ORCID Work has never been synchronized yet -> create it in ORCID profile
      return this.createOrcidRecord(work, client, headers, orcidToken);
    }
  }

  private BigInteger updateOrcidRecord(Work work, BigInteger putCode, RestTemplate client, HttpHeaders headers, OrcidToken orcidToken) {
    try {
      work.setPutCode(putCode);
      final String workAsXml = this.serializeWorkToXml(work);
      final HttpEntity<String> request = new HttpEntity<>(workAsXml, headers);
      client.put(this.apiUrl + orcidToken.getOrcid() + "/work/" + putCode, request, String.class);
      return putCode;
    } catch (HttpClientErrorException.NotFound e) {
      throw new SolidifyOrcidWorkMissingInProfileException(
              "Work '" + work.getTitle().getTitle() + "' with putCode " + putCode + " could not be updated as it doesn't exist in ORCID profile "
                      + orcidToken.getOrcid());
    }
  }

  private BigInteger createOrcidRecord(Work work, RestTemplate client, HttpHeaders headers, OrcidToken orcidToken) {
    // Create ORCID record
    final String workAsXml = this.serializeWorkToXml(work);
    final HttpEntity<String> request = new HttpEntity<>(workAsXml, headers);
    URI locationUri = client.postForEntity(this.apiUrl + orcidToken.getOrcid() + "/work", request, String.class).getHeaders().getLocation();
    if (locationUri != null) {
      String path = locationUri.getPath();
      return BigInteger.valueOf(Long.parseLong(path.substring(path.lastIndexOf("/") + 1)));
    } else {
      return null;
    }
  }

  /**
   * Update an existing Work in an ORCID profile. If the Work doesn't exist anymore, it creates a new one.
   *
   * @param work
   * @param putCode
   * @param client
   * @param headers
   * @param orcidToken
   * @return
   */
  private BigInteger updateOrCreateOrcidRecord(Work work, BigInteger putCode, RestTemplate client, HttpHeaders headers, OrcidToken orcidToken) {
    try {
      return this.updateOrcidRecord(work, putCode, client, headers, orcidToken);
    } catch (SolidifyOrcidWorkMissingInProfileException e) {
      // ORCID record has been removed from the ORCID servers since it was synchronized --> create a new one
      work.setPutCode(null);
      return this.createOrcidRecord(work, client, headers, orcidToken);
    }
  }

  private String serializeWorkToXml(Work work) {
    try {
      final JAXBContext jaxbContext = JAXBContext.newInstance(Work.class);
      final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
      StringWriter sw = new StringWriter();
      jaxbMarshaller.marshal(work, sw);
      return sw.toString();
    } catch (RuntimeException | JAXBException e) {
      throw new SolidifyRuntimeException("Unable to serialize ORCID Work to XML", e);
    }
  }
}
