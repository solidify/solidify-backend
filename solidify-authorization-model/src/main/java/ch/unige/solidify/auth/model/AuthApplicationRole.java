/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Authorization Model - AuthApplicationRole.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model;

import java.util.Arrays;
import java.util.List;

public class AuthApplicationRole implements ApplicationRole {
  public static final ApplicationRole ROOT = new AuthApplicationRole("Root", 10);
  public static final ApplicationRole TRUSTED_CLIENT = new AuthApplicationRole("Trusted_client", 20);
  public static final ApplicationRole ADMIN = new AuthApplicationRole("Admin", 30);
  public static final ApplicationRole USER = new AuthApplicationRole("User", 40);

  public static final String ROOT_ID = "ROOT";
  public static final String ADMIN_ID = "ADMIN";
  public static final String TRUSTED_CLIENT_ID = "TRUSTED_CLIENT";
  public static final String USER_ID = "USER";
  public static final String AUTH = "auth";
  public static final String TOKEN_AUTH = "tokenAuth";

  private static final List<ApplicationRole> applicationRoleList = Arrays.asList(ROOT, ADMIN, TRUSTED_CLIENT, USER);

  private String resId;
  private String name;
  private int level;

  public AuthApplicationRole() {
  }

  public AuthApplicationRole(String name, int level) {
    this.resId = name.toUpperCase();
    this.name = name;
    this.level = level;
  }

  public AuthApplicationRole(ApplicationRole applicationRole) {
    this.resId = applicationRole.getResId();
    this.name = applicationRole.getName();
    this.level = applicationRole.getLevel();
  }

  @Override
  public String getResId() {
    return this.resId;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public int getLevel() {
    return this.level;
  }

  public static ApplicationRole getRoleFromName(String roleName) {
    if (roleName.equalsIgnoreCase(ADMIN.getName())) {
      return ADMIN;
    }
    if (roleName.equalsIgnoreCase(ROOT.getName())) {
      return ROOT;
    }
    if (roleName.equalsIgnoreCase(TRUSTED_CLIENT.getName())) {
      return TRUSTED_CLIENT;
    }
    if (roleName.equalsIgnoreCase(USER.getName())) {
      return USER;
    }
    throw new RuntimeException("Unkown role : " + roleName);
  }

  public static List<ApplicationRole> getApplicationRoleList() {
    return applicationRoleList;
  }

  @Override
  public String toString() {
    final StringBuilder builder = new StringBuilder();
    builder.append("Application Role [name=")
            .append(this.name)
            .append("]")
            .append("[resId=")
            .append(this.resId)
            .append("]");
    return builder.toString();
  }
}
