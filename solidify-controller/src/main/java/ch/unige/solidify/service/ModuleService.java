/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - ModuleService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.TargetClassAware;
import org.springframework.aop.framework.Advised;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import ch.unige.solidify.controller.ControllerWithHateoasHome;
import ch.unige.solidify.controller.ModuleController;
import ch.unige.solidify.exception.SolidifyRuntimeException;

@Service
public class ModuleService implements ApplicationContextAware {
  private static final Logger log = LoggerFactory.getLogger(ModuleService.class);
  private final List<ModuleController> moduleControllerList = new ArrayList<>();

  public List<ModuleController> getModuleControllerList() {
    return this.moduleControllerList;
  }

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) {

    final String[] modList = applicationContext.getBeanNamesForType(ModuleController.class);
    log.info("Modules found : {}", modList.length);
    for (final String b : modList) {
      log.info("Finding module: {}", b);
      this.moduleControllerList.add((ModuleController) applicationContext.getBean(b));
    }
    final String[] resList = applicationContext.getBeanNamesForType(ControllerWithHateoasHome.class);
    for (final String r : resList) {
      log.info("Finding resource: {}", r);
      if (!this.addResource((ControllerWithHateoasHome) applicationContext.getBean(r))) {
        log.error("Resource ( {} ) unassigned to a module", r);
      }
    }
  }

  private boolean addResource(ControllerWithHateoasHome resource) {

    final String name = resource.getClass().getName();
    final String module = name.substring(0, name.lastIndexOf('.')).toLowerCase();
    for (final ModuleController mod : this.moduleControllerList) {
      if (mod.getClass().getCanonicalName().toLowerCase().startsWith(module)) {
        if (log.isDebugEnabled()) {
          log.debug("Handling resource controller {}", resource);
        }
        if (resource instanceof TargetClassAware) {
          try {
            mod.addResource(
                    (ControllerWithHateoasHome) ((Advised) resource).getTargetSource().getTarget());
          } catch (final Exception e) {
            throw new SolidifyRuntimeException("Unproxy object resource rejection. Resource: ["
                    + ((Advised) resource).getTargetClass() + "]", e);
          }
        } else {
          mod.addResource(resource);
        }

        return true;
      }
    }
    return false;
  }

}
