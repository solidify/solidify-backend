/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ResourceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.specification;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import jakarta.annotation.Resource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;

import ch.unige.solidify.test.config.MockUserJpaConfig;
import ch.unige.solidify.test.specification.dao.EntityMock;
import ch.unige.solidify.test.specification.dao.EntityMockRepository;

@DataJpaTest
@ContextConfiguration(classes = { MockUserJpaConfig.class })
class ResourceTest {
  @Resource
  private EntityMockRepository entityMockRepository;

  @BeforeEach
  void setup() {
    final EntityMock entity = new EntityMock();
    entity.setEntityId("1");
    entity.setLabel("first");
    entity.setCreatedBy("unitTEST");
    this.entityMockRepository.save(entity);
  }

  @Test
  void Test_delete() {
    this.entityMockRepository.deleteById("1");
    final Optional<EntityMock> entity2 = this.entityMockRepository.findById("1");
    assertFalse(entity2.isPresent());
  }

  @Test
  void Test_insert_metaData() {

    final Optional<EntityMock> entity2 = this.entityMockRepository.findById("1");
    assertNotNull(entity2.get().getCreationTime());
    assertNotNull(entity2.get().getUpdateTime());
  }

  @Test
  void Test_update() {
    final Optional<EntityMock> entity = this.entityMockRepository.findById("1");
    final EntityMock entityMock = entity.get();
    entityMock.getUpdateTime();
    entityMock.setLabel("changed");
    this.entityMockRepository.save(entityMock);
    final Optional<EntityMock> entity2 = this.entityMockRepository.findById("1");
    assertEquals("changed", entity2.get().getLabel());
  }
}
