/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - SolidifyRestClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.StringTool;

public abstract class SolidifyRestClientService {
  private static final Logger log = LoggerFactory.getLogger(SolidifyRestClientService.class);

  private static final String RES_ID_SHOULD_NOT_BE_NULL_NOR_EMPTY = "resId should not be null nor empty";
  private final AbstractRestClientTool restClientTool;
  private final ResponseErrorHandler responseErrorHandler;

  protected SolidifyRestClientService(AbstractRestClientTool restClientTool, ResponseErrorHandler responseErrorHandler) {
    this.restClientTool = restClientTool;
    this.responseErrorHandler = responseErrorHandler;
  }

  public void checkResId(String resId) {
    if (StringTool.isNullOrEmpty(resId)) {
      throw new SolidifyRuntimeException(RES_ID_SHOULD_NOT_BE_NULL_NOR_EMPTY);
    }
  }

  // *******************
  // ** GET Functions **
  // *******************

  public String getResource(String url) {
    return this.getResource(url, String.class);
  }

  public <T> T getResource(String url, Class<T> clazz) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.GET);
    try {
      return restClt.getForObject(url, clazz);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  public String getResource(String url, Pageable pageable) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.GET);

    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    // Add Pagination info
    if (pageable != null) {
      uriBuilder.queryParam(ActionName.PAGE, pageable.getPageNumber());
      uriBuilder.queryParam(ActionName.SIZE, pageable.getPageSize());
      // Add Sort info
      pageable.getSort()
              .stream()
              .forEach(s -> uriBuilder.queryParam(ActionName.SORT, s.getProperty() + "," + s.getDirection().toString().toLowerCase()));
    }
    URI uri = uriBuilder.build(true).encode().toUri();

    try {
      this.logModuleCall(url);
      return restClt.getForObject(uri, String.class);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  public <T> List<T> getResourceList(String url, Class<T[]> clazz) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.GET);
    try {
      return Arrays.asList(restClt.getForObject(url, clazz));
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  public <T> RestCollection<T> getResourceList(String url, Class<T> clazz, Pageable pageable) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.GET);
    url = this.completeUrlWithPagination(url, pageable);
    this.logModuleCall(url);
    try {
      final String jsonResult = restClt.getForObject(url, String.class);
      final List<T> list = CollectionTool.getList(jsonResult, clazz);
      final RestCollectionPage page = CollectionTool.getPage(jsonResult);
      return new RestCollection<>(list, page);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  public <T> List<T> getAllResources(String url, Class<T> clazz) {
    Pageable page = PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE);
    List<T> subResourceList = new ArrayList<>();
    RestCollection<T> subResourceCollection;
    // List all sub-resources
    do {
      subResourceCollection = this.getResourceList(url, clazz, page);
      page = page.next();
      for (final T subResource : subResourceCollection.getData()) {
        subResourceList.add(subResource);
      }
    } while (subResourceCollection.getPage().hasNext());

    return subResourceList;
  }

  // ********************
  // ** HEAD Functions **
  // ********************

  public HttpStatusCode headResource(String url) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.HEAD);
    try {
      restClt.headForHeaders(url);
    } catch (final HttpClientErrorException e) {
      return e.getStatusCode();
    }
    return HttpStatus.OK;
  }

  /**
   * Perform a HEAD request and return the Content-Length header value
   *
   * @param url The url of the resource to get Content-Length from
   * @return The value of the Content-Length header
   */
  public long headContentLength(String url, TokenUsage withToken) {
    final RestTemplate restClt;
    if (withToken == TokenUsage.WITH_TOKEN) {
      restClt = this.getRestTemplate(HttpMethod.HEAD);
    } else if (withToken == TokenUsage.WITHOUT_TOKEN) {
      restClt = new RestTemplate();
    } else {
      throw new SolidifyRuntimeException("Unknown token usage value: " + withToken);
    }
    HttpHeaders requestHeaders = new HttpHeaders();
    // explicitly tell server to disable encoding to have more probability to get a Content-Length value
    requestHeaders.add("Accept-Encoding", "");
    final HttpEntity<?> httpEntity = new HttpEntity<>(requestHeaders);
    try {
      HttpEntity<Void> response = restClt.exchange(url, HttpMethod.HEAD, httpEntity, Void.class);
      HttpHeaders headers = response.getHeaders();
      List<String> lengths = headers.get("Content-Length");
      if (lengths != null && lengths.size() == 1) {
        return Long.parseLong(lengths.get(0));
      } else if (lengths != null && lengths.size() > 1) {
        throw new SolidifyRuntimeException("too many 'Content-Length' headers received");
      } else {
        throw new SolidifyRuntimeException("No 'Content-Length' headers received");
      }
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  // ********************
  // ** PATCH Function **
  // ********************

  public <T> T patchResource(String url, T updatedResource, Class<T> clazz) {
    return this.patchResourceWithObject(url, updatedResource, clazz);
  }

  public <T> T patchResource(String url, Map<String, Object> updatedResource, Class<T> clazz) {
    return this.patchResourceWithObject(url, updatedResource, clazz);
  }

  private <T> T patchResourceWithObject(String url, Object updatedResource, Class<T> clazz) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.PATCH);
    try {
      return restClt.patchForObject(url, updatedResource, clazz);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  // ********************
  // ** POST Functions **
  // ********************

  public <T> T postResource(String url, Class<T> clazz) {
    final RestTemplate restClt = this.restClientTool.getClient();
    try {
      return restClt.postForObject(url, HttpEntity.EMPTY, clazz);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  public <T> T postResource(String url, T newResource, Class<T> clazz) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.POST);
    final HttpEntity<?> httpEntity = new HttpEntity<>(newResource);
    try {
      return restClt.postForObject(url, httpEntity, clazz);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  public <T> T postObject(String url, Object request, Class<T> responseType) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.POST);
    try {
      return restClt.postForObject(url, request, responseType);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  public Result postResourceAction(String url) {
    return this.postResource(url, Result.class);
  }

  public <T, V> T postResourceOther(String url, V otherResource, Class<T> clazz) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.POST);
    final HttpEntity<?> httpEntity = new HttpEntity<>(otherResource);
    try {
      return restClt.postForObject(url, httpEntity, clazz);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  public <T> RestCollection<T> postResourcesList(String url, Object request, Class<T> clazz, Pageable pageable) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.POST);
    url = this.completeUrlWithPagination(url, pageable);
    this.logModuleCall(url);
    try {
      final String jsonResult = restClt.postForObject(url, request, String.class);
      return new RestCollection<>(jsonResult, clazz);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  // **********************
  // ** DELETE Functions **
  // **********************

  public void deleteResource(String url) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.DELETE);
    try {
      restClt.delete(url, Void.class);
    } catch (final HttpClientErrorException e) {
      throw this.getSolidifyExceptionFromHttpClientErrorException(url, e);
    }
  }

  public void deleteResourceList(String url, List<String> ids) {
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.DELETE);
    final HttpEntity<?> httpEntity = new HttpEntity<>(ids);
    try {
      restClt.exchange(url, HttpMethod.DELETE, httpEntity, String.class);
    } catch (final HttpClientErrorException e) {
      if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
        throw new SolidifyResourceNotFoundException(url);
      }
      final String message = this.getResourceErrorMessage(url);
      throw new SolidifyRestException(message, e);
    }
  }

  // *********************
  // ** Misc. Functions **
  // *********************

  public <T> T uploadContent(String url, org.springframework.core.io.Resource resource, String parameterName, Class<T> clazz) {

    final MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

    map.add(parameterName, resource);

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.MULTIPART_FORM_DATA);

    final HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
    final RestTemplate restClt = this.getRestTemplate(HttpMethod.PATCH);
    final ResponseEntity<T> res = restClt.exchange(url, HttpMethod.POST, requestEntity, clazz);

    return res.getBody();
  }

  public void downloadContent(String url, Path destData, TokenUsage tokenUsage) {
    this.restClientTool.downloadContent(URI.create(url), destData, tokenUsage);
  }

  protected RestTemplate getRestTemplate(HttpMethod httpMethod) {
    final RestTemplate restClt = this.restClientTool.getClient();
    if (!httpMethod.equals(HttpMethod.GET) && !httpMethod.equals(HttpMethod.HEAD)) {
      restClt.setErrorHandler(this.responseErrorHandler);
    }
    return restClt;
  }

  public AbstractRestClientTool getRestClientTool() {
    return this.restClientTool;
  }

  /**
   * Complete an URL with pagination query parameters
   *
   * @param url      url to complete
   * @param pageable pageable object used to complete the URL
   * @return completed URL
   */
  protected String completeUrlWithPagination(String url, Pageable pageable) {
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);

    if (pageable != null) {
      final MultiValueMap<String, String> existingQueryParams = new LinkedMultiValueMap<>(uriBuilder.build().getQueryParams());
      existingQueryParams.remove(ActionName.PAGE);
      existingQueryParams.remove(ActionName.SIZE);
      existingQueryParams.remove(ActionName.SORT);

      final int page = pageable.getPageNumber();
      final int size = pageable.getPageSize();
      final Sort sort = pageable.getSort();

      final MultiValueMap<String, String> pageParams = new LinkedMultiValueMap<>();
      pageParams.set(ActionName.PAGE, Integer.toString(page));
      pageParams.set(ActionName.SIZE, Integer.toString(size));

      // TODO: support sorting on many fields ?
      for (final Order order : sort) {
        final String sortProperty = order.getProperty();
        final String sortDirection = order.getDirection().toString();
        pageParams.set(ActionName.SORT, sortProperty + "," + sortDirection);
      }
      uriBuilder.replaceQueryParams(existingQueryParams).queryParams(pageParams);
    }
    return uriBuilder.build().toString();
  }

  private String getResourceErrorMessage(String url) {
    return "Error in getting resource (" + url + ")";
  }

  private void logModuleCall(String moduleUrl) {
    try {
      final URL url = new URL(moduleUrl);
      final String userInfo = url.getUserInfo();
      if (userInfo != null) {
        final String userInfoWithoutPassword = userInfo.substring(0, userInfo.indexOf(':')) + ":**********";
        final String urlWithoutPassword = url.getProtocol() + "://" + userInfoWithoutPassword + "@" + url.getHost() + url
                .getPort() + url.getPath() + "?" + url.getQuery();
        log.info("Module call GET: {}", urlWithoutPassword);
      } else {
        log.info("Module call GET: {}", moduleUrl);
      }
    } catch (final MalformedURLException e) {
      log.error("Malformed url : {}", moduleUrl, e);
    }
  }

  private SolidifyRuntimeException getSolidifyExceptionFromHttpClientErrorException(String url, HttpClientErrorException e) {
    if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
      return new SolidifyResourceNotFoundException(url);
    }
    final String message = this.getResourceErrorMessage(url);
    return new SolidifyRestException(message, e);
  }
}
