/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - GitInfoProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:git.properties")
@ConfigurationProperties(prefix = "git")
public class GitInfoProperties {

  private String branch;
  private Build build = new Build();
  private Commit commit = new Commit();
  private Remote remote = new Remote();

  public static class Build {
    private String host;
    private String time;
    private String version;
    private User user = new User();

    public static class User {
      private String email;
      private String name;

      public String getEmail() {
        return email;
      }

      public void setEmail(String email) {
        this.email = email;
      }

      public String getName() {
        return name;
      }

      public void setName(String name) {
        this.name = name;
      }
    }

    public String getHost() {
      return host;
    }

    public void setHost(String host) {
      this.host = host;
    }

    public String getTime() {
      return time;
    }

    public void setTime(String time) {
      this.time = time;
    }

    public String getVersion() {
      return version;
    }

    public void setVersion(String version) {
      this.version = version;
    }

    public User getUser() {
      return user;
    }

    public void setUser(User user) {
      this.user = user;
    }
  }
  public static class Commit {
    private String time;
    private Id id = new Id();
    private Message message = new Message();
    private User user = new User();

    public static class Id {
      private String abbrev;

      public String getAbbrev() {
        return abbrev;
      }

      public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
      }
    }
    public static class Message {
      private String full;

      public String getFull() {
        return full;
      }

      public void setFull(String full) {
        this.full = full;
      }
    }
    public static class User {
      private String email;
      private String name;

      public String getEmail() {
        return email;
      }

      public void setEmail(String email) {
        this.email = email;
      }

      public String getName() {
        return name;
      }

      public void setName(String name) {
        this.name = name;
      }
    }

    public String getTime() {
      return time;
    }

    public void setTime(String time) {
      this.time = time;
    }

    public Id getId() {
      return id;
    }

    public void setId(Id id) {
      this.id = id;
    }

    public Message getMessage() {
      return message;
    }

    public void setMessage(Message message) {
      this.message = message;
    }

    public User getUser() {
      return user;
    }

    public void setUser(User user) {
      this.user = user;
    }
  }
  public static class Remote {
    private Origin origin = new Origin();
    public static class Origin {
      private String url;

      public String getUrl() {
        return url;
      }

      public void setUrl(String url) {
        this.url = url;
      }
    }

    public Origin getOrigin() {
      return origin;
    }

    public void setOrigin(Origin origin) {
      this.origin = origin;
    }
  }

  public String getBranch() {
    return branch;
  }

  public void setBranch(String branch) {
    this.branch = branch;
  }

  public Build getBuild() {
    return build;
  }

  public void setBuild(Build build) {
    this.build = build;
  }

  public Commit getCommit() {
    return commit;
  }

  public void setCommit(Commit commit) {
    this.commit = commit;
  }

  public Remote getRemote() {
    return remote;
  }

  public void setRemote(Remote remote) {
    this.remote = remote;
  }
}
