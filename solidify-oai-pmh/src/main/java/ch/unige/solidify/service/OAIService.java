/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH - OAIService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

import javax.xml.namespace.QName;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.event.EventListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.OAIProperties;
import ch.unige.solidify.business.OAIMetadataPrefixService;
import ch.unige.solidify.business.OAISetService;
import ch.unige.solidify.controller.OAIController;
import ch.unige.solidify.exception.OAIException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.message.oai.RefreshOaiPmhMessage;
import ch.unige.solidify.model.oai.MetadataContent;
import ch.unige.solidify.model.oai.OAIMetadataPrefix;
import ch.unige.solidify.model.oai.OAIRecord;
import ch.unige.solidify.model.oai.OAIRepositoryInfo;
import ch.unige.solidify.model.oai.OAISet;
import ch.unige.solidify.model.oai.OAIToken;
import ch.unige.solidify.model.xml.oai.v2.DeletedRecordType;
import ch.unige.solidify.model.xml.oai.v2.DescriptionType;
import ch.unige.solidify.model.xml.oai.v2.GetRecordType;
import ch.unige.solidify.model.xml.oai.v2.GranularityType;
import ch.unige.solidify.model.xml.oai.v2.HeaderType;
import ch.unige.solidify.model.xml.oai.v2.IdentifyType;
import ch.unige.solidify.model.xml.oai.v2.ListIdentifiersType;
import ch.unige.solidify.model.xml.oai.v2.ListMetadataFormatsType;
import ch.unige.solidify.model.xml.oai.v2.ListRecordsType;
import ch.unige.solidify.model.xml.oai.v2.ListSetsType;
import ch.unige.solidify.model.xml.oai.v2.MetadataFormatType;
import ch.unige.solidify.model.xml.oai.v2.MetadataType;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHerrorType;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHerrorcodeType;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHtype;
import ch.unige.solidify.model.xml.oai.v2.RecordType;
import ch.unige.solidify.model.xml.oai.v2.RequestType;
import ch.unige.solidify.model.xml.oai.v2.SetType;
import ch.unige.solidify.model.xml.oai.v2.VerbType;
import ch.unige.solidify.model.xml.oai.v2.oai_dc.ElementType;
import ch.unige.solidify.model.xml.oai.v2.oai_dc.OaiDcType;
import ch.unige.solidify.model.xml.oai.v2.oai_dc.ObjectFactory;
import ch.unige.solidify.model.xml.oai.v2.oai_identifier.OaiIdentifierType;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.specification.OAIMetadataPrefixSpecification;
import ch.unige.solidify.specification.OAISetSpecification;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

@Service
@ConditionalOnBean(OAIController.class)
public class OAIService {

  private final DateTimeFormatter iso8601Formatter = DateTimeFormatter.ofPattern(StringTool.DATE_TIME_FORMAT_WITH_SECONDS);

  private final JAXBContext oaiJaxbContext;

  private final Map<String, JAXBContext> jaxbContexts = new HashMap<>();

  private final int oaiPmhPageSize;
  private final int oaiPmhTokenLifeTime;

  private final ObjectFactory oaiDcFactory = new ObjectFactory();
  // OAI Objects
  private final OAIRepositoryInfo oaiRepositoryInfo;

  private final OAIMetadataPrefixService oaiMetadataPrefixService;
  private final OAISetService oaiSetService;
  // OAI Service
  private final OAISearchService searchService;
  private final OAIRecordService recordService;
  private final OAIMetadataService metadataService;

  boolean serializeMetadataWithNamespaces;

  public OAIService(
          OAIProperties oaiProperties,
          OAIRepositoryInfo oaiRepositoryInfo,
          OAIMetadataPrefixService oaiMetadataPrefixService,
          OAISetService oaiSetService,
          OAISearchService searchService,
          OAIRecordService recordService,
          OAIMetadataService metadataService) {
    this.oaiRepositoryInfo = oaiRepositoryInfo;
    this.oaiPmhPageSize = oaiProperties.getPageSize();
    this.oaiPmhTokenLifeTime = oaiProperties.getTokenLifeTimeMinutes();
    this.serializeMetadataWithNamespaces = oaiProperties.isSerializeMetadataWithNamespaces();
    this.oaiMetadataPrefixService = oaiMetadataPrefixService;
    this.oaiSetService = oaiSetService;
    this.searchService = searchService;
    this.recordService = recordService;
    this.metadataService = metadataService;
    // JAXB contexts
    try {
      // Add project XML Classes
      final List<Class<?>> xmlClasses = new ArrayList<>(
              Arrays.asList(OAIPMHtype.class, OaiDcType.class, OaiIdentifierType.class, MetadataContent.class));
      xmlClasses.addAll(this.metadataService.getOaiXmlClasses());
      this.oaiJaxbContext = JAXBContext.newInstance(xmlClasses.toArray(Class<?>[]::new));
      this.loadJaxbContextsForOaiMetadataPrefix();
    } catch (JAXBException | ClassNotFoundException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  @EventListener
  @Async
  public void refreshJaxbContexts(RefreshOaiPmhMessage refreshOaiPmhMessage) {
    this.jaxbContexts.clear();
    try {
      this.jaxbContexts.clear();
      this.loadJaxbContextsForOaiMetadataPrefix();
    } catch (JAXBException | ClassNotFoundException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  public void loadJaxbContextsForOaiMetadataPrefix() throws ClassNotFoundException, JAXBException {
    final List<Class<?>> xmlClasses = Arrays.asList(OAIPMHtype.class, OaiIdentifierType.class, MetadataContent.class);

    for (OAIMetadataPrefix oaiMetadataPrefix : this.oaiMetadataPrefixService.findAll()) {
      if (!StringTool.isNullOrEmpty(oaiMetadataPrefix.getXmlClass())) {
        final List<Class<?>> oaiClasses = new ArrayList<>(xmlClasses);
        oaiClasses.add(Class.forName(oaiMetadataPrefix.getXmlClass()));
        this.jaxbContexts.put(oaiMetadataPrefix.getPrefix(), JAXBContext.newInstance(oaiClasses.toArray(Class<?>[]::new)));
      }
    }
  }

  public OAIPMHerrorType getError(OAIPMHerrorcodeType code, String message) {
    final OAIPMHerrorType oaiError = new OAIPMHerrorType();
    oaiError.setCode(code);
    oaiError.setValue(message);
    return oaiError;
  }

  public OAIRepositoryInfo getOaiRepositoryInfo() {
    return this.oaiRepositoryInfo;
  }

  public IdentifyType getIdentify(String oaiUrl) {
    final IdentifyType oaiIdentify = new IdentifyType();
    oaiIdentify.setRepositoryName(this.oaiRepositoryInfo.getLongName());
    oaiIdentify.setBaseURL(oaiUrl);
    oaiIdentify.setProtocolVersion("2.0");
    oaiIdentify.getAdminEmail().add(this.oaiRepositoryInfo.getEmail());
    oaiIdentify.setEarliestDatestamp(this.oaiRepositoryInfo.getProductionDate());
    oaiIdentify.setDeletedRecord(DeletedRecordType.PERSISTENT);
    oaiIdentify.setGranularity(GranularityType.YYYY_MM_DD_THH_MM_SS_Z);
    // Add oai-identifier
    final DescriptionType identifierDesc = new DescriptionType();
    final OaiIdentifierType oaiIdentifier = new OaiIdentifierType();
    oaiIdentifier.setScheme(OAIConstants.OAI_PREFIX);
    oaiIdentifier.setRepositoryIdentifier(this.oaiRepositoryInfo.getDomain());
    oaiIdentifier.setDelimiter(OAIConstants.OAI_SEP);
    oaiIdentifier.setSampleIdentifier(
            OAIConstants.OAI_PREFIX
                    + OAIConstants.OAI_SEP
                    + this.oaiRepositoryInfo.getDomain()
                    + OAIConstants.OAI_SEP
                    + this.oaiRepositoryInfo.getPrefix()
                    + OAIConstants.OAI_SEP
                    + StringTool.generateResId());
    identifierDesc.setAny(new JAXBElement<>(new QName(OAIConstants.OAI_ID_NAMESPACE, OAIConstants.OAI_ID),
            OaiIdentifierType.class, oaiIdentifier));
    oaiIdentify.getDescription().add(identifierDesc);
    // Add local description
    final DescriptionType repoSolution = new DescriptionType();
    repoSolution.setAny(this.oaiRepositoryInfo.getRepositoryDefinition());
    oaiIdentify.getDescription().add(repoSolution);
    return oaiIdentify;
  }

  public OAIMetadataPrefix getOAIMetadataPrefix(String metadataPrefix) {
    return this.oaiMetadataPrefixService.findByPrefix(metadataPrefix);
  }

  public GetRecordType getRecord(String baseUrl, String identifier, String metadataPrefix) throws JAXBException {
    // Check Parameters
    this.checkParameters(identifier, metadataPrefix);

    // GetRecordType
    final OAIRecord oaiRecord = this.recordService.getRecord(this.getOaiIdFromFullOaiId(identifier));
    if (oaiRecord == null) {
      throw new OAIException(OAIPMHerrorcodeType.ID_DOES_NOT_EXIST, identifier);
    }
    final GetRecordType getRecord = new GetRecordType();
    getRecord.setRecord(this.getRecord(baseUrl, oaiRecord, metadataPrefix));
    return getRecord;
  }

  public RequestType getRequest(String url, String verb, String identifier, String metadataPrefix, String from, String until, String set,
          String token) {
    final RequestType req = new RequestType();
    req.setValue(url);
    if (!StringTool.isNullOrEmpty(verb)) {
      req.setVerb(VerbType.fromValue(verb));
    }
    if (!StringTool.isNullOrEmpty(identifier)) {
      req.setIdentifier(identifier);
    }
    if (!StringTool.isNullOrEmpty(metadataPrefix)) {
      req.setMetadataPrefix(metadataPrefix);
    }
    if (!StringTool.isNullOrEmpty(from)) {
      req.setFrom(from);
    }
    if (!StringTool.isNullOrEmpty(until)) {
      req.setUntil(until);
    }
    if (!StringTool.isNullOrEmpty(set)) {
      req.setSet(set);
    }
    if (!StringTool.isNullOrEmpty(token)) {
      req.setResumptionToken(token);
    }
    return req;
  }

  public VerbType getVerb(String verb) {
    return VerbType.fromValue(verb);
  }

  public ListIdentifiersType listIdentifiers(String metadataPrefix, String from, String until, String set, String token) {
    // Check Parameters
    final OAIToken oaiToken = this.checkParameters(metadataPrefix, from, until, set, token);
    // Get Set Query
    String setQuery = null;
    if (oaiToken.getOaiParameters().getSet() != null) {
      setQuery = oaiToken.getOaiParameters().getSet().getQuery();
    }
    final RestCollection<OAIRecord> collection = this.searchService.searchOAIRecords(oaiToken.getOaiParameters().getFrom(),
            oaiToken.getOaiParameters().getUntil(), setQuery, oaiToken.getPageable());
    if (collection.getPage().getTotalItems() == 0) {
      throw new OAIException(OAIPMHerrorcodeType.NO_RECORDS_MATCH, "No identifier found");
    }
    final ListIdentifiersType idList = new ListIdentifiersType();
    final List<OAIRecord> list = collection.getData();
    for (final OAIRecord oaiRecord : list) {
      idList.getHeader().add(this.getIdentifier(oaiRecord));
    }
    if (collection.getPage().hasNext()) {
      idList.setResumptionToken(
              oaiToken.getToken(collection.getPage().getTotalItems(), collection.getPage().getPreviousItems(), this.oaiPmhTokenLifeTime, false));
    } else if (token != null) {
      // If results are paginated, a resumptionToken must be present on the last page, but with an empty value
      idList.setResumptionToken(
              oaiToken.getToken(collection.getPage().getTotalItems(), collection.getPage().getPreviousItems(), this.oaiPmhTokenLifeTime, true));
    }
    return idList;
  }

  public ListMetadataFormatsType listMetadataFormats(String identifier) {
    if (identifier != null && !identifier.isEmpty() && this.recordService.getRecord(this.getOaiIdFromFullOaiId(identifier)) == null) {
      throw new OAIException(OAIPMHerrorcodeType.ID_DOES_NOT_EXIST, identifier);
    }

    final ListMetadataFormatsType mdFormats = new ListMetadataFormatsType();
    for (OAIMetadataPrefix oaiMetadataPrefix : this.listMetadataPrefixes()) {
      // Add Metadata prefix
      final MetadataFormatType metadataPrefix = new MetadataFormatType();
      metadataPrefix.setMetadataPrefix(oaiMetadataPrefix.getPrefix());
      metadataPrefix.setSchema(oaiMetadataPrefix.getSchemaUrl().toString());
      metadataPrefix.setMetadataNamespace(oaiMetadataPrefix.getSchemaNamespace());
      mdFormats.getMetadataFormat().add(metadataPrefix);
    }
    return mdFormats;
  }

  private Page<OAIMetadataPrefix> listMetadataPrefixes() {
    OAIMetadataPrefix oaiMetadataPrefixFilter = new OAIMetadataPrefix();
    oaiMetadataPrefixFilter.setEnabled(true);
    return this.oaiMetadataPrefixService.findAll(new OAIMetadataPrefixSpecification(oaiMetadataPrefixFilter),
            PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE, Sort.by("prefix")));
  }

  public ListRecordsType listRecords(String baseUrl, String metadataPrefix, String from, String until, String set, String token)
          throws JAXBException {
    // Check Parameters
    final OAIToken oaiToken = this.checkParameters(metadataPrefix, from, until, set, token);
    String setQuery = null;
    if (oaiToken.getOaiParameters().getSet() != null) {
      setQuery = oaiToken.getOaiParameters().getSet().getQuery();
    }

    final RestCollection<OAIRecord> collection = this.searchService.searchOAIRecords(oaiToken.getOaiParameters().getFrom(),
            oaiToken.getOaiParameters().getUntil(), setQuery, oaiToken.getPageable());
    if (collection.getPage().getTotalItems() == 0) {
      throw new OAIException(OAIPMHerrorcodeType.NO_RECORDS_MATCH, "No record found");
    }
    final ListRecordsType recList = new ListRecordsType();
    final List<OAIRecord> list = collection.getData();
    for (final OAIRecord oaiRecord : list) {
      recList.getRecord().add(this.getRecord(baseUrl, oaiRecord, oaiToken.getOaiParameters().getMetadataPrefix()));
    }

    if (collection.getPage().hasNext()) {
      recList.setResumptionToken(
              oaiToken.getToken(collection.getPage().getTotalItems(), collection.getPage().getPreviousItems(), this.oaiPmhTokenLifeTime, false));
    } else if (token != null) {
      // If results are paginated, a resumptionToken must be present on the last page, but with an empty value
      recList.setResumptionToken(
              oaiToken.getToken(collection.getPage().getTotalItems(), collection.getPage().getPreviousItems(), this.oaiPmhTokenLifeTime, true));
    }

    return recList;
  }

  public ListSetsType listSets(String token) {

    /*
     * Pagination may be given by an URL token
     */
    final OAIToken oaiToken = new OAIToken(this.oaiPmhPageSize, this.oaiPmhTokenLifeTime, Sort.by("name"));
    if (!StringTool.isNullOrEmpty(token)) {
      oaiToken.setToken(token);
    }

    final ListSetsType setTypes = new ListSetsType();
    for (final OAISet oaiSet : this.getOaiSetPage(oaiToken.getPageable())) {

      final SetType setType = new SetType();

      setType.setSetSpec(oaiSet.getSpec());
      setType.setSetName(oaiSet.getName());

      final OaiDcType oaiDc = this.oaiDcFactory.createOaiDcType();
      oaiDc.getTitleOrCreatorOrSubject().add(this.oaiDcFactory.createDescription(this.getOaiDcEntry(oaiSet.getDescription(), null)));
      final DescriptionType descriptionType = new DescriptionType();
      final QName qName = new QName(OAIConstants.OAI_DC_NAMESPACE, OAIConstants.OAI_DC);
      descriptionType.setAny(new JAXBElement<>(qName, OaiDcType.class, oaiDc));

      setType.getSetDescription().add(descriptionType);

      setTypes.getSet().add(setType);
    }

    return setTypes;
  }

  public String printOAIPMH(OAIPMHtype oai, String rootUrl, String metadataPrefix, Map<String, String> schemaLocationList) throws JAXBException {
    final Marshaller marshaller = this.metadataService.getMarshaller(this.getJaxbContext(metadataPrefix));
    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    if (!StringTool.isNullOrEmpty(rootUrl)) {
      marshaller.setProperty("org.glassfish.jaxb.xmlHeaders", "<?xml-stylesheet type='text/xsl' href='" + rootUrl + "'?>");
    }
    // Build schema location string
    StringBuilder schemaLocations = new StringBuilder();
    schemaLocations
            .append(OAIConstants.OAI_PMH_NAMESPACE)
            .append(" ")
            .append(OAIConstants.OAI_PMH_SCHEMA);
    if (schemaLocationList != null && !schemaLocationList.isEmpty()) {
      for (Map.Entry<String, String> schemaLocation : schemaLocationList.entrySet()) {
        schemaLocations
                .append(" ")
                .append(schemaLocation.getKey())
                .append(" ")
                .append(schemaLocation.getValue());
      }
    }

    marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, schemaLocations.toString());
    final JAXBElement<OAIPMHtype> root = this.getXML(oai);

    if (this.serializeMetadataWithNamespaces) {
      return this.serializeXmlWithNamespacesInMetadata(oai, root, marshaller, metadataPrefix, schemaLocations.toString());
    } else {
      final ByteArrayOutputStream bos = new ByteArrayOutputStream();
      marshaller.marshal(root, bos);
      return bos.toString();
    }
  }

  private JAXBContext getJaxbContext(String metadataPrefix) {
    if (this.jaxbContexts.containsKey(metadataPrefix)) {
      return this.jaxbContexts.get(metadataPrefix);
    } else {
      return this.oaiJaxbContext;
    }
  }

  private String serializeXmlWithNamespacesInMetadata(OAIPMHtype oai, JAXBElement<OAIPMHtype> root, Marshaller marshaller, String metadataPrefix,
          String schemaLocations) throws JAXBException {
    // Get the list of metadata contents unmarshalled separately to get their XML forms including namespaces
    List<String> metadataContents = this.getMetadataContents(oai, metadataPrefix, schemaLocations);

    // Replace metadata nodes content by an XML object that can be replaced later once serialized as String
    MetadataContent metadataContentToReplace = new MetadataContent();

    List<RecordType> recordTypes = this.getRecordTypes(oai);
    for (RecordType recordType : recordTypes) {
      recordType.getMetadata().setAny(metadataContentToReplace);
    }

    // Serialize OAI-PMH XML as String
    final ByteArrayOutputStream bos = new ByteArrayOutputStream();
    marshaller.marshal(root, bos);
    String xml = bos.toString();

    // Replace each metadata content by the original XML contents including namespaces
    for (String metadataContent : metadataContents) {
      xml = xml.replaceFirst(OAIConstants.METADATA_CONTENT_TAG, Matcher.quoteReplacement(metadataContent));
    }
    return xml;
  }

  private List<String> getMetadataContents(OAIPMHtype oai, String metadataPrefix, String schemaLocations) throws JAXBException {
    final Marshaller metadataMarshaller = this.metadataService.getMarshaller(this.getJaxbContext(metadataPrefix));
    metadataMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
    metadataMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
    metadataMarshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, schemaLocations);

    List<RecordType> recordTypes = this.getRecordTypes(oai);
    List<String> metadataFragments = new ArrayList<>();
    for (RecordType recordType : recordTypes) {
      Object metadataXml = recordType.getMetadata().getAny();
      final ByteArrayOutputStream metadataOutput = new ByteArrayOutputStream();
      metadataMarshaller.marshal(metadataXml, metadataOutput);
      metadataFragments.add(metadataOutput.toString().trim());
    }
    return metadataFragments;
  }

  private List<RecordType> getRecordTypes(OAIPMHtype oai) {
    List<RecordType> recordTypes = new ArrayList<>();
    if (oai.getGetRecord() != null) {
      recordTypes.add(oai.getGetRecord().getRecord());
    } else if (oai.getListRecords() != null) {
      for (RecordType recordType : oai.getListRecords().getRecord()) {
        recordTypes.add(recordType);
      }
    }
    return recordTypes;
  }

  private void addOaiSetInfo(HeaderType hdr, String resId) {
    final Pageable pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    for (final OAISet oaiSet : this.getOaiSetPage(pageable)) {
      if (this.searchService.isInSet(resId, oaiSet.getQuery(), pageable)) {
        hdr.getSetSpec().add(oaiSet.getSpec());
      }
    }
  }

  private void checkParameters(String identifier, String metadataPrefix) {
    if (StringTool.isNullOrEmpty(identifier)) {
      throw new OAIException(OAIPMHerrorcodeType.BAD_ARGUMENT, "Identifier is mandatory");
    }
    if (StringTool.isNullOrEmpty(metadataPrefix)) {
      throw new OAIException(OAIPMHerrorcodeType.BAD_ARGUMENT, "Metadata prefix is mandatory");
    }
  }

  private OAIToken checkParameters(String metadataPrefix, String from, String until, String set, String token) {
    final OAIToken oaiToken = new OAIToken(this.oaiPmhPageSize, this.oaiPmhTokenLifeTime);

    // Token parameter
    if (!StringTool.isNullOrEmpty(token)) {
      oaiToken.setToken(token);
    } else {
      // Metadata Prefix parameter
      if (StringTool.isNullOrEmpty(metadataPrefix)) {
        throw new OAIException(OAIPMHerrorcodeType.BAD_ARGUMENT, "Metadata prefix is mandatory");
      }
      if (this.oaiMetadataPrefixService.findByPrefix(metadataPrefix) == null) {
        throw new OAIException(OAIPMHerrorcodeType.CANNOT_DISSEMINATE_FORMAT, "Metadata prefix (" + metadataPrefix + ") not supported");
      }
      oaiToken.getOaiParameters().setMetadataPrefix(metadataPrefix);

      // From parameter
      if (!StringTool.isNullOrEmpty(from)) {
        if (!StringTool.checkDate(from)) {
          throw new OAIException(OAIPMHerrorcodeType.BAD_ARGUMENT, "Wrong 'from' date format (" + from + ")");
        }
        oaiToken.getOaiParameters().setFrom(from);
      }
      // Until parameter
      if (!StringTool.isNullOrEmpty(until)) {
        if (!StringTool.checkDate(until)) {
          throw new OAIException(OAIPMHerrorcodeType.BAD_ARGUMENT, "Wrong 'until' date format (" + until + ")");
        }
        oaiToken.getOaiParameters().setUntil(until);
      }
      if (!StringTool.isNullOrEmpty(from) && !StringTool.isNullOrEmpty(until) && from.length() != until.length()) {
        throw new OAIException(OAIPMHerrorcodeType.BAD_ARGUMENT,
                "'from' and 'until' dates do not have the same granularity: " + from + " / " + until);
      }
    }

    // Set parameter
    if (!StringTool.isNullOrEmpty(set)
            || (oaiToken.getOaiParameters().getSet() != null && !StringTool.isNullOrEmpty(oaiToken.getOaiParameters().getSet().getSpec()))) {
      set = !StringTool.isNullOrEmpty(set) ? set : oaiToken.getOaiParameters().getSet().getSpec();
      final OAISet oaiSet = this.oaiSetService.findBySpec(set);
      if (oaiSet != null) {
        oaiToken.getOaiParameters().setSet(oaiSet);
      } else {
        throw new OAIException(OAIPMHerrorcodeType.NO_SET_HIERARCHY, "Set " + set + " does not exist");
      }
    }

    return oaiToken;
  }

  private String getArchiveUrl() {
    String archiveUrl = this.oaiRepositoryInfo.getArchiveHomePage();
    if (StringTool.isNullOrEmpty(archiveUrl)) {
      throw new SolidifyRuntimeException("Missing 'archive-home-page' parameter");
    }
    return StringTool.normalizeBaseUrl(archiveUrl);
  }

  private ElementType getOaiDcEntry(String value, String language) {
    final ElementType fieldValue = new ElementType();
    fieldValue.setValue(value);
    if (!StringTool.isNullOrEmpty(language)) {
      fieldValue.setLang(language);
    }
    return fieldValue;
  }

  private HeaderType getIdentifier(OAIRecord oaiRecord) {
    final HeaderType hdr = new HeaderType();
    hdr.setIdentifier(this.getFullOaiId(oaiRecord.getOaiId()));
    hdr.setDatestamp(this.iso8601Formatter.format(oaiRecord.getCreationDate()));
    this.addOaiSetInfo(hdr, oaiRecord.getResId());
    return hdr;
  }

  private String getFullOaiId(String oaiId) {
    return this.getFullOaiIdPrefix() + oaiId;
  }

  private String getOaiIdFromFullOaiId(String fullOaiId) {
    return StringTool.removePrefix(fullOaiId, this.getFullOaiIdPrefix());
  }

  private String getFullOaiIdPrefix() {
    return OAIConstants.OAI_PREFIX
            + OAIConstants.OAI_SEP
            + this.oaiRepositoryInfo.getDomain()
            + OAIConstants.OAI_SEP
            + this.oaiRepositoryInfo.getPrefix()
            + OAIConstants.OAI_SEP;
  }

  @SuppressWarnings("squid:S128")
  private Object getMetadata(String baseUrl, String metadataPrefix, OAIRecord oaiRecord) throws JAXBException {
    final OAIMetadataPrefix oaiMetadataPrefix = this.getOAIMetadataPrefix(metadataPrefix);
    if (oaiMetadataPrefix == null) {
      throw new OAIException(OAIPMHerrorcodeType.NO_METADATA_FORMATS, "Undefined metadata prefix " + metadataPrefix + " this record");
    }
    if (oaiMetadataPrefix.isReference()) {
      return this.metadataService.getOAIMetadata(oaiRecord.getOAIMetadata());
    } else {
      Map<String, String> xsltParameters = this.metadataService.getXsltParameters(oaiRecord);
      xsltParameters.putIfAbsent(OAIConstants.XSL_PARAM_ARCHIVE_URL, this.getArchiveUrl());

      // Apply XSL
      final StringReader newMd = new StringReader(
              XMLTool.transform(oaiRecord.getOAIMetadata(), oaiMetadataPrefix.getMetadataXmlTransformation(), xsltParameters));
      final JAXBContext jaxbContext = this.getJaxbContext(metadataPrefix);
      return jaxbContext.createUnmarshaller().unmarshal(newMd);
    }
  }

  private Page<OAISet> getOaiSetPage(Pageable pageable) {
    /*
     * filter on enabled OAI sets
     */
    final OAISet criteria = new OAISet();
    criteria.setEnabled(true);
    final OAISetSpecification spec = new OAISetSpecification(criteria);
    return this.oaiSetService.findAll(spec, pageable);
  }

  private RecordType getRecord(String baseUrl, OAIRecord oaiRecord, String metadataPrefix) throws JAXBException {
    final RecordType rec = new RecordType();
    // Header
    rec.setHeader(this.getIdentifier(oaiRecord));
    // Metadata
    final MetadataType md = new MetadataType();
    md.setAny(this.getMetadata(baseUrl, metadataPrefix, oaiRecord));
    rec.setMetadata(md);
    return rec;
  }

  private JAXBElement<OAIPMHtype> getXML(OAIPMHtype oai) {
    final QName qName = new QName(OAIConstants.OAI_PMH_NAMESPACE, OAIConstants.OAI_PMH);
    return new JAXBElement<>(qName, OAIPMHtype.class, oai);
  }

}
