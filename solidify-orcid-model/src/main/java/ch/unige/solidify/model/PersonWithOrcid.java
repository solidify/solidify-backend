/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID Model - PersonWithOrcid.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.auth.model.OrcidInfo;

@Schema(description = "Person with ORCID")
public interface PersonWithOrcid extends OrcidInfo {
  @Schema(description = SolidifyConstants.RES_ID_DESCRIPTION)
  String getResId();

  void setOrcid(String orcid);

  void setVerifiedOrcid(Boolean verifiedOrcid);

  void setOrcidToken(OrcidToken orcidToken);

  OrcidToken getOrcidToken();

  @Schema(description = "The full name of the person", accessMode = Schema.AccessMode.READ_ONLY)
  String getFullName();
}
