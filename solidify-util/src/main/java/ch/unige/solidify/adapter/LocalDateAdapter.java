/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - LocalDateAdapter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.adapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import ch.unige.solidify.SolidifyConstants;

public class LocalDateAdapter {

  public static LocalDate parseDate(String value) {
    if (value != null) {
      return LocalDate.parse(value);
    }
    return null;
  }

  public static String printDate(LocalDate value) {
    if (value != null) {
      return value.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }
    return null;
  }

  /**
   * This constructor should not be called
   */
  private LocalDateAdapter() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
