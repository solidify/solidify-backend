/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID - OrcidSynchronizationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */
package ch.unige.solidify.business;

import java.math.BigInteger;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.xml.bind.JAXBElement;

import ch.unige.solidify.exception.SolidifyOrcidWorkMissingInProfileException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.OrcidSynchronization;
import ch.unige.solidify.model.OrcidToken;
import ch.unige.solidify.model.OrcidWorkUpdateMode;
import ch.unige.solidify.model.PersonWithOrcid;
import ch.unige.solidify.model.xml.orcid.v3_0.common.OrcidId;
import ch.unige.solidify.model.xml.orcid.v3_0.work.Work;
import ch.unige.solidify.repository.OrcidSynchronizationRepository;
import ch.unige.solidify.service.OrcidClientService;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.OrcidSynchronizationSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

public abstract class OrcidSynchronizationService extends ResourceService<OrcidSynchronization> {

  private static final Logger log = LoggerFactory.getLogger(OrcidSynchronizationService.class);

  protected static final String ORCID_COMMON_NS = "http://www.orcid.org/ns/common";
  protected static final String ORCID_DATASET_TYPE = "data-set";
  protected static final String ORCID_DOI_TYPE = "doi";
  protected static final String ORCID_SELF_RELATIONSHIP = "self";

  protected final OrcidClientService orcidClientService;

  protected OrcidSynchronizationService(OrcidClientService orcidClientService) {
    this.orcidClientService = orcidClientService;
  }

  public List<OrcidSynchronization> findByObjectId(String aipId) {
    return ((OrcidSynchronizationRepository) this.itemRepository).findByObjectId(aipId);
  }

  public List<OrcidSynchronization> findByPersonIdAndObjectId(String personId, String aipId) {
    return ((OrcidSynchronizationRepository) this.itemRepository).findByPersonIdAndObjectId(personId, aipId);
  }

  public Optional<OrcidSynchronization> findByPersonIdAndPutCode(String personId, BigInteger putCode) {
    return ((OrcidSynchronizationRepository) this.itemRepository).findByPersonIdAndPutCode(personId, putCode);
  }

  public OrcidSynchronization sendToOrcidProfile(PersonWithOrcid person, String objectId, OrcidWorkUpdateMode updateMode) {
    OrcidToken orcidToken = person.getOrcidToken();
    if (orcidToken == null) {
      throw new SolidifyRuntimeException("Person " + person.getFullName() + " (" + person.getResId() + ") doesn't have any ORCID token");
    }
    final Work work = this.buildWork(objectId);
    final List<OrcidSynchronization> previousSynchronizationList = this.findByPersonIdAndObjectId(person.getResId(), objectId);
    if (previousSynchronizationList.isEmpty()) {
      // Create ORCID record
      BigInteger putCode = this.orcidClientService.uploadWork(work, orcidToken);
      log.info("Object '{}' ({}) has been uploaded to ORCID profile of person {} as Work with putCode {}", work.getTitle().getTitle(), objectId,
              person, putCode);
      return this.createOrcidSynchronization(person.getResId(), putCode, objectId, OffsetDateTime.now());
    } else if (previousSynchronizationList.size() == 1) {
      // Update ORCID record
      final OrcidSynchronization previousSynchronization = previousSynchronizationList.get(0);
      BigInteger previousPutCode = previousSynchronization.getPutCode();
      try {
        BigInteger putCode = this.orcidClientService.uploadWork(work, orcidToken, previousPutCode, updateMode);
        // Upload succeeded
        if (previousPutCode.equals(putCode)) {
          log.info("Object '{}' ({}) has been updated on ORCID profile of person {} as Work with putCode {}", work.getTitle().getTitle(),
                  objectId, person, putCode);
        } else {
          log.info("Object '{}' ({}) has been created again on ORCID profile of person {} as Work with putCode {}", work.getTitle().getTitle(),
                  objectId, person, putCode);
        }

        // Upload succeeded, update OrcidSynchronization in database
        previousSynchronization.setPutCode(putCode);
        previousSynchronization.setUploadDate(OffsetDateTime.now());
        return this.save(previousSynchronization);
      } catch (SolidifyOrcidWorkMissingInProfileException e) {
        log.warn("Object '{}' ({}) with putCode {} could not be updated in ORCID profile of person {} as it doesn't exist",
                work.getTitle().getTitle(), objectId, previousPutCode, person);
        throw e;
      } catch (RuntimeException e) {
        throw new SolidifyRuntimeException("An error occurred while uploading a Work to ORCID", e);
      }
    } else {
      throw new IllegalStateException(
              "More than one OrcidSynchronization found for person " + person + " for object '" + work.getTitle().getTitle() + "' (" + objectId
                      + ")");
    }
  }

  public abstract OrcidSynchronization createOrcidSynchronization(String personId, BigInteger putCode, String objectId,
          OffsetDateTime uploadDate);

  protected abstract Work buildWork(String objectId);

  protected OrcidId buildOrcidId(String orcidValue) {
    OrcidId orcidId = new OrcidId();
    JAXBElement<String> uri = new JAXBElement<>(new QName(ORCID_COMMON_NS, "uri"), String.class,
            "https://orcid.org/" + orcidValue);
    orcidId.getContent().add(uri);
    JAXBElement<String> path = new JAXBElement<>(new QName(ORCID_COMMON_NS, "path"), String.class, orcidValue);
    orcidId.getContent().add(path);
    JAXBElement<String> host = new JAXBElement<>(new QName(ORCID_COMMON_NS, "host"), String.class, "orcid.org");
    orcidId.getContent().add(host);
    return orcidId;
  }

  @Override
  public SolidifySpecification<OrcidSynchronization> getSpecification(OrcidSynchronization orcidSynchronization) {
    return new OrcidSynchronizationSpecification(orcidSynchronization);
  }
}
