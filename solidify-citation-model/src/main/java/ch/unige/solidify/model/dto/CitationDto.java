/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Citation Model - CitationDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.dto;

import ch.unige.solidify.model.Citation;

public class CitationDto {

  private Citation citation;

  public CitationDto(Citation citation) {
    this.citation = citation;
  }

  public String getStyle() {
    return this.citation.getStyle().toLowerCase();
  }

  public String getLanguage() {
    return this.citation.getLanguage().toLowerCase();
  }

  public String getOutputFormat() {
    return this.citation.getOutputFormat().toString().toLowerCase();
  }

  public String getText() {
    return this.citation.getText();
  }
}
