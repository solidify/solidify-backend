/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - LSInputImpl.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.InputStream;
import java.io.Reader;

import org.w3c.dom.ls.LSInput;

public class LSInputImpl implements LSInput {
  private String baseURI;
  private InputStream byteStream;
  private boolean certifiedText;
  private Reader characterStream;
  private String encoding;
  private String publicId;
  private String stringData;
  private String systemId;

  @Override
  public String getBaseURI() {
    return this.baseURI;
  }

  @Override
  public InputStream getByteStream() {
    return this.byteStream;
  }

  @Override
  public boolean getCertifiedText() {
    return false;
  }

  @Override
  public Reader getCharacterStream() {
    return this.characterStream;
  }

  @Override
  public String getEncoding() {
    return this.encoding;
  }

  @Override
  public String getPublicId() {
    return this.publicId;
  }

  @Override
  public String getStringData() {
    return this.stringData;
  }

  @Override
  public String getSystemId() {
    return this.systemId;
  }

  public boolean isCertifiedText() {
    return this.certifiedText;
  }

  @Override
  public void setBaseURI(String baseURI) {
    this.baseURI = baseURI;
  }

  @Override
  public void setByteStream(InputStream byteStream) {
    this.byteStream = byteStream;
  }

  @Override
  public void setCertifiedText(boolean certifiedText) {
    this.certifiedText = certifiedText;
  }

  @Override
  public void setCharacterStream(Reader characterStream) {
    this.characterStream = characterStream;
  }

  @Override
  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }

  @Override
  public void setPublicId(String publicId) {
    this.publicId = publicId;
  }

  @Override
  public void setStringData(String stringData) {
    this.stringData = stringData;
  }

  @Override
  public void setSystemId(String systemId) {
    this.systemId = systemId;
  }

}
