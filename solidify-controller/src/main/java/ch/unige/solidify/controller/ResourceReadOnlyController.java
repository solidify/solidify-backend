/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - ResourceReadOnlyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.Searchable;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.StringParserTool;

@RootPermissions
public abstract class ResourceReadOnlyController<T extends Resource> extends SolidifyController
        implements ControllerWithHateoasHome {

  @Autowired
  public ResourceService<T> itemService;

  @PostMapping("/" + ActionName.SEARCH)
  public HttpEntity<RestCollection<T>> advancedSearch(@RequestBody T search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    if (search instanceof Searchable) {
      final Page<T> listItem = this.itemService
              .findBySearchCriteria((Searchable<T>) search, matchtype, ((Searchable<T>) search).getSearchCriterias(), pageable);
      this.setResourceLinks(listItem);
      final RestCollection<T> collection = this.setCollectionLinks(listItem, pageable);
      return new ResponseEntity<>(collection, HttpStatus.OK);
    } else {
      throw new SolidifyRuntimeException("resource is not searchable");
    }
  }

  @GetMapping("/" + ActionName.SEARCH)
  @ResponseBody
  public HttpEntity<RestCollection<T>> advancedSearch(@ModelAttribute T resource,
          @RequestParam("search") String search,
          @RequestParam(value = "match", required = false) String matchtype, Pageable pageable) {
    if (resource instanceof Searchable) {
      final List<SearchCriteria> criterias = StringParserTool.parseSearchString(search);
      final Page<T> listItem = this.itemService.findBySearchCriteria((Searchable<T>) resource, matchtype, criterias, pageable);
      final WebMvcLinkBuilder linkBuilder = linkTo(methodOn(this.getClass()).advancedSearch(null, search, matchtype, pageable));
      this.setResourceLinks(listItem);
      final RestCollection<T> collection = this.setCollectionLinksForMethod(listItem, pageable, linkBuilder);
      return new ResponseEntity<>(collection, HttpStatus.OK);
    } else {
      throw new SolidifyRuntimeException("resource is not searchable");
    }
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<T> get(@PathVariable String id) {
    final T t = this.itemService.findOne(id);
    this.addLinks(t);
    return new ResponseEntity<>(t, HttpStatus.OK);
  }

  @GetMapping
  public HttpEntity<RestCollection<T>> list(@ModelAttribute T search, Pageable pageable) {
    final Specification<T> spec = this.itemService.getSpecification(search);
    final Page<T> listItem = this.itemService.findAll(spec, pageable);
    this.setResourceLinks(listItem);
    final RestCollection<T> collection = this.setCollectionLinks(listItem, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  protected void addLinks(T t) {
    t.removeLinks();
    t.addLinks(linkTo(this.getClass()), true, true);
  }

  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    // Do nothing
    // Could be overridden by subclass
  }

  protected RestCollection<T> setCollectionLinks(Page<T> listItem, Pageable pageable) {
    final RestCollection<T> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkTo(this.getClass()).withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder())
            .withRel(ActionName.MODULE));

    this.addSortLinks(linkTo(this.getClass()), collection);
    this.addPageLinks(linkTo(this.getClass()), collection, pageable);
    this.addOthersLinks(collection);
    return collection;
  }

  protected RestCollection<T> setCollectionLinksForMethod(Page<T> listItem, Pageable pageable, WebMvcLinkBuilder linkBuilder) {
    final RestCollection<T> collection = new RestCollection<>(listItem, pageable);
    collection.add(linkBuilder.withSelfRel());
    collection.add(Tool.parentLink((linkTo(this.getClass())).toUriComponentsBuilder())
            .withRel(ActionName.MODULE));
    this.addSortLinks(linkBuilder, collection);
    this.addPageLinks(linkBuilder, collection, pageable);
    this.addOthersLinks(collection);
    return collection;
  }

  protected void setResourceLinks(Page<T> listItem) {
    if (listItem == null) {
      return;
    }
    for (final T t : listItem) {
      this.addLinks(t);
    }
  }

}
