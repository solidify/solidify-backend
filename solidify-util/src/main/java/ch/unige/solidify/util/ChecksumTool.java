/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - ChecksumTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;

public class ChecksumTool {

  public static String computeChecksum(InputStream inputStream, ChecksumAlgorithm algorithm) throws NoSuchAlgorithmException, IOException {
    final String[] checksums = ChecksumTool.computeChecksum(inputStream, new String[] { algorithm.name() });
    return checksums[0];
  }

  // *************************
  // ** Checksums functions **
  // *************************

  public static String[] computeChecksum(InputStream inputStream,
          List<ChecksumAlgorithm> algorithms) throws NoSuchAlgorithmException, IOException {
    final String[] algos = new String[algorithms.size()];
    for (int i = 0; i < algorithms.size(); i++) {
      algos[i] = algorithms.get(i).name();
    }

    return ChecksumTool.computeChecksum(inputStream, algos);
  }

  public static String[] computeChecksum(InputStream inputStream, String[] checksums)
          throws NoSuchAlgorithmException, IOException {
    final MessageDigest[] digests = new MessageDigest[checksums.length];
    for (int i = 0; i < checksums.length; i++) {
      digests[i] = getMessageDigest(ChecksumAlgorithm.valueOf(checksums[i]));
    }
    return getFileChecksum(inputStream, digests);
  }

  public static List<String> getChecksums(InputStream stream,
          List<ChecksumAlgorithm> algorithmsToCalculate)
          throws NoSuchAlgorithmException, IOException {

    /*
     * Get all MessageDigest
     */
    final List<MessageDigest> digests = ChecksumTool.getMessageDigests(algorithmsToCalculate);

    /*
     * Read stream and calculate checksums
     */
    final byte[] byteArray = new byte[SolidifyConstants.BUFFER_SIZE];
    int bytesCount;
    while ((bytesCount = stream.read(byteArray)) != -1) {
      for (final MessageDigest digest : digests) {
        digest.update(byteArray, 0, bytesCount);
      }
    }
    stream.close();

    /*
     * For each MessageDigest, get the hash value and fill the result
     */
    final List<String> checksums = new ArrayList<>();
    for (final MessageDigest digest : digests) {
      checksums.add(StringTool.toHexaString(digest.digest()));
    }

    return checksums;
  }

  public static List<MessageDigest> getMessageDigests(
          List<ChecksumAlgorithm> algorithms)
          throws NoSuchAlgorithmException {

    final List<MessageDigest> digests = new ArrayList<>();

    for (final ChecksumAlgorithm algorithm : algorithms) {
      digests.add(MessageDigest.getInstance((algorithm.toString())));
    }

    return digests;
  }

  private static String[] getFileChecksum(InputStream inputStream, MessageDigest[] digests)
          throws IOException {
    final String[] checksums = new String[digests.length];
    final byte[] byteArray = new byte[SolidifyConstants.BUFFER_SIZE];
    int bytesCount;

    // Read file data and update in message digest
    while ((bytesCount = inputStream.read(byteArray)) != -1) {
      for (final MessageDigest digest : digests) {
        digest.update(byteArray, 0, bytesCount);
      }
    }
    inputStream.close();

    for (int i = 0; i < digests.length; i++) {
      checksums[i] = StringTool.toHexaString(digests[i].digest());
    }

    return checksums;
  }

  private static MessageDigest getMessageDigest(ChecksumAlgorithm checksumAlgo)
          throws NoSuchAlgorithmException {

    return MessageDigest.getInstance(checksumAlgo.toString());
  }

  private ChecksumTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
