/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - ChecksumAlgorithm.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

//Embedded classes
public enum ChecksumAlgorithm {
  CRC32("CRC32"), MD5("MD5"), SHA1("SHA-1"), SHA256("SHA-256"), SHA512("SHA-512");

  /*
   * Override constructor and toString() to get supported values for algorithm names (see
   * https://stackoverflow.com/questions/12642742/exception-when-calling-messagedigest-
   * getinstancesha256#12642834)
   */
  private final String value;

  private ChecksumAlgorithm(String realName) {
    this.value = realName;
  }

  @Override
  public String toString() {
    return this.value;
  }
}
