/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Authorization Client - JwtOpaqueTokenIntrospector.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.client.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.DefaultOAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.server.resource.InvalidBearerTokenException;
import org.springframework.security.oauth2.server.resource.introspection.NimbusOpaqueTokenIntrospector;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenIntrospector;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import net.minidev.json.JSONArray;

@Service
public class JwtOpaqueTokenIntrospector implements OpaqueTokenIntrospector {
  private static final Logger logger = LoggerFactory.getLogger(JwtOpaqueTokenIntrospector.class);
  public static final String OAUTH_CHECK_TOKEN_URL = "/oauth/check_token";
  public static final String TOKEN_CACHE_NAME = "tokenCache";

  public static final String TOKEN_APPLICATION_PREFIX = "app-";

  private final OpaqueTokenIntrospector nimbusTokenIntrospector;
  private final UserCreatorService userCreatorService;
  private final String applicationName;
  private final boolean tokenCacheEnabled;

  private final Map<String, OAuth2AuthenticatedPrincipal> tokenCache = new HashMap<>();


  public JwtOpaqueTokenIntrospector(UserCreatorService userCreatorService, AuthorizationClientProperties authProperties) {
    this.applicationName = authProperties.getApplicationName();
    this.userCreatorService = userCreatorService;
    final String checkTokenUrl = authProperties.getAuthorizationServerUrl() + OAUTH_CHECK_TOKEN_URL;
    final String trustedUserName = authProperties.getTrustedUserName();
    final String trustedUserPassword = authProperties.getTrustedUserPassword();
    this.nimbusTokenIntrospector = new NimbusOpaqueTokenIntrospector(checkTokenUrl, trustedUserName, trustedUserPassword);
    this.tokenCacheEnabled = authProperties.isOpaqueTokenIntrospectorCacheEnabled();
  }

  public OAuth2AuthenticatedPrincipal introspect(String token) {
    synchronized(this) {
      if (this.tokenCacheEnabled && this.tokenCache.containsKey(token)) {
        return this.tokenCache.get(token);
      }
    }
    final OAuth2AuthenticatedPrincipal principal = this.nimbusTokenIntrospector.introspect(token);
    final Collection<GrantedAuthority> authorities = new ArrayList<>();
    final JSONArray jsonAuthorities = principal.getAttribute("authorities");
    if(jsonAuthorities == null) {
      throw new InvalidBearerTokenException("Authorities field is missing");
    }
    for (final Object authority : jsonAuthorities) {
      authorities.add(new SimpleGrantedAuthority((String) authority));
    }
    final JSONArray scopes = principal.getAttribute("scope");
    Assert.notNull(scopes, "No scope field present in token");
    Assert.notEmpty(scopes, "scope field is empty");
    final String scope = (String)scopes.get(0);
    if(!this.isApplicationInScope(scopes, this.applicationName)) {
      throw new InvalidBearerTokenException("Scope is " + scope + " it should be " + this.applicationName);
    }
    final String externalUid = principal.getAttribute("user_name");
    Assert.notNull(externalUid, "No user_name field present in token");
    this.userCreatorService.createNewUser(externalUid);
    final OAuth2AuthenticatedPrincipal oauth2Principal = new DefaultOAuth2AuthenticatedPrincipal(externalUid, principal.getAttributes(), authorities);
    if(this.tokenCacheEnabled) {
      this.tokenCache.put(token, oauth2Principal);
    }
    return oauth2Principal;
  }

  @Scheduled(fixedRateString = "${auth.client.tokenCacheDuration:300000}",
             initialDelayString = "${auth.client.tokenCacheDuration:300000}")
  public synchronized void clearCacheSearch() {
    this.tokenCache.clear();
    logger.debug(TOKEN_CACHE_NAME + " cleaned");
  }

  private boolean isApplicationInScope(JSONArray scopes, String applicationName) {
    for(Object scope : scopes) {
      if(scope.equals(TOKEN_APPLICATION_PREFIX + applicationName)) {
        return true;
      }
    }
    return false;
  }

}
