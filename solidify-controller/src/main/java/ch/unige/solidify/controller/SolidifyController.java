/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SolidifyController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import jakarta.servlet.http.HttpServletRequest;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.ResourceBase;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.service.MessageService;

public abstract class SolidifyController {

  @Autowired
  protected MessageService messageService;

  protected <T> void addPageLinks(WebMvcLinkBuilder linkBuilder, RestCollection<T> collection,
          Pageable pageable) {
    if (collection.getPage().hasPrevious()) {
      collection.add(
              Tool.pageLink(linkBuilder.toUriComponentsBuilder(), pageable.previousOrFirst().getPageSize(),
                      pageable.previousOrFirst().getPageNumber()).withRel(ActionName.PREVIOUS));
    }
    if (collection.getPage().hasNext()) {
      collection.add(Tool.pageLink(linkBuilder.toUriComponentsBuilder(), pageable.next().getPageSize(),
              pageable.next().getPageNumber()).withRel(ActionName.NEXT));
    }
  }

  protected <T> void addSortLinks(WebMvcLinkBuilder linkBuilder, RestCollection<T> collection) {
    boolean creationExisted = true;
    boolean lastUpdateExisted = true;
    if (collection == null) {
      return;
    }
    if (collection.getData() != null && !collection.getData().isEmpty()) {
      final T t = collection.getData().iterator().next();
      if (t instanceof ResourceBase res) {
        if (res.getCreation() == null) {
          creationExisted = false;
        }
        if (res.getLastUpdate() == null) {
          lastUpdateExisted = false;
        }
      }
    }
    if (creationExisted) {
      collection.add(Tool.sort(linkBuilder.toUriComponentsBuilder(), ActionName.CREATION).withRel(ActionName.LAST_CREATED));
    }
    if (lastUpdateExisted) {
      collection.add(Tool.sort(linkBuilder.toUriComponentsBuilder(), ActionName.UPDATED).withRel(ActionName.LAST_UPDATED));
    }
  }

  protected ResponseEntity<StreamingResponseBody> buildDownloadResponseEntity(InputStream inputStream, String fileTarget, String fileContentType,
          long fileSize) {
    final HttpHeaders respHeaders = this.buildDownloadResponseHeader(fileTarget, fileContentType, fileSize);

    // See
    // https://stackoverflow.com/questions/51845228/proper-way-of-streaming-using-responseentity-and-making-sure-the-inputstream-get
    final StreamingResponseBody responseBody = outputStream -> {
      int numberOfBytesToWrite;
      final byte[] data = new byte[SolidifyConstants.BUFFER_SIZE];
      while ((numberOfBytesToWrite = inputStream.read(data, 0, data.length)) != -1) {
        outputStream.write(data, 0, numberOfBytesToWrite);
      }
      inputStream.close();
    };

    return new ResponseEntity<>(responseBody, respHeaders, HttpStatus.OK);
  }

  protected ResponseEntity<FileSystemResource> buildDownloadResponseEntity(String path, String fileTarget, String fileContentType,
          long fileSize) {
    final HttpHeaders respHeaders = this.buildDownloadResponseHeader(fileTarget, fileContentType, fileSize);
    final FileSystemResource fsr = new FileSystemResource(path);
    return new ResponseEntity<>(fsr, respHeaders, HttpStatus.OK);
  }

  protected String getAuthenticatedUserExternalUid() {
    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    if (authentication != null && !(authentication instanceof AnonymousAuthenticationToken)) {
      return authentication.getName();
    } else {
      return null;
    }
  }

  protected HttpEntity<Result> returnHttpResult(Result result) {
    if (result == null) {
      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return switch (result.getStatus()) {
      default -> new ResponseEntity<>(result, HttpStatus.OK);
    };

  }

  private HttpHeaders buildDownloadResponseHeader(String fileTarget, String fileContentType, long fileSize) {
    final HttpHeaders respHeaders = new HttpHeaders();
    respHeaders.setContentType(MediaType.valueOf(fileContentType));
    if (fileSize > 0) {
      respHeaders.setContentLength(fileSize);
    }
    respHeaders.setContentDispositionFormData("attachment", fileTarget);
    return respHeaders;
  }

  protected String extractEndingPath(HttpServletRequest request) {

    String path = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
    String bestMatchPattern = (String) request.getAttribute(HandlerMapping.BEST_MATCHING_PATTERN_ATTRIBUTE);

    return new AntPathMatcher().extractPathWithinPattern(bestMatchPattern, path);
  }
}
