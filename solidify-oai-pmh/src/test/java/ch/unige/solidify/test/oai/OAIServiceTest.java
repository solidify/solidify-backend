/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH - OAIServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.oai;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.JAXBException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.OAIProperties;
import ch.unige.solidify.business.OAIMetadataPrefixService;
import ch.unige.solidify.business.OAISetService;
import ch.unige.solidify.exception.OAIException;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.oai.OAIMetadataPrefix;
import ch.unige.solidify.model.oai.OAISet;
import ch.unige.solidify.model.xml.oai.v2.GetRecordType;
import ch.unige.solidify.model.xml.oai.v2.IdentifyType;
import ch.unige.solidify.model.xml.oai.v2.ListIdentifiersType;
import ch.unige.solidify.model.xml.oai.v2.ListMetadataFormatsType;
import ch.unige.solidify.model.xml.oai.v2.ListRecordsType;
import ch.unige.solidify.model.xml.oai.v2.ListSetsType;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHtype;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.service.OAIService;
import ch.unige.solidify.specification.OAIMetadataPrefixSpecification;
import ch.unige.solidify.specification.OAISetSpecification;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.XMLTool;

@ActiveProfiles({ "sec-noauth" })
@ExtendWith(SpringExtension.class)
public abstract class OAIServiceTest {

  protected static final String HOST_URL = "http://localhost";
  private static final String QUERY = "*";

  protected OAIProperties oaiConfig;
  protected OAIService oaiService;

  protected GitInfoProperties gitInfoProperties = new GitInfoProperties();

  @Mock
  protected MessageService messageService;

  @Mock
  protected OAIMetadataPrefixService oaiMetadataPrefixService;

  @Mock
  protected OAISetService oaiSetService;

  protected Path workingDir = Paths.get("src", "test", "resources", "testing");

  @BeforeEach
  public void setup() {
    // Create working directory
    if (!FileTool.ensureFolderExists(this.workingDir)) {
      throw new SolidifyRuntimeException("Cannot create " + this.workingDir);
    }
    // Create DLCM Beans
    this.oaiConfig = new OAIProperties();
    // Mock OAI Metadata Prefix
    try {
      // OAI Dublin Core
      OAIMetadataPrefix oaidcMetadataPrefix = new OAIMetadataPrefix();
      oaidcMetadataPrefix.setPrefix(OAIConstants.OAI_DC);
      oaidcMetadataPrefix.setSchemaUrl(new URL(OAIConstants.OAI_DC_SCHEMA));
      oaidcMetadataPrefix.setSchemaNamespace(OAIConstants.OAI_DC_NAMESPACE);
      oaidcMetadataPrefix.setMetadataXmlTransformation(this.getMetadataXmlTransformation());
      oaidcMetadataPrefix.setEnabled(true);
      when(this.oaiMetadataPrefixService.findByPrefix(OAIConstants.OAI_DC)).thenReturn(oaidcMetadataPrefix);
      // Reference OAI metadata prefix
      String referenceMetadataPrefixName = this.getReferenceOAIMetadataPrefixName();
      OAIMetadataPrefix referenceMetadataPrefix = this.getReferenceOAIMetadataPrefix();
      when(this.oaiMetadataPrefixService.findByPrefix(referenceMetadataPrefixName)).thenReturn(referenceMetadataPrefix);
      // List
      List<OAIMetadataPrefix> oaiMetadataPrefixList = new ArrayList<>();
      oaiMetadataPrefixList.add(oaidcMetadataPrefix);
      oaiMetadataPrefixList.add(referenceMetadataPrefix);
      Page<OAIMetadataPrefix> oaiMetadataPrefixPage = new PageImpl<>(oaiMetadataPrefixList);
      when(this.oaiMetadataPrefixService.findAll(any(OAIMetadataPrefixSpecification.class), any(Pageable.class)))
              .thenReturn(oaiMetadataPrefixPage);
    } catch (IOException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }

    // Mock oaiSetService
    OAISet oaiSet = new OAISet();
    oaiSet.setSpec(OAIConstants.OAI_DOI_REGISTRATION);
    oaiSet.setName("DOI Registration");
    oaiSet.setDescription("Set for DOI harvesting");
    oaiSet.setQuery(QUERY);
    oaiSet.setEnabled(Boolean.TRUE);
    List<OAISet> oaiSetList = new ArrayList<>();
    oaiSetList.add(oaiSet);
    Page<OAISet> oaiSetPage = new PageImpl<>(oaiSetList);
    when(this.oaiSetService.findAll(any(OAISetSpecification.class), any(Pageable.class))).thenReturn(oaiSetPage);
    this.localSetup();
  }

  protected abstract void localSetup();

  protected abstract String getMetadataXmlTransformation() throws IOException;

  protected abstract String[] getMetadataList();

  protected abstract List<String> getIdentifierList();

  protected abstract String getReferenceOAIMetadataPrefixName();

  protected abstract OAIMetadataPrefix getReferenceOAIMetadataPrefix() throws MalformedURLException;

  @AfterEach
  public void purgeData() {
    FileTool.deleteFolder(this.workingDir);
  }

  @Test
  void identityTest() throws JAXBException, IOException {
    final OAIPMHtype oai = new OAIPMHtype();
    IdentifyType identify = this.oaiService.getIdentify(HOST_URL);
    assertNotNull(identify.getRepositoryName(), "Identify empty");
    oai.setIdentify(identify);
    String result = this.oaiService.printOAIPMH(oai, null, null, null);
    this.checkResult(result, "oaiIdentity.xml");
  }

  @Test
  void listMetadataFormatsTest() throws JAXBException, IOException {
    final OAIPMHtype oai = new OAIPMHtype();
    ListMetadataFormatsType metadataList = this.oaiService.listMetadataFormats(null);
    assertTrue(metadataList.getMetadataFormat().size() >= 2, "Missing metadata prefix");
    oai.setListMetadataFormats(metadataList);
    String result = this.oaiService.printOAIPMH(oai, null, null, null);
    this.checkResult(result, "oaiListMetadataFormats.xml");
  }

  @Test
  void listIdentifiersTest() {
    for (String metadataPrefix : this.getMetadataList()) {
      try {
        final OAIPMHtype oai = new OAIPMHtype();
        ListIdentifiersType idList = this.oaiService.listIdentifiers(metadataPrefix, null, null, null, null);
        assertTrue(idList.getHeader().size() > 0, "No IDs for " + metadataPrefix);
        oai.setListIdentifiers(idList);
        String result = this.oaiService.printOAIPMH(oai, null, metadataPrefix, null);
        this.checkResult(result, "oaiIdentity-" + metadataPrefix + ".xml");
      } catch (final Exception e) {
        this.returnException(e);
      }
    }
  }

  @Test
  void getRecordNotFoundTest() {
    assertThrows(OAIException.class, () -> this.oaiService.getRecord(HOST_URL, "xxx", OAIConstants.OAI_DC));
  }

  @Test
  void getRecordNotFoundResultTest() throws JAXBException, IOException {
    final OAIPMHtype oai = new OAIPMHtype();
    try {
      oai.setGetRecord(this.oaiService.getRecord(HOST_URL, "xxx", OAIConstants.OAI_DC));
    } catch (OAIException e) {
      String result = this.oaiService.printOAIPMH(oai, null, null, null);
      this.checkResult(result, "oaiGetRecordNotFound.xml");
    }
  }

  @Test
  void getRecordTest() {
    for (String metadataPrefix : this.getMetadataList()) {
      for (final String id : this.getIdentifierList()) {
        try {
          final OAIPMHtype oai = new OAIPMHtype();
          GetRecordType record = this.oaiService.getRecord(HOST_URL, id, metadataPrefix);
          assertNotNull(record.getRecord().getMetadata().getAny(), "Record empty for " + metadataPrefix);
          oai.setGetRecord(record);
          String result = this.oaiService.printOAIPMH(oai, null, metadataPrefix, null);
          this.checkResult(result, "oaiGetRecord-" + metadataPrefix + "-" + id + ".xml");
        } catch (final Exception e) {
          this.returnException(e);
        }
      }
    }
  }

  @Test
  void listRecordsTest() {
    for (String metadataPrefix : this.getMetadataList()) {
      try {
        final OAIPMHtype oai = new OAIPMHtype();
        ListRecordsType records = this.oaiService.listRecords(HOST_URL, metadataPrefix, null, null, null, null);
        assertTrue(records.getRecord().size() > 0, "Record list empty");
        oai.setListRecords(records);
        String result = this.oaiService.printOAIPMH(oai, null, metadataPrefix, null);
        this.checkResult(result, "oaiListRecords-" + metadataPrefix + ".xml");
      } catch (final Exception e) {
        this.returnException(e);
      }
    }
  }

  @Test
  void listSetsTest() throws JAXBException, IOException {
    final OAIPMHtype oai = new OAIPMHtype();
    ListSetsType setList = this.oaiService.listSets(null);
    assertTrue(setList.getSet().size() > 0, "Set list empty");
    oai.setListSets(setList);
    String result = this.oaiService.printOAIPMH(oai, null, null, null);
    this.checkResult(result, "oaiListSets.xml");
  }

  private void checkResult(String result, String fileName) throws IOException {
    assertFalse(StringTool.isNullOrEmpty(result));
    Path outputFile = this.workingDir.resolve(fileName);
    Files.write(outputFile, result.getBytes());
    XMLTool.wellformed(outputFile);
  }

  protected Path getPath(String f) {
    try {
      return new ClassPathResource(f).getFile().toPath();
    } catch (final IOException e) {
      return new FileSystemResource(f).getFile().toPath();
    }
  }

  protected void returnException(Exception e) {
    throw new SolidifyCheckingException(e.getMessage(), e);
  }
}
