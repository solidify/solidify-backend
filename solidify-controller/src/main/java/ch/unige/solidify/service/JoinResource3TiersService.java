/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - JoinResource3TiersService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.specification.Join3TiersSpecification;
import ch.unige.solidify.specification.JoinedTableInValues;
import ch.unige.solidify.specification.SolidifySpecification;

/**
 * Service layer used to manage a 3-tiers relationship
 *
 * @param <T> Type of the entity considered as the parent when managing the relation through the Relation3TiersController
 * @param <V> Type of the entity considered as the child when managing the relation through the Relation3TiersController
 * @param <W> Type of the entities considered as the grandchildren when managing the relation through the Relation3TiersController
 * @param <J> Type of the join resource entity (the relation table) between &lt;T&gt;, &lt;V&gt; and &lt;W&gt;
 */
public abstract class JoinResource3TiersService<T extends Resource, V extends Resource, W extends Resource, J extends JoinResource<?>>
        extends JoinResourceNTiersService<T, V, J> {

  /**
   * Find all relations in which the parent is involved
   *
   * @param parentId
   * @return
   */
  public List<J> findAllRelations(String parentId) {
    Join3TiersSpecification<J> specification = this.getJoinSpecification(parentId, null, null);
    specification.setFilterOnChildResId(false);
    specification.setFilterOnGrandChildResId(false);
    return this.joinRepository.findAll(specification);
  }

  /**
   * Find all relations between the parent and the child
   *
   * @param parentId
   * @param childId
   * @return
   */
  public List<J> findAllRelations(String parentId, String childId) {
    Join3TiersSpecification<J> specification = this.getJoinSpecification(parentId, childId, null);
    specification.setFilterOnChildResId(true);
    return this.joinRepository.findAll(specification);
  }

  /**
   * Return the relation between the parent, the child and the grandchild
   * <p>
   * Throws an exception if it doesn't exist
   *
   * @param parentId
   * @param childId
   * @param grandChildId
   * @return
   */
  public J findRelation(String parentId, String childId, String grandChildId) {
    Join3TiersSpecification<J> specification = this.getJoinSpecification(parentId, childId, grandChildId);
    specification.setFilterOnChildResId(true);
    specification.setFilterOnGrandChildResId(true);

    return this.joinRepository.findOne(specification)
            .orElseThrow(() -> new NoSuchElementException(
                    "No relation with parentId " + parentId + ", childId " + childId + " and grandChildId " + grandChildId));
  }

  /**
   * Check if a relation between the parent, the child and the grandchild exists
   *
   * @param parentId
   * @param childId
   * @param grandChildId
   * @return
   */
  public boolean relationExists(String parentId, String childId, String grandChildId) {
    try {
      this.findRelation(parentId, childId, grandChildId);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  /**
   * Save a relation between the parent, the child and the grandchild, with additional properties of the join resource
   *
   * @param parentId
   * @param childId
   * @param grandChildId
   * @param joinResource
   * @return
   */
  public J saveRelation(String parentId, String childId, String grandChildId, J joinResource) {
    return this.saveRelation(this.completeJoinResourceRelations(joinResource, parentId, childId, grandChildId));
  }

  /**
   * Return a specification used to filter a list of children linked to parent
   *
   * @param parentId
   * @param childCriteria
   * @return
   */
  public SolidifySpecification<V> getChildSpecification(String parentId, V childCriteria) {
    SolidifySpecification<V> childSpecification = this.getChildSpecification(childCriteria);

    // select a distinct list of children linked to parent
    childSpecification.addJoinTableInValues(new JoinedTableInValues(this.getChildToRelationPropertyName(),
            this.getJoinSpecification(parentId, childCriteria).getPathToParentId(), Arrays.asList(parentId)));

    return childSpecification;
  }

  /**
   * Return a specification to search for a specific relation
   *
   * @param parentId
   * @param childId
   * @param grandChildId
   * @return
   */
  public Join3TiersSpecification<J> getJoinSpecification(String parentId, String childId, String grandChildId) {
    J joinResource = this.completeJoinResourceRelations(this.getEmptyJoinResourceObject(), parentId, childId, grandChildId);
    return this.getJoinSpecification(joinResource);
  }

  /**
   * Return a specification used to filter a list of children
   *
   * @param parentId
   * @param childResource
   * @return
   */
  public Join3TiersSpecification<J> getJoinSpecification(String parentId, V childResource) {

    T parentResource = this.getEmptyParentResourceObject();
    parentResource.setResId(parentId);

    J joinResource = this.getEmptyJoinResourceObject();
    this.setParentResource(joinResource, parentResource);
    this.setChildResource(joinResource, childResource);

    return this.getJoinSpecification(joinResource);
  }

  /**
   * Return a new JoinResource that can be saved in database to create a new relation between entities the parent, the child and the grandchild,
   * with properties of J
   *
   * @param parentId
   * @param childId
   * @param grandChildId
   * @param joinResource
   * @return
   */
  public J completeJoinResourceRelations(J joinResource, String parentId, String childId, String grandChildId) {
    T parentResource = this.getEmptyParentResourceObject();
    parentResource.setResId(parentId);

    V childResource = this.getEmptyChildResourceObject();
    childResource.setResId(childId);

    W grandChildResource = this.getEmptyGrandChildResourceObject();
    grandChildResource.setResId(grandChildId);

    this.setParentResource(joinResource, parentResource);
    this.setChildResource(joinResource, childResource);
    this.setGrandChildResource(joinResource, grandChildResource);

    return joinResource;
  }

  /**
   * Get default list of grandchildren ids when creating relations with the parent and the child only
   * <p>
   * Not implemented by default. Override if required.
   *
   * @return
   */
  public List<String> getDefaultGrandChildIds() {
    throw new SolidifyRuntimeException("No default grandchildren defined for this relation");
  }

  /****************************************************************************************************/

  public abstract Join3TiersSpecification<J> getJoinSpecification(J joinResource);

  /**
   * Return a specification to search on child
   *
   * @param childCriteria
   * @return
   */
  protected abstract SolidifySpecification<V> getChildSpecification(V childCriteria);

  /**
   * Return the name of the child property that links to the join table
   *
   * @return
   */
  protected abstract String getChildToRelationPropertyName();

  /**
   * Return a new instance of grandchild
   *
   * @return
   */
  public abstract W getEmptyGrandChildResourceObject();

  /**
   * Set the grandchild property of the join resource
   *
   * @param joinResource
   * @param grandChildResource
   */
  public abstract void setGrandChildResource(J joinResource, W grandChildResource);

  /**
   * Return the grandchild property of the join resource
   *
   * @param joinResource
   * @return
   */
  public abstract W getGrandChildResource(J joinResource);

  /**
   * Return an object used to display the child resource along with its list of grandchildren DTO
   *
   * @param childItem
   * @param joinResources
   * @return
   */
  public abstract Relation3TiersChildDTO getChildDTO(V childItem, List<J> joinResources);

  /**
   * Return an object used to display a grandchild with the relation properties
   *
   * @param joinResource
   * @param <D>
   * @return
   */
  public abstract <D extends JoinResourceContainer<J>> D getGrandChildDTO(J joinResource);
}
