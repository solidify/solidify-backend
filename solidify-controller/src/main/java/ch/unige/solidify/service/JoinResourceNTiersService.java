/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - JoinResourceNTiersService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.repository.JoinRepository;
import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.validation.ValidationError;

public abstract class JoinResourceNTiersService<T extends Resource, V extends Resource, J extends JoinResource<?>> extends
        AbstractResourceService {

  @Autowired
  private Validator validator;

  @Autowired
  protected MessageService messageService;

  @Autowired
  protected JoinRepository<J> joinRepository;

  /****************************************************************************************************/

  /**
   * Save the relation
   *
   * @param joinResource
   * @return
   */
  public J saveRelation(J joinResource) {
    this.beforeValidate(joinResource);
    this.validate(joinResource);
    this.beforeSave(joinResource);
    return this.joinRepository.save(joinResource);
  }

  /**
   * Delete the relation
   *
   * @param joinResource
   */
  public void deleteRelation(J joinResource) {
    this.joinRepository.delete(joinResource);
  }

  /**
   * Return a JoinResource with eventual default relation's properties
   * <p>
   * By default return a join resource with no additional property. Override to set them if there are any.
   *
   * @return
   */
  public J getDefaultJoinResource() {
    return this.getEmptyJoinResourceObject();
  }

  /****************************************************************************************************/

  /**
   * Return a new instance of join resource
   *
   * @return
   */
  public abstract J getEmptyJoinResourceObject();

  /**
   * Return a new instance of parent
   *
   * @return
   */
  public abstract T getEmptyParentResourceObject();

  /**
   * Return a new instance of child
   *
   * @return
   */
  public abstract V getEmptyChildResourceObject();

  /**
   * Set the parent property of the join resource
   *
   * @param joinResource
   * @param parentResource
   */
  public abstract void setParentResource(J joinResource, T parentResource);

  /**
   * Set the child property of the join resource
   *
   * @param joinResource
   * @param childResource
   */
  public abstract void setChildResource(J joinResource, V childResource);

  /****************************************************************************************************/

  /**
   * Override to set default data or clean properties
   *
   * @param joinResource
   */
  protected void beforeValidate(J joinResource) {
  }

  /**
   * Code executed after validation but before saving entity
   *
   * @param joinResource
   */
  protected void beforeSave(J joinResource) {
    this.setAuthenticatedUserProperties(joinResource);
    joinResource.updateTime();
  }

  protected void validate(J joinResource) {

    final BindingResult errors = new BeanPropertyBindingResult(joinResource, joinResource.getClass().getName());

    this.validateConstraints(joinResource, errors);
    this.validateItemSpecificRules(joinResource, errors);

    if (errors.hasErrors()) {
      throw new SolidifyValidationException(new ValidationError(errors), errors.toString());
    }
  }

  /**
   * Validate Bean constraints
   *
   * @param joinResource
   * @param errors
   */
  protected void validateConstraints(J joinResource, BindingResult errors) {
    this.validator.validate(joinResource, errors);
  }

  /**
   * Override to add specific validation logic
   *
   * @param joinResource
   * @param errors
   */
  protected void validateItemSpecificRules(J joinResource, BindingResult errors) {
  }
}
