/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - MockUser.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.specification.dao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;

@Entity
public class MockUser {
  private int age;
  private LocalDate birthday;
  private Long carKilos;
  private Integer childNumber;
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private LocalDateTime marriageTime;
  private OffsetDateTime meetingTime;
  private String name;
  private BigDecimal salary;
  @OneToOne
  @JoinColumn(name = "info_id")
  private MockUserInfo userInfo;

  public MockUser() {
    super();
  }

  public MockUser(String name, int age) {
    this.name = name;
    this.age = age;
  }

  public MockUser(String name, int age, Integer childNumber, Long carKilos, BigDecimal salary,
          LocalDate birthday,
          LocalDateTime marriageTime, OffsetDateTime meetingTime) {
    this.name = name;
    this.age = age;
    this.childNumber = childNumber;
    this.carKilos = carKilos;
    this.salary = salary;
    this.birthday = birthday;
    this.marriageTime = marriageTime;
    this.meetingTime = meetingTime;
  }

  public int getAge() {
    return this.age;
  }

  public LocalDate getBirthday() {
    return this.birthday;
  }

  public Long getCarKilos() {
    return this.carKilos;
  }

  public Integer getChildNumber() {
    return this.childNumber;
  }

  public Long getId() {
    return this.id;
  }

  public LocalDateTime getMarriageTim() {
    return this.marriageTime;
  }

  public LocalDateTime getMarriageTime() {
    return this.marriageTime;
  }

  public OffsetDateTime getMeetingTime() {
    return this.meetingTime;
  }

  public String getName() {
    return this.name;
  }

  public BigDecimal getSalary() {
    return this.salary;
  }

  public MockUserInfo getUserInfo() {
    return this.userInfo;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public void setBirthday(LocalDate birthday) {
    this.birthday = birthday;
  }

  public void setCarKilos(Long carKilos) {
    this.carKilos = carKilos;
  }

  public void setChildNumber(Integer childNumber) {
    this.childNumber = childNumber;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setMarriageTim(LocalDateTime marriageTim) {
    this.marriageTime = marriageTim;
  }

  public void setMarriageTime(LocalDateTime marriageTime) {
    this.marriageTime = marriageTime;
  }

  public void setMeetingTime(OffsetDateTime meetingTime) {
    this.meetingTime = meetingTime;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSalary(BigDecimal salary) {
    this.salary = salary;
  }

  public void setUserInfo(MockUserInfo userInfo) {
    this.userInfo = userInfo;
  }

  @Override
  public String toString() {
    return "MockUser{" +
            "id=" + this.id +
            ", name='" + this.name + '\'' +
            ", age=" + this.age +
            ", childNumber=" + this.childNumber +
            ", carKilos=" + this.carKilos +
            ", salary=" + this.salary +
            ", birthday=" + this.birthday +
            ", marriageTime=" + this.marriageTime +
            '}';
  }
}
