/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Email - EmailConfiguration.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.config;

import java.nio.charset.StandardCharsets;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.thymeleaf.spring6.SpringTemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.util.StringTool;

@Configuration
@Profile("email-service")
public class EmailConfiguration {

  public EmailConfiguration(SolidifyProperties config, SpringTemplateEngine templateEngine) {
    // add template resolver for external templates
    if (!StringTool.isNullOrEmpty(config.getEmail().getExternalTemplates())) {
      templateEngine.addTemplateResolver(this.filesystemTemplateResolver(config.getEmail().getExternalTemplates()));
    }
    // Add template resolver for internal templates
    if (!StringTool.isNullOrEmpty(config.getEmail().getInternalTemplates())) {
      templateEngine.addTemplateResolver(this.emailTemplateResolver(config.getEmail().getInternalTemplates()));
    }
  }

  private ITemplateResolver filesystemTemplateResolver(String location) {
    FileTemplateResolver templateResolver = new FileTemplateResolver();
    templateResolver.setPrefix(location);
    templateResolver.setSuffix(SolidifyConstants.HTML_EXT);
    templateResolver.setTemplateMode(TemplateMode.HTML);
    templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
    templateResolver.setOrder(0);
    templateResolver.setCheckExistence(true);
    templateResolver.setCacheable(false);
    return templateResolver;
  }

  private ITemplateResolver emailTemplateResolver(String location) {
    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
    templateResolver.setPrefix(location);
    templateResolver.setSuffix(SolidifyConstants.HTML_EXT);
    templateResolver.setTemplateMode(TemplateMode.HTML);
    templateResolver.setCharacterEncoding(StandardCharsets.UTF_8.name());
    templateResolver.setOrder(1);
    templateResolver.setCheckExistence(true);
    templateResolver.setCacheable(false);
    return templateResolver;
  }

}
