/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - ValidationToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import ch.unige.solidify.util.ValidationTool;

class ValidationToolTest {
  @Test
  void testNegativePMID() {
    assertFalse(ValidationTool.isValidPMID("-1"));
  }

  @Test
  void testAlphabeticalPMID() {
    assertFalse(ValidationTool.isValidPMID("abc"));
  }

  @Test
  void testZeroPMID() {
    assertFalse(ValidationTool.isValidPMID("0"));
  }

  @Test
  void testRegularPMID() {
    assertTrue(ValidationTool.isValidPMID("33740420"));
  }

  @Test
  void testBigPMID() {
    assertTrue(ValidationTool.isValidPMID("094215760941650195690145612049561240956109564129056129056910256914025691056901659012564105"
            + "275621049576041957601560124965109650942165094215691056910659102569205612654012650251025702475204756024967209467029679026661651"));
  }

  @Test
  void testMaxYear() {
    int currentYear = LocalDate.now().getYear();
    int maxYear = currentYear + 10;
    assertTrue(ValidationTool.isValidDate(String.valueOf(maxYear), new String[] { "d.m.yyyy" }));
    assertFalse(ValidationTool.isValidDate(String.valueOf(maxYear + 1), new String[] { "d.m.yyyy" }));
  }

  @Test
  void testDateAsYear() {
    assertTrue(ValidationTool.isValidDate("2020", null));
    assertTrue(ValidationTool.isValidDate("2020", new String[] { "yyyy-MM-dd", "yyyy" }));

    assertFalse(ValidationTool.isValidDate("202", new String[] { "d.m.yyyy" }));
    assertFalse(ValidationTool.isValidDate("-202", new String[] { "d.m.yyyy" }));
    assertFalse(ValidationTool.isValidDate("2500", new String[] { "d.m.yyyy" }));
  }

  @Test
  void testDateWithFormatSpecified() {
    int currentYear = LocalDate.now().getYear();
    int maxYear = currentYear + 10;

    assertTrue(ValidationTool.isValidDate("2020-08", new String[] { "yyyy-MM-dd", "yyyy-MM" }));
    assertTrue(ValidationTool.isValidDate("2020-11", new String[] { "yyyy-MM" }));
    assertTrue(ValidationTool.isValidDate("2020-9", new String[] { "yyyy-M" }));
    assertTrue(ValidationTool.isValidDate("2020-09-01", new String[] { "yyyy-MM-dd" }));
    assertTrue(ValidationTool.isValidDate("2020-09-01", new String[] { "yyyy-M-d" }));
    assertTrue(ValidationTool.isValidDate("2020-9-1", new String[] { "yyyy-M-d" }));
    assertTrue(ValidationTool.isValidDate("01.09.2020", new String[] { "dd.MM.yyyy" }));
    assertTrue(ValidationTool.isValidDate("01.09.2020", new String[] { "yyyy-MM", "d.M.yyyy" }));
    assertTrue(ValidationTool.isValidDate("1.9.2020", new String[] { "d.M.yyyy" }));
    assertTrue(ValidationTool.isValidDate("1.9." + maxYear, new String[] { "d.M.yyyy" }));

    assertFalse(ValidationTool.isValidDate("2020-9-", new String[] { "yyyy-MM-yy", "yyyy-MM" }));
    assertFalse(ValidationTool.isValidDate("2020-9", new String[] { "yyyy-MM-yy" }));
    assertFalse(ValidationTool.isValidDate("2020-10-12", new String[] { "yyyy-MM" }));
    assertFalse(ValidationTool.isValidDate("2020-09-01", null));
    assertFalse(ValidationTool.isValidDate("2020-9-", new String[] { "yyyy-MM" }));
    assertFalse(ValidationTool.isValidDate("2020-9-ww", new String[] { "yyyy-MM-yy" }));
    assertFalse(ValidationTool.isValidDate("10-2020", new String[] { "yyyy-MM" }));
    assertFalse(ValidationTool.isValidDate("2020-09-01", new String[] { "" }));
    assertFalse(ValidationTool.isValidDate("2020-09-01", new String[] { "dd.MM.yyyy" }));
    assertFalse(ValidationTool.isValidDate("2020-9-1", new String[] { "d.M.yyyy" }));
    assertFalse(ValidationTool.isValidDate("01.09.2020", null));
    assertFalse(ValidationTool.isValidDate("01.09.2020", new String[] { "" }));
    assertFalse(ValidationTool.isValidDate("01.09.2020", new String[] { "yyyy-MM-dd" }));
    assertFalse(ValidationTool.isValidDate("1.9.2020", new String[] { "dd.MM.yyyy" }));
    assertFalse(ValidationTool.isValidDate("1.9." + (maxYear + 1), new String[] { "d.M.yyyy" }));
  }

  @ParameterizedTest
  @ValueSource(strings = { "112", "112.456", "112.456.678", "112/456", "112.456/789", "112.456 / 789", "112.456/789.986",
          "112.456 / 789.986/45678", "112-456", "112 - 456", "112 - 456 /314.9" })
  void testValidDewey(String dewey) {
    assertTrue(ValidationTool.isValidDeweyCode(dewey));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "abcd", "1234\\789", "1234//789", "1234--789", "12345.568 / 12546 / test" })
  void testInvalidDewey(String dewey) {
    assertFalse(ValidationTool.isValidDeweyCode(dewey));
  }

  @ParameterizedTest
  @ValueSource(strings = { "0000-0001-2345-6789", "0000-0001-5109-3700", "0000-0002-1694-233X" })
  void testValidOrcid(String orcid) {
    assertTrue(ValidationTool.isValidOrcid(orcid));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "0000-0001-2345-678", "0000-0001-234-6789", "0000-001-2345-6789", "000-0001-2345-6789", "00000001-2345-6789",
          "0000-0001-2345-678Y" })
  void testInvalidOrcid(String orcid) {
    assertFalse(ValidationTool.isValidOrcid(orcid));
  }

  @ParameterizedTest
  @ValueSource(strings = { "1910.10964", "patt-sol/9311007", "cond-mat/9507074", "cond-mat/9507074v1", "1501.00001v1", "0706.0001v2" })
  void testValidArxivId(String arxivId) {
    assertTrue(ValidationTool.isValidArxivId(arxivId));
  }

  @ParameterizedTest
  @ValueSource(strings = { "", "aaaa+", "%v2v2v2v", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" })
  void testInvalidArxivId(String arxivId) {
    assertFalse(ValidationTool.isValidArxivId(arxivId));
  }

  @ParameterizedTest
  @ValueSource(strings = { "urn:nbn:ch:unige-1495076", "urn:nbn:ch:unige-6002", "urn:lex:eu:council:directive:2010-03-09;2010-19-UE",
          "urn:my-nid:my-custo@m-nss/test:yiha" })
  void testValidURN(String urn) {
    assertTrue(ValidationTool.isValidURN(urn));
  }

  @ParameterizedTest
  @ValueSource(strings = { "", "urn:::", "urn;nbn:ch:unige-1495076", "urn:nbn-ch;unige-1495076", "urn:my-nid:my-custo@m-nss/tes\nt:yiha" })
  void testInvalidURN(String urn) {
    assertFalse(ValidationTool.isValidURN(urn));
  }

  @ParameterizedTest
  @ValueSource(strings = { "10.26037/yareta:aehmmdpe7rhorli7h5xcmtlm24", "10.1007/978-3-642-28108-2_19", "10.1016/S0735-1097(98)00347-7",
          "10.1109/5.771073", "10.1186/s40101-021-00255-z", "10.1002/1522-2675(20010131)84:1<141::AID-HLCA141>3.0.CO;2-R",
          "10.1002/1522-2675(20010228)84:2<416::AID-HLCA416>3.0.CO;2-K" })
  void testValidDOI(String doi) {
    assertTrue(ValidationTool.isValidDOI(doi));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "0000-0001-2345-678", "11.1109/5.771073", "10.109/5.771073", "10.nature/34.77802", "10.10056.21/34.77802" })
  void testInvalidDOI(String doi) {
    assertFalse(ValidationTool.isValidDOI(doi));
  }

  @ParameterizedTest
  @ValueSource(strings = { "01swzsf04", "02s376052", "05a28rw58", "02crff812", "01xkakk17", "05pmsvm27", "01m1pv723", "007ygn379", "02fsagg23",
          "00yjd3n13", "012y9zp98" })
  void testValidRORid(String rorId) {
    assertTrue(ValidationTool.isValidRorId(rorId));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "0000-0001-2345-678", "11.1109/5.771073", "01swif04", "01slzsf04", "01uwzsf04", "01sozsf04" })
  void testInvalidRORid(String rorId) {
    assertFalse(ValidationTool.isValidRorId(rorId));
  }

  @ParameterizedTest
  @ValueSource(strings = { "ark:/99999/0123456789abcdefghijklmnopqrstuvwxyz=#*+@_$%-", "ark:/64642/yareta#dgaky2kvejeblnnsm577koaljy",
          "ark:/64642/yareta#aehmmdpe7rhorli7h5xcmtlm24", "ark:/12025/654xz321", "ark:/12025/654xz321", "ark:/53355/cl010066723",
          "ark:/12148/bpt6k15164019" })
  void testValidARK(String arkId) {
    assertTrue(ValidationTool.isValidARKv18(arkId));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "ark:/NAAN/Name", "ark:/NAAN/Name", "0000-0001-2345-678", "11.1109/5.771073", "01swif04" })
  void testInvalidARK(String arkId) {
    assertFalse(ValidationTool.isValidARKv18(arkId));
  }

  @ParameterizedTest
  @ValueSource(strings = { "ark:99999/fk9bcdfghjkmnpqrstvwxz0123456789=~*+@_$%-", "ark:/99999/fk9bcdfghjkmnpqrstvwxz0123456789=~*+@_$%-",
          "ark:64642/rt9ght56", "ark:12345/x6np1wh8k", "ark:12148/bpt6k15164019", "ark:/12148/bpt6k15164019" })
  void testValidNewARK(String arkId) {
    assertTrue(ValidationTool.isValidARKv37(arkId));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "ark:999999/9bcdfghjkmnpqrstvwxz0123456789=~*+@_$", "ark:/NAAN/Name", "ark:/NAAN/Name", "0000-0001-2345-678",
          "11.1109/5.771073", "01swif04" })
  void testInvalidNewARK(String arkId) {
    assertFalse(ValidationTool.isValidARKv37(arkId));
  }

  @ParameterizedTest
  @ValueSource(strings = { "001a23cc-c5d4-41fa-a1e9-014622f7716a", "00b348b6-4794-41e9-914b-679eeb2e3fb9",
          "26929514-237c-11ed-861d-0242ac120002" })
  void testValidUUID(String uuid) {
    assertTrue(ValidationTool.isValidUUID(uuid));
  }

  @ParameterizedTest
  @NullSource
  @ValueSource(strings = { "", "ark:/NAAN/Name", "10.26037/yareta:aehmmdpe7rhorli7h5xcmtlm24", "0000-0001-2345-678", "11.1109/5.771073",
          "01swif04" })
  void testInvalidUUID(String uuid) {
    assertFalse(ValidationTool.isValidUUID(uuid));
  }

}
