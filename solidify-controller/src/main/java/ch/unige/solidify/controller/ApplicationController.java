/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - ApplicationController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.Link;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.model.Module;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.security.UserPermissions;
import ch.unige.solidify.service.ModuleService;

@UserPermissions
@RestController
public class ApplicationController {

  private SolidifyProperties config;

  private ModuleService moduleService;

  public ApplicationController(SolidifyProperties config, ModuleService moduleService) {
    this.config = config;
    this.moduleService = moduleService;
  }

  @GetMapping("/")
  public HttpEntity<Module> home() {
    final Module backend = new Module(this.config.getServerDisplayName());
    backend.add(linkTo(methodOn(ApplicationController.class).home()).withSelfRel());
    backend.add(Link.of(linkTo(methodOn(ApplicationController.class).home()) + ActionName.DASHBOARD).withRel(ActionName.DASHBOARD));
    if (this.config.getSitemap().isEnable()) {
      backend.add(Link.of(linkTo(methodOn(ApplicationController.class).home()) + ActionName.SITEMAP_XML).withRel(ActionName.SITEMAP_XML));
    }
    for (final ModuleController module : this.moduleService.getModuleControllerList()) {
      backend.add(linkTo(methodOn(module.getClass()).home()).withRel(module.getName()));
    }
    return new ResponseEntity<>(backend, HttpStatus.OK);
  }

}
