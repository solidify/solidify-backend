/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - JoinedTableInValues.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import java.util.ArrayList;
import java.util.List;

public class JoinedTableInValues {
  private String relationName;
  private String fieldName;
  private final List<Object> values = new ArrayList<>();

  public JoinedTableInValues(String relationName, String fieldName, List<Object> values) {
    this.relationName = relationName;
    this.fieldName = fieldName;
    this.values.addAll(values);
  }

  public String getRelationName() {
    return this.relationName;
  }

  public String getFieldName() {
    return this.fieldName;
  }

  public List<Object> getValues() {
    return this.values;
  }

  public void addValue(Object value) {
    this.values.add(value);
  }
}
