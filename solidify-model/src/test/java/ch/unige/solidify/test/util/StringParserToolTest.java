/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - StringParserToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.SearchOperation;
import ch.unige.solidify.util.StringParserTool;

class StringParserToolTest {

  @Test
  void testString() {
    final String search = "user.lastName:<w!.*\\\\,ang%,age≥30_60,i-my_test_9.is§this@gmail.com";
    final List<SearchCriteria> criterias = StringParserTool.parseSearchString(search);
    assertEquals(3, criterias.size());
    for (final SearchCriteria item : criterias) {
      System.out.println(item);
    }
  }

  @Test
  void testStringWithEsacpecharacters() {
    final String search = "statut%C2%ACA,isDomPers:true,meetingTime:2007-12-03T10:15:30+01:00";
    final List<SearchCriteria> criterias = StringParserTool.parseSearchString(search);
    assertEquals(3, criterias.size());

  }

  @Test
  void testUrlWithAllOperation() {
    for (final String operator : SearchOperation.getOperationList()) {
      String search = "meetingTime" + operator + "2007-12-03T10:15:30+01:00";
      if (SearchOperation.getOperation(operator) == SearchOperation.BETWEEN) {
        search = "marriageTime∞2003-10-03T10:15:30_2016-02-03T12:15:30";
      }
      final List<SearchCriteria> criterias = StringParserTool.parseSearchString(search);
      assertEquals(1, criterias.size());
      for (final SearchCriteria item : criterias) {
        System.out.println(item);
      }
    }

  }

}
