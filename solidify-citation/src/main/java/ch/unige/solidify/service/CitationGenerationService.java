/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Citation - CitationGenerationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.undercouch.citeproc.CSL;
import de.undercouch.citeproc.ListItemDataProvider;
import de.undercouch.citeproc.csl.CSLItemData;

import ch.unige.solidify.business.CitationService;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.Citation;
import ch.unige.solidify.model.config.FormatsConfig;
import ch.unige.solidify.model.dto.CitationDto;

/**
 * This service allows to generate citations or bibliographies texts by using a Citation Style Language (CSL) processor (see
 * https://citationstyles.org/). It uses an ItemDataProvider class defined at the project level which is responsible for retrieving the metadata
 * used to generate texts.
 * <p>
 * As computing texts may be a relatively slow process, the results are stored in database and thus computed only once.
 */
public abstract class CitationGenerationService {

  private static final Logger log = LoggerFactory.getLogger(CitationGenerationService.class);

  private FormatsConfig bibliographiesFormats;
  private FormatsConfig citationsFormats;

  private CitationService citationService;

  private SolidifyItemDataProvider itemDataProvider;

  protected CitationGenerationService(CitationService citationService, SolidifyItemDataProvider itemDataProvider,
          FormatsConfig bibliographiesFormats, FormatsConfig citationsFormats) {
    this.citationService = citationService;
    this.itemDataProvider = itemDataProvider;

    this.bibliographiesFormats = bibliographiesFormats;
    this.citationsFormats = citationsFormats;
  }

  /**
   * Get the 'bibliography' text for the item, according to the given style, language and output format. The text is generated on the fly if it
   * doesn't already exist in database.
   *
   * @param itemId
   * @param style
   * @param language
   * @param outputFormat
   * @return
   */
  public String getBibliographyText(String itemId, String style, String language, Citation.OutputFormat outputFormat) {
    return this.getCitation(Citation.Mode.BIBLIOGRAPHY, itemId, style, language, outputFormat).getText();
  }

  /**
   * Get the 'citation' text for the item, according to the given style, language and output format. The text is generated on the fly if it
   * doesn't already exist in database.
   *
   * @param itemId
   * @param style
   * @param language
   * @param outputFormat
   * @return
   */
  public String getCitationText(String itemId, String style, String language, Citation.OutputFormat outputFormat) {
    return this.getCitation(Citation.Mode.CITATION, itemId, style, language, outputFormat).getText();
  }

  /**
   * Get all bibliographies according to the configured styles, languages and output formats for the given archive
   *
   * @param itemId
   * @return
   */
  public List<CitationDto> getBibliographiesDtos(String itemId) {
    return this.getDtos(Citation.Mode.BIBLIOGRAPHY, itemId);
  }

  public List<CitationDto> getCitationsDtos(String itemId) {
    return this.getDtos(Citation.Mode.CITATION, itemId);
  }

  /**
   * Delete already generated Citations for an item. Useful if the citation texts must be regenerated.
   *
   * @param itemId
   */
  public void deleteAllStoredCitationsForItem(String itemId) {
    this.citationService.deleteAllForItem(itemId);
  }

  /**
   * Get a CSL processor for the given style and language.
   *
   * @param style
   * @param language
   * @return
   */
  protected CSL getCslProcessor(String itemId, String style, String language, Citation.OutputFormat outputFormat) {
    try {
      log.debug("Initializing CSL processor for style '{}', language '{}'", style, language);
      CSLItemData cslItemData = this.itemDataProvider.retrieveItem(itemId, style, language, outputFormat);
      CSL cslProcessor = new CSL(new ListItemDataProvider(cslItemData), style, language);
      log.debug("CSL processor for style '{}', language '{}' initialized", style, language);
      return cslProcessor;
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Unable to initialize CSL processor", e);
    }
  }

  private List<CitationDto> getDtos(Citation.Mode mode, String itemId) {

    FormatsConfig formatsConfig;
    if (mode == Citation.Mode.BIBLIOGRAPHY) {
      formatsConfig = this.bibliographiesFormats;
    } else {
      formatsConfig = this.citationsFormats;
    }

    List<CitationDto> bibliographies = new ArrayList<>();
    for (String style : formatsConfig.getStyles()) {
      for (String language : formatsConfig.getLanguages()) {
        for (String outputFormatStr : formatsConfig.getOutputFormats()) {
          Citation.OutputFormat outputFormat = Citation.OutputFormat.valueOf(outputFormatStr.toUpperCase());
          Citation citation = this.getCitation(mode, itemId, style, language, outputFormat);
          bibliographies.add(new CitationDto(citation));
        }
      }
    }
    return bibliographies;
  }

  /**
   * Fetch the corresponding Citation entity from database if it already exists or generate the text by using a CSL processor and store the
   * result in database.
   *
   * @param mode
   * @param itemId
   * @param style
   * @param language
   * @param outputFormat
   * @return
   */
  private Citation getCitation(Citation.Mode mode, String itemId, String style, String language,
          Citation.OutputFormat outputFormat) {
    Citation citation = this.citationService.findByItemIdAndModeAndStyleAndLanguageAndOutputFormat(itemId, mode, style, language,
            outputFormat);
    if (citation == null) {
      return this.generateCitationIfRequired(mode, itemId, style, language, outputFormat);
    }
    return citation;
  }

  /**
   * This synchronized method is called only if citation doesn't exist yet. The aim is to slow down process execution only when a citation
   * has not been generated yet.
   *
   * @param mode
   * @param itemId
   * @param style
   * @param language
   * @param outputFormat
   * @return
   */
  private synchronized Citation generateCitationIfRequired(Citation.Mode mode, String itemId, String style, String language,
          Citation.OutputFormat outputFormat) {
    Citation citation = this.citationService.findByItemIdAndModeAndStyleAndLanguageAndOutputFormat(itemId, mode, style, language,
            outputFormat);
    if (citation == null) {
      String citationText;
      if (mode == Citation.Mode.BIBLIOGRAPHY) {
        citationText = this.generateBibliography(itemId, style, language, outputFormat);
      } else {
        citationText = this.generateCitation(itemId, style, language, outputFormat);
      }
      citation = this.citationService.saveNewCitation(itemId, mode, style, language, outputFormat, citationText);
    }
    return citation;
  }

  /**
   * Use a CSL processor to generate a 'bibliography' text for the given item id corresponding the given style, language and output format.
   *
   * @param itemId
   * @param style
   * @param language
   * @param outputFormat
   * @return
   */
  private String generateBibliography(String itemId, String style, String language, Citation.OutputFormat outputFormat) {
    CSL cslProcessor = this.getCslProcessor(itemId, style, language, outputFormat);
    cslProcessor.registerCitationItems(itemId);
    cslProcessor.setOutputFormat(outputFormat.name().toLowerCase());
    return cslProcessor.makeBibliography().makeString();
  }

  /**
   * Use a CSL processor to generate a 'citation' text for the given item id corresponding the given style, language and output format.
   *
   * @param itemId
   * @param style
   * @param language
   * @param outputFormat
   * @return
   */
  private String generateCitation(String itemId, String style, String language, Citation.OutputFormat outputFormat) {
    CSL cslProcessor = this.getCslProcessor(itemId, style, language, outputFormat);
    cslProcessor.registerCitationItems(itemId);
    cslProcessor.setOutputFormat(outputFormat.name().toLowerCase());
    return cslProcessor.makeCitation(itemId).get(0).getText();
  }
}
