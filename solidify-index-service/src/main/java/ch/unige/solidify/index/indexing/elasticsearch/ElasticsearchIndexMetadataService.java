/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Service - ElasticsearchIndexMetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.index.indexing.elasticsearch;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.index.ElasticsearchClientProvider;
import ch.unige.solidify.model.index.IndexMetadata;

public abstract class ElasticsearchIndexMetadataService<T extends IndexMetadata> extends ElasticsearchService<T> {

  private final String indexName;

  protected ElasticsearchIndexMetadataService(
          IndexConfigProperties indexConfigProperties,
          ElasticsearchClientProvider elasticsearchClientProvider,
          String indexName,
          Class<T> indexEntryClass) {
    super(indexConfigProperties, elasticsearchClientProvider, indexEntryClass);
    this.indexName = indexName;
  }

  @Override
  public String getIndexName() {
    return this.indexName;
  }

}
