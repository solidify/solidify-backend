/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ResourceFile.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.util.Arrays;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import jakarta.persistence.Lob;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;

@Schema(description = "ResourceFile represents a files.")
@MappedSuperclass
public abstract class ResourceFile extends ResourceNormalized {

  private static final int DB_FILENAME_LENGTH = 2024;

  @Schema(description = "The file content.")
  @JsonIgnore
  @Lob
  @Column(length = SolidifyConstants.DB_MAX_STRING_LENGTH)
  @NotNull
  private byte[] fileContent;

  @Schema(description = "The file name.")
  @Size(max = DB_FILENAME_LENGTH)
  private String fileName;

  @Schema(description = "The file size in bytes.")
  private Long fileSize;

  @Schema(description = "The content type of the file. It could be named as MIME type or media type.")
  private String mimeType;

  protected ResourceFile() {
  }

  public void setFileContent(byte[] fileContent) {
    this.fileContent = fileContent;
  }

  public void setFileSize(Long fileSize) {
    this.fileSize = fileSize;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public byte[] getFileContent() {
    return this.fileContent;
  }

  public Long getFileSize() {
    return this.fileSize;
  }

  public String getFileName() {
    return this.fileName;
  }

  public String getMimeType() {
    return this.mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || this.getClass() != o.getClass())
      return false;
    if (!super.equals(o))
      return false;
    ResourceFile that = (ResourceFile) o;
    return Arrays.equals(this.fileContent, that.fileContent) && Objects.equals(this.fileName, that.fileName) && Objects.equals(
            this.fileSize, that.fileSize) && Objects.equals(this.mimeType, that.mimeType);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(super.hashCode(), this.fileName, this.fileSize, this.mimeType);
    result = 31 * result + Arrays.hashCode(this.fileContent);
    return result;
  }
}
