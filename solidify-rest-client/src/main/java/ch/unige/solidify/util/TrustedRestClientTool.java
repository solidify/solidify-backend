/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - TrustedRestClientTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.interceptor.BearerAuthorizationInterceptor;

/**
 * This RestClientTool use the tokens obtained by the trusted users from the authorization server. This is useful for inter-modules REST call not
 * initiated by a user, or when elevating the user privileges is necessary to perform an action.
 */
@Profile({ "client-backend", "test" })
@Component
public class TrustedRestClientTool extends AbstractRestClientTool {

  private static final Logger log = LoggerFactory.getLogger(TrustedRestClientTool.class);

  private String trustedToken;
  private RestTemplateBuilder restTemplateBuilder;
  private RestTemplate restTemplate;
  private RestTemplate restTemplateWithPlusEncoder;

  @Override
  protected RestTemplateBuilder addToken(RestTemplateBuilder restTemplateBuilder) {
    if (this.trustedToken != null) {
      if(log.isDebugEnabled()) {
        log.debug("Use trusted user token: {}", this.parseToken(this.trustedToken));
      }
      restTemplateBuilder = restTemplateBuilder.additionalInterceptors(new BearerAuthorizationInterceptor(this.trustedToken));
    }
    else {
      throw new SolidifyRuntimeException("Missing trusted token");
    }
    return restTemplateBuilder;
  }

  @Override
  protected synchronized RestTemplateBuilder getRestTemplateBuilder() {
    if (this.restTemplateBuilder == null) {
      this.restTemplateBuilder = super.getRestTemplateBuilder();
    }
    return this.restTemplateBuilder;
  }

  @Override
  public synchronized RestTemplate getClient() {
    if (this.restTemplate == null) {
      this.restTemplate = super.getClient();
    }
    return this.restTemplate;
  }

  @Override
  public synchronized RestTemplate getClientWithPlusEncoderInterceptor() {
    if (this.restTemplateWithPlusEncoder == null) {
      this.restTemplateWithPlusEncoder = super.getClientWithPlusEncoderInterceptor();
    }
    return this.restTemplateWithPlusEncoder;
  }

  public synchronized void setTrustedToken(String trustedToken) {
    this.restTemplateBuilder = null;
    this.restTemplate = null;
    this.restTemplateWithPlusEncoder = null;
    this.trustedToken = trustedToken;
  }

}
