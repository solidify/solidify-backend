/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - ResourceReadOnlyControllerTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import ch.unige.solidify.test.controller.config.MockApplication;
import ch.unige.solidify.test.controller.dao.MockUser;
import ch.unige.solidify.test.controller.dao.MockUserRepository;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.SearchOperation;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = MockApplication.class)
@Disabled("Until finishing Spring Boot 2.4 migration")
class ResourceReadOnlyControllerTest {

  @Autowired
  private MockMvc mockMvc;

  LocalDate baseDate = LocalDate.parse("2016-08-16");
  LocalDateTime baseDateTime = LocalDateTime.parse("2015-12-03T10:15:30");
  OffsetDateTime baseOffsetTime = OffsetDateTime.parse("2007-12-03T10:15:30+01:00");
  MockUser user1;
  MockUser user2;
  MockUser user3;

  @Autowired
  MockUserRepository userRepository;

  @BeforeEach
  public void setup() {
    this.user1 = new MockUser("1", "john", 22, 3, 2000L, new BigDecimal("200.58"),
            this.baseDate.minusYears(10), this.baseDateTime.minusYears(5).minusMonths(2), this.baseOffsetTime);
    this.user2 = new MockUser("2", "doe", 15, 1, 4000L, new BigDecimal("300.25"),
            this.baseDate.minusYears(20), this.baseDateTime.minusYears(10).minusMonths(2), this.baseOffsetTime);
    this.user3 = new MockUser("3", "jo", 40, 5, 3000L, new BigDecimal("400.13"),
            this.baseDate.minusYears(30), this.baseDateTime.minusYears(15).minusMonths(2), this.baseOffsetTime);

    this.userRepository.save(this.user1);
    this.userRepository.save(this.user2);
    this.userRepository.save(this.user3);
  }

  @Test
  void testGetRequest() throws Exception {
    this.mockMvc.perform(get("/test?size=2&sort=age")).andExpect(status().isOk())
            .andExpect(content().contentType("application/hal+json"))
            .andExpect(jsonPath("_data[0].name", is("doe")));
  }

  @Test
  void testGetSearch() throws Exception {
    this.mockMvc.perform(get("/test/search?search=i-name~o,age:15&match=all")).andExpect(status().isOk())
            .andExpect(content().contentType("application/hal+json"))
            // .andDo(print());
            .andExpect(jsonPath("_data[0].name", is("doe")));
  }

  @Test
  void testGetSearchwithDate() throws Exception {
    this.mockMvc.perform(
            get("/test/search?search=meetingTime:2007-12-03T10:15:30+01:00,marriageTime∞2003-10-03T10:15:30_2016-02-03T12:15:30"))
            .andExpect(status().isOk())
            .andExpect(content().contentType("application/hal+json"))
            .andExpect(jsonPath("_data[0].name", is("john")));
  }

  @Test
  void testGetWithId() throws Exception {
    this.mockMvc.perform(get("/test/3")).andExpect(status().isOk())
            .andExpect(content().contentType("application/hal+json"))
            .andExpect(jsonPath("name", is("jo")));
  }

  @Test
  void testPostSearch() throws Exception {
    final List<SearchCriteria> allCriterias = new ArrayList<>();
    SearchCriteria criteria = new SearchCriteria("i", "name", SearchOperation.CONTAINS, "o");
    allCriterias.add(criteria);
    criteria = new SearchCriteria("c", "age", SearchOperation.LESS_THAN, "18");
    allCriterias.add(criteria);
    final MockUser user = new MockUser();
    user.setSearchCriterias(allCriterias);

    this.mockMvc.perform(post("/test/search?match=all").contentType(MediaType.APPLICATION_JSON)
            .content(TestTools.convertObjectToJsonBytes(user))).andExpect(status().isOk())
            .andExpect(content().contentType("application/hal+json"))
            .andExpect(jsonPath("_data[0].searchCriterias").doesNotExist())
            .andExpect(jsonPath("_data[0].name", is("doe")));
  }
}
