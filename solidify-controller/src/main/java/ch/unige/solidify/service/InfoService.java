/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - InfoService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.Attributes.Name;
import java.util.jar.Manifest;

import jakarta.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.info.Info.Builder;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyConstants;

@Service
public class InfoService implements InfoContributor {
  private static final Logger log = LoggerFactory.getLogger(InfoService.class);

  private final ServletContext context;
  private final Environment env;
  private final GitInfoProperties gitInfoProperties;

  @Value("${spring.application.name}")
  private String applicationName;

  public InfoService(ServletContext context, Environment env, GitInfoProperties gitInfoProperties) {
    this.context = context;
    this.env = env;
    this.gitInfoProperties = gitInfoProperties;
  }

  @Override
  public void contribute(Builder builder) {
    // Application info
    final Map<String, String> appDetails = new HashMap<>();
    appDetails.put("name", this.applicationName);
    appDetails.put("version", this.gitInfoProperties.getBuild().getVersion());
    appDetails.put("profiles", String.join(",", this.env.getActiveProfiles()));
    builder.withDetail("application", appDetails);

    // Manifest
    try {
      final InputStream manifestInputStream = this.context.getResourceAsStream(SolidifyConstants.MANIFEST);
      if (manifestInputStream != null) {
        Attributes manifest = new Manifest(manifestInputStream).getMainAttributes();
        final Map<String, String> manifestDetails = new HashMap<>();
        this.addInfo(manifest, manifestDetails, Name.IMPLEMENTATION_TITLE);
        this.addInfo(manifest, manifestDetails, Name.IMPLEMENTATION_VERSION);
        this.addInfo(manifest, manifestDetails, new Name("Spring-Boot-Version"));
        if (!manifestDetails.isEmpty()) {
          builder.withDetail("manifest", manifestDetails);
        }
      }
    } catch (IOException e) {
      log.warn("Cannot read {}", SolidifyConstants.MANIFEST, e);
    }

  }

  private void addInfo(Attributes attrs, Map<String, String> attrsDetails, Name name) {
    try {
      attrsDetails.put(name.toString(), attrs.get(name).toString());
    } catch (final NullPointerException e) {
      log.error("Cannot find {} attribute", name);
    }

  }
}
