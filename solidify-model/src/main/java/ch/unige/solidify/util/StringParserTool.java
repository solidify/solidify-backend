/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - StringParserTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringParserTool {

  public static List<SearchCriteria> parseSearchString(String searchinput) {
    if (searchinput == null || searchinput.trim().isEmpty()) {
      return Collections.emptyList();
    }
    String search;
    if (searchinput.contains("+")) {
      searchinput = searchinput.replace("+", "%2B");
    }

    try {
      search = java.net.URLDecoder.decode(searchinput, StandardCharsets.UTF_8.name());
    } catch (final Exception e) {
      search = searchinput;
    }

    final String delim = ",";
    final String regex = "(?<!\\\\)" + Pattern.quote(delim);
    final String[] searchList = search.split(regex);
    final List<SearchCriteria> criterias = new ArrayList<>();
    final String operationSetExper = String.join("|", SearchOperation.OPERATION_SET);
    final Pattern pattern = Pattern.compile("([ic]-)??([\\w|_|.]+?)(" + operationSetExper + ")(.+?)$");
    for (final String item : searchList) {
      final Matcher matcher = pattern.matcher(item);
      if (matcher.find()) {
        final String caseFlag = matcher.group(1) == null ? null : matcher.group(1).trim().substring(0, 1);
        final SearchCriteria criteria = new SearchCriteria(caseFlag, matcher.group(2), matcher.group(3),
                matcher.group(4));
        criterias.add(criteria);
      }
    }
    return criterias;
  }
}
