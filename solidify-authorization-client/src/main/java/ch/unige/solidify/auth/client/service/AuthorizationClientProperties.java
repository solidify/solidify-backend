/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Authorization Client - AuthorizationClientProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2022 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.client.service;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

import ch.unige.solidify.util.StringTool;

@Profile("!test & !sec-noauth & client-backend")
@Component
@ConfigurationProperties(prefix = "auth.client")
public class AuthorizationClientProperties {

  @PostConstruct
  private void checkConfiguration() {
    this.needsToBeSet(this.getTrustedUserName(), "trusted username");
    this.needsToBeSet(this.getTrustedUserPassword(), "trusted user password");
    this.needsToBeSet(this.getClientId(), "OAuth2 client Id");
    this.needsToBeSet(this.getClientSecret(), "OAuth2 client secret");
    this.needsToBeSet(this.getRedirectUri(), "OAuth2 client redirect");
    this.needsToBeSet(this.getAuthorizationServerUrl(), "authorization server URL");
    this.needsToBeSet(this.getApplicationName(), "application name");
  }

  private String trustedUserName;
  private String trustedUserPassword;
  private String clientId;
  private String clientSecret;
  private String redirectUri;
  private String authorizationServerUrl;
  private String publicAuthorizationServerUrl;
  private String applicationName;
  private boolean opaqueTokenIntrospectorCacheEnabled = true;

  public String getTrustedUserName() {
    return this.trustedUserName;
  }

  public void setTrustedUserName(String trustedUserName) {
    this.trustedUserName = trustedUserName;
  }

  public String getTrustedUserPassword() {
    return this.trustedUserPassword;
  }

  public void setTrustedUserPassword(String trustedUserPassword) {
    this.trustedUserPassword = trustedUserPassword;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public String getClientSecret() {
    return clientSecret;
  }

  public void setClientSecret(String clientSecret) {
    this.clientSecret = clientSecret;
  }

  public String getRedirectUri() {
    return redirectUri;
  }

  public void setRedirectUri(String redirectUri) {
    this.redirectUri = redirectUri;
  }

  public String getAuthorizationServerUrl() {
    return authorizationServerUrl;
  }

  public void setAuthorizationServerUrl(String authorizationServerUrl) {
    this.authorizationServerUrl = authorizationServerUrl;
  }

  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  public boolean isOpaqueTokenIntrospectorCacheEnabled() {
    return opaqueTokenIntrospectorCacheEnabled;
  }

  public void setOpaqueTokenIntrospectorCacheEnabled(boolean opaqueTokenIntrospectorCacheEnabled) {
    this.opaqueTokenIntrospectorCacheEnabled = opaqueTokenIntrospectorCacheEnabled;
  }

  public String getPublicAuthorizationServerUrl() {
    if (this.publicAuthorizationServerUrl == null) {
      return this.authorizationServerUrl;
    }
    return this.publicAuthorizationServerUrl;
  }

  public void setPublicAuthorizationServerUrl(String publicAuthorizationServerUrl) {
    this.publicAuthorizationServerUrl = publicAuthorizationServerUrl;
  }

  private void needsToBeSet(String item, String itemMessage) {
    if (StringTool.isNullOrEmpty(item)) {
      throw new IllegalStateException("A " + itemMessage + " must be set");
    }
  }

}
