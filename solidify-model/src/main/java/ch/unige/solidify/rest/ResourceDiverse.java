/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ResourceDiverse.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;

import ch.unige.solidify.listener.CacheListener;
import ch.unige.solidify.model.ChangeInfo;

/**
 * For the existed tables, the name of historical columns (creationWhen/who lastUpdateWhen/who) are not normalized. So they have a different
 * name. And certain tables don't contain historical columns or contain only part of historical columns. This class allows his subclass to define
 * historical columns and set ResId from EntityId(entity primary key)
 */
@MappedSuperclass
@EntityListeners({ CacheListener.class })
public abstract class ResourceDiverse extends Resource {

  @Override
  public String getCreatedBy() {
    if (this.getCreation() == null) {
      return null;
    }
    return this.getCreation().getWho();
  }

  @Override
  public String getCreatorName() {
    if (this.getCreation() == null) {
      return null;
    }
    return this.getCreation().getFullName();
  }

  @Override
  public abstract ChangeInfo getCreation();

  @Override
  public OffsetDateTime getCreationTime() {
    if (this.getCreation() == null) {
      return null;
    }
    return this.getCreation().getWhen();
  }

  public abstract String getEntityId();

  @Override
  public abstract ChangeInfo getLastUpdate();

  @Override
  public String getResId() {
    return this.getEntityId();
  }

  @Override
  public String getUpdatedBy() {
    if (this.getLastUpdate() == null) {
      return null;
    }
    return this.getLastUpdate().getWho();
  }

  @Override
  public String getEditorName() {
    if (this.getLastUpdate() == null) {
      return null;
    }
    return this.getLastUpdate().getFullName();
  }

  @Override
  public OffsetDateTime getUpdateTime() {
    if (this.getLastUpdate() == null) {
      return null;
    }
    return this.getLastUpdate().getWhen();
  }

  @Override
  public void setCreatedBy(String createdBy) {
    if (this.getCreation() != null) {
      this.getCreation().setWho(createdBy);
      this.updateTime();
    }
  }

  @Override
  public void setCreatorName(String creatorName) {
    if (this.getCreation() != null) {
      this.getCreation().setFullName(creatorName);
      this.updateTime();
    }
  }

  /**
   * ResId is always the entityId. So setter will do nothing
   *
   * @param resId
   */
  @Override
  public void setResId(String resId) {
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    if (this.getLastUpdate() != null) {
      this.getLastUpdate().setWho(updatedBy);
      this.updateTime();
    }
  }

  @Override
  public void setEditorName(String creatorName) {
    if (this.getLastUpdate() != null) {
      this.getLastUpdate().setFullName(creatorName);
      this.updateTime();
    }
  }

  @Override
  public void updateTime() {
    if (this.getLastUpdate() != null) {
      this.getLastUpdate().setWhen(OffsetDateTime.now(ZoneOffset.UTC));
    }
  }

}
