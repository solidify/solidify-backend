/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - AbstractRestClientTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.nimbusds.jwt.SignedJWT;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.interceptor.BearerAuthorizationInterceptor;
import ch.unige.solidify.interceptor.PlusEncoderInterceptor;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.service.HttpRequestInfoProvider;

public abstract class AbstractRestClientTool {
  private static final Logger log = LoggerFactory.getLogger(AbstractRestClientTool.class);

  private static class UTF8MessageConverterCustomizer implements RestTemplateCustomizer {
    @Override
    public void customize(RestTemplate restTemplate) {
      final List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();
      converters.removeIf(StringHttpMessageConverter.class::isInstance);
      converters.add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
    }
  }

  public void downloadContent(URI uri, Path path, TokenUsage tokenUsage) {
    this.downloadContent(uri, path, tokenUsage, false);
  }

  public void downloadContent(URI uri, Path path, TokenUsage tokenUsage, boolean downloadToDirectory) {
    ResponseExtractor<Void> responseExtractor;
    if (downloadToDirectory) {
      responseExtractor = this.buildExtractorToDirectory(uri, path);
    } else {
      responseExtractor = this.buildExtractor(uri, path);
    }
    final RequestCallback requestCallback = request -> request.getHeaders()
            .setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL));
    final RestTemplate restTemplate;
    if (tokenUsage == TokenUsage.WITH_TOKEN) {
      restTemplate =  this.getClient();
    } else if (tokenUsage == TokenUsage.WITHOUT_TOKEN) {
      restTemplate = new RestTemplateBuilder().build();
    } else {
      throw new SolidifyRuntimeException("Unknown token usage value: " + tokenUsage);
    }
    restTemplate.execute(uri, HttpMethod.GET, requestCallback, responseExtractor);
  }

  private ResponseExtractor<Void> buildExtractor(URI uri, Path filePath) {
    return response -> {
      final byte[] data = new byte[SolidifyConstants.BUFFER_SIZE];
      try (final InputStream inputStream = response.getBody();
              final FileOutputStream fileOutputStream = new FileOutputStream(filePath.toFile());
              final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream, SolidifyConstants.BUFFER_SIZE)) {
        int nRead;
        while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
          bufferedOutputStream.write(data, 0, nRead);
        }
      } catch (final IOException e) {
        throw new SolidifyRuntimeException("Error downloading " + uri.toString() + " to " + filePath.toString(), e);
      }
      return null;
    };
  }


  private ResponseExtractor<Void> buildExtractorToDirectory(URI uri, Path directoryPath) {
    return response -> {
      final byte[] data = new byte[SolidifyConstants.BUFFER_SIZE];
      String disposition = response.getHeaders().getFirst("Content-Disposition");
      // Check if the provided path is a directory or a filename
      Path fullPath;
      if (!StringTool.isNullOrEmpty(disposition) && Files.isDirectory(directoryPath)) {
        String filename = disposition.replaceFirst("(?i)^.*filename=\"([^\"]*)\".*$", "$1");
        fullPath = directoryPath.resolve(filename);
      } else {
        fullPath = directoryPath;
      }

      try (final InputStream inputStream = response.getBody();
              final FileOutputStream fileOutputStream = new FileOutputStream(fullPath.toFile());
              final BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream, SolidifyConstants.BUFFER_SIZE)) {
        int nRead;
        while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
          bufferedOutputStream.write(data, 0, nRead);
        }
      } catch (final IOException e) {
        throw new SolidifyRuntimeException("Error downloading " + uri.toString() + " to " + fullPath.toString(), e);
      }
      return null;
    };
  }

  public RestTemplate getClient() {
    return this.getRestTemplateBuilder().build();
  }

  public RestTemplate getClient(String rootUri) {
    return this.getRestTemplateBuilder().rootUri(rootUri).build();
  }

  public RestTemplate getClientWithPlusEncoderInterceptor() {
    return this.getRestTemplateBuilder().additionalInterceptors(new PlusEncoderInterceptor()).build();
  }

  protected RestTemplateBuilder getRestTemplateBuilder() {
    RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
    restTemplateBuilder = this.addToken(restTemplateBuilder);
    restTemplateBuilder = this.addUTF8Converter(restTemplateBuilder);
    restTemplateBuilder = this.disableJsonFilter(restTemplateBuilder);
    return restTemplateBuilder;
  }

  private RestTemplateBuilder addUTF8Converter(RestTemplateBuilder restTemplateBuilder) {
    return restTemplateBuilder.additionalCustomizers(new UTF8MessageConverterCustomizer());
  }

  private RestTemplateBuilder disableJsonFilter(RestTemplateBuilder restTemplateBuilder) {
    final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    final ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.registerModule(new JavaTimeModule());
    objectMapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    converter.setObjectMapper(objectMapper);
    final List<HttpMessageConverter<?>> defaultConverterList = new RestTemplate().getMessageConverters();
    defaultConverterList.add(0, converter);
    return restTemplateBuilder.additionalMessageConverters(defaultConverterList);
  }

  protected abstract RestTemplateBuilder addToken(RestTemplateBuilder restTemplateBuilder);

  protected String parseToken(String token) {
    try {
      return SignedJWT.parse(token).getJWTClaimsSet().toString();
    } catch (ParseException e) {
      return e.getMessage();
    }
  }

  protected RestTemplateBuilder addTokenFromRequest(RestTemplateBuilder restTemplateBuilder, HttpRequestInfoProvider httpRequestInfoProvider) {
    final String incomingToken = httpRequestInfoProvider.getIncomingToken();
    if(incomingToken != null) {
      // Access token of incoming user
      if(log.isDebugEnabled()) {
        log.debug("Use incoming user token: {}", this.parseToken(incomingToken));
      }
      restTemplateBuilder = restTemplateBuilder.additionalInterceptors(new BearerAuthorizationInterceptor(incomingToken));
    } else {
      // No Auth
      log.debug("Use no authorization");
    }
    return restTemplateBuilder;
  }

}
