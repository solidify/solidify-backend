/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAISetSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.model.oai.OAISet;
import ch.unige.solidify.util.StringTool;

public class OAISetSpecification extends SolidifySpecification<OAISet> {
  @Serial
  private static final long serialVersionUID = -8046624533135459649L;

  public OAISetSpecification(OAISet criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<OAISet> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    if (!StringTool.isNullOrEmpty(this.criteria.getSpec())) {
      predicatesList.add(builder.like(root.get("spec"), "%" + this.criteria.getSpec() + "%"));
    }

    if (!StringTool.isNullOrEmpty(this.criteria.getName())) {
      predicatesList.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }

    if (!StringTool.isNullOrEmpty(this.criteria.getDescription())) {
      predicatesList.add(builder.like(root.get("description"), "%" + this.criteria.getDescription() + "%"));
    }

    if (!StringTool.isNullOrEmpty(this.criteria.getQuery())) {
      predicatesList.add(builder.like(root.get("query"), "%" + this.criteria.getQuery() + "%"));
    }

    if (this.criteria.getEnabled() != null) {
      Predicate p = null;
      if (this.criteria.isEnabled()) {
        p = builder.isTrue(root.get("enabled"));
      } else {
        p = builder.isFalse(root.get("enabled"));
      }
      predicatesList.add(p);
    }
  }
}
