/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - SchemaResourceResolver.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

import ch.unige.solidify.SolidifyConstants;

public class SchemaResourceResolver implements LSResourceResolver {
  private static final Logger logger = LoggerFactory.getLogger(SchemaResourceResolver.class);

  @Override
  public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId,
          String baseURI) {
    final LSInputImpl input = new LSInputImpl();

    // Find in package or in file system
    InputStream stream = this.getSchemaInputStream(systemId);
    if (stream == null) {
      final Path schemaDir = this.getSchemaPath(systemId);
      if (schemaDir == null) {
        return null;
      }
      try {
        stream = Files.newInputStream(schemaDir);
      } catch (final IOException e) {
        logger.trace("Unable to resolve resource", e);
        return null;
      }
    }

    input.setPublicId(publicId);
    input.setSystemId(systemId);
    input.setBaseURI(baseURI);
    input.setCharacterStream(new InputStreamReader(stream));
    return input;
  }

  private InputStream getSchemaInputStream(String systemId) {
    // Search in class path of the package (JAR or WAR)
    try {
      return new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + systemId).getInputStream();
    } catch (final IOException e) {
      logger.trace("Unable to get schema input stream", e);
      return null;
    }
  }

  private Path getSchemaPath(String systemId) {
    // Search in class path of the file system
    try {
      return new ClassPathResource(SolidifyConstants.SCHEMA_HOME + "/" + systemId).getFile().toPath();
    } catch (final IOException e) {
      logger.trace("Unable to get schema path", e);
      return null;
    }
  }
}
