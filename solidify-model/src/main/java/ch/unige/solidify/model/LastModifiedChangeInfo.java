/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - LastModifiedChangeInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import static ch.unige.solidify.SolidifyConstants.USER_PROPERTY_FILTER_NAME;

import java.io.Serial;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Objects;

import jakarta.persistence.Embeddable;
import jakarta.persistence.Transient;

import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.util.StringTool;

@Schema(description = "The information of last modification action.")
@Embeddable
@JsonFilter(USER_PROPERTY_FILTER_NAME)
public class LastModifiedChangeInfo implements ChangeInfo, Serializable {

  @Serial
  private static final long serialVersionUID = -3801924551562715912L;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @LastModifiedDate
  private OffsetDateTime when;

  private String who;

  @Transient
  private String name;

  @Override
  public OffsetDateTime getWhen() {
    return this.when;
  }

  @Override
  public String getWho() {
    return this.who;
  }

  @Override
  public void setWhen(OffsetDateTime when) {
    this.when = when;
  }

  @Override
  public void setWho(String who) {
    this.who = who;
  }

  @Override
  public String getFullName() {
    if (StringTool.isNullOrEmpty(this.name)) {
      return this.getWho();
    } else {
      return this.name;
    }
  }

  @Override
  public void setFullName(String fullName) {
    this.name = fullName;
  }

  @Override
  public String toString() {
    return "LastModifiedChangeInfo{" +
            "when=" + this.when +
            ", who='" + this.who + '\'' +
            '}';
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name, this.when, this.who);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    LastModifiedChangeInfo other = (LastModifiedChangeInfo) obj;
    return Objects.equals(this.name, other.name) && Objects.equals(this.when, other.when) && Objects.equals(this.who, other.who);
  }
}
