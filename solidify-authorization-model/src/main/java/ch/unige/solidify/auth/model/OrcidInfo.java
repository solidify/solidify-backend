/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Authorization Model - OrcidInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "ORCID information identify individuals.")
public interface OrcidInfo {

  // ORCID
  public static final String ORCID_PATTERN = "|[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}|[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{3}X";
  public static final String ORCID_MESSAGE = "The ORCID should be of the form 1234-1234-1234-1234 or 1234-1234-1234-123X";

  @Schema(description = "The ORCID of the person (Format: xxxx-xxxx-xxxx-xxxx).")
  String getOrcid();

  @Schema(description = "if the person ORCID is verified with ORCID authentication.")
  Boolean isVerifiedOrcid();

}
