/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SolidifyProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.config;

import java.net.URL;
import java.net.URLClassLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import jakarta.validation.Valid;

import ch.unige.solidify.model.OrcidWebsite;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.util.StringTool;

@Component
@Validated
@ConfigurationProperties(prefix = "solidify")
public class SolidifyProperties {
  private static final Logger log = LoggerFactory.getLogger(SolidifyProperties.class);

  public SolidifyProperties(GitInfoProperties gitInfoProperties) {
    this.gitInfoProperties = gitInfoProperties;
  }

  // ****************
  // ** Properties **
  // ****************
  private GitInfoProperties gitInfoProperties;

  @Value("${spring.application.name}")
  private String applicationName;

  @Value("${server.servlet.application-display-name:Default Server Display Name}")
  private String serverDisplayName;

  private Email email = new Email();

  private Orcid orcid = new Orcid();

  @Valid
  private Sitemap sitemap = new Sitemap();

  private Debug debug = new Debug();

  private Security security = new Security();

  // *********************
  // ** Getters/Setters **
  // *********************
  public static Logger getLog() {
    return log;
  }

  public String getApplicationName() {
    return this.applicationName;
  }

  public String getApplicationVersion() {
    return this.gitInfoProperties.getBuild().getVersion();
  }

  public GitInfoProperties getGitInfoService() {
    return this.gitInfoProperties;
  }

  public String getServerDisplayName() {
    return this.serverDisplayName;
  }

  public void logClassPath() {
    final ClassLoader cl = ClassLoader.getSystemClassLoader();
    final URL[] urls = ((URLClassLoader) cl).getURLs();
    log.info("List CLASSPATH");
    for (final URL url : urls) {
      log.info(url.getFile());
    }
  }

  public Email getEmail() {
    return this.email;
  }

  public void setEmail(Email email) {
    this.email = email;
  }

  public Orcid getOrcid() {
    return this.orcid;
  }

  public void setOrcid(Orcid orcid) {
    this.orcid = orcid;
  }

  public Sitemap getSitemap() {
    return this.sitemap;
  }

  public void setSitemap(Sitemap sitemap) {
    this.sitemap = sitemap;
  }

  public Debug getDebug() {
    return this.debug;
  }

  public void setDebug(Debug debug) {
    this.debug = debug;
  }

  public Security getSecurity() {
    return this.security;
  }

  public void setSecurity(Security security) {
    this.security = security;
  }

  public static class Email {
    private String senderAddress;
    private String[] ccList = {};
    private String[] bccList = {};
    private String externalTemplates;
    private String internalTemplates = "email-templates/";

    public String getSenderAddress() {
      return this.senderAddress;
    }

    public void setSenderAddress(String senderAddress) {
      this.senderAddress = senderAddress;
    }

    public String getExternalTemplates() {
      return this.externalTemplates;
    }

    public String[] getCcList() {
      return this.ccList;
    }

    public void setCcList(String[] ccList) {
      this.ccList = ccList;
    }

    public String[] getBccList() {
      return this.bccList;
    }

    public void setBccList(String[] bccList) {
      this.bccList = bccList;
    }

    public void setExternalTemplates(String externalTemplates) {
      this.externalTemplates = externalTemplates;
    }

    public String getInternalTemplates() {
      return this.internalTemplates;
    }

    public void setInternalTemplates(String internalTemplates) {
      this.internalTemplates = internalTemplates;
    }
  }

  public static class Orcid {

    private String baseUrl = "https://orcid.org/";
    private String clientId;
    private String clientSecret;
    private String grantType = "authorization_code";
    private String scope = "/authenticate";
    private String apiBaseUrl = "https://api.orcid.org/v3.0/";

    private OrcidWebsite[] websites = {};

    public String getClientId() {
      return this.clientId;
    }

    public void setClientId(String clientId) {
      this.clientId = clientId;
    }

    public String getClientSecret() {
      return this.clientSecret;
    }

    public void setClientSecret(String clientSecret) {
      this.clientSecret = clientSecret;
    }

    public String getBaseUrl() {
      return this.baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
      this.baseUrl = baseUrl;
    }

    public String getAuthorizeUrl() {
      return StringTool.normalizeBaseUri(this.baseUrl) + "oauth/authorize";
    }

    public String getTokenUrl() {
      return StringTool.normalizeBaseUri(this.baseUrl) + "oauth/token";
    }

    public String getGrantType() {
      return this.grantType;
    }

    public void setGrantType(String grantType) {
      this.grantType = grantType;
    }

    public String getApiBaseUrl() {
      return StringTool.normalizeBaseUri(this.apiBaseUrl);
    }

    public void setApiBaseUrl(String apiBaseUrl) {
      this.apiBaseUrl = apiBaseUrl;
    }

    public String getScope() {
      return this.scope;
    }

    public void setScope(String scope) {
      this.scope = scope;
    }

    public OrcidWebsite[] getWebsites() {
      return this.websites;
    }

    public void setWebsites(OrcidWebsite[] websites) {
      this.websites = websites;
    }
  }

  public static class Sitemap {
    private boolean enable = false;
    private int pageSize = 100;
    private String locationPrefix;

    public boolean isEnable() {
      return this.enable;
    }

    public void setEnable(boolean enable) {
      this.enable = enable;
    }

    public int getPageSize() {
      return this.pageSize;
    }

    public void setPageSize(int pageSize) {
      this.pageSize = pageSize;
    }

    public String getLocationPrefix() {
      return this.locationPrefix;
    }

    public void setLocationPrefix(String locationPrefix) {
      this.locationPrefix = locationPrefix;
    }
  }

  public static class Debug {
    private boolean badRequestStacktrace = true;
    private boolean accessDeniedStacktrace = false;

    public boolean isBadRequestStacktrace() {
      return this.badRequestStacktrace;
    }

    public void setBadRequestStacktrace(boolean badRequestStacktrace) {
      this.badRequestStacktrace = badRequestStacktrace;
    }

    public boolean isAccessDeniedStacktrace() {
      return this.accessDeniedStacktrace;
    }

    public void setAccessDeniedStacktrace(boolean accessDeniedStacktrace) {
      this.accessDeniedStacktrace = accessDeniedStacktrace;
    }

  }

  // Security
  public static class Security {
    private int downloadTokenLifeTimeMinutes = 24 * 60;

    private String cipherPassword;

    private Cors cors = new Cors();

    public Cors getCors() {
      return this.cors;
    }

    public void setCors(Cors cors) {
      this.cors = cors;
    }

    private DownloadToken downloadToken = new DownloadToken();

    public DownloadToken getDownloadToken() {
      return this.downloadToken;
    }

    public void setDownloadToken(DownloadToken downloadToken) {
      this.downloadToken = downloadToken;
    }

    public int getDownloadTokenLifeTimeMinutes() {
      return this.downloadTokenLifeTimeMinutes;
    }

    public void setDownloadTokenLifeTimeMinutes(int downloadTokenLifeTimeMinutes) {
      this.downloadTokenLifeTimeMinutes = downloadTokenLifeTimeMinutes;
    }

    public String getCipherPassword() {
      return this.cipherPassword;
    }

    public void setCipherPassword(String cipherPassword) {
      this.cipherPassword = cipherPassword;
    }

    public static class Cors {
      // #You can send more than one separated by space
      // http://my.domain.com https://my.domain.com http://my.otherdomain.com
      private String allowedDomains = "";

      private String allowedMethods = "POST, GET, OPTIONS, DELETE, PUT, PATCH, HEAD";

      private String allowedHeaders = "x-requested-with, authorization, content-type, content-disposition, ngsw-bypass, origin, accept";

      public String getAllowedDomains() {
        return this.allowedDomains;
      }

      public void setAllowedDomains(String allowedDomains) {
        this.allowedDomains = allowedDomains;
      }

      public String getAllowedMethods() {
        return this.allowedMethods;
      }

      public void setAllowedMethods(String allowedMethods) {
        this.allowedMethods = allowedMethods;
      }

      public String getAllowedHeaders() {
        return this.allowedHeaders;
      }

      public void setAllowedHeaders(String allowedHeaders) {
        this.allowedHeaders = allowedHeaders;
      }
    }

    public static class DownloadToken {
      private Cookie cookie = new Cookie();

      public Cookie getCookie() {
        return this.cookie;
      }

      public void setCookie(Cookie cookie) {
        this.cookie = cookie;
      }

      public static class Cookie {
        private boolean secure = true;

        public boolean getSecure() {
          return this.secure;
        }

        public void setSecure(boolean secure) {
          this.secure = secure;
        }
      }
    }
  }
}
