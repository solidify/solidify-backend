/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - ResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;

import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.repository.SolidifyRepository;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.Searchable;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.BasicEntityMapper;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.SearchOperation;
import ch.unige.solidify.validation.ValidationError;

public abstract class ResourceService<T extends Resource> extends AbstractResourceService {

  @Autowired
  private Validator validator;

  @Autowired
  protected MessageService messageService;

  @Autowired
  protected SolidifyRepository<T> itemRepository;
  protected BasicEntityMapper basicEntityMapper = new BasicEntityMapper();

  public List<T> findAll() {
    List<T> result = new ArrayList<>();
    for (T item : this.itemRepository.findAll(Pageable.unpaged())) {
      result.add(this.afterFind(item));
    }
    return result;
  }

  public Page<T> findAll(Pageable pageable) {
    Page<T> results = this.itemRepository.findAll(pageable);
    results.forEach(this::afterFind);
    return results;
  }

  public Page<T> findAll(Specification<T> spec, Pageable pageable) {
    final Specification<T> completedSpec = this.addSpecificationFilter(spec);
    Page<T> results = this.itemRepository.findAll(completedSpec, pageable);
    results.forEach(this::afterFind);
    return results;
  }

  public T findOne(String resId) {
    T item = this.itemRepository.findById(resId)
            .orElseThrow(() -> new NoSuchElementException("No resource with id " + resId));
    this.afterFind(item);
    return item;
  }

  public Page<T> findBySearchCriteria(Specification<T> spec, Pageable pageable) {
    return this.findAll(spec, pageable);
  }

  public Page<T> findBySearchCriteria(Searchable<T> resource, String orOpredicateFlag, List<SearchCriteria> criterias, Pageable pageable) {
    if (criterias == null || criterias.isEmpty()) {
      return null;
    }
    final Specification<T> spec = this.findBySearchCriteria(resource, orOpredicateFlag, criterias);
    return this.findAll(spec, pageable);
  }

  public Page<T> findBySubResourceSearchCriteria(Searchable<T> subresource, String orOpredicateFlag, List<SearchCriteria> criterias,
          SearchCriteria parent, Pageable pageable) {
    if (criterias == null || criterias.isEmpty()) {
      return null;
    }
    Specification<T> spec = this.findBySearchCriteria(subresource, orOpredicateFlag, criterias);

    spec = Specification.where(spec).and(subresource.getSearchSpecification(parent));
    return this.findAll(spec, pageable);
  }

  public void delete(String id) {
    this.itemRepository.deleteById(id);
  }

  public void deleteAll(Iterable<? extends T> resources) {
    this.itemRepository.deleteAll(resources);
  }

  public boolean existsById(String resId) {
    return this.itemRepository.existsById(resId);
  }

  public void existsByIdOrThrowException(String resId) {
    if (!this.itemRepository.existsById(resId)) {
      throw new NoSuchElementException("No such element with id " + resId);
    }
  }

  /**
   * Override to add criteria to the Specification. May be useful to add criteria based on permissions fetched from Authentication
   *
   * @param spec
   * @return
   */
  protected Specification<T> addSpecificationFilter(Specification<T> spec) {
    return spec;
  }

  private Specification<T> findBySearchCriteria(Searchable<T> resource, String orOpredicateFlag, List<SearchCriteria> criterias) {
    Specification<T> spec = resource.getSearchSpecification(criterias.get(0));
    if (spec == null) {
      return null;
    }
    return this.applyCriteriaInSpecification(spec, resource, orOpredicateFlag, criterias);
  }

  public Specification<T> applyCriteriaInSpecification(Specification<T> spec, Searchable<T> resource, String orOpredicateFlag,
          List<SearchCriteria> criterias) {
    final boolean isOrPredicate = SearchOperation.OR_PREDICATE_FLAG.equals(orOpredicateFlag);
    for (int i = 1; i < criterias.size(); i++) {
      if (isOrPredicate) {
        spec = Specification.where(spec)
                .or(resource.getSearchSpecification(criterias.get(i)));
      } else {
        spec = Specification.where(spec)
                .and(resource.getSearchSpecification(criterias.get(i)));
      }
    }
    return spec;
  }

  /**
   * Checks that all linked RESTResources identified by their resId exist. Must be overridden if the Resource has linked RESTResources
   *
   * @param Resource item
   * @param BindingResult errors
   */
  public void validateLinkedResources(T item, BindingResult errors) {
  }

  /**
   * Override to set default data or clean properties
   *
   * @param item
   */
  protected void beforeValidate(T item) {
  }

  /**
   * Code executed after validation but before saving entity
   *
   * @param item
   */
  protected void beforeSave(T item) {
    this.setAuthenticatedUserProperties(item);
    item.updateTime();
  }

  protected void validate(T item) {

    final BindingResult errors = new BeanPropertyBindingResult(item, item.getClass().getName());

    this.validateConstraints(item, errors);
    this.validateItemSpecificRules(item, errors);

    if (errors.hasErrors()) {
      throw new SolidifyValidationException(new ValidationError(errors), errors.toString());
    }
  }

  /**
   * Validate Bean constraints
   *
   * @param Resource item
   * @param BindingResult errors
   */
  protected void validateConstraints(T item, BindingResult errors) {
    this.validator.validate(item, errors);
  }

  /**
   * Override to add specific validation logic
   *
   * @param item
   * @param errors
   */
  protected void validateItemSpecificRules(T item, BindingResult errors) {
  }

  public T save(T item) {
    item.init();
    this.beforeValidate(item);
    this.validate(item);
    this.beforeSave(item);
    return this.itemRepository.save(item);
  }

  public abstract SolidifySpecification<T> getSpecification(T resource);

  /**
   * Override if treatment must be done after the entity has been fetched from db
   *
   * @param resource
   * @return
   */
  protected T afterFind(T resource) {
    this.setChangeInfoFullName(resource);
    return resource;
  }

  public void patchResource(T item, Map<String, Object> updateMap) {
    this.basicEntityMapper.patchResource(item, updateMap);
  }
}
