/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - CompositeNoSqlResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public abstract class CompositeNoSqlResourceService<T> extends NoSqlResourceService<T> {
  public <V> Page<V> findByParentId(String parentId, Class<?> parentClass, Class<V> childClass,
          Pageable pageable) {
    throw new UnsupportedOperationException("Missing implementation 'findByParentId' method ("
            + parentClass.getSimpleName() + " -> " + childClass.getSimpleName() + ")");
  }

  public <V> V findByParentIdAndChildId(String parentId, Class<?> parentClass, Class<V> childClass,
          String id) {
    throw new UnsupportedOperationException("Missing implementation 'findByParentIdAndChildId' method ("
            + parentClass.getSimpleName() + " -> " + childClass.getSimpleName() + ")");
  }
}
