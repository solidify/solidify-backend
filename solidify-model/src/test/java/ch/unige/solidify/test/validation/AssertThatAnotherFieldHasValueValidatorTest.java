/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - AssertThatAnotherFieldHasValueValidatorTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.validation;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.annotation.Annotation;

import jakarta.validation.ClockProvider;
import jakarta.validation.ConstraintValidatorContext;
import jakarta.validation.Payload;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.validation.AssertThatAnotherFieldHasValue;
import ch.unige.solidify.validation.AssertThatAnotherFieldHasValueValidator;

class AssertThatAnotherFieldHasValueValidatorTest {

  private static class TestClass {
    private final String field1;
    private final String field2;

    public TestClass(String field1, String field2) {
      this.field1 = field1;
      this.field2 = field2;
    }
  }

  /**
   * Test that if a TestClass instance has a field named "field1" with value "value1" then its field field2 is not null and not empty
   */
  @Test
  void testIsValid() {
    // Instantiate the annotation for the test
    final AssertThatAnotherFieldHasValue annotation = new AssertThatAnotherFieldHasValue() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return AssertThatAnotherFieldHasValue.class;
      }

      @Override
      public String dependFieldName() {
        return "field2";
      }

      @Override
      public String fieldName() {
        return "field1";
      }

      @Override
      public String fieldValue() {
        return "value1";
      }

      @Override
      public Class<?>[] groups() {
        return new Class<?>[0];
      }

      @Override
      public String message() {
        return "";
      }

      @Override
      public Class<? extends Payload>[] payload() {
        return new Class[0];
      }
    };
    // Instantiate the ConstraintValidatorContext for the test
    final ConstraintValidatorContext ctx = new ConstraintValidatorContext() {
      @Override
      public ConstraintViolationBuilder buildConstraintViolationWithTemplate(
              String messageTemplate) {
        return null;
      }

      @Override
      public void disableDefaultConstraintViolation() {

      }

      @Override
      public ClockProvider getClockProvider() {
        return null;
      }

      @Override
      public String getDefaultConstraintMessageTemplate() {
        return "";
      }

      @Override
      public <T> T unwrap(Class<T> type) {
        return null;
      }
    };

    final AssertThatAnotherFieldHasValueValidator validator = new AssertThatAnotherFieldHasValueValidator();
    validator.initialize(annotation);

    final TestClass testObj1 = new TestClass("value1", "value2");
    final TestClass testObj2 = new TestClass("valueXYZ", "");
    assertTrue(validator.isValid(testObj1, ctx));
    assertTrue(validator.isValid(testObj2, ctx));

  }

}
