/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID - OrcidConfigDTO.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import ch.unige.solidify.config.SolidifyProperties;

public class OrcidConfigDTO {
  private final String baseUrl;
  private final String authorizeUrl;
  private final String tokenUrl;
  private final String clientId;
  private final String scope;

  public OrcidConfigDTO(SolidifyProperties.Orcid orcidProperties) {
    this.baseUrl = orcidProperties.getBaseUrl();
    this.authorizeUrl = orcidProperties.getAuthorizeUrl();
    this.tokenUrl = orcidProperties.getTokenUrl();
    this.clientId = orcidProperties.getClientId();
    this.scope = orcidProperties.getScope();
  }

  public String getBaseUrl() {
    return this.baseUrl;
  }

  public String getAuthorizeUrl() {
    return this.authorizeUrl;
  }

  public String getTokenUrl() {
    return this.tokenUrl;
  }

  public String getClientId() {
    return this.clientId;
  }

  public String getScope() {
    return this.scope;
  }

}
