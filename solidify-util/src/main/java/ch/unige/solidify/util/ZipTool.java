/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - ZipTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.AntPathMatcher;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public class ZipTool {

  private static final Logger log = LoggerFactory.getLogger(ZipTool.class);

  private final List<Charset> supportedCharsets = List.of(
          StandardCharsets.UTF_8,
          StandardCharsets.ISO_8859_1,
          StandardCharsets.US_ASCII,
          StandardCharsets.UTF_16,
          StandardCharsets.UTF_16BE,
          StandardCharsets.UTF_16LE);

  private final Path archive;

  private Charset archiveCharset;

  public ZipTool(String archive) {
    this.archive = Paths.get(archive);
  }

  public ZipTool(URI archive) {
    this.archive = Paths.get(archive);
  }

  public static boolean unzipFiles(InputStream zipData, Path destination, List<String> antPatternList, String relativeFolder,
          boolean checkEmptyDestination) {
    int entryCount = 0;
    long totalSize = 0;
    try (ZipInputStream zis = new ZipInputStream(zipData)) {
      if (checkEmptyDestination) {
        checkIfFolderDestinationIsEmpty(destination);
      }
      ZipEntry ze = zis.getNextEntry();
      // Extract zip entry
      while (ze != null) {
        if (needToBeExtracted(antPatternList, ze)) {
          totalSize += extractFile(zis, ze, destination, relativeFolder);
          entryCount++;
        }
        ze = zis.getNextEntry();
      }
      zis.closeEntry();
    } catch (final IOException e) {
      log.error("Problem in unzipping package to {}", destination, e);
      return false;
    }
    if (log.isInfoEnabled()) {
      log.info("Unzip package in '{}': {} entries extracted for {}", destination, entryCount, StringTool.formatSmartSize(totalSize));
    }
    return true;
  }

  private static Path checkDestination(ZipEntry zipEntry, Path destination, String relativeFolder) throws IOException {
    // Normalize name
    final String entryFileName = normalizePath(zipEntry.getName());
    // Manage relative path
    Path newFile;
    if (relativeFolder == null) {
      newFile = destination.resolve(entryFileName).toAbsolutePath();
    } else {
      final String newName = entryFileName.substring(entryFileName.indexOf(normalizePath(relativeFolder)) + relativeFolder.length());
      newFile = destination.resolve(newName).toAbsolutePath();
    }
    // Check target file to avoid ZipSlip
    String canonicalPath = newFile.toFile().getCanonicalPath();
    if (!canonicalPath.startsWith(destination.toFile().getCanonicalPath() + File.separator)) {
      throw new ZipException("Illegal name: " + zipEntry.getName());
    }
    // Check if folders exists
    if (zipEntry.isDirectory()) {
      FileTool.ensureFolderExists(newFile);
      return null;
    } else {
      if (!FileTool.ensureFolderExists(newFile.getParent())) {
        final String message = "Cannot create parent directory (" + newFile + ")";
        log.error(message);
        throw new IOException(message);
      }
      return newFile;
    }
  }

  private static long extractFile(ZipInputStream zipFile, ZipEntry zipEntry, Path destination, String relativeFolder) throws IOException {
    final Path newFile = checkDestination(zipEntry, destination, relativeFolder);
    long fileSize = 0;

    if (newFile != null) {
      try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(newFile.toFile()), SolidifyConstants.BUFFER_SIZE)) {
        final byte[] buffer = new byte[SolidifyConstants.BUFFER_SIZE];
        int len;
        while ((len = zipFile.read(buffer)) > 0) {
          fileSize += len;
          bos.write(buffer, 0, len);
        }
      }
      if (!FileTool.checkFile(newFile)) {
        throw new IOException("Cannot extract file: " + newFile);
      }
    }
    return fileSize;
  }

  private static boolean needToBeExtracted(List<String> antPatternList, ZipEntry ze) {
    Objects.requireNonNull(antPatternList);
    final AntPathMatcher antPathMatcher = new AntPathMatcher();
    for (final String file : antPatternList) {
      final String normalizedPath = normalizePath(file);
      final String zePath = ze.getName();
      if (antPathMatcher.match(normalizedPath, zePath)) {
        return true;
      }
    }
    return false;
  }

  private static String normalizePath(String path) {
    if (File.separatorChar == '/') {
      // Linux compatible OS
      return path.replace("\\", "/");
    }
    if (File.separatorChar == '\\') {
      // Windows compatible OS
      return path.replace("/", "\\");
    }
    return path;
  }

  public boolean addFilesToZip(Path source, List<Path> files, List<Path> destinationPath, boolean excludeContainingFolder, boolean addFolders) {
    files = files.stream().map(source::relativize).toList();
    return this.createZip(source, files, destinationPath, excludeContainingFolder, addFolders);
  }

  public boolean checkZip() {
    try (ZipFile zipFile = new ZipFile(this.getArchive(), this.getArchiveCharset())) {
      final Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
        final ZipEntry zipEntry = entries.nextElement();
        zipFile.getInputStream(zipEntry);
      }
      return true;
    } catch (final IOException e) {
      log.error("IOException when checking zip file {}", this.getArchive(), e);
      return false;
    }
  }

  public boolean contains(String fileName) {
    final String normalizedFileName = normalizePath(fileName);
    try (ZipInputStream zis = new ZipInputStream(
            new BufferedInputStream(new FileInputStream(this.getArchive()), SolidifyConstants.BUFFER_SIZE),
            this.getArchiveCharset())) {
      ZipEntry entry;
      while ((entry = zis.getNextEntry()) != null) {
        if (entry.getName().equals(normalizedFileName)) {
          return true;
        }
      }
    } catch (final IOException e) {
      log.error("Problem in reading package ({})", this.archive, e);
    }
    return false;
  }

  public boolean containsFolder(String folderName) {
    final String normalizedFolderName = normalizePath(folderName);
    try (ZipInputStream zis = new ZipInputStream(
            new BufferedInputStream(new FileInputStream(this.getArchive()), SolidifyConstants.BUFFER_SIZE),
            this.getArchiveCharset())) {
      ZipEntry entry;
      while ((entry = zis.getNextEntry()) != null) {
        if (entry.getName().startsWith(normalizedFolderName)) {
          return true;
        }
      }
    } catch (final IOException e) {
      log.error("Problem in reading package ({})", this.archive, e);
    }
    return false;
  }

  public String getArchive() {
    return this.archive.toString();
  }

  public Charset getArchiveCharset() throws IOException {
    if (this.archiveCharset == null) {
      this.archiveCharset = this.defineCharset();
    }
    return this.archiveCharset;
  }

  public int getEntryNumber() {
    try (ZipFile zipFile = new ZipFile(this.getArchive(), this.getArchiveCharset())) {
      return zipFile.size();
    } catch (final IOException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  public int getFileNumber() {
    try (ZipFile zipFile = new ZipFile(this.getArchive(), this.getArchiveCharset())) {
      return zipFile.size() - (int) zipFile.stream().filter(ZipEntry::isDirectory).count();
    } catch (final IOException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  public int getFolderNumber() {
    try (ZipFile zipFile = new ZipFile(this.getArchive(), this.getArchiveCharset())) {
      return (int) zipFile.stream().filter(ZipEntry::isDirectory).count();
    } catch (final IOException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  public boolean unzipFiles(Path destination, boolean checkEmptyDestination) {
    try {
      this.unzipFilesAndReturnList(destination, checkEmptyDestination);
    } catch (final IOException e) {
      log.error("Problem in unzipping package {} to destination {}", this.archive, destination, e);
      return false;
    }
    return true;
  }

  public List<Path> unzipFilesAndReturnList(Path destination, boolean checkEmptyDestination) throws IOException {
    if (checkEmptyDestination) {
      checkIfFolderDestinationIsEmpty(destination);
    }
    int entryCount = 0;
    long totalSize = 0;
    List<Path> extractedFiles = new ArrayList<>();
    try (ZipInputStream zis = new ZipInputStream(
            new BufferedInputStream(new FileInputStream(this.getArchive()), SolidifyConstants.BUFFER_SIZE),
            this.getArchiveCharset())) {
      ZipEntry ze = zis.getNextEntry();
      while (ze != null) {
        totalSize += extractFile(zis, ze, destination, null);
        final Path extractedFilePath = Path.of(destination.toString(), ze.getName());
        extractedFiles.add(destination.resolve(extractedFilePath));
        entryCount++;
        ze = zis.getNextEntry();
      }
      zis.closeEntry();
    }
    if (log.isInfoEnabled()) {
      log.info("Unzip package in '{}': {} entries extracted for {}", destination, entryCount, StringTool.formatSmartSize(totalSize));
    }
    return extractedFiles;
  }

  /**
   * Unzip files selected by a list of Ant-style patterns into the given destination.
   *
   * @param destination the directory in which to extract the provided files and directories
   * @param antPatternList a list of Ant-style patterns used to select files
   * @param checkEmptyDestination if {@code true}, verifies that the destination directory is empty before unzipping
   * @return {@code true} if the unzip operation completes successfully; {@code false} otherwise
   */
  public boolean unzipFiles(Path destination, List<String> antPatternList, boolean checkEmptyDestination) {
    return this.unzipFiles(destination, antPatternList, null, checkEmptyDestination);
  }

  public boolean unzipFiles(URI destination, boolean checkEmptyDestination) {
    return this.unzipFiles(Paths.get(destination), checkEmptyDestination);
  }

  public boolean unzipFolder(Path destination, String folder, boolean checkEmptyDestination) {
    if (folder.endsWith(File.separator)) {
      folder = folder.substring(0, folder.length() - 1);
    }
    return this.unzipFiles(destination, Collections.singletonList(folder + "/**"), folder + File.separator, checkEmptyDestination);
  }

  public boolean unzipFiles(Path destination, List<String> antPatternList, String relativeFolder, boolean checkEmptyDestination) {
    try (InputStream is = new BufferedInputStream(new FileInputStream(this.getArchive()), SolidifyConstants.BUFFER_SIZE)) {
      return unzipFiles(is, destination, antPatternList, relativeFolder, checkEmptyDestination);
    } catch (final IOException e) {
      log.error("Problem in unzipping package ({})", this.archive, e);
    }
    return false;
  }

  public boolean zipFiles(Path source) {
    try {
      return this.createZip(source, this.generateFileList(source, false), null, false, false);
    } catch (final IOException e) {
      log.error("Problem in creating Zip package ({}.zip)", source, e);
      return false;
    }
  }

  public boolean zipFilesWithEmptyFolders(Path source) {
    try {
      return this.createZip(source, this.generateFileList(source, true), null, false, true);
    } catch (final IOException e) {
      log.error("Problem in creating Zip package with folders ({}.zip)", source, e);
      return false;
    }
  }

  public boolean zipFiles(URI source) {
    return this.zipFiles(Paths.get(source));
  }

  public boolean zipFilesWithEmptyFolders(URI source) {
    return this.zipFilesWithEmptyFolders(Paths.get(source));
  }

  private boolean createZip(Path source, List<Path> relativePaths, List<Path> destinationPath, boolean excludeContainingFolder,
          boolean addFolders) {
    final byte[] buffer = new byte[SolidifyConstants.BUFFER_SIZE];
    int count = 0;
    try (ZipOutputStream zipOut = new ZipOutputStream(
            new BufferedOutputStream(new FileOutputStream(this.getArchive()), SolidifyConstants.BUFFER_SIZE))) {
      for (final Path file : relativePaths) {
        final Path f = source.resolve(file);
        if (FileTool.isFile(f)) {
          final ZipEntry zipEntry = this.createZipEntry(file, false, count, destinationPath, excludeContainingFolder);
          zipOut.putNextEntry(zipEntry);
          try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(f.toFile()), SolidifyConstants.BUFFER_SIZE)) {
            int len;
            while ((len = bis.read(buffer)) > 0) {
              zipOut.write(buffer, 0, len);
            }
          }
          zipOut.closeEntry();
          count++;
        } else if (addFolders) {
          final ZipEntry zipEntry = this.createZipEntry(file, true, count, destinationPath, excludeContainingFolder);
          zipOut.putNextEntry(zipEntry);
          zipOut.closeEntry();
          count++;
        }
      }
    } catch (final FileNotFoundException e) {
      log.error("Cannot create Zip package ({})", this.archive, e);
      return false;
    } catch (final IOException e) {
      log.error("Problem in creating Zip package ({}.zip)", source, e);
      return false;
    }
    log.info("Zip package ('{}' in '{}') created with {} entries", source, this.archive, count);
    return true;
  }

  private ZipEntry createZipEntry(Path file, boolean isFolder, int count, List<Path> destinationPath, boolean excludeContainingFolder) {
    final String suffix = isFolder ? "/" : "";
    String entryName = normalizePath(file.getFileName().toString());
    if (!excludeContainingFolder) {
      if (destinationPath != null && !destinationPath.isEmpty()) {
        entryName = normalizePath(destinationPath.get(count).toString());
      } else {
        entryName = normalizePath(file.toString());
      }
    }
    return new ZipEntry(entryName + suffix);
  }

  private List<Path> generateFileList(Path src, boolean withEmptyFolders) throws IOException {
    try (Stream<Path> walk = Files.walk(src)) {
      return walk
              .filter(path -> FileTool.isFile(path)
                      || (withEmptyFolders && FileTool.isEmptyFolder(path)))
              .map(src::relativize)
              .toList();
    }
  }

  private Charset defineCharset() throws IOException {
    if (!FileTool.checkFile(this.archive)) {
      throw new IOException("Cannot find the archive: " + this.getArchive());
    }
    for (Charset charset : this.supportedCharsets) {
      try (ZipFile zip = new ZipFile(this.getArchive(), charset)) {
        return charset;
      } catch (final Exception e) {
        // try next charset
      }
    }
    throw new ZipException("No supported charset");
  }

  private static void checkIfFolderDestinationIsEmpty(Path destination) throws IOException {
    if (!FileTool.ensureFolderExists(destination)) {
      throw new IOException(destination + " is not a folder");
    }
    if (!FileTool.isEmptyFolder(destination)) {
      throw new IOException(destination + " is not empty");
    }
  }
}
