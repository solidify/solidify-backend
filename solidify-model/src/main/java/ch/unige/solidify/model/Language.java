/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - Language.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;

/**
 * Language representation names based in ISO 639-1 (2002)
 */
@Schema(description = "The language for internationalization (i18n) & localization (L10n)")
@Entity
public class Language extends ResourceNormalized {

  @Schema(description = "The ISO 639-1 code of the language")
  @Column(unique = true, nullable = false)
  private String code;

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof Language castOther)) {
      return false;
    }
    return Objects.equals(this.getCode(), castOther.getCode());
  }

  public String getCode() {
    return this.code;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.getCode());
  }

  @Override
  public void init() {
    // Do nothing
  }

  @Override
  public String managedBy() {
    return SolidifyConstants.ADMIN_MODULE;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
