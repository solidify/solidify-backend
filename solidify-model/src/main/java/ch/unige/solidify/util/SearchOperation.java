/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SearchOperation.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

/**
 * Different operation types, used by SearchCriteria
 *
 * @see ch.unige.solidify.util.SearchCriteria
 */
public enum SearchOperation {
  BETWEEN, CONTAINS, ENDS_WITH, EQUALITY, GREATER_EQUAL, GREATER_THAN, LESS_EQUAL, LESS_THAN, NEGATION, ORACLE_CONTAINS, STARTS_WITH;

  public static final String CASE_INSENSITIVE_FLAG = "i";
  public static final String OR_PREDICATE_FLAG = "any";
  protected static final String[] OPERATION_SET = { ":", "!", ">", "<", "~", "¬", "§", "≥", "≤", "∞", "¢" };

  public static SearchOperation getOperation(final String input) {
    switch (input) {
      case ":":
        return EQUALITY;
      case "!":
        return NEGATION;
      case ">":
        return GREATER_THAN;
      case "<":
        return LESS_THAN;
      case "~":
        return CONTAINS;
      case "¬":
        return STARTS_WITH;
      case "§":
        return ENDS_WITH;
      case "≥":
        return GREATER_EQUAL;
      case "≤":
        return LESS_EQUAL;
      case "∞":
        return BETWEEN;
      case "¢":
        return ORACLE_CONTAINS;
      default:
        return null;
    }
  }

  public static String[] getOperationList() {
    return OPERATION_SET;
  }

}
