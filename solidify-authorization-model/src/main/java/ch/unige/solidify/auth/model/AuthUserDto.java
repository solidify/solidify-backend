/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Authorization Model - AuthUserDto.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.model;

import java.time.OffsetDateTime;

/**
 * DTO used to communicate between administration module and authorization server
 */
public class AuthUserDto implements OrcidInfo {

  private String resId;
  private String externalUid;
  private OffsetDateTime creationWhen;
  private OffsetDateTime lastUpdateWhen;
  private String creationWho;
  private String lastUpdateWho;
  private Boolean disabled = Boolean.FALSE;
  private String email;
  private String firstName;
  private String homeOrganization;
  private String lastName;
  private AuthApplicationRole applicationRole;
  private AuthLoginInfo firstLogin;
  private AuthLoginInfo lastLogin;
  private String orcid;
  private Boolean verifiedOrcid;

  public OffsetDateTime getCreationWhen() {
    return this.creationWhen;
  }

  public void setCreationWhen(OffsetDateTime creationWhen) {
    this.creationWhen = creationWhen;
  }

  public OffsetDateTime getLastUpdateWhen() {
    return this.lastUpdateWhen;
  }

  public void setLastUpdateWhen(OffsetDateTime lastUpdateWhen) {
    this.lastUpdateWhen = lastUpdateWhen;
  }

  public String getCreationWho() {
    return this.creationWho;
  }

  public void setCreationWho(String creationWho) {
    this.creationWho = creationWho;
  }

  public String getLastUpdateWho() {
    return this.lastUpdateWho;
  }

  public void setLastUpdateWho(String lastUpdateWho) {
    this.lastUpdateWho = lastUpdateWho;
  }

  public AuthLoginInfo getFirstLogin() {
    return this.firstLogin;
  }

  public void setFirstLogin(AuthLoginInfo firstLogin) {
    this.firstLogin = firstLogin;
  }

  public LoginInfo getLastLogin() {
    return this.lastLogin;
  }

  public void setLastLogin(AuthLoginInfo lastLogin) {
    this.lastLogin = lastLogin;
  }

  public String getResId() {
    return this.resId;
  }

  public void setResId(String resId) {
    this.resId = resId;
  }

  public String getExternalUid() {
    return this.externalUid;
  }

  public void setExternalUid(String externalUid) {
    this.externalUid = externalUid;
  }

  public ApplicationRole getApplicationRole() {
    return this.applicationRole;
  }

  public void setApplicationRole(AuthApplicationRole applicationRole) {
    this.applicationRole = applicationRole;
  }

  public void setDisabled(Boolean disabled) {
    this.disabled = disabled;
  }

  public String getEmail() {
    return this.email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFullName() {
    return this.getLastName() + ", " + this.getFirstName();
  }

  public String getHomeOrganization() {
    return this.homeOrganization;
  }

  public void setHomeOrganization(String homeOrganization) {
    this.homeOrganization = homeOrganization;
  }

  public String getLastName() {
    return this.lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public boolean isEnabled() {
    return !this.disabled;
  }

  public String getOrcid() {
    return this.orcid;
  }

  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  public Boolean isVerifiedOrcid() {
    return this.verifiedOrcid;
  }

  public void setVerifiedOrcid(Boolean verifiedOrcid) {
    this.verifiedOrcid = verifiedOrcid;
  }

}
