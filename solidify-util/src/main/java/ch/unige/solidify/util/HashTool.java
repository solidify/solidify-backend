/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - HashTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import jakarta.xml.bind.DatatypeConverter;

import ch.unige.solidify.exception.SolidifyRuntimeException;

public class HashTool {

  public static String hash(String token) {
    MessageDigest messageDigest;
    try {
      messageDigest = MessageDigest.getInstance("SHA-256");
    } catch (final NoSuchAlgorithmException e) {
      throw new SolidifyRuntimeException("SHA-256 algorithm not available", e);
    }
    messageDigest.update(token.getBytes());
    return DatatypeConverter.printHexBinary(messageDigest.digest());
  }

  public static String hash(String algo, String s) throws NoSuchAlgorithmException {
    return HashTool.hash(algo, s.getBytes());
  }

  public static String hash(String algo, byte[] byteArray) throws NoSuchAlgorithmException {
    final MessageDigest md = MessageDigest.getInstance(algo);
    md.update(byteArray);
    final byte[] digest = md.digest();
    return DatatypeConverter.printHexBinary(digest);
  }

}
