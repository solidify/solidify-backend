/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Management - IndexSettingsWriteController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller.index;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.IndexUrlPath;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.controller.IndexConfigurationController;
import ch.unige.solidify.controller.NoSqlResourceController;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexProperties;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.RootPermissions;

@RootPermissions
@RestController
@ConditionalOnBean(IndexConfigurationController.class)
@RequestMapping(IndexUrlPath.INDEX_SETTING)
public class IndexSettingsWriteController extends NoSqlResourceController<IndexProperties> {
  private static final Logger log = LoggerFactory.getLogger(IndexSettingsWriteController.class);

  public IndexSettingsWriteController(IndexingSettingsService<IndexProperties> noSqlResourceService) {
    super(noSqlResourceService);
  }

  @Override
  public HttpEntity<IndexProperties> create(@RequestBody IndexProperties archiveIndex) {
    return super.create(archiveIndex);
  }

  @Override
  public HttpEntity<IndexProperties> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<IndexProperties>> list(@ModelAttribute IndexProperties search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<IndexProperties> update(@PathVariable String id, @RequestBody IndexProperties archiveIndex) {
    return super.update(id, archiveIndex);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @DeleteMapping("/" + IndexConstants.DELETE_ALL)
  public ResponseEntity<Void> deleteAll(@RequestParam(defaultValue = "no") String confirm) {
    final List<IndexProperties> list = this.noSqlResourceService.findAll(null);
    if (list.isEmpty()) {
      throw new SolidifyHttpErrorException(HttpStatus.GONE, " gone");
    }
    for (final IndexProperties i : list) {
      if (confirm.equals(SolidifyConstants.YES)) {
        if (!this.noSqlResourceService.delete(i)) {
          log.error(this.messageService.get("data-mgmt.index.delete.error", new Object[] { i.getResId() }));
          throw new SolidifyHttpErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot delete index " + i.getResId());
        }
        log.info(this.messageService.get("data-mgmt.index.delete.info", new Object[] { i.getResId() }));
      } else {
        log.info(this.messageService.get("data-mgmt.index.delete.preview", new Object[] { i.getResId() }));
      }
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
