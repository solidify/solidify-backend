/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIUrlPath.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

public final class OAIUrlPath {

  // ==========
  // OAI
  // ==========
  public static final String OAI = "/" + OAIConstants.OAI_MODULE;

  public static final String OAI_PROVIDER = OAI + "/" + OAIConstants.OAI_PROVIDER;
  public static final String OAI_SET = OAI + "/" + OAIConstants.OAI_SETS;
  public static final String OAI_METADATA_PREFIX = OAI + "/" + OAIConstants.OAI_METADATA_PREFIX;

  private OAIUrlPath() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
