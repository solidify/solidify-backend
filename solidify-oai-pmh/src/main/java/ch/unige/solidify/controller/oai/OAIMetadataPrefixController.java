/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH - OAIMetadataPrefixController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller.oai;

import java.util.Map;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.OAIUrlPath;
import ch.unige.solidify.controller.OAIController;
import ch.unige.solidify.controller.ResourceController;
import ch.unige.solidify.model.oai.OAIMetadataPrefix;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.AdminPermissions;

@RestController
@AdminPermissions
@ConditionalOnBean(OAIController.class)
@RequestMapping(OAIUrlPath.OAI_METADATA_PREFIX)
public class OAIMetadataPrefixController extends ResourceController<OAIMetadataPrefix> {

  @Override
  public HttpEntity<OAIMetadataPrefix> create(@RequestBody OAIMetadataPrefix oaiMetadataPrefix) {
    return super.create(oaiMetadataPrefix);
  }

  @Override
  public HttpEntity<OAIMetadataPrefix> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<OAIMetadataPrefix>> list(@ModelAttribute OAIMetadataPrefix search, Pageable pageable) {
    return super.list(search, pageable);
  }

  @Override
  public HttpEntity<OAIMetadataPrefix> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {
    return super.update(id, updateMap);
  }

  @Override
  public ResponseEntity<Void> delete(@PathVariable String id) {
    return super.delete(id);
  }

  @Override
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    return super.deleteList(ids);
  }

}
