/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - SudoRestClientTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import static ch.unige.solidify.auth.model.AuthApplicationRole.ADMIN_ID;
import static ch.unige.solidify.auth.model.AuthApplicationRole.ROOT_ID;
import static ch.unige.solidify.auth.model.AuthApplicationRole.USER_ID;

import java.util.ArrayDeque;
import java.util.Deque;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.interceptor.BearerAuthorizationInterceptor;

/**
 * This RestClientTool use the set of tokens defined in the
 * solidify.oauth2.accesstoken.[root|admin|user] properties. This is useful for launching tests with
 * different roles.
 */
@Profile("client-sudo")
@Component
public class SudoRestClientTool extends AbstractRestClientTool {

  private String accessToken;
  private final String rootToken;
  private final String adminToken;
  private final String userToken;
  private final String thirdPartyUserToken;
  private final Deque<String> previousTokens = new ArrayDeque<>();

  public SudoRestClientTool(@Value("${solidify.oauth2.accesstoken.root:#{null}}") String rootToken,
          @Value("${solidify.oauth2.accesstoken.admin:#{null}}") String adminToken,
          @Value("${solidify.oauth2.accesstoken.user:#{null}}") String userToken,
          @Value("${solidify.oauth2.accesstoken.thirdPartyUser:#{null}}") String thirdPartyUserToken) {
    this.rootToken = rootToken;
    this.adminToken = adminToken;
    this.userToken = userToken;
    this.thirdPartyUserToken = thirdPartyUserToken;
    this.accessToken = this.userToken;
  }

  @Override
  protected RestTemplateBuilder addToken(RestTemplateBuilder restTemplateBuilder) {
    return restTemplateBuilder.additionalInterceptors(new BearerAuthorizationInterceptor(this.accessToken));
  }

  public void sudoRoot() {
    this.previousTokens.push(this.accessToken);
    this.accessToken = this.rootToken;
  }

  public void sudoAdmin() {
    this.previousTokens.push(this.accessToken);
    this.accessToken = this.adminToken;
  }

  public void sudoUser() {
    this.previousTokens.push(this.accessToken);
    this.accessToken = this.userToken;
  }

  public void sudoThirdPartyUser() {
    this.previousTokens.push(this.accessToken);
    this.accessToken = this.thirdPartyUserToken;
  }

  public void exitSudo() {
    if (!this.previousTokens.isEmpty()) {
      this.accessToken = this.previousTokens.pop();
    }
  }

  public String getCurrentRole() {
    if (this.accessToken.equals(this.userToken) || this.accessToken.equals(this.thirdPartyUserToken)) {
      return USER_ID;
    }
    if (this.accessToken.equals(this.adminToken)) {
      return ADMIN_ID;
    }
    if (this.accessToken.equals(this.rootToken)) {
      return ROOT_ID;
    }
    throw new SolidifyRuntimeException("No correct access token set in SudoRestClientTool");
  }
}
