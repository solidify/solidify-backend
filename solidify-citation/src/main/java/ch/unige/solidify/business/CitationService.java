/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Citation - CitationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.business;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ch.unige.solidify.model.Citation;
import ch.unige.solidify.repository.CitationRepository;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.CitationSpecification;
import ch.unige.solidify.specification.SolidifySpecification;

@Service
public class CitationService extends ResourceService<Citation> {

  private static final Logger log = LoggerFactory.getLogger(CitationService.class);

  public Citation findByItemIdAndModeAndStyleAndLanguageAndOutputFormat(String itemId, Citation.Mode mode, String style, String language,
          Citation.OutputFormat outputFormat) {
    return ((CitationRepository) this.itemRepository).findByItemIdAndModeAndStyleAndLanguageAndOutputFormat(itemId, mode, style, language,
            outputFormat);
  }

  public Citation saveNewCitation(String itemId, Citation.Mode mode, String style, String language,
          Citation.OutputFormat outputFormat, String citationText) {
    Citation newCitation = new Citation();
    newCitation.setItemId(itemId);
    newCitation.setStyle(style);
    newCitation.setLanguage(language);
    newCitation.setOutputFormat(outputFormat);
    newCitation.setMode(mode);
    newCitation.setText(citationText);
    Citation savedCitation = this.save(newCitation);
    log.info("new citation generated: {}", savedCitation);
    return savedCitation;
  }

  /**
   * Delete all stored citations for the given item
   *
   * @param itemId
   */
  public void deleteAllForItem(String itemId) {
    ((CitationRepository) this.itemRepository).deleteAllByItemId(itemId);
  }

  @Override
  public SolidifySpecification<Citation> getSpecification(Citation resource) {
    return new CitationSpecification(resource);
  }
}
