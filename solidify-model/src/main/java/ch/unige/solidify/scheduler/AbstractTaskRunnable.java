/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - AbstractTaskRunnable.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.scheduler;

import java.time.OffsetDateTime;
import java.util.Objects;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

public abstract class AbstractTaskRunnable<T extends ScheduledTaskInterface> implements Runnable {
  private static final Logger log = LoggerFactory.getLogger(AbstractTaskRunnable.class);

  protected T scheduledTask;
  protected ScheduledFuture<AbstractTaskRunnable<T>> scheduledFuture;

  /**
   * Variable used to be able to use the previous task run start time. Required if the task execution needs this value as its begins by storing a
   * new value.
   */
  protected OffsetDateTime lastExecutionStartTime = null;

  protected AbstractTaskRunnable(T scheduledTask) {
    this.scheduledTask = scheduledTask;

    this.copyLastExecutionStartTime();
  }

  public ScheduledFuture<AbstractTaskRunnable<T>> getScheduledFuture() {
    return this.scheduledFuture;
  }

  public void setScheduledFuture(ScheduledFuture<AbstractTaskRunnable<T>> scheduledFuture) {
    this.scheduledFuture = scheduledFuture;
  }

  public T getScheduledTask() {
    return this.scheduledTask;
  }

  @Override
  @Transactional
  public void run() {
    log.info("Task '{}' ({}) starting", this.scheduledTask.getResId(), this.scheduledTask.getName());
    this.copyLastExecutionStartTime();
    this.updateLastExecutionStartTime();
    this.executeTask();
    this.updateLastExecutionEndTime();
    log.info("Task '{}' ({}) is over", this.scheduledTask.getResId(), this.scheduledTask.getName());
  }

  protected void updateLastExecutionStartTime() {
    this.scheduledTask.setLastExecutionStart(OffsetDateTime.now());
    this.saveScheduledTask(this.scheduledTask);
  }

  protected void copyLastExecutionStartTime() {
    this.lastExecutionStartTime = this.scheduledTask.getLastExecutionStart();
  }

  protected void updateLastExecutionEndTime() {
    this.scheduledTask.setLastExecutionEnd(OffsetDateTime.now());
    this.saveScheduledTask(this.scheduledTask);
  }

  /**
   * Update the scheduled task, but only if it hasn't been deleted while it was running
   *
   * @param scheduledTask
   */
  protected abstract void saveScheduledTask(T scheduledTask);

  protected abstract void executeTask();

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || this.getClass() != o.getClass()) {
      return false;
    }

    AbstractTaskRunnable<T> that = (AbstractTaskRunnable) o;
    if (this.scheduledTask != null) {
      return Objects.equals(this.scheduledTask.getResId(), that.scheduledTask.getResId());
    }

    return false;
  }

  @Override
  public int hashCode() {
    return 31 * (this.scheduledTask != null ? this.scheduledTask.hashCode() : 0);
  }
}
