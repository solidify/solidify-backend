/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - ChecksumTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Security;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.ChecksumAlgorithm;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProvider;
import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.util.ChecksumTool;

class ChecksumTest {
  private static final String CRC32 = "c78d3ef5";

  private static final String FILE = "test/test.xsd";
  private static final Logger log = LoggerFactory.getLogger(ChecksumTest.class);
  private static final String MD5 = "2d1f64e4d76f51b58131a65581005192";
  private static final String SHA1 = "5c14b2ff327256c98a01e58bf5aa7a38b25fbbe0";
  private static final String SHA256 = "1e40be16ef7d3fb3e9e41de85eb5fc4a0fed7e8e13cb357e3f07895b633c567b";
  private static final String SHA512 = "c3c1f4b83cc3cc4e561103113a6ef28f1ff7d7404a045d859ef3000b33a24c207bda94b5633b07a799a2562602f2d03949c25eaf53d37ee7aa878a589f6a5e31";

  @Test
  void computeChecksumTest() {
    Security.addProvider(new SolidifyProvider());
    final String[] algoList = { "CRC32", "MD5", "SHA1", "SHA256", "SHA512" };
    final String[] checksumList = { CRC32, MD5, SHA1, SHA256, SHA512 };
    assertEquals(algoList.length, checksumList.length);
    try {
      final Path path = Paths.get(new ClassPathResource(FILE).getURI());
      assertNotNull(path);
      final String[] checksums = ChecksumTool.computeChecksum(
              new BufferedInputStream(new FileInputStream(path.toFile()), SolidifyConstants.BUFFER_SIZE),
              algoList);
      assertEquals(algoList.length, checksums.length);
      for (int i = 0; i < algoList.length; i++) {
        assertEquals(checksumList[i], checksums[i], algoList[i]);
      }
    } catch (final Exception e) {
      log.error("Error", e);
      fail("Exception occurred");
    }
  }

  @Test
  void loadChecksumEnumTest() throws SolidifyCheckingException {
    final ChecksumAlgorithm algoFromEnum = ChecksumAlgorithm.SHA256;
    final ChecksumAlgorithm algoFromString = ChecksumAlgorithm.valueOf("SHA256");
    log.info("From Enum: {} => {} vs {}", algoFromEnum.name(), algoFromEnum, algoFromEnum);
    log.info("From String(SHA256): {} => {} vs {} ", algoFromString.name(), algoFromString, algoFromString);

    assertEquals(algoFromEnum, algoFromString);
  }

}
