/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - StandaloneRestClientTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import jakarta.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import ch.unige.solidify.interceptor.BearerAuthorizationInterceptor;

/**
 * This RestClientTool use the token defined in the solidify.oauth2.accesstoken property. This is useful for standalone tools.
 */
@Profile("client-standalone")
@Component
public class StandaloneRestClientTool extends AbstractRestClientTool {

  private String accessToken;

  @Value("${solidify.oauth2.accesstoken:#{null}}")
  private String mainAccessToken;

  @PostConstruct
  public void init() {
    this.accessToken = this.mainAccessToken;
  }

  public StandaloneRestClientTool() {
    if (this.mainAccessToken == null) {
      this.mainAccessToken = System.getProperty("solidify.oauth2.accesstoken");
    }
  }

  @Override
  protected RestTemplateBuilder addToken(RestTemplateBuilder restTemplateBuilder) {
    return restTemplateBuilder.additionalInterceptors(new BearerAuthorizationInterceptor(this.accessToken));
  }

}
