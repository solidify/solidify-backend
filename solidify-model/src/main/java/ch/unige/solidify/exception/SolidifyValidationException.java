/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SolidifyValidationException.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.exception;

import java.io.Serial;
import java.util.List;

import ch.unige.solidify.validation.FieldValidationError;
import ch.unige.solidify.validation.ValidationError;

public class SolidifyValidationException extends SolidifyRuntimeException {
  @Serial
  private static final long serialVersionUID = 1L;

  private final transient ValidationError validationError;

  public SolidifyValidationException(ValidationError error) {
    super("Validation failed");
    this.validationError = error;
  }

  public SolidifyValidationException(ValidationError error, String message) {
    super(message);
    this.validationError = error;
  }

  public SolidifyValidationException(ValidationError error, String message, Throwable exception) {
    super(message, exception);
    this.validationError = error;
  }


  public ValidationError getValidationError() {
    return this.validationError;
  }

  @Override
  public String toString() {

    final StringBuilder builder = new StringBuilder(this.getValidationError().getMainErrorMessage() + ": ");

    for (final FieldValidationError error : this.getValidationError().getErrors()) {
      final String fieldname = error.getFieldName();
      final List<String> errorMsgs = error.getErrorMessages();
      builder.append("[" + fieldname + ": " + String.join(", ", errorMsgs) + "]");
    }

    return builder.toString();
  }
}
