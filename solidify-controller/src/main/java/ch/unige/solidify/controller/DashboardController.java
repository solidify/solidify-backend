/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - DashboardController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import jakarta.servlet.http.HttpServletRequest;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.security.EveryonePermissions;
import ch.unige.solidify.service.GitInfoProperties;
import ch.unige.solidify.service.ModuleService;

@EveryonePermissions
@Controller
public class DashboardController {

  private final SolidifyProperties config;
  private final GitInfoProperties gitInfoProperties;
  private final ModuleService moduleService;
  private final HttpServletRequest httpServletRequest;

  public DashboardController(SolidifyProperties config, GitInfoProperties gitInfoProperties, ModuleService moduleService,
          HttpServletRequest httpServletRequest) {
    this.config = config;
    this.gitInfoProperties = gitInfoProperties;
    this.moduleService = moduleService;
    this.httpServletRequest = httpServletRequest;
  }

  @GetMapping(SolidifyConstants.URL_DASHBOARD)
  public String dashboard(Model model) {
    model.addAttribute("requestUrl", httpServletRequest.getRequestURL());
    model.addAttribute("applicationName", this.config.getServerDisplayName());
    model.addAttribute("applicationVersion", this.gitInfoProperties.getBuild().getVersion());
    model.addAttribute("modules", this.moduleService.getModuleControllerList());
    return "dashboard";
  }
}
