/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - MultiRelation2TiersController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.specification.Join2TiersSpecification;

/**
 * Parent controller to manage relations between 2 entity types linked by a relation table with properties. Unlike Relation2TiersController
 * multiple relations with the same entities and different properties is allowed.
 * Unlike Relation2TiersController the key of the relation must be a UUID
 *
 * @param <T> Type of the entity considered as the parent when managing the relation
 * @param <V> Type of the entity considered as the children when managing the relation
 * @param <J> Type of the join resource entity (the relation table) between &lt;T&gt; and &lt;V&gt;
 */
@RootPermissions
public abstract class MultiRelation2TiersController<T extends Resource, V extends Resource, J extends JoinResource<String>>
        extends Relation2TiersController<T, V, J, String> {

  @Override
  @PostMapping
  public HttpEntity<List<JoinResourceContainer<J>>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    final T parentItem = this.itemService.findOne(parentid);
    final List<JoinResourceContainer<J>> list = new ArrayList<>();
    for (final String i : ids) {
      final V childItem = this.childItemService.findOne(i);
      J savedJoinResource = this.relationItemService.saveRelation(parentItem, childItem, this.relationItemService.getDefaultJoinResource());
      list.add(this.relationItemService.getChildDTO(savedJoinResource));
    }
    if (!list.isEmpty()) {
      return new ResponseEntity<>(list, HttpStatus.CREATED);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @Override
  @PostMapping(SolidifyConstants.URL_ID)
  public HttpEntity<JoinResourceContainer<J>> create(@PathVariable String parentid, @PathVariable String id, @RequestBody J joinResource) {
    final T parentItem = this.itemService.findOne(parentid);
    final V childItem = this.childItemService.findOne(id);

    if (joinResource == null) {
      joinResource = this.relationItemService.getDefaultJoinResource();
    }
    J savedJoinResource = this.relationItemService.saveRelation(parentItem, childItem, joinResource);
    this.addLinks(parentid, this.relationItemService.getChildResource(joinResource));
    return new ResponseEntity<>(this.relationItemService.getChildDTO(savedJoinResource), HttpStatus.OK);
  }

  @GetMapping(value = SolidifyConstants.URL_ID, params = {SolidifyConstants.JOIN_RESOURCE_ID_PARAM})
  public HttpEntity<JoinResourceContainer<J>> get(@PathVariable String parentid, @PathVariable String id,
          @RequestParam String joinResourceId) {
    V childItem = this.childItemService.findOne(id);
    Join2TiersSpecification<J> joinSpecification = this.relationItemService.getJoinSpecification(parentid, childItem);
    joinSpecification.setFilterOnJoinResourceId(true);
    joinSpecification.getJoinCriteria().setCompositeKey(joinResourceId);

    final Page<J> page = this.relationItemService.findAllRelations(joinSpecification, Pageable.unpaged());
    if (page.isEmpty()) {
      throw new SolidifyResourceNotFoundException("Child item with id '" + id + "' is not linked to parent '" + parentid + "'");
    }
    if (page.getSize() > 1) {
      throw new IllegalStateException("Multiple relations found with the same compositeKey " + joinResourceId);
    }
    J joinResource = page.getContent().get(0);
    this.addLinks(parentid, this.relationItemService.getChildResource(joinResource));
    return new ResponseEntity<>(this.relationItemService.getChildDTO(joinResource), HttpStatus.OK);
  }

  @DeleteMapping(value = SolidifyConstants.URL_ID, params = {SolidifyConstants.JOIN_RESOURCE_ID_PARAM})
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id, @RequestParam String joinResourceId) {
    final T parentItem = this.itemService.findOne(parentid);
    final V childItem = this.childItemService.findOne(id);
    if (this.relationItemService.relationExists(parentItem, childItem)) {
      this.relationItemService.deleteRelation(parentItem, childItem, joinResourceId);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

}
