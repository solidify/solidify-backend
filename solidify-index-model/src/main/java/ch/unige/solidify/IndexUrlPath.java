/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - IndexUrlPath.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

import ch.unige.solidify.rest.ActionName;

public final class IndexUrlPath {

  // ==========
  // Index
  // ==========
  public static final String INDEX = "/" + IndexConstants.INDEX_MODULE;

  public static final String INDEX_SETTING = INDEX + "/" + IndexConstants.SETTING;
  public static final String INDEX_INDEX = INDEX + "/" + IndexConstants.INDEX;
  public static final String INDEX_INDEX_FIELD_ALIAS = INDEX + "/" + IndexConstants.INDEX_FIELD_ALIAS;
  public static final String INDEX_ACTION_LIST_FACET_REQUEST = "/" + ActionName.LIST_FACET_REQUESTS + "/{indexName}";

  private IndexUrlPath() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
