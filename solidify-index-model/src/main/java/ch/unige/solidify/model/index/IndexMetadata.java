/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - IndexMetadata.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.index;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.util.JSONTool;

@Schema(description = "A index metadata defines the properties to index.")
@JsonDeserialize(using = IndexMetadataDeserializer.class)
public class IndexMetadata extends NoSqlResource {

  private String index;
  private Map<String, Object> metadata = new HashMap<>();

  public boolean check() {
    return (this.getResId() != null && this.getIndex() != null && !this.getMetadata().isEmpty());
  }

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof IndexMetadata castOther)) {
      return false;
    }
    return Objects.equals(this.index, castOther.index) && Objects.equals(this.metadata, castOther.metadata);
  }

  public String getIndex() {
    return this.index;
  }

  public Map<String, Object> getMetadata() {
    return this.metadata;
  }

  public Object getMetadata(String... xpath) {
    return this.getMetadata(this.metadata, xpath);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.index, this.metadata);
  }

  public void setIndex(String index) {
    this.index = index;
  }

  public void setMetadata(Map<String, Object> metadata) {
    this.metadata = metadata;
  }

  public void setMetadata(String metadata) {
    this.metadata = JSONTool.convertJson2Map(metadata);
  }

  @Override
  public String toString() {
    return "IndexMetadataInfo [resId=" + this.getResId() + ", index=" + this.index + ", metadata=" + this.metadata + "]";
  }

  @Override
  public boolean update(NoSqlResource item) {
    return true;
  }

  @SuppressWarnings("unchecked")
  protected Object getMetadata(Map<String, Object> map, String... xpath) {
    if (map != null) {
      if (xpath.length == 1) {
        return map.get(xpath[0]);
      }
      return this.getMetadata((Map<String, Object>) map.get(xpath[0]), Arrays.copyOfRange(xpath, 1, xpath.length));
    } else {
      return null;
    }
  }

  protected List<Map<String, Object>> getField(String fieldName) {
    Object fieldObject = this.getMetadata(fieldName.split(SolidifyConstants.REGEX_FIELD_PATH_SEP));
    if (fieldObject instanceof ArrayList) {
      return (ArrayList<Map<String, Object>>) fieldObject;
    } else if (fieldObject instanceof Map) {
      return new ArrayList<>(Arrays.asList((Map<String, Object>) fieldObject));
    }
    return Collections.emptyList();
  }

}
