/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Documentation - AbstractResourceDoc.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.helper;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.json.JSONObject;
import org.springframework.restdocs.payload.FieldDescriptor;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.test.model.OpenApiExtraDocumentation;
import ch.unige.solidify.test.model.OpenApiExtraDocumentationProperties;
import ch.unige.solidify.test.model.TypeField;

public abstract class AbstractResourceDoc {

  public static final String CONSTRAINTS = "constraints";
  public static final String ENUM_VALUE_QUOTE = "`";
  public static final char ENUM_VALUE_SEPARATOR = ',';
  public static final String FORMAT_INTEGER_INT32 = "int32";
  public static final String FORMAT_INTEGER_INT64 = "int64";
  public static final String FORMAT_NUMBER_DOUBLE = "double";
  public static final String FORMAT_NUMBER_FLOAT = "float";
  public static final String FORMAT_PREFIX = "_Format_:";

  public static final String FORMAT_STRING_BINARY = "binary";
  public static final String FORMAT_STRING_BYTE = "byte";
  public static final String FORMAT_STRING_DATE = "date";
  public static final String FORMAT_STRING_DATE_TIME = "date-time";
  @SuppressWarnings("squid:S2068")
  public static final String FORMAT_STRING_PWD = "password";
  public static final String MANDATORY_VALUE = "mandatoryValue";
  public static final String NO_CONSTRAINT = "";
  public static final String READ_ONLY_VALUE = "readOnlyValue";
  public static final String TEST_STRING_VALUE = "test";
  public static final String VALUE_PREFIX = "_Values_:";
  protected static final String CONSTRAINT_CODE_ISO = "code ISO 3166-2";

  protected static final String CONSTRAINT_DATE_INTEGER = "3650 days ~ 10 years, 0 day ~ forever";
  protected static final String CONSTRAINT_VALID_DOI = "Valid DOI";
  protected static final String CONSTRAINT_VALID_ID = "Valid ID";
  protected static final String CONSTRAINT_VALID_URL = "Valid URL";
  protected static final String CONSTRAINT_WELLFORMED_XML = "Well-formed XML";
  protected static final String CONSTRAINT_VALID_JSON = "Valid JSON";
  protected static final String NO_TEST_VALUE = "NO_TEST_VALUE";

  protected static final String TEST_CODE_ISO = "CH";
  // Test value list
  protected static final String TEST_DATE_FORMAT_VALUE = "2018-07-10T15:36:50.323+0200";
  protected static final String TEST_DOI_VALID = "10.12345/dlcm:4zz7upkrbjd5hhaakssa6wm7yy";
  protected static final String TEST_DATE_SIMPLE_VALUE = "2018-07-10";
  protected static final String TEST_EMAIL_VALUE = "solidify@unige.ch";
  protected static final String TEST_NUMBER_VALUE = "11";
  protected static final String TEST_ORCID = "0000-1111-2222-3333";
  protected static final String TEST_SEARCH_QUERY = "*";
  protected static final String TEST_TRUE_VALUE = "true";
  protected static final String TEST_URL_VALUE = "https://www.unige.ch/";
  protected static final String TEST_JSON_OBJECT_VALUE = "{ \"test\" : \"value\" }";
  protected static final String TEST_JSON_ARRAY_VALUE = "[{ \"test\" : \"value\" }, { \"test2\" : \"value2\" }]";
  protected static final String TEST_XML_VALUE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><test />";

  public static final String getCamelCase(String className) {
    return className.substring(0, 1).toLowerCase().concat(className.substring(1));
  }

  private final List<FieldDescriptor> docFields;

  private final OpenApiExtraDocumentation openApiExtraDocumentation;

  protected AbstractResourceDoc() {
    this.openApiExtraDocumentation = new OpenApiExtraDocumentation(this.getName(), this.getModelName(), this.isNormalResource());
    this.docFields = this.getDocumentationFields();
    this.sortFields();
  }

  public final JSONObject getJsonFields() {
    return TestObjectBuilder.buildJsonObject(this.docFields);
  }

  public abstract List<String> getListLinks();

  public String getModelName() {
    if (this.getName().lastIndexOf('/') == -1) {
      return null;
    }
    final String resource = this.getName().substring(this.getName().lastIndexOf('/') + 1);
    if (resource.endsWith("ies")) {
      return resource.substring(0, resource.length() - 3) + "y";
    } else if (resource.endsWith("iases") || resource.endsWith("xes")) {
      return resource.substring(0, resource.length() - 2);
    } else if (resource.endsWith("s")) {
      return resource.substring(0, resource.length() - 1);
    }
    return resource;
  }

  public abstract String getName();

  public final int getNumberOfExpectedReadOnlyFields() {
    final Set<String> firstLevelPathSet = new HashSet<>();
    for (final FieldDescriptor fieldDescriptor : this.docFields) {
      if (!fieldDescriptor.isOptional() && String.valueOf(true).equals(fieldDescriptor.getAttributes().get(READ_ONLY_VALUE))) {
        final String firstLevelPath = fieldDescriptor.getPath().split(SolidifyConstants.REGEX_FIELD_PATH_SEP)[0];
        firstLevelPathSet.add(firstLevelPath);
      }
    }
    return firstLevelPathSet.size();
  }

  public final OpenApiExtraDocumentation getOpenApiExtraDocumentation() {
    return this.openApiExtraDocumentation;
  }

  public final List<FieldDescriptor> getReadOnlyDocumentationFields() {
    return this.docFields.stream().filter(
            field -> field.getAttributes().get(READ_ONLY_VALUE).equals(String.valueOf(true)))
            .collect(Collectors.toList());
  }

  public List<String> getCommonResourceFields() {
    return List.of(RestDocsTemplates.RESOURCE_ID_FIELD, RestDocsTemplates.CREATION_FIELD, RestDocsTemplates.LAST_UPDATE_FIELD,
            RestDocsTemplates.LINKS_FIELD);
  }

  public boolean isNormalResource() {
    // By default
    return true;
  }

  public abstract List<String> getResourceLinks();

  protected final String displayFormat(String format) {
    return FORMAT_PREFIX + " " + ENUM_VALUE_QUOTE + format + ENUM_VALUE_QUOTE;
  }

  protected final <T> List<String> enumToList(T[] enumValues) {
    return this.enumToList(enumValues, false);
  }

  protected final <T> List<String> enumToList(T[] enumValues, boolean toLowerCase) {
    final List<String> listStringEnumValues = new ArrayList<>();
    for (final T value : enumValues) {
      String val = value.toString();
      if (toLowerCase) {
        val = val.toLowerCase();
      }
      listStringEnumValues.add(val);
    }
    return listStringEnumValues;
  }

  protected abstract List<FieldDescriptor> getDocumentationFields();

  public final void updateValues(Map<String, String> properties) {
    // Do nothing by default
  }

  final void addToOpenApiExtraDocumentation(String fieldName, List<String> enumValues,
          TypeField type) {
    final String format = this.getExtraDocumentationFormat(type);
    if (format != null || enumValues != null) {
      this.openApiExtraDocumentation.getProperties()
              .add(new OpenApiExtraDocumentationProperties(fieldName, format, enumValues));
    }
  }

  private final String getExtraDocumentationFormat(TypeField type) {
    String format = null;
    switch (type) {
      case INT32:
        format = FORMAT_INTEGER_INT32;
        break;
      case INT64:
        format = FORMAT_INTEGER_INT64;
        break;
      case FLOAT:
        format = FORMAT_NUMBER_FLOAT;
        break;
      case DOUBLE:
        format = FORMAT_NUMBER_DOUBLE;
        break;
      case BYTE:
        format = FORMAT_STRING_BYTE;
        break;
      case BINARY:
        format = FORMAT_STRING_BINARY;
        break;
      case BOOLEAN:
        break;
      case DATE:
        format = FORMAT_STRING_DATE;
        break;
      case DATE_TIME:
        format = FORMAT_STRING_DATE_TIME;
        break;
      case PASSWORD:
        format = FORMAT_STRING_PWD;
        break;
      case STRING:
      case UNDEFINED:
      case ARRAY:
      case URL:
        break;
      default:
        break;
    }
    return format;
  }

  private final void sortFields() {
    this.docFields.sort((Comparator.comparing(FieldDescriptor::getPath)));
  }
}
