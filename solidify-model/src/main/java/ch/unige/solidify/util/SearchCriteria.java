/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SearchCriteria.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.Serial;
import java.io.Serializable;

/**
 * This is class is used to present a search criteria. It contains the key (normally it is an entity filed name), value (expected value) and
 * operator (less than , equals...) Optionally it contains a flag who indicate if the value is case sensitive or not. The default behavior is
 * case sensitive.
 */
public class SearchCriteria implements Serializable {
  @Serial
  private static final long serialVersionUID = -6340839586653964229L;
  private boolean caseInsensitive = false;
  private String caseType;
  private String key;
  private String operation;
  private SearchOperation operationType;
  private Object value;

  public SearchCriteria() {
    super();
  }

  /**
   * @param key normally it is an entity filed name
   * @param operationType operation type @see ch.unige.solidify.util.SearchOperation
   * @param value expected value
   */
  public SearchCriteria(final String key, SearchOperation operationType, final Object value) {
    super();
    this.key = key;
    this.operationType = operationType;
    this.value = value;
  }

  /**
   * @param key normally it is an entity filed name
   * @param operation operation type string value @see ch.unige.solidify.util.SearchOperation
   * @param value expected value
   */
  public SearchCriteria(final String key, final String operation, final Object value) {
    super();
    this.operation = operation;
    this.key = key;
    this.operationType = SearchOperation.getOperation(operation);
    this.value = value;
  }

  /**
   * @param caseType "i" or "c" i means case Insensitive
   * @param key normally it is an entity filed name
   * @param operationType operation type @see ch.unige.solidify.util.SearchOperation
   * @param value expected value
   */

  public SearchCriteria(final String caseType, final String key,
          final SearchOperation operationType, final Object value) {
    super();
    this.caseInsensitive = (caseType != null
            && caseType.equals(SearchOperation.CASE_INSENSITIVE_FLAG));
    this.key = key;
    this.operationType = operationType;
    this.value = value;
  }

  /**
   * @param caseType "i" or "c" i means case Insensitive
   * @param key normally it is an entity filed name
   * @param operation operation type string value @see ch.unige.solidify.util.SearchOperation
   * @param value expected value
   */
  public SearchCriteria(final String caseType, final String key, final String operation,
          final Object value) {
    super();
    this.caseInsensitive = (caseType != null
            && caseType.equals(SearchOperation.CASE_INSENSITIVE_FLAG));
    this.key = key;
    this.operation = operation;
    this.operationType = SearchOperation.getOperation(operation);
    this.value = value;
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof SearchCriteria c2)) {
      return false;
    }
    if (this.getKey().equals(c2.getKey()) && this.getValue().equals(c2.getValue())
            && this.getOperationType().equals(c2.getOperationType())
            && this.isCaseInsensitive() == (c2.isCaseInsensitive())) {
      return true;
    }
    return false;
  }

  public String getCaseType() {
    return this.caseType;
  }

  public String getKey() {
    return this.key;
  }

  public String getOperation() {
    return this.operation;
  }

  public SearchOperation getOperationType() {
    return this.operationType;
  }

  public Object getValue() {
    return this.value;
  }

  @Override
  public int hashCode() {
    int result = super.hashCode();
    result = 31 * result + (this.getOperationType() != null ? this.getOperationType().hashCode() : 0);
    result = 31 * result + (this.getOperation() != null ? this.getOperation().hashCode() : 0);
    result = 31 * result + (this.getKey() != null ? this.getKey().hashCode() : 0);
    result = 31 * result + (this.getValue() != null ? this.getValue().hashCode() : 0);
    result = 31 * result + (this.getCaseType() != null ? this.getCaseType().hashCode() : 0);
    return result;
  }

  public boolean isCaseInsensitive() {
    return this.caseInsensitive;
  }

  public void setCaseInsensitive(boolean caseInsensitive) {
    this.caseInsensitive = caseInsensitive;
  }

  public void setCaseType(String caseType) {
    this.caseType = caseType;
    this.caseInsensitive = (caseType != null
            && caseType.equals(SearchOperation.CASE_INSENSITIVE_FLAG));
  }

  public void setKey(String key) {
    this.key = key;
  }

  public void setOperation(String operation) {
    this.operation = operation;
    this.operationType = SearchOperation.getOperation(operation);
  }

  public void setValue(Object value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "SearchCriteria{" + "key='" + this.key + '\'' + ", operationType=" + this.operationType
            + ", operator='" + this.operation + '\'' + ", value=" + this.value + ", caseInsensitive="
            + this.caseInsensitive + '}';
  }
}
