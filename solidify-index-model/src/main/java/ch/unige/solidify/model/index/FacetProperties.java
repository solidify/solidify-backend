/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - FacetProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.index;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.unige.solidify.dto.LabelDTO;

public class FacetProperties implements Serializable {
  @Serial
  private static final long serialVersionUID = 6144380678824550462L;

  private String name;
  private Integer defaultVisibleValues;
  private List<LabelDTO> labels = new ArrayList<>();

  private List<LabelDTO> descriptionLabels = new ArrayList<>();

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getDefaultVisibleValues() {
    return this.defaultVisibleValues;
  }

  public void setDefaultVisibleValues(Integer defaultVisibleValues) {
    this.defaultVisibleValues = defaultVisibleValues;
  }

  public List<LabelDTO> getLabels() {
    return this.labels;
  }

  public List<LabelDTO> getDescriptionLabels() {
    return this.descriptionLabels;
  }

  public boolean isWithDescription() {
    return !this.getDescriptionLabels().isEmpty();
  }
}
