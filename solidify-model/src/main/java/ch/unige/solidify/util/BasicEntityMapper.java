/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - BasicEntityMapper.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import static ch.unige.solidify.SolidifyConstants.DB_RES_ID;
import static ch.unige.solidify.SolidifyConstants.RES_ID_FIELD;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ReflectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.rest.EmbeddableEntity;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.ResourceBase;

public class BasicEntityMapper {

  private static final Logger log = LoggerFactory.getLogger(BasicEntityMapper.class);

  private final ObjectMapper mapper = new ObjectMapper();

  public BasicEntityMapper() {
    this.mapper.registerModule(new JavaTimeModule());
    this.mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }

  public void patchResource(ResourceBase resource, Map<String, Object> properties) {
    if (resource != null && properties != null) {
      this.patchObject(resource, properties, resource.getNonUpdatableFields());
    }
  }

  public void patchResource(EmbeddableEntity embeddableEntity, Map<String, Object> properties) {
    if (embeddableEntity != null && properties != null) {
      this.patchObject(embeddableEntity, properties, embeddableEntity.getNonUpdatableFields());
    }
  }

  private void patchObject(Object objectToUpdate, Map<String, Object> properties, List<String> nonUpdatableFields) {
    try {

      Object newPropertiesAsObject = this.getObjectFromMap(properties, objectToUpdate);

      this.checkIfUpdatable(objectToUpdate, properties, newPropertiesAsObject, nonUpdatableFields);

      for (Map.Entry<String, Object> propertyEntry : properties.entrySet()) {
        String propertyName = propertyEntry.getKey();
        Object valueFromMap = propertyEntry.getValue();

        Field field = this.getField(objectToUpdate.getClass(), propertyName);

        // Ignore properties in map that are not object properties and ignore fields that are not updatable
        if (field != null && !nonUpdatableFields.contains(propertyName)) {
          // Get the value from the object built with Jackson in order to get it with correct type
          Object typedValue = ReflectionUtils.getField(field, newPropertiesAsObject);
          // update the property
          this.patchProperty(objectToUpdate, field, valueFromMap, typedValue);
        }
      }
    } catch (JsonProcessingException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  private void patchProperty(Object objectToUpdate, Field field, Object valueFromMap, Object typedValue) {
    try {
      Class<?> propertyClass = field.getType();
      if (Resource.class.isAssignableFrom(propertyClass)) {
        this.patchResourceProperty((Resource) objectToUpdate, field, (Map<String, Object>) valueFromMap, typedValue);
      } else if (EmbeddableEntity.class.isAssignableFrom(propertyClass)) {
        this.patchEmbeddableEntityProperty(objectToUpdate, field, (Map<String, Object>) valueFromMap, typedValue);
      } else if (!ChangeInfo.class.isAssignableFrom(field.getType()) && !this.ignoreListForPatch(field, objectToUpdate, typedValue)) {
        // Property is not an Entity nor an EmbeddableEntity nor a ChangeInfo --> assign value
        this.setField(objectToUpdate, field, typedValue);
      }
    } catch (IllegalAccessException | JsonProcessingException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  private void patchResourceProperty(Resource objectToUpdate, Field field, Map<String, Object> valueFromMap, Object typedValue)
          throws IllegalAccessException, JsonProcessingException {
    final Resource currentSubResource = (Resource) field.get(objectToUpdate);
    if (currentSubResource == null || valueFromMap == null) {
      // if the current value is null simply set the property as it won't update any current object
      // or if the new value is null, set the property to null
      this.setField(objectToUpdate, field, typedValue);
    } else {
      final boolean isSubresourcePatchable = objectToUpdate.getPatchableSubResources().contains(field.getName());
      final boolean isResIdInMap = valueFromMap.containsKey(RES_ID_FIELD);
      if (isSubresourcePatchable && (!isResIdInMap || valueFromMap.get(RES_ID_FIELD).equals(currentSubResource.getResId()))) {
        this.patchResource(currentSubResource, valueFromMap);
      } else if (isResIdInMap) {
        // Remove all sub resource properties except resId if present
        Map<String, Object> resIdMap = Map.of(RES_ID_FIELD, valueFromMap.get(RES_ID_FIELD));
        Object newValue = this.getObjectFromMap(resIdMap, typedValue);
        this.setField(objectToUpdate, field, newValue);
      } else {
        throw new SolidifyRuntimeException("Impossible to patch subresource without resId");
      }
    }
  }

  private void patchEmbeddableEntityProperty(Object objectToUpdate, Field field, Map<String, Object> valueFromMap, Object typedValue)
          throws IllegalAccessException {
    if (valueFromMap == null) {
      // if the new value is null, set the property to null
      this.setField(objectToUpdate, field, typedValue);
    } else {
      this.patchResource((EmbeddableEntity) field.get(objectToUpdate), valueFromMap);
    }
  }

  /**
   * Set the object value by using its setter if it exists, otherwise set the field's value directly with reflection
   *
   * @param objectToUpdate
   * @param field
   * @param typedValue
   */
  private void setField(Object objectToUpdate, Field field, Object typedValue) {
    String setterName = ReflectionTool.getSetterName(objectToUpdate, field.getName());
    if (setterName != null) {
      ReflectionTool.setValueWithSetter(objectToUpdate, field.getName(), typedValue);
    } else {
      log.warn(
              "Property '" + field.getName() + "' not updated on object of type '" + objectToUpdate.getClass().getSimpleName()
                      + "' as no setter was found");
    }
  }

  /**
   * Raise an exception if the properties map contains values that cannot be updated and have changed. Note: resId is ignored from the
   * verification in order to allow ignoring it silently
   *
   * @param objectToUpdate
   * @param properties
   * @param nonUpdatableFields
   */
  private void checkIfUpdatable(Object objectToUpdate, Map<String, Object> properties, Object newPropertiesAsObject,
          List<String> nonUpdatableFields) {
    for (String nonUpdatableFieldName : nonUpdatableFields) {
      if (properties.containsKey(nonUpdatableFieldName)) {
        Field field = this.getField(objectToUpdate.getClass(), nonUpdatableFieldName);
        try {
          if (field != null && !this.isFieldUpdatable(field, objectToUpdate, newPropertiesAsObject)) {

            // Always ignore resId without raising exception
            if (field.getName().equals(DB_RES_ID)) {
              continue;
            }

            throw new SolidifyUnmodifiableException("Field " + nonUpdatableFieldName + " is not modifiable");
          }
        } catch (IllegalAccessException e) {
          throw new SolidifyRuntimeException("Cannot access field", e);
        }
      }
    }
  }

  /**
   * Build an object of the given class with the given map properties by using Jackson
   *
   * @param properties
   * @param target
   * @return
   * @throws JsonProcessingException
   */
  private Object getObjectFromMap(Map<String, Object> properties, Object target) throws JsonProcessingException {
    String mapJson = this.mapper.writerWithDefaultPrettyPrinter().writeValueAsString(properties);
    return this.mapper.readValue(mapJson, target.getClass());
  }

  private Field getField(Class<?> clazz, String fieldName) {
    Field field = ReflectionUtils.findField(clazz, fieldName);
    if (field == null) {
      return null;
    }
    field.setAccessible(true);
    return field;
  }

  private boolean isFieldUpdatable(Field field, Object objectToUpdate, Object newPropertiesAsObject) throws IllegalAccessException {
    final Object oldFieldValue = field.get(objectToUpdate);
    final Object newFieldValue = field.get(newPropertiesAsObject);
    if (newFieldValue instanceof Resource && oldFieldValue instanceof Resource) {
      String oldResId = ((Resource) oldFieldValue).getResId();
      String newResId = ((Resource) newFieldValue).getResId();
      return oldResId.equals(newResId);
    } else {
      return Objects.equals(field.get(objectToUpdate), field.get(newPropertiesAsObject));
    }
  }

  /**
   * Return true if the property is a List of Resources or if both lists (current and new value) are null or empty. Note: lists of Resources must
   * be managed by using relation controllers and are ignored during patch.
   *
   * @param field
   * @param objectToUpdate
   * @param typedValue
   * @return
   * @throws IllegalAccessException
   */
  private boolean ignoreListForPatch(Field field, Object objectToUpdate, Object typedValue) throws IllegalAccessException {
    if (List.class.isAssignableFrom(field.getType())) {
      List<?> currentList = (List<?>) field.get(objectToUpdate);
      List<?> newList = (List<?>) typedValue;

      boolean currentListNullOrEmpty = currentList == null || currentList.isEmpty();
      boolean newListNullOrEmpty = newList == null || newList.isEmpty();

      if ((currentListNullOrEmpty && newListNullOrEmpty)
              || (!currentListNullOrEmpty && currentList.get(0) instanceof ResourceBase)
              || (!newListNullOrEmpty && newList.get(0) instanceof ResourceBase)) {
        return true;
      }
    }
    return false;
  }
}
