/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - Relation2TiersController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.service.JoinResource2TiersService;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.Join2TiersSpecification;
import ch.unige.solidify.validation.ValidationError;

/**
 * Parent controller to manage relations between 2 entity types linked by a relation table with properties Note: if there is no need to manage
 * properties in the relation table, the AssociationController can be used instead
 *
 * @param <T> Type of the entity considered as the parent when managing the relation
 * @param <V> Type of the entity considered as the children when managing the relation
 * @param <J> Type of the join resource entity (the relation table) between &lt;T&gt; and &lt;V&gt;
 * @param <K> Type of the composite key of the relation
 */
@RootPermissions
public abstract class Relation2TiersController<T extends Resource, V extends Resource, J extends JoinResource<K>, K>
        extends SolidifyController {

  @Autowired
  protected ResourceService<T> itemService;

  @Autowired
  protected ResourceService<V> childItemService;

  @Autowired
  protected JoinResource2TiersService<T, V, J, K> relationItemService;

  @GetMapping
  public HttpEntity<RestCollection<JoinResourceContainer<J>>> list(@PathVariable String parentid, @ModelAttribute V filterItem,
          Pageable pageable) {
    Join2TiersSpecification<J> specification = this.relationItemService.getJoinSpecification(parentid, filterItem);
    return this.getSubResourcesCollectionAsResponseEntity(parentid, specification, pageable);
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<JoinResourceContainer<J>> get(@PathVariable String parentid, @PathVariable String id) {
    V childItem = this.childItemService.findOne(id);
    Join2TiersSpecification<J> joinSpecification = this.relationItemService.getJoinSpecification(parentid, childItem);

    // generate a where clause with child.resId = id (to select only the requested child)
    joinSpecification.setFilterOnChildResId(true);

    final Page<J> page = this.relationItemService.findAllRelations(joinSpecification, Pageable.unpaged());
    if (page.getTotalElements() > 0) {
      J joinResource = page.getContent().get(0);
      this.addLinks(parentid, this.relationItemService.getChildResource(joinResource));
      return new ResponseEntity<>(this.relationItemService.getChildDTO(joinResource), HttpStatus.OK);
    } else {
      throw new SolidifyResourceNotFoundException("Child item with id '" + id + "' is not linked to parent '" + parentid + "'");
    }
  }

  /**
   * Create with explicit values for the relation properties
   *
   * @param parentid
   * @param id
   * @param joinResource
   * @return
   */
  @PostMapping(SolidifyConstants.URL_ID)
  public HttpEntity<JoinResourceContainer<J>> create(@PathVariable String parentid, @PathVariable String id, @RequestBody J joinResource) {
    final T parentItem = this.itemService.findOne(parentid);
    final V childItem = this.childItemService.findOne(id);

    if (joinResource == null) {
      joinResource = this.relationItemService.getDefaultJoinResource();
    }

    if (this.relationItemService.relationExists(parentItem, childItem)) {
      throw new SolidifyValidationException(new ValidationError("There is already a relation between " + parentid + " and " + id));
    } else {
      J savedJoinResource = this.relationItemService.saveRelation(parentItem, childItem, joinResource);
      this.addLinks(parentid, this.relationItemService.getChildResource(joinResource));
      return new ResponseEntity<>(this.relationItemService.getChildDTO(savedJoinResource), HttpStatus.OK);
    }
  }

  /**
   * Create with default values for the relations properties
   *
   * @param parentid
   * @param ids
   * @return
   */
  @PostMapping
  public HttpEntity<List<JoinResourceContainer<J>>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    final T parentItem = this.itemService.findOne(parentid);
    final List<JoinResourceContainer<J>> list = new ArrayList<>();
    for (final String i : ids) {
      final V childItem = this.childItemService.findOne(i);
      if (!this.relationItemService.relationExists(parentItem, childItem)) {
        J savedJoinResource = this.relationItemService.saveRelation(parentItem, childItem, this.relationItemService.getDefaultJoinResource());
        list.add(this.relationItemService.getChildDTO(savedJoinResource));
      }
    }
    if (!list.isEmpty()) {
      return new ResponseEntity<>(list, HttpStatus.CREATED);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<JoinResourceContainer<J>> update(@PathVariable String parentid, @PathVariable String id, @RequestBody J joinResource) {
    final T parentItem = this.itemService.findOne(parentid);
    final V childItem = this.childItemService.findOne(id);
    if (this.relationItemService.relationExists(parentItem, childItem)) {
      J savedJoinResource = this.relationItemService.saveRelation(parentItem, childItem, joinResource);
      this.addLinks(parentid, this.relationItemService.getChildResource(joinResource));
      return new ResponseEntity<>(this.relationItemService.getChildDTO(savedJoinResource), HttpStatus.OK);
    } else {
      throw new SolidifyResourceNotFoundException("Child item with id '" + id + "' is not linked to parent '" + parentid + "'");
    }
  }

  @DeleteMapping(SolidifyConstants.URL_ID)
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return this.deleteList(parentid, new String[] { id });
  }

  @DeleteMapping
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody(required = false) String[] ids) {
    final T parentItem = this.itemService.findOne(parentid);
    for (final String i : ids) {
      final V childItem = this.childItemService.findOne(i);
      if (this.relationItemService.relationExists(parentItem, childItem)) {
        this.relationItemService.deleteRelation(parentItem, childItem);
      }
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**********************/

  protected HttpEntity<RestCollection<JoinResourceContainer<J>>> getSubResourcesCollectionAsResponseEntity(String parentId,
          Join2TiersSpecification<J> specification, Pageable pageable) {
    final Page<J> page = this.relationItemService.findAllRelations(specification, this.buildPageableForSubresource(pageable));

    List<JoinResourceContainer<J>> childDTOs = new ArrayList<>();
    for (final J joinResource : page) {
      this.addLinks(parentId, this.relationItemService.getChildResource(joinResource));
      childDTOs.add(this.relationItemService.getChildDTO(joinResource));
    }

    final RestCollection<JoinResourceContainer<J>> collection = this.processPageAndLinks(childDTOs, parentId, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  /**
   * Transform the sorted fieldname into a fieldname that can be used to sort children
   *
   * @param pageable
   * @return
   */
  private Pageable buildPageableForSubresource(Pageable pageable) {
    if (pageable.getSort().isSorted()) {
      Sort.Order order = pageable.getSort().iterator().next();
      String sortProperty = order.getProperty();
      Sort.Direction direction = order.getDirection();

      String subresourceSortProperty = this.relationItemService.getChildPathFromJoinResource() + "." + sortProperty;
      Sort subresourceSort = Sort.by(subresourceSortProperty);
      if (direction == Sort.Direction.ASC) {
        subresourceSort = subresourceSort.ascending();
      } else {
        subresourceSort = subresourceSort.descending();
      }

      return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), subresourceSort);
    } else {
      return pageable;
    }
  }

  protected RestCollection<JoinResourceContainer<J>> processPageAndLinks(List<JoinResourceContainer<J>> list, String parentId,
          Pageable pageable) {
    final RestCollection<JoinResourceContainer<J>> collection = new RestCollection<>(list);
    collection.add(linkTo(this.getClass(), parentId).withSelfRel());
    collection.add((Tool.parentLink((linkTo(this.getClass(), parentId)).toUriComponentsBuilder()))
            .withRel(ActionName.PARENT));
    this.addSortLinks(linkTo(this.getClass(), parentId, pageable), collection);
    this.addPageLinks(linkTo(this.getClass(), parentId), collection, pageable);
    return collection;
  }

  protected void addLinks(String parentid, V v) {
    v.removeLinks();
    v.addLinks(linkTo(methodOn(this.getClass()).list(parentid, null, null)), false, false);
  }
}
