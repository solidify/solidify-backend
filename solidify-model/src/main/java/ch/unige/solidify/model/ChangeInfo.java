/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ChangeInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.io.Serializable;
import java.time.OffsetDateTime;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Schema(description = "The change information of an action.")
public interface ChangeInfo extends Serializable {

  @Schema(description = "The date when the action was done on the resources.", accessMode = AccessMode.READ_ONLY)
  OffsetDateTime getWhen();

  @Schema(description = "The user who did the action on the resource.", accessMode = AccessMode.READ_ONLY)
  String getWho();

  @Schema(description = "The full name of the user who did the action on the resource.", accessMode = AccessMode.READ_ONLY)
  String getFullName();

  void setWhen(OffsetDateTime when);

  void setWho(String who);

  void setFullName(String fullName);
}
