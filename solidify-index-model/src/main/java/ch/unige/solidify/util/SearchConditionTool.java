/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - SearchConditionTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;

public class SearchConditionTool {

  public static List<SearchCondition> generateFromString(String conditions) {
    List<SearchCondition> searchConditions = new ArrayList<>();
    if (!StringTool.isNullOrEmpty(conditions)) {
      ObjectMapper mapper = new ObjectMapper();
      try {
        searchConditions = mapper.readValue(conditions, new TypeReference<>() {
        });
      } catch (JsonProcessingException e) {
        throw new SolidifyRuntimeException("Unable to parse search conditions", e);
      }
    }
    return searchConditions;
  }

  public static void addQueryCondition(List<SearchCondition> searchConditions, String query) {
    if (!StringTool.isNullOrEmpty(query)) {
      final SearchCondition condition = new SearchCondition(SearchConditionType.QUERY);
      condition.setValue(query);
      searchConditions.add(condition);
    }
  }

  public static void addTermCondition(List<SearchCondition> searchConditions, String fieldName, String fieldValue) {
    searchConditions.add(SearchConditionTool.createTermSearchCondition(fieldName, fieldValue));
  }

  public static void addRangeCondition(List<SearchCondition> searchConditions, String fieldName, String fromValue, String toValue) {
    final SearchCondition condition = new SearchCondition(SearchConditionType.RANGE);
    condition.setField(fieldName);
    if (!StringTool.isNullOrEmpty(fromValue)) {
      condition.setLowerValue(fromValue);
    }
    if (!StringTool.isNullOrEmpty(toValue)) {
      condition.setUpperValue(toValue);
    }
    searchConditions.add(condition);
  }

  public static SearchCondition createTermSearchCondition(String fieldName, String fieldValue) {
    return new SearchCondition(SearchConditionType.TERM, fieldName, fieldValue);
  }

  public static SearchCondition createNestedSearchCondition(List<SearchCondition> conditions) {
    final SearchCondition searchCondition = new SearchCondition(SearchConditionType.NESTED_BOOLEAN);
    for (SearchCondition condition : conditions) {
      searchCondition.getNestedConditions().add(condition);
    }
    return searchCondition;
  }

  /**
   * This constructor should not be called
   */
  private SearchConditionTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
