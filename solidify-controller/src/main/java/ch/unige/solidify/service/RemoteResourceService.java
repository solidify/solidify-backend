/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - RemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.CacheNames;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.util.StringTool;

public abstract class RemoteResourceService<T extends Resource> {

  protected final SolidifyRestClientService restClientService;

  @Autowired
  private AbstractCacheService cacheService;

  protected RemoteResourceService(SolidifyRestClientService restClientService) {
    this.restClientService = restClientService;
  }

  /**
   * Retrieves a list of all RESTResource by sending a REST request to the corresponding webservice
   *
   * @param pageable A pageable
   * @return The Paged RestCollection of all resources
   */
  public RestCollection<T> findAll(Pageable pageable) {
    return this.restClientService.getResourceList(this.getResourceUrl(), this.getResourceClass(), pageable);
  }

  /**
   * Retrieves a RESTResource by sending a REST request to the corresponding webservice
   *
   * @param resId The id of the RESTResource to fetch
   * @return The resource
   */
  public T findOne(String resId) {
    if (!StringTool.isNullOrEmpty(resId)) {
      final String url = this.getResourceUrl() + "/" + resId;
      return this.restClientService.getResource(url, this.getResourceClass());
    } else {
      throw new SolidifyRuntimeException("resId should not be null nor empty");
    }
  }

  @Cacheable(CacheNames.RESOURCES)
  public T findOneWithCache(String resId) {
    final T resource = this.findOne(resId);
    this.cacheService.addSubResourcesIds(resource);
    return resource;
  }

  protected abstract Class<T> getResourceClass();

  protected String getResourceUrl() {
    throw new UnsupportedOperationException("Missing resource URL for type " + this.getClass().getName());
  }
}
