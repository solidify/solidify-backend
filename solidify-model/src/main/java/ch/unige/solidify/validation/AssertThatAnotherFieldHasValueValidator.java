/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - AssertThatAnotherFieldHasValueValidator.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.util.ReflectionTool;
import ch.unige.solidify.util.StringTool;

/**
 * Validator used to check that if the field fieldName has the expected value fieldValue then the
 * field dependFieldName must be not null and not empty.
 */
public class AssertThatAnotherFieldHasValueValidator
        implements ConstraintValidator<AssertThatAnotherFieldHasValue, Object> {
  private String dependFieldName;
  private String expectedFieldValue;
  private String fieldName;

  @Override
  public void initialize(AssertThatAnotherFieldHasValue annotation) {
    this.fieldName = annotation.fieldName();
    this.expectedFieldValue = annotation.fieldValue();
    this.dependFieldName = annotation.dependFieldName();
  }

  @Override
  public boolean isValid(Object value, ConstraintValidatorContext ctx) {

    if (value == null) {
      return true;
    }

    try {
      final String fieldValue = ReflectionTool.getProperty(value, this.fieldName);
      final String dependFieldValue = ReflectionTool.getProperty(value, this.dependFieldName);

      if (this.expectedFieldValue.equals(fieldValue) && StringTool.isNullOrEmpty(dependFieldValue)) {

        ctx.disableDefaultConstraintViolation();
        ctx.buildConstraintViolationWithTemplate(ctx.getDefaultConstraintMessageTemplate())
                .addPropertyNode(this.dependFieldName).addConstraintViolation();

        return false;
      }

    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new SolidifyRuntimeException("Error during validation", e);
    }

    return true;
  }
}
