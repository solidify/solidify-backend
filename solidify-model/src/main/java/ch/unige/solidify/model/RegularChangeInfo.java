/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - RegularChangeInfo.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;
import static ch.unige.solidify.SolidifyConstants.USER_PROPERTY_FILTER_NAME;

import java.io.Serial;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonFormat;

import ch.unige.solidify.util.StringTool;

@Embeddable
@JsonFilter(USER_PROPERTY_FILTER_NAME)
public class RegularChangeInfo implements ChangeInfo, Serializable {

  @Serial
  private static final long serialVersionUID = -1153421579085880364L;

  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = DB_DATE_LENGTH)
  private OffsetDateTime when;

  private String who;

  @Transient
  private String name;

  public RegularChangeInfo() {
    this.when = OffsetDateTime.now(ZoneOffset.UTC);
    this.who = "";
    this.name = "";
  }

  public RegularChangeInfo(OffsetDateTime changeTime) {
    this.when = changeTime;
    this.who = "";
    this.name = "";
  }

  public RegularChangeInfo(OffsetDateTime changeTime, String changeBy) {
    this.when = changeTime;
    this.who = changeBy;
  }

  @Override
  public OffsetDateTime getWhen() {
    return this.when;
  }

  @Override
  public String getWho() {
    return this.who;
  }

  @Override
  public String getFullName() {
    if (StringTool.isNullOrEmpty(this.name)) {
      return this.getWho();
    } else {
      return this.name;
    }
  }

  @Override
  public void setWhen(OffsetDateTime when) {
    this.when = when;
  }

  @Override
  public void setWho(String who) {
    this.who = who;
  }

  @Override
  public void setFullName(String fullName) {
    this.name = fullName;
  }

  @Override
  public String toString() {
    return "RegularChangeInfo{" +
            "when=" + this.when +
            ", who='" + this.who + '\'' +
            '}';
  }
}
