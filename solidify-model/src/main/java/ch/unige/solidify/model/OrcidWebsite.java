/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - OrcidWebsite.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.util.ArrayList;
import java.util.List;

public class OrcidWebsite {
  private List<OrcidWebsiteLabel> labels = new ArrayList<>();

  /**
   * URL used to check if the ORCID exists on the website
   */
  private String checkUrl;

  public List<OrcidWebsiteLabel> getLabels() {
    return this.labels;
  }

  public String getCheckUrl() {
    return this.checkUrl;
  }

  public void setCheckUrl(String checkUrl) {
    this.checkUrl = checkUrl;
  }
}
