/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH - OAISetService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.business;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.controller.OAIController;
import ch.unige.solidify.model.oai.OAISet;
import ch.unige.solidify.repository.OAISetRepository;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.OAISetSpecification;
import ch.unige.solidify.util.StringTool;

@Service
@ConditionalOnBean(OAIController.class)
public class OAISetService extends ResourceService<OAISet> {
  private static final Logger log = LoggerFactory.getLogger(OAISetService.class);

  private final OAISetRepository oaiSetRepository;

  public OAISetService(OAISetRepository oaiSetRepository) {
    this.oaiSetRepository = oaiSetRepository;
  }

  public OAISet findByName(String name) {
    return this.oaiSetRepository.findByName(name);
  }

  public OAISet findBySpec(String spec) {
    return this.oaiSetRepository.findBySpec(spec);
  }

  @Override
  public OAISetSpecification getSpecification(OAISet resource) {
    return new OAISetSpecification(resource);
  }

  public void initDefaultData() {
    this.createIfNotExists(OAIConstants.OAI_DOI_REGISTRATION, "DOI Registration", "Set to registry DOIs", "*");
  }

  public void createIfNotExists(String spec, String name, String description, String query) {
    if (this.findByName(name) == null) {
      OAISet oaiSet = new OAISet();
      oaiSet.setSpec(spec);
      oaiSet.setName(name);
      oaiSet.setQuery(query);
      oaiSet.setEnabled(Boolean.TRUE);
      if (!StringTool.isNullOrEmpty(description)) {
        oaiSet.setDescription(description);
      }
      this.save(oaiSet);
      log.info("OAISet '{}' created", name);
    }
  }

  @Override
  protected void validateItemSpecificRules(OAISet oaiSet, BindingResult errors) {

    // Check if Spec is a valid URI
    try {
      new URI(oaiSet.getSpec());
    } catch (URISyntaxException e) {
      errors.addError(new FieldError(oaiSet.getClass().getSimpleName(), "spec", e.getMessage()));
    }

  }

}
