/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Documentation - OpenApiExtraDocumentationProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.model;

import java.util.List;

public class OpenApiExtraDocumentationProperties {
  private List<String> enumsValue;
  private final String fieldName;
  private String format;
  private String item;

  public OpenApiExtraDocumentationProperties(String fieldName) {
    this.fieldName = fieldName;
    this.format = null;
    this.item = null;
    this.enumsValue = null;
  }

  public OpenApiExtraDocumentationProperties(String fieldName, String format) {
    this(fieldName);
    this.format = format;

  }

  public OpenApiExtraDocumentationProperties(String fieldName, String format, List<String> enumsValue) {
    this(fieldName, format);
    this.enumsValue = enumsValue;
  }

  public OpenApiExtraDocumentationProperties(String fieldName, String format, String item) {
    this(fieldName, format);
    this.item = item;
    this.enumsValue = null;
  }

  public List<String> getEnumsValue() {
    return this.enumsValue;
  }

  public String getFieldName() {
    return this.fieldName;
  }

  public String getFormat() {
    return this.format;
  }

  public String getItem() {
    return this.item;
  }

}
