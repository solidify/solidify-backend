/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - UrlConverter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.converter;

import java.net.MalformedURLException;
import java.net.URL;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class UrlConverter implements AttributeConverter<URL, String> {

  @Override
  public String convertToDatabaseColumn(URL url) {
    return (url == null) ? null : url.toString();
  }

  @Override
  public URL convertToEntityAttribute(String stringUrl) {
    if (stringUrl == null) {
      return null;
    }
    try {
      return new URL(stringUrl);
    } catch (final MalformedURLException e) {
      throw new IllegalArgumentException(e.getMessage());
    }
  }
}
