/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - StringTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONString;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public class StringTool {
  public static final String DATE_FORMAT = "yyyy-MM-dd";
  public static final String DATE_TIME_FORMAT_FOR_FILE = "yyyyMMdd-HHmmss";
  public static final String DATE_TIME_FORMAT_WITH_NANO = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
  public static final String DATE_TIME_FORMAT_WITH_SECONDS = "yyyy-MM-dd'T'HH:mm:ss'Z'";
  public static final String DATE_TIME_FORMAT = DATE_TIME_FORMAT_WITH_NANO;
  public static final String BYTES = "B";
  public static final String KILO_BYTES = "KB";
  public static final String MEGA_BYTES = "MB";
  public static final String GIGA_BYTES = "GB";
  public static final String TERA_BYTES = "TB";
  public static final String DEFAULT_ELLIPSIS = "...";
  public static final int INDEX_NOT_FOUND = -1;
  public static final int HEXADECIMAL_RADIX = 16;

  private static final Logger log = LoggerFactory.getLogger(StringTool.class);

  public static String capitalize(String str) {
    return StringTool.capitalize(str, true);
  }

  public static String capitalize(String str, boolean lowercase) {
    if (!StringTool.isNullOrEmpty(str)) {
      if (lowercase) {
        return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
      } else {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
      }
    } else {
      return str;
    }
  }

  /**
   * Check if the date is valid
   *
   * @param dateString the date as a string
   * @return true if valid false if not
   */
  public static boolean checkDate(String dateString) {
    if (dateString.matches("\\d{4}\\-\\d{2}\\-\\d{2}")) {
      dateString += "T00:00:00Z";
    }
    try {
      getDate(dateString);
      return true;
    } catch (final DateTimeParseException e) {
      return false;
    }
  }

  public static String convertToQueryString(Object object) {
    return convertToQueryString(object, Collections.emptyList());
  }

  public static String convertToQueryString(Object object, List<String> exceptionList) {
    Objects.requireNonNull(exceptionList, "The exception list must not be null");

    // Object --> map
    final ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.setFilterProvider(new SimpleFilterProvider().setFailOnUnknownId(false));

    @SuppressWarnings("unchecked")
    final Map<String, Object> map = objectMapper.convertValue(object, Map.class);

    final StringBuilder queryString = new StringBuilder();
    for (final Entry<String, Object> entry : map.entrySet()) {

      // Do not process if
      // empty object or HATEOAS links or Exception list

      if (entry.getValue() == null
              || entry.getKey().equals("links")
              || (!exceptionList.isEmpty() && exceptionList.contains(entry.getKey()))) {
        continue;
      }

      // Add & if needed
      if (queryString.length() > 0) {
        queryString.append("&");
      }
      // key=value
      queryString.append(entry.getKey());
      queryString.append("=");
      queryString.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
    }
    return queryString.toString();
  }

  public static String decode64(String encodedstring) {
    return new String(Base64.getDecoder().decode(encodedstring));
  }

  public static String decode64IfEncoded(String string) {
    if (string.startsWith(SolidifyConstants.BASE64_PREFIX)) {
      return StringTool.decode64(string.substring(SolidifyConstants.BASE64_PREFIX.length()));
    }
    return string;
  }

  public static String decodeUrl(String s) {
    return URLDecoder.decode(s, StandardCharsets.UTF_8);
  }

  public static String encode64(String string) {
    return Base64.getEncoder().withoutPadding().encodeToString(string.getBytes());
  }

  public static String encode64WithPadding(String string) {
    return Base64.getEncoder().encodeToString(string.getBytes());
  }

  public static String encodeUrl(String s) {
    return URLEncoder.encode(s, StandardCharsets.UTF_8);
  }

  public static void filterMap(Map<String, Object> metadata, String[] exceptions) {
    filterObject(metadata, exceptions);
  }

  public static String formatDate(OffsetDateTime date, DateTimeFormatter formatTemplate) {
    return date.toLocalDateTime().format(formatTemplate);
  }

  public static String formatSmartSize(long size) {
    String unit;
    double s;
    if (size < 1024l) {
      s = size;
      unit = BYTES;
    } else if (size < (1024l * 1024l)) {
      s = (double) size / 1024l;
      unit = KILO_BYTES;
    } else if (size < (1024l * 1024l * 1024l)) {
      s = (double) size / (1024l * 1024l);
      unit = MEGA_BYTES;
    } else if (size < (1024l * 1024l * 1024l * 1024l)) {
      s = (double) size / (1024l * 1024l * 1024l);
      unit = GIGA_BYTES;
    } else {
      s = (double) size / (1024l * 1024l * 1024l * 1024l);
      unit = TERA_BYTES;
    }
    return String.format(Locale.ROOT, "%.1f %s", s, unit);
  }

  public static long getSizeFromSmartSize(String smartSize) {
    double shortSize = Double.parseDouble(smartSize.substring(0, smartSize.indexOf(' ')));
    long size = 0;
    String unit = smartSize.substring(smartSize.indexOf(' ') + 1);
    size = switch (unit) {
      case BYTES -> (long) shortSize;
      case KILO_BYTES -> (long) (shortSize * 1024l);
      case MEGA_BYTES -> (long) (shortSize * 1024l * 1024l);
      case GIGA_BYTES -> (long) (shortSize * 1024l * 1024l * 1024l);
      case TERA_BYTES -> (long) (shortSize * 1024l * 1024l * 1024l * 1024l);
      default -> throw new SolidifyRuntimeException("Unknow unit " + unit);
    };
    return size;
  }

  public static String generateResId() {
    return UUID.randomUUID().toString();
  }

  /**
   * Get an OffsetDateTime instance from a String
   *
   * @param dateString the date as a string
   * @return the date as an OffsetDateTime
   * @throws DateTimeParseException
   */
  public static OffsetDateTime getDate(String dateString) {
    return OffsetDateTime.parse(dateString).toInstant().atOffset(ZoneOffset.UTC);
  }

  /*
   * Get an OffsetDateTime instance from a String and a DateTimeFormatter
   * @param dateString the date as a string
   * @param formatter the formatter
   * @return the date as an OffsetDateTime
   * @throws DateTimeParseException
   */
  public static OffsetDateTime getDate(String dateString, DateTimeFormatter formatter) {
    return OffsetDateTime.parse(dateString, formatter).toInstant().atOffset(ZoneOffset.UTC);
  }

  public static String getUrlPath(String url) {
    final URI uri = URI.create(url.trim());
    return uri.getPath();
  }

  public static boolean isNullOrEmpty(String value) {
    return (value == null || value.isEmpty());
  }

  @SuppressWarnings("squid:S00117")
  public static String JSON2XML(String jsonString) {
    final JSONObject json = new JSONObject(jsonString);
    return XML.toString(json);
  }

  @SuppressWarnings("squid:S00117")
  public static String Map2JSON(Map<String, Object> metadata) {
    final JSONObject json = new JSONObject(metadata);
    return json.toString();
  }

  @SuppressWarnings("squid:S00117")
  public static String Map2XML(Map<String, Object> metadata) {
    return JSON2XML(Map2JSON(metadata));
  }

  public static String toCamelCase(String strTocamel) {
    if (isNullOrEmpty(strTocamel)) {
      return strTocamel;
    }
    return strTocamel.substring(0, 1).toUpperCase() + strTocamel.substring(1).toLowerCase();
  }

  public static String toHexaString(byte[] bytes) {
    // Convert it to hexadecimal format
    final StringBuilder sb = new StringBuilder();
    for (final byte aByte : bytes) {
      sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
    }
    return sb.toString();
  }

  public static boolean toUpdate(String vOri, String vNew) {
    if (vNew == null || vNew.isEmpty()) {
      return false;
    }
    return (vOri == null || !vOri.equals(vNew));
  }

  public static String truncate(String str, int length) {
    if (StringTool.isNullOrEmpty(str) || length >= str.length() || length < 0) {
      return str;
    }
    return str.substring(0, length);
  }

  public static String substringBetween(String str, String startStr, String endStr) {
    if (str.contains(startStr)) {
      int startIndex = str.indexOf(startStr) + startStr.length();
      int endIndex = str.indexOf(endStr, startIndex);

      if (endIndex > -1) {
        str = str.substring(startIndex, endIndex);
      } else {
        str = str.substring(startIndex);
      }
    }
    return str;
  }

  public static int countSubstringOccurrences(String str, String subStr) {
    return (str.length() - str.replace(subStr, "").length()) / subStr.length();
  }

  public static String truncateWithEllipsis(String str, int length) {
    return StringTool.truncateWithEllipsis(str, length, DEFAULT_ELLIPSIS);
  }

  public static String truncateWithEllipsis(String str, int length, String ellipsis) {
    if (StringTool.isNullOrEmpty(str) || length >= str.length() || length < 0
            || StringTool.isNullOrEmpty(ellipsis)) {
      return str;
    } else {
      final int lengthWithoutEllipsis = length - ellipsis.length();
      if (lengthWithoutEllipsis > 0) {
        return StringTool.truncate(str, lengthWithoutEllipsis) + ellipsis;
      } else {
        return StringTool.truncate(str, length);
      }
    }
  }

  /**
   * Try to truncate a string with an ellipsis without cutting in the middle of a word:
   * <p>
   * "lorem ipsum dolores" | ---> "lorem ipsum dolores"
   * "lorem ipsum dolores | " ---> "lorem ipsum dolores..."
   * "lorem ipsum dolor|es" ---> "lorem ipsum..."
   * "lorem ipsum |dolores" ---> "lorem ipsum..."
   * "lorem ipsum| dolores" ---> "lorem ipsum..."
   * "lorem ipsu|m dolores" ---> "lorem..."
   *
   * @param str
   * @param length
   * @param ellipsis
   * @return
   */
  public static String truncateOnSpaceWithEllipsis(String str, int length, String ellipsis) {
    String truncatedStr = StringTool.truncateWithEllipsis(str, length, ellipsis);

    if (!StringTool.isNullOrEmpty(truncatedStr) && !truncatedStr.equals(str) && truncatedStr.endsWith(ellipsis)
            && truncatedStr.indexOf(' ') != -1) {
      // the result is not equal to the original string, ends with the ellipsis and contains space
      // --> if the cut has been made in the middle of a word, remove the remaining part of this word
      if (str.charAt(length - ellipsis.length()) != ' ') {
        // the cut wasn't already on a space --> we cut on the previous space
        truncatedStr = StringTool.truncate(truncatedStr, truncatedStr.lastIndexOf(" ")).stripTrailing() + ellipsis;
      }

      // check if there are remaining spaces just before the ellipsis
      // "lorem ..." --> "lorem..."
      String truncatedStrWithoutEllipsis = truncatedStr.substring(0, truncatedStr.lastIndexOf(ellipsis));
      truncatedStrWithoutEllipsis = truncatedStrWithoutEllipsis.stripTrailing();
      truncatedStr = truncatedStrWithoutEllipsis + ellipsis;
    }

    return truncatedStr;
  }

  public static String truncateOnSpaceWithEllipsis(String str, int length) {
    return StringTool.truncateOnSpaceWithEllipsis(str, length, DEFAULT_ELLIPSIS);
  }

  @SuppressWarnings("squid:S00117")
  public static String XML2JSON(String xmlString) {
    final JSONObject json = XML.toJSONObject(xmlString);
    return json.toString();
  }

  private static void filterObject(Object obj, String[] exceptions) {
    if (obj == null) {
      return;
    }
    if (obj instanceof JSONObject || obj instanceof JSONArray || obj instanceof JSONString
            || obj instanceof Byte || obj instanceof Character || obj instanceof Short
            || obj instanceof Integer || obj instanceof Long || obj instanceof Boolean
            || obj instanceof Float || obj instanceof Double || obj instanceof String) {
      return;
    }

    if (obj instanceof Collection) {
      for (Object o : (Collection<?>) obj) {
        filterObject(o, exceptions);
      }
    }
    if (obj.getClass().isArray()) {
      final int length = Array.getLength(obj);
      for (int i = 0; i < length; i += 1) {
        filterObject(Array.get(obj, i), exceptions);
      }
    }
    if (obj instanceof Map) {
      final List<String> toDelete = new ArrayList<>();
      final Iterator<?> i = ((Map<?, ?>) obj).entrySet().iterator();
      while (i.hasNext()) {
        final Map.Entry<?, ?> e = (Map.Entry<?, ?>) i.next();
        final String k = ((String) e.getKey());
        // Do not index exception
        for (final String x7 : exceptions) {
          if (k.endsWith(x7)) {
            toDelete.add(k);
            break;
          }
        }
        // Remove empty object
        if (e.getValue() instanceof String v) {
          if (StringTool.isNullOrEmpty(v)) {
            log.trace("To remove {}", k);
            toDelete.add(k);
          }
        } else {
          filterObject(e.getValue(), exceptions);
        }
      }
      for (final String k : toDelete) {
        log.trace("Removing {}", k);
        ((Map<?, ?>) obj).remove(k);
      }
    }
  }

  /**
   * @param str the String to get a substring from, may be null
   * @param separator the String to search for, may be null
   * @return the substring before the first occurrence of the separator, {@code null} if null String input
   */
  public static String substringBefore(final String str, final String separator) {
    if (StringTool.isNullOrEmpty(str)) {
      return str;
    }
    final int pos = str.indexOf(separator);
    if (pos == INDEX_NOT_FOUND) {
      return str;
    }
    return str.substring(0, pos);
  }

  /**
   * @param str
   * @return The URL string without the trailing '/'
   */
  public static String normalizeUrl(final String str) {
    StringTool.checkIsNullOrEmpty(str);
    StringTool.checkUrl(str.trim());
    return StringTool.normalizeUri(str.trim());
  }

  /**
   * @param str
   * @return The base URL string with the trailing '/'
   * @throws URISyntaxException
   */
  public static String normalizeBaseUrl(final String str) {
    StringTool.checkIsNullOrEmpty(str);
    StringTool.checkUrl(str.trim());
    return StringTool.normalizeBaseUri(str.trim());
  }

  /**
   * @param str
   * @return The URI string without the trailing '/'
   */
  public static String normalizeUri(final String str) {
    StringTool.checkIsNullOrEmpty(str);
    StringTool.checkUri(str.trim());
    return StringTool.removeTrailing(str.trim(), "/");
  }

  /**
   * @param str
   * @return The base URI string with the trailing '/'
   * @throws URISyntaxException
   */
  public static String normalizeBaseUri(final String str) {
    StringTool.checkIsNullOrEmpty(str);
    StringTool.checkUri(str.trim());
    if (!str.trim().endsWith("/")) {
      return str + "/";
    }
    return str.trim();
  }

  /**
   * @param str
   * @param trailingStr
   * @return The string without the given final part
   */
  public static String removeTrailing(final String str, String trailingStr) {
    if (StringTool.isNullOrEmpty(str)) {
      return str;
    }
    if (str.endsWith(trailingStr)) {
      return str.substring(0, str.lastIndexOf(trailingStr));
    }
    return str;
  }

  public static String removePrefix(final String str, String prefix) {
    if (StringTool.isNullOrEmpty(str)) {
      return str;
    }
    if (str.startsWith(prefix)) {
      return str.substring(prefix.length());
    }
    return str;
  }

  /**
   * Encode all non-ASCII characters into their HTML hexadecimal entities
   *
   * @param str
   * @return
   */
  public static String encodeNonAsciiCharsIntoHtmlHexadecimalEntities(String str) {
    if (StringTool.isNullOrEmpty(str)) {
      return str;
    }
    StringBuilder sb = new StringBuilder(str.length());
    for (int i = 0; i < str.length(); i++) {
      char charAt = str.charAt(i);
      if (charAt > 127) {
        sb.append("&#x");
        sb.append(Integer.toString(charAt, HEXADECIMAL_RADIX));
        sb.append(";");
      } else {
        sb.append(charAt);
      }
    }
    return sb.toString();
  }

  /**
   * Replace text, ignoring case considerations.
   *
   * @param source
   * @param textToReplace
   * @param newText
   * @return
   */
  public static String replaceIgnoreCase(String source, String textToReplace, String newText) {
    Pattern caseInsensitivePattern = StringTool.getCaseInsensitivePattern(textToReplace);
    return caseInsensitivePattern.matcher(source).replaceAll(Matcher.quoteReplacement(newText));
  }

  /**
   * Convert String in Inputstream
   *
   * @param str
   * @return
   */
  public static InputStream toInputStream(String str) {
    return new ByteArrayInputStream(str.getBytes());
  }

  /**
   * Find a substring in a string, ignoring case considerations, and return the value from the source string matching the searched text.
   * The case of the returned value may thus differ from the searched text.
   *
   * @param source
   * @param searchedText
   * @return
   */
  public static String findIgnoreCase(String source, String searchedText) {
    Pattern caseInsensitivePattern = StringTool.getCaseInsensitivePattern(searchedText);
    Optional<MatchResult> matchResultOpt = caseInsensitivePattern.matcher(source).results().findFirst();
    if (matchResultOpt.isPresent()) {
      return matchResultOpt.get().group();
    }
    return null;
  }

  private static Pattern getCaseInsensitivePattern(String text) {
    return Pattern.compile(text, Pattern.LITERAL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
  }

  private static void checkIsNullOrEmpty(String str) {
    if (StringTool.isNullOrEmpty(str)) {
      throw new SolidifyRuntimeException("Empty or null string");
    }
  }

  private static void checkUrl(String str) {
    try {
      new URL(str);
    } catch (MalformedURLException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  private static void checkUri(String str) {

    try {
      new URI(str);
    } catch (URISyntaxException e) {
      throw new SolidifyRuntimeException(e.getMessage(), e);
    }
  }

  /**
   * This constructor should not be called
   */
  private StringTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
