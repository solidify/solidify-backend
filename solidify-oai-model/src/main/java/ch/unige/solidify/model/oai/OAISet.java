/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAISet.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.oai;

import static ch.unige.solidify.SolidifyConstants.SEARCH_KEY;

import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.rest.ResourceNormalized;

@Schema(description = "OAI set.")
@Entity
@Table(name = "oaiSet")
public class OAISet extends ResourceNormalized {

  @Schema(description = "The description of the OAI set.")
  @Size(min = 1, max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "If the OAI set is enable.")
  @NotNull
  private Boolean enabled;

  @Schema(description = "The name of the OAI set.")
  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String name;

  /**
   * The query on the index service (e.g Elasticsearch) used to populate the OAI set
   */
  @Schema(description = "The criteria to define the OAI set: query compliant with indexing search.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String query;

  @Schema(description = "The spec of OAI set, which is an unique identifier for the set.")
  @NotNull
  @Size(min = 1)
  @Column(unique = true)
  private String spec;

  /****************************************************************/

  @Override
  @Transient
  @JsonIgnore
  public CacheMessage getCacheMessage() {
    final CacheMessage cacheMessage = super.getCacheMessage();
    cacheMessage.addOtherProperty(SEARCH_KEY, Boolean.TRUE);
    return cacheMessage;
  }

  public String getDescription() {
    return this.description;
  }

  public String getName() {
    return this.name;
  }

  public String getQuery() {
    return this.query;
  }

  public Boolean getEnabled() {
    return this.enabled;
  }

  /****************************************************************/

  public String getSpec() {
    return this.spec;
  }

  /****************************************************************/

  @Override
  @PrePersist
  @PreUpdate
  public void init() {
    if (this.enabled == null) {
      this.enabled = false;
    }
  }

  public boolean isEnabled() {
    return Objects.requireNonNullElse(this.enabled, Boolean.FALSE);
  }

  @Override
  public String managedBy() {
    return OAIConstants.OAI_MODULE;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setQuery(String query) {
    this.query = query;
  }

  public void setSpec(String spec) {
    this.spec = spec;
  }

}
