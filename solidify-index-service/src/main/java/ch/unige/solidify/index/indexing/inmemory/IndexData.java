/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Service - IndexData.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.index.indexing.inmemory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import ch.unige.solidify.model.index.IndexMetadata;

@Profile("index-inmemory")
@Component
public class IndexData {
  private final Map<String, Map<String, IndexMetadata>> indexes = new HashMap<>();

  public Map<String, Map<String, IndexMetadata>> getIndexes() {
    return this.indexes;
  }

  public Map<String, Map<String, IndexMetadata>> getActiveIndexes(List<String> indexes) {
    Map<String, Map<String, IndexMetadata>> activeIndexes = new HashMap<>();
    for (String index : indexes) {
      activeIndexes.put(index, this.getIndexValues(index));
    }
    return activeIndexes;
  }

  public Map<String, IndexMetadata> getIndexValues(String index) {
    return this.indexes.get(index);
  }

}
