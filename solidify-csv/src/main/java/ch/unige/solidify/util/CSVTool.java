/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify CSV - CSVTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyCheckingException;

public class CSVTool {

  /**
   * This constructor should not be called
   */
  private CSVTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  public static void wellformed(Path csv) {
    try {
      wellformed(Files.newInputStream(csv));
    } catch (IOException e) {
      throw new SolidifyCheckingException("Error during the well-formedness verification", e);
    }
  }

  public static void wellformed(String csv) {
    wellformed(new ByteArrayInputStream(csv.getBytes()));
  }

  private static void wellformed(InputStream csvStream) {
    int colNumber = 0;
    int lineNumber = 0;
    try (CSVReader csvReader = new CSVReader(new InputStreamReader(csvStream))) {
      String[] line;
      while ((line = csvReader.readNext()) != null) {
        lineNumber++;
        // Check column number if CSV is valid
        if (colNumber == 0) {
          colNumber = line.length;
        } else if (colNumber != line.length) {
          throw new SolidifyCheckingException("Wrong column number at line " + lineNumber);
        }
      }
    } catch (CsvValidationException | IOException e) {
      throw new SolidifyCheckingException("Error during the well-formedness verification", e);
    }
  }

}
