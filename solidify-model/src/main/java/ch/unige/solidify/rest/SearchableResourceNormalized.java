/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SearchableResourceNormalized.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.util.List;

import jakarta.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.unige.solidify.util.SearchCriteria;

public abstract class SearchableResourceNormalized<T> extends ResourceNormalized implements Searchable<T> {

  @JsonProperty
  @JsonInclude(JsonInclude.Include.NON_NULL)
  @Transient
  private List<SearchCriteria> searchCriterias;

  @Override
  @JsonIgnore
  public List<SearchCriteria> getSearchCriterias() {
    return this.searchCriterias;
  }

  @Override
  @JsonProperty
  public void setSearchCriterias(List<SearchCriteria> searchCriterias) {
    this.searchCriterias = searchCriterias;
  }

}
