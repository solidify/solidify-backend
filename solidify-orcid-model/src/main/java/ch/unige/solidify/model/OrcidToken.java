/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify ORCID Model - OrcidToken.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.time.OffsetDateTime;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.converter.CryptoConverter;
import ch.unige.solidify.rest.ResourceNormalized;

@Schema(description = "Personal token allowing to interact with ORCID API.")
@Entity
public class OrcidToken extends ResourceNormalized {

  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_LARGE_STRING_LENGTH)
  @Convert(converter = CryptoConverter.class)
  private String accessToken;
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_LARGE_STRING_LENGTH)
  @Convert(converter = CryptoConverter.class)
  private String refreshToken;

  @NotNull
  private String scope;

  /**
   * The user's name retrieved from an ORCID record. This field may be null or empty if the user has set their name to private in their profile.
   */
  private String name;

  @NotNull
  private String orcid;

  private OffsetDateTime expiresIn;

  @Override
  public void init() {

  }

  public String getAccessToken() {
    return this.accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public String getRefreshToken() {
    return this.refreshToken;
  }

  public void setRefreshToken(String refreshToken) {
    this.refreshToken = refreshToken;
  }

  public String getScope() {
    return this.scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOrcid() {
    return this.orcid;
  }

  public void setOrcid(String orcid) {
    this.orcid = orcid;
  }

  public OffsetDateTime getExpiresIn() {
    return this.expiresIn;
  }

  public void setExpiresIn(OffsetDateTime expireIn) {
    this.expiresIn = expireIn;
  }

  @Override
  public String managedBy() {
    return SolidifyConstants.ADMIN_MODULE;
  }
}
