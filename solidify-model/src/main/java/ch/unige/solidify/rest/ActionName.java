/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ActionName.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import ch.unige.solidify.SolidifyConstants;

public class ActionName {

  public static final String ALL = "All";

  public static final String CLOSE = "close";
  public static final String COUNT = "count";
  public static final String CREATE = "new";
  public static final String CREATION = "creation.when";
  public static final String DASHBOARD = "dashboard";
  public static final String DELETE = "remove";
  public static final String DELETE_CACHE = "delete-cache";
  public static final String DELETE_BY_QUERY = "delete-by-query";
  public static final String DOWNLOAD = "download";
  public static final String DOWNLOAD_TOKEN = "download-token";
  public static final String ERROR = "error";
  public static final String FILE = "file";
  public static final String HISTORY = "history";
  public static final String IMPORT = "import";
  public static final String IMPORTED = "imported";
  public static final String EXPORT = "export";
  public static final String EXPORTED = "exported";
  public static final String LANDING = "landing";
  public static final String LAST_CREATED = "lastCreated";
  public static final String LAST_UPDATED = "lastUpdated";
  public static final String LIST = "list";
  public static final String MODULE = "module";
  public static final String NEXT = "next";
  public static final String PAGE = "page";
  public static final String PARENT = "parent";
  public static final String PREVIOUS = "previous";
  public static final String RANGE = "searchRange";
  public static final String READ = "get";
  public static final String RESOURCES = "resources";
  public static final String RESUME = "resume";
  public static final String SAVE = "save";
  public static final String SEARCH = "search";
  public static final String LIST_FACET_REQUESTS = "list-facet-requests";
  public static final String SELF = "self";
  public static final String SIZE = "size";
  public static final String SITEMAP = "sitemap";
  public static final String XML_EXTENSION = ".xml";
  public static final String SITEMAP_XML = SITEMAP + XML_EXTENSION;
  public static final String SORT = "sort";
  public static final String START = "start";
  public static final String START_ORCID_AUTH = "start-orcid-auth";
  public static final String ORCID_EXTERNAL_WEBSITES = "external-websites";
  public static final String STATUS = "status";
  public static final String UPLOAD = "upload";
  public static final String UPLOAD_ARCHIVE = "upload-archive";
  public static final String UPDATE = "edit";
  public static final String UPDATED = "lastUpdate.when";
  public static final String VALUES = "values";
  public static final String VIEW = "view";

  private ActionName() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
