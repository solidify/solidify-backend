/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - GlobalBanner.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.UniqueConstraint;
import jakarta.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.StringTool;

/**
 * Global Banner
 */
@Schema(description = "The global banner allows to communicate general informations.")
@Entity
public class GlobalBanner extends ResourceNormalized {

  public enum GlobalBannerType {
    CRITICAL, WARNING, INFO
  }

  @Schema(description = "The name of the global banner.")
  @NotNull
  @Column(unique = true)
  private String name;

  @Schema(description = "The type of the global banner.")
  @Enumerated(EnumType.STRING)
  @NotNull
  private GlobalBanner.GlobalBannerType type;

  @Schema(description = "The title list by language of the global banner.")
  @ElementCollection
  @Column(name = SolidifyConstants.DB_GLOBAL_BANNER_ID)
  @CollectionTable(joinColumns = {
          @JoinColumn(name = SolidifyConstants.DB_GLOBAL_BANNER_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = {
                  SolidifyConstants.DB_GLOBAL_BANNER_ID, SolidifyConstants.DB_LANGUAGE_ID }))
  private List<Label> titleLabels = new ArrayList<>();

  @Schema(description = "The description list by language of the global banner.")
  @ElementCollection
  @Column(name = SolidifyConstants.DB_GLOBAL_BANNER_ID)
  @CollectionTable(joinColumns = {
          @JoinColumn(name = SolidifyConstants.DB_GLOBAL_BANNER_ID) }, uniqueConstraints = @UniqueConstraint(columnNames = {
                  SolidifyConstants.DB_GLOBAL_BANNER_ID, SolidifyConstants.DB_LANGUAGE_ID }))
  private List<LargeLabel> descriptionLabels = new ArrayList<>();

  @Schema(description = "If the global banner is enable.")
  @Column(nullable = false)
  private Boolean enabled;

  @Schema(description = "The start date of the global banner.")
  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = SolidifyConstants.DB_DATE_LENGTH)
  private OffsetDateTime startDate;

  @Schema(description = "The end date of the global banner.")
  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = StringTool.DATE_TIME_FORMAT)
  @Column(length = SolidifyConstants.DB_DATE_LENGTH)
  private OffsetDateTime endDate;

  /***********************************************************/

  public String getName() {
    return this.name;
  }

  @Schema(description = "If the global banner has a description.", accessMode = AccessMode.READ_ONLY)
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  public Boolean isWithDescription() {
    return !this.getDescriptionLabels().isEmpty();
  }

  public GlobalBanner.GlobalBannerType getType() {
    return this.type;
  }

  public List<Label> getTitleLabels() {
    return this.titleLabels;
  }

  public List<LargeLabel> getDescriptionLabels() {
    return this.descriptionLabels;
  }

  public Boolean getEnabled() {
    return this.enabled;
  }

  public OffsetDateTime getStartDate() {
    return this.startDate;
  }

  public OffsetDateTime getEndDate() {
    return this.endDate;
  }

  /***********************************************************/

  public void setName(String name) {
    this.name = name;
  }

  public void setType(GlobalBanner.GlobalBannerType type) {
    this.type = type;
  }

  public void setTitleLabels(List<Label> titleLabels) {
    this.titleLabels = titleLabels;
  }

  public void setDescriptionLabels(List<LargeLabel> descriptionLabels) {
    this.descriptionLabels = descriptionLabels;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public void setStartDate(OffsetDateTime startDate) {
    this.startDate = startDate;
  }

  public void setEndDate(OffsetDateTime endDate) {
    this.endDate = endDate;
  }

  @Override
  public void init() {
    if (this.enabled == null) {
      this.enabled = true;
    }
  }

  @Override
  public String managedBy() {
    return SolidifyConstants.ADMIN_MODULE;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result
            + Objects.hash(this.descriptionLabels, this.enabled, this.endDate, this.name, this.startDate, this.titleLabels, this.type);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    GlobalBanner other = (GlobalBanner) obj;
    return Objects.equals(this.descriptionLabels, other.descriptionLabels) && Objects.equals(this.enabled, other.enabled)
            && Objects.equals(this.endDate, other.endDate) && Objects.equals(this.name, other.name)
            && Objects.equals(this.startDate, other.startDate) && Objects.equals(this.titleLabels, other.titleLabels) && this.type == other.type;
  }

}
