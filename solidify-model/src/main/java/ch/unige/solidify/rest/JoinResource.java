/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - JoinResource.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import static ch.unige.solidify.SolidifyConstants.DB_DATE_LENGTH;

import java.time.OffsetDateTime;
import java.util.Objects;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ch.unige.solidify.listener.CacheListener;
import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.model.CreatedChangeInfo;
import ch.unige.solidify.model.LastModifiedChangeInfo;

/**
 * Class that can inherited to represent a join table with additional properties
 *
 * @param <K>
 */
@MappedSuperclass
@EntityListeners({ CacheListener.class, AuditingEntityListener.class })
public abstract class JoinResource<K> extends ResourceBase {

  @AttributeOverride(name = "when", column = @Column(name = "creationWhen", length = DB_DATE_LENGTH))
  @AttributeOverride(name = "who", column = @Column(name = "creationWho"))
  @Embedded
  protected CreatedChangeInfo creation = new CreatedChangeInfo();

  @AttributeOverride(name = "when", column = @Column(name = "lastUpdateWhen", length = DB_DATE_LENGTH))
  @AttributeOverride(name = "who", column = @Column(name = "lastUpdateWho"))
  @Embedded
  protected LastModifiedChangeInfo lastUpdate = new LastModifiedChangeInfo();

  /*************************************************************************************/

  @Override
  public ChangeInfo getCreation() {
    return this.creation;
  }

  @Override
  @JsonIgnore
  public String getCreatedBy() {
    return this.getCreation().getWho();
  }

  @Override
  @JsonIgnore
  public String getCreatorName() {
    return this.getCreation().getFullName();
  }

  @Override
  @JsonIgnore
  public OffsetDateTime getCreationTime() {
    return this.getCreation().getWhen();
  }

  @Override
  public void setCreatedBy(String createdBy) {
    this.getCreation().setWho(createdBy);
  }

  @Override
  public void setCreatorName(String creatorName) {
    this.getCreation().setFullName(creatorName);
  }

  /*************************************************************************************/

  @Override
  public ChangeInfo getLastUpdate() {
    return this.lastUpdate;
  }

  @Override
  @JsonIgnore
  public String getUpdatedBy() {
    return this.getLastUpdate().getWho();
  }

  @Override
  @JsonIgnore
  public String getEditorName() {
    return this.getLastUpdate().getFullName();
  }

  @Override
  @JsonIgnore
  public OffsetDateTime getUpdateTime() {
    return this.getLastUpdate().getWhen();
  }

  @Override
  public void setUpdatedBy(String updatedBy) {
    this.getLastUpdate().setWho(updatedBy);
  }

  @Override
  public void setEditorName(String editorName) {
    this.getLastUpdate().setFullName(editorName);
  }

  @Override
  public void updateTime() {
    // Let the @LastModifiedDate annotation do the job
  }

  @JsonIgnore
  public CacheMessage getCacheMessage() {
    return null;
  }

  /*************************************************************************************/

  public abstract K getCompositeKey();

  public abstract void setCompositeKey(K compositeKey);

  /*************************************************************************************/

  @Override
  public boolean equals(final Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof JoinResource)) {
      return false;
    }
    if (!super.equals(other)) {
      return false;
    }
    final JoinResource<K> that = (JoinResource<K>) other;
    return Objects.equals(this.getCompositeKey(), that.getCompositeKey()) &&
            Objects.equals(this.getCreation(), that.getCreation()) &&
            Objects.equals(this.getLastUpdate(), that.getLastUpdate());
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.getCompositeKey(), this.getCreation(), this.getLastUpdate());
  }
}
