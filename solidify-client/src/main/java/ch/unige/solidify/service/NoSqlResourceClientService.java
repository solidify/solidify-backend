/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Client - NoSqlResourceClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.net.URI;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

public abstract class NoSqlResourceClientService<T extends NoSqlResource> extends ResourceBaseClientService<T> {

  protected NoSqlResourceClientService(
          String application,
          String module,
          String resourcePath,
          Class<T> resourceClass,
          AbstractRestClientTool restClientTool,
          Environment env) {
    super(application, module, resourcePath, resourceClass, restClientTool, env);
  }

  protected RestCollection<T> deserializeResponse(String jsonResult) {
    return this.deserializeResponse(jsonResult, this.getResourceClass());
  }

  protected <U> RestCollection<U> deserializeResponse(String jsonResult, Class<U> itemClass) {
    final List<U> list = CollectionTool.getList(jsonResult, itemClass);
    final RestCollectionPage page = CollectionTool.getPage(jsonResult);
    return new RestCollection<>(list, page);
  }

  protected void downloadWithToken(String urlString, Path path) {
    final String url = this.getBaseUrl() + urlString;
    this.restClientTool.downloadContent(URI.create(url), path, TokenUsage.WITH_TOKEN, false);
  }

  protected void downloadContentToDirectory(String urlString, Path path) {
    final String url = this.getBaseUrl() + urlString;
    this.restClientTool.downloadContent(URI.create(url), path, TokenUsage.WITH_TOKEN, true);
  }

  protected String getResources(String url, Map<String, String> params) {
    final String finalUrl = url + "?size=" + RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    return restTemplate.getForObject(finalUrl, String.class, params);
  }

  protected String getResources(String url, Pageable pageable) {
    url = this.completeUrlWithPagination(url, null, pageable);
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    return restTemplate.getForObject(url, String.class);
  }

  protected <U> List<U> searchByProperties(String url, Map<String, String> params, Class<U> itemClass) {
    StringBuilder urlBuilder = new StringBuilder(url);
    urlBuilder.append("?size=")
            .append(RestCollectionPage.MAX_SIZE_PAGE);
    for (final Map.Entry<String, String> entry : params.entrySet()) {
      urlBuilder.append("&")
              .append(entry.getKey())
              .append("=")
              .append(entry.getValue());
    }
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getBaseUrl());
    final String jsonString = restTemplate.getForObject(urlBuilder.toString(), String.class);
    return CollectionTool.getList(jsonString, itemClass);
  }

  private String completeUrlWithPagination(String url, Map<String, String> properties, Pageable pageable) {
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);
    if (pageable != null) {
      final int page = pageable.getPageNumber();
      final int size = pageable.getPageSize();
      final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
      params.set(ActionName.PAGE, Integer.toString(page));
      params.set(ActionName.SIZE, Integer.toString(size));
      for (final Sort.Order order : pageable.getSort()) {
        final String sortProperty = order.getProperty();
        final String sortDirection = order.getDirection().toString();
        params.set(ActionName.SORT, sortProperty + "," + sortDirection);
      }
      if (properties != null && !properties.isEmpty()) {
        params.setAll(properties);
      }
      uriBuilder.queryParams(params);
    }

    return uriBuilder.toUriString();
  }

}
