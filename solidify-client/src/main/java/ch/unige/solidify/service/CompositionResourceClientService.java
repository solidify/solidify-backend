/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Client - CompositionResourceClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;

public abstract class CompositionResourceClientService<V extends Resource, T extends Resource>
        extends ResourceBaseClientService<T>
        implements CompositionResourceClientServiceInterface<V, T> {

  protected CompositionResourceClientService(
          String application,
          String module,
          String resourcePath,
          Class<T> resourceClass,
          AbstractRestClientTool restClientTool,
          Environment env) {
    super(application, module, resourcePath, resourceClass, restClientTool, env);
  }

  /******************/
  /** CRUD methods **/
  /******************/

  /**
   * CREATE
   **/
  @Override
  public T create(String parentId, T resource) {
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_PARENT_ID_FIELD, parentId);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.postForObject(this.getResourceUrl(), resource, this.getResourceClass(), params);
  }

  /**
   * GET
   **/
  @Override
  public T findOne(String parentId, String resId) {
    final String url = this.getResourceUrl() + SolidifyConstants.URL_ID;
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_PARENT_ID_FIELD, parentId);
    params.put(SolidifyConstants.URL_ID_FIELD, resId);
    return this.getObjectFromUrl(url, params);
  }

  /**
   * LIST
   **/
  protected List<T> findAll(String parentId, Class<T> itemClass) {
    final String url = this.getResourceUrl() + "?size=" + RestCollectionPage.MAX_SIZE_PAGE;
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_PARENT_ID_FIELD, parentId);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(url, String.class, params);
    return CollectionTool.getList(jsonString, itemClass);
  }

  /**
   * UPDATE
   **/
  @Override
  public T update(String parentId, String resId, T resource) {
    final String url = this.getResourceUrl() + SolidifyConstants.URL_ID;
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.URL_PARENT_ID_FIELD, parentId);
    params.put(SolidifyConstants.URL_ID_FIELD, resId);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.patchForObject(url, new HttpEntity<>(resource), this.getResourceClass(), params);
  }

  /**
   * DELETE
   **/
  @Override
  public void delete(String parentId, String resId) {
    String url = this.getResourceUrl();
    url = url.replace(SolidifyConstants.URL_PARENT_ID, "/" + parentId + "/");
    url += "/" + resId;

    final RestTemplate restTemplate = this.restClientTool.getClient();
    restTemplate.delete(url);
  }

}
