/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - Relation3TiersController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.rest.JoinResourceContainer;
import ch.unige.solidify.rest.Relation3TiersChildDTO;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.service.JoinResource3TiersService;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.util.BasicEntityMapper;

/**
 * Parent controller to manage relations between 3 entity types linked by a relation table with properties
 *
 * @param <T> Type of the entity considered as the parent when managing the relation
 * @param <V> Type of the entity considered as the child when managing the relation
 * @param <W> Type of the entities considered as the grandchildren when managing the relation
 * @param <J> Type of the join resource entity (the relation table) between &lt;T&gt;, &lt;V&gt; and &lt;W&gt;
 */
@RootPermissions
public abstract class Relation3TiersController<T extends Resource, V extends Resource, W extends Resource, J extends JoinResource<?>>
        extends SolidifyController {

  protected static final String NOTHING_TO_DELETE = "Nothing to delete";

  private final BasicEntityMapper basicEntityMapper = new BasicEntityMapper();

  @Autowired
  protected ResourceService<T> itemService;

  @Autowired
  protected ResourceService<V> childItemService;

  @Autowired
  protected ResourceService<W> grandChildItemService;

  @Autowired
  protected JoinResource3TiersService<T, V, W, J> relationItemService;

  /**
   * Return a list of children (DTO) linked to parent, along with a list grandchildren and relation properties
   *
   * @param parentid
   * @param filterItem
   * @param pageable
   * @return
   */
  @GetMapping
  public HttpEntity<RestCollection<Relation3TiersChildDTO>> list(@PathVariable String parentid, @ModelAttribute V filterItem,
          Pageable pageable) {

    SolidifySpecification<V> childSpecification = this.relationItemService.getChildSpecification(parentid, filterItem);

    final Page<V> childrenPage = this.childItemService.findAll(childSpecification, pageable);

    // Create DTO list
    final List<Relation3TiersChildDTO> dtoList = new ArrayList<>();
    for (final V childItem : childrenPage) {
      this.addChildLinks(parentid, childItem);
      // complete the result with their list of grandchildren
      Relation3TiersChildDTO childDTO = this.getChildDTOCompletedWithGrandChildren(parentid, childItem);
      dtoList.add(childDTO);
    }

    // Create REST collection with pagnation
    final RestCollection<Relation3TiersChildDTO> collection = new RestCollection<>(
            dtoList,
            new RestCollectionPage(
                    pageable.getPageNumber(),
                    pageable.getPageSize(),
                    childrenPage.getTotalPages(),
                    childrenPage.getTotalElements()));
    collection.add(linkTo(this.getClass(), parentid).withSelfRel());
    collection.add((Tool.parentLink((linkTo(this.getClass(), parentid)).toUriComponentsBuilder())).withRel(ActionName.PARENT));
    this.addSortLinks(linkTo(this.getClass(), parentid, pageable), collection);
    this.addPageLinks(linkTo(this.getClass(), parentid), collection, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  /**
   * Get a grandchild (DTO) along with the relation properties
   *
   * @param parentid
   * @param id
   * @param grandChildId
   * @return
   */
  @GetMapping(SolidifyConstants.URL_ID + SolidifyConstants.URL_GRAND_CHILD_ID)
  public <D extends JoinResourceContainer<J>> HttpEntity<D> get(@PathVariable String parentid, @PathVariable String id,
          @PathVariable String grandChildId) {
    J joinResource = this.relationItemService.findRelation(parentid, id, grandChildId);
    this.addGrandChildLinks(parentid, id, this.relationItemService.getGrandChildResource(joinResource));
    D grandChildDTO = this.relationItemService.getGrandChildDTO(joinResource);
    return new ResponseEntity<>(grandChildDTO, HttpStatus.OK);
  }

  /**
   * Return a child (DTO) with a list grandchildren
   *
   * @param parentid
   * @param id
   * @return
   */
  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<Relation3TiersChildDTO> get(@PathVariable String parentid, @PathVariable String id) {
    V childItem = this.childItemService.findOne(id);
    SolidifySpecification<V> childSpec = this.relationItemService.getChildSpecification(parentid, childItem);

    // generate a where clause with resId = id (to select only the requested child)
    childSpec.setFilterOnRootResId(true);

    final Page<V> list = this.childItemService.findAll(childSpec, Pageable.unpaged());
    if (list.getTotalElements() == 1) {
      this.addChildLinks(parentid, childItem);
      // complete the result with the list of grandchildren
      Relation3TiersChildDTO childDTO = this.getChildDTOCompletedWithGrandChildren(parentid, childItem);
      return new ResponseEntity<>(childDTO, HttpStatus.OK);
    } else {
      throw new SolidifyResourceNotFoundException("Child item with id '" + id + "' is not linked to parent '" + parentid + "'");
    }
  }

  /**
   * Create a relation between parent, child and grandchild with specified relation properties If no joinResource is given, create relation with
   * default relation properties
   *
   * @param parentid
   * @param id
   * @param grandChildId
   * @param joinResource
   * @return
   */
  @PostMapping(SolidifyConstants.URL_ID + SolidifyConstants.URL_GRAND_CHILD_ID)
  public HttpEntity<J> create(@PathVariable String parentid, @PathVariable String id, @PathVariable String grandChildId, J joinResource) {
    this.itemService.existsByIdOrThrowException(parentid);
    this.childItemService.existsByIdOrThrowException(id);
    this.grandChildItemService.existsByIdOrThrowException(grandChildId);

    if (!this.relationItemService.relationExists(parentid, id, grandChildId)) {
      J newJoinResource = this.relationItemService.saveRelation(parentid, id, grandChildId, joinResource);
      return new ResponseEntity<>(newJoinResource, HttpStatus.CREATED);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Create relations between parent, child and a list of grandchildren, with default relation properties
   *
   * @param parentid
   * @param id
   * @param gdChildIds
   * @return
   */
  @PostMapping(SolidifyConstants.URL_ID)
  public HttpEntity<List<J>> create(@PathVariable String parentid, @PathVariable String id, @RequestBody String[] gdChildIds) {
    // Check if parents exist
    this.itemService.existsByIdOrThrowException(parentid);
    this.childItemService.existsByIdOrThrowException(id);

    final List<J> list = new ArrayList<>();
    for (final String grandChildId : gdChildIds) {
      // Check if grand child exists
      this.grandChildItemService.existsByIdOrThrowException(grandChildId);
      if (!this.relationItemService.relationExists(parentid, id, grandChildId)) {
        J newJoinResource = this.relationItemService.saveRelation(parentid, id, grandChildId, this.relationItemService.getDefaultJoinResource());
        list.add(newJoinResource);
      }
    }
    if (!list.isEmpty()) {
      return new ResponseEntity<>(list, HttpStatus.CREATED);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Create relations between parent and a list of children, with default list of grandchildren and default relation properties
   *
   * @param parentid
   * @param ids
   * @return
   */
  @PostMapping
  public HttpEntity<List<J>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    // Check if parent exists
    this.itemService.existsByIdOrThrowException(parentid);

    List<String> defaultGrandChildIds = this.relationItemService.getDefaultGrandChildIds();
    if (defaultGrandChildIds == null || defaultGrandChildIds.isEmpty()) {
      throw new SolidifyRuntimeException("No default grandchildren defined for this relation");
    }

    final List<J> list = new ArrayList<>();
    for (final String id : ids) {
      // Check if child exists
      this.childItemService.existsByIdOrThrowException(id);
      for (final String grandChildId : defaultGrandChildIds) {
        // Check if grand child exists
        this.grandChildItemService.existsByIdOrThrowException(grandChildId);
        if (!this.relationItemService.relationExists(parentid, id, grandChildId)) {
          J newJoinResource = this.relationItemService
                  .saveRelation(parentid, id, grandChildId, this.relationItemService.getDefaultJoinResource());
          list.add(newJoinResource);
        }
      }
    }
    if (!list.isEmpty()) {
      return new ResponseEntity<>(list, HttpStatus.CREATED);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**
   * Delete all relations between the parent and all elements in the list of children
   * <p>
   * If list of children is not given, delete all relations to the parent.
   *
   * @param parentid
   * @param ids
   * @return
   */
  @DeleteMapping
  public ResponseEntity<Void> delete(@PathVariable String parentid, @RequestBody(required = false) String[] ids) {
    // Check if parent exists
    this.itemService.existsByIdOrThrowException(parentid);

    SolidifySpecification<V> childSpecification = this.relationItemService
            .getChildSpecification(parentid, this.relationItemService.getEmptyChildResourceObject());

    final List<V> childList = this.childItemService.findAll(childSpecification, Pageable.unpaged()).getContent();
    if (childList.isEmpty()) {
      throw new SolidifyHttpErrorException(HttpStatus.GONE, NOTHING_TO_DELETE);
    }

    final List<J> list = new ArrayList<>();

    for (final V childItem : childList) {
      if (this.arrayIsEmptyOrContains(ids, childItem.getResId())) {
        // find the list of corresponding grandchildren
        List<J> joinResources = this.relationItemService.findAllRelations(parentid, childItem.getResId());
        for (final J joinResource : joinResources) {
          this.relationItemService.deleteRelation(joinResource);
          list.add(joinResource);
        }
      }
    }

    if (!list.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      throw new SolidifyHttpErrorException(HttpStatus.GONE, NOTHING_TO_DELETE);
    }
  }

  /**
   * Delete all relations between the parent, the child and all elements in the list of grandchildren
   * <p>
   * If list of grandchildren is not given, delete all relations to the parent and the child.
   *
   * @param parentid
   * @param id
   * @param gdChildIds
   * @return
   */
  @DeleteMapping(SolidifyConstants.URL_ID)
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id,
          @RequestBody(required = false) String[] gdChildIds) {
    // Check if parents exist
    this.itemService.existsByIdOrThrowException(parentid);
    this.childItemService.existsByIdOrThrowException(id);

    final List<J> list = new ArrayList<>();

    List<J> joinResources = new ArrayList<>();

    if (gdChildIds == null || gdChildIds.length == 0) {
      // find the list of corresponding grandchildren
      joinResources = this.relationItemService.findAllRelations(parentid, id);
    } else {
      for (String grandChildId : gdChildIds) {
        J joinResource = this.relationItemService.findRelation(parentid, id, grandChildId);
        if (joinResource != null) {
          joinResources.add(joinResource);
        }
      }
    }

    for (final J joinResource : joinResources) {
      this.relationItemService.deleteRelation(joinResource);
      list.add(joinResource);
    }

    if (!list.isEmpty()) {
      return new ResponseEntity<>(HttpStatus.OK);
    } else {
      throw new SolidifyHttpErrorException(HttpStatus.GONE, NOTHING_TO_DELETE);
    }
  }

  /**
   * Update the relation between the parent, the child and the grandchild with given relation properties
   *
   * @param parentid
   * @param id
   * @param grandChildId
   * @param joinResource
   * @return
   */
  @PatchMapping(SolidifyConstants.URL_ID + SolidifyConstants.URL_GRAND_CHILD_ID)
  public HttpEntity<J> update(@PathVariable String parentid, @PathVariable String id, @PathVariable String grandChildId,
          @RequestBody Map<String, Object> joinResource) {
    J existingJoinResource = this.relationItemService.findRelation(parentid, id, grandChildId);
    if (existingJoinResource != null) {
      this.basicEntityMapper.patchResource(existingJoinResource, joinResource);
      final J savedRelation = this.relationItemService.saveRelation(existingJoinResource);
      return new ResponseEntity<>(savedRelation, HttpStatus.OK);
    } else {
      throw new SolidifyResourceNotFoundException("This relation does not exist");
    }
  }

  public HttpEntity<HttpStatus> setGrandChildList(@PathVariable String parentid, @PathVariable String id, @RequestBody String[] gdChildIds) {
    try {
      this.delete(parentid, id, null);
    } catch (SolidifyHttpErrorException e) {
      // No-op
    }
    this.create(parentid, id, gdChildIds);
    return new HttpEntity<>(HttpStatus.OK);
  }

  /**
   * Update all relations between the parent and the child, with given relation properties
   *
   * @param parentid
   * @param id
   * @param joinResource
   * @return
   */
  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<List<J>> update(@PathVariable String parentid, @PathVariable String id, @RequestBody J joinResource) {
    // Check if parents exist
    this.itemService.existsByIdOrThrowException(parentid);
    this.childItemService.existsByIdOrThrowException(id);

    final List<J> savedRelationsList = new ArrayList<>();
    for (final J existingJoinResource : this.relationItemService.findAllRelations(parentid, id)) {
      final String grandChildId = this.relationItemService.getGrandChildResource(existingJoinResource).getResId();
      final J updatedJoinResource = this.relationItemService.completeJoinResourceRelations(joinResource, parentid, id, grandChildId);
      final J savedRelation = this.relationItemService.saveRelation(updatedJoinResource);
      savedRelationsList.add(savedRelation);
    }
    if (!savedRelationsList.isEmpty()) {
      return new ResponseEntity<>(savedRelationsList, HttpStatus.CREATED);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  /**********************/

  private boolean arrayIsEmptyOrContains(String[] ids, String resId) {
    if (ids == null || ids.length == 0) {
      return true;
    }
    return Arrays.asList(ids).contains(resId);
  }

  /**
   * select grandchildren linked to parent and child and return the child and the list in a DTO
   *
   * @param parentId
   * @param childItem
   * @return
   */
  private Relation3TiersChildDTO getChildDTOCompletedWithGrandChildren(String parentId, V childItem) {
    List<J> relations = this.relationItemService.findAllRelations(parentId, childItem.getResId());
    return this.relationItemService.getChildDTO(childItem, relations);
  }

  protected void addChildLinks(String parentid, V v) {
    v.removeLinks();
    v.addLinks(linkTo(methodOn(this.getClass()).list(parentid, null, null)), false, false);
  }

  protected void addGrandChildLinks(String parentid, String id, W w) {
    w.removeLinks();
    w.addLinks(linkTo(methodOn(this.getClass()).get(parentid, id)), false, true);
  }

}
