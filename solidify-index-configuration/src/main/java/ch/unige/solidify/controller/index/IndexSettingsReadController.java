/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Management - IndexSettingsReadController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller.index;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.unige.solidify.IndexUrlPath;
import ch.unige.solidify.controller.IndexConfigurationController;
import ch.unige.solidify.controller.NoSqlResourceReadOnlyController;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexProperties;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.TrustedUserPermissions;

@TrustedUserPermissions
@RestController
@ConditionalOnBean(IndexConfigurationController.class)
@RequestMapping(IndexUrlPath.INDEX_INDEX)
public class IndexSettingsReadController extends NoSqlResourceReadOnlyController<IndexProperties> {

  public IndexSettingsReadController(IndexingSettingsService<IndexProperties> noSqlResourceService) {
    super(noSqlResourceService);
  }

  @Override
  public HttpEntity<IndexProperties> get(@PathVariable String id) {
    return super.get(id);
  }

  @Override
  public HttpEntity<RestCollection<IndexProperties>> list(@ModelAttribute IndexProperties search, Pageable pageable) {
    return super.list(search, pageable);
  }

}
