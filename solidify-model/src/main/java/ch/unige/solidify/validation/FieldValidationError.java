/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - FieldValidationError.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.validation;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class FieldValidationError {

  private List<String> errorMessages = new ArrayList<>();
  private String fieldName;
  private String code;

  public FieldValidationError() {

  }

  public FieldValidationError(String fieldName, String errorMessage) {
    this.fieldName = fieldName;
    this.errorMessages.add(errorMessage);
  }

  public FieldValidationError(String fieldName, String errorMessage, String code) {
    this.fieldName = fieldName;
    this.errorMessages.add(errorMessage);
    this.code = code;
  }

  public void addErrorMessage(String errorMessage) {
    this.errorMessages.add(errorMessage);
  }

  public void addErrorMessage(String errorMessage, String code) {
    this.errorMessages.add(errorMessage);
    this.code = code;
  }

  public List<String> getErrorMessages() {
    return this.errorMessages;
  }

  public String getFieldName() {
    return this.fieldName;
  }

  public void setErrorMessages(List<String> errorMessages) {
    this.errorMessages = errorMessages;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

}
