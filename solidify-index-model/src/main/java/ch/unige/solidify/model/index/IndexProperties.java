/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - IndexProperties.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.index;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.util.JSONTool;

@Schema(description = "An IndexProperties defines the settings and the mapping of the index.")
@JsonDeserialize(using = IndexPropertiesDeserializer.class)
public class IndexProperties extends NoSqlResource {

  private Map<String, Object> settings = new HashMap<>();

  private Map<String, Object> mapping = new HashMap<>();

  @Override
  public boolean equals(final Object other) {
    if (!(other instanceof IndexProperties castOther)) {
      return false;
    }
    return Objects.equals(this.settings, castOther.settings);
  }

  public Map<String, Object> getSettings() {
    return this.settings;
  }

  public Map<String, Object> getMapping() {
    return this.mapping;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.settings);
  }

  public void setSettings(Map<String, Object> settings) {
    this.settings = settings;
  }

  public void setSettings(String settings) {
    this.settings = JSONTool.convertJson2Map(settings);
  }

  public void setMapping(Map<String, Object> mapping) {
    this.mapping = mapping;
  }

  public void setMapping(String mapping) {
    this.mapping = JSONTool.convertJson2Map(mapping);
  }

  @Override
  public boolean update(NoSqlResource item) {
    return false;
  }
}
