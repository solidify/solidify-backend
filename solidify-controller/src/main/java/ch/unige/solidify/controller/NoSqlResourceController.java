/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - NoSqlResourceController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.rest.NoSqlResource;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.service.NoSqlResourceService;

@RootPermissions
public abstract class NoSqlResourceController<T extends NoSqlResource>
        extends NoSqlResourceReadOnlyController<T> {

  protected NoSqlResourceController(NoSqlResourceService<T> noSqlResourceService) {
    super(noSqlResourceService);
  }

  @PostMapping
  public HttpEntity<T> create(@RequestBody T t) {
    final T item = this.noSqlResourceService.save(t);
    return new ResponseEntity<>(item, HttpStatus.CREATED);
  }

  @DeleteMapping(SolidifyConstants.URL_ID)
  public ResponseEntity<Void> delete(@PathVariable String id) {
    final T t = this.noSqlResourceService.findOne(id);
    if (t == null) {
      throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND, "Item " + id + " not found");
    }
    if (!this.noSqlResourceService.delete(t)) {
      throw new SolidifyHttpErrorException(HttpStatus.GONE, "Resource " + id + " gone");
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @DeleteMapping
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    // Remove each item
    for (final String id : ids) {
      final T t = this.noSqlResourceService.findOne(id);
      if (t == null) {
        throw new SolidifyHttpErrorException(HttpStatus.NOT_FOUND, "Parent item " + id + " not found");
      }
      this.noSqlResourceService.delete(t);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<T> update(@PathVariable String id, @RequestBody T t2) {
    final T t1 = this.noSqlResourceService.findOne(id);
    if (t1 == null) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    if (t1.update(t2)) {
      final T item = this.noSqlResourceService.update(t2);
      return new ResponseEntity<>(item, HttpStatus.OK);
    }
    return new ResponseEntity<>(t1, HttpStatus.OK);
  }

}
