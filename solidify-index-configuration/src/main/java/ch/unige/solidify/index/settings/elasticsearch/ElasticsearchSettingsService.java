/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Management - ElasticsearchSettingsService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.index.settings.elasticsearch;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.AcknowledgedResponse;
import co.elastic.clients.elasticsearch._types.ElasticsearchException;
import co.elastic.clients.elasticsearch._types.mapping.TypeMapping;
import co.elastic.clients.elasticsearch.indices.AliasDefinition;
import co.elastic.clients.elasticsearch.indices.CreateIndexRequest;
import co.elastic.clients.elasticsearch.indices.CreateIndexResponse;
import co.elastic.clients.elasticsearch.indices.DeleteIndexRequest;
import co.elastic.clients.elasticsearch.indices.GetAliasRequest;
import co.elastic.clients.elasticsearch.indices.GetAliasResponse;
import co.elastic.clients.elasticsearch.indices.GetIndexRequest;
import co.elastic.clients.elasticsearch.indices.GetIndexResponse;
import co.elastic.clients.elasticsearch.indices.GetIndicesSettingsRequest;
import co.elastic.clients.elasticsearch.indices.GetIndicesSettingsResponse;
import co.elastic.clients.elasticsearch.indices.GetMappingRequest;
import co.elastic.clients.elasticsearch.indices.GetMappingResponse;
import co.elastic.clients.elasticsearch.indices.IndexSettings;
import co.elastic.clients.elasticsearch.indices.IndexState;
import co.elastic.clients.elasticsearch.indices.UpdateAliasesRequest;
import co.elastic.clients.elasticsearch.indices.UpdateAliasesResponse;
import co.elastic.clients.elasticsearch.indices.get_alias.IndexAliases;
import co.elastic.clients.elasticsearch.indices.get_mapping.IndexMappingRecord;
import co.elastic.clients.json.JsonpSerializable;
import co.elastic.clients.json.JsonpUtils;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.controller.IndexConfigurationController;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.index.ElasticsearchClientProvider;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexDefinition;
import ch.unige.solidify.model.index.IndexDefinitionList;
import ch.unige.solidify.model.index.IndexProperties;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.JSONTool;
import ch.unige.solidify.util.StringTool;

@Profile("index-elasticsearch")
@Service
@ConditionalOnBean(IndexConfigurationController.class)
public class ElasticsearchSettingsService extends IndexingSettingsService<IndexProperties> {
  private static final Logger log = LoggerFactory.getLogger(ElasticsearchSettingsService.class);

  private static final String MAPPING_HOME = "mapping";

  private static final String INDEX_CREATION_SUFFIX = "-0001";

  private final ElasticsearchClient elasticsearchClient;
  private final long fieldsLimit;
  private String indexPrefix;
  private final int replicaNumber;
  private final int shardNumber;

  public ElasticsearchSettingsService(
          IndexConfigProperties indexConfigProperties,
          ElasticsearchClientProvider elasticsearchClientProvider) {
    this.indexPrefix = indexConfigProperties.getIndexPrefix();
    this.elasticsearchClient = elasticsearchClientProvider.getElasticsearchClient();
    this.shardNumber = indexConfigProperties.getConfig().getShardNumber();
    this.replicaNumber = indexConfigProperties.getConfig().getReplicaNumber();
    this.fieldsLimit = indexConfigProperties.getConfig().getFieldsLimit();
  }

  @Override
  public boolean delete(IndexProperties ai) {
    if (!this.checkIndexName(ai.getResId())) {
      return false;
    }
    final DeleteIndexRequest.Builder builder = new DeleteIndexRequest.Builder()
            .index(ai.getResId());
    final AcknowledgedResponse response;
    try {
      response = this.elasticsearchClient.indices().delete(builder.build());
      return (response.acknowledged());
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Error while deleting index " + ai.getResId(), e);
    }
  }

  @Override
  public List<IndexProperties> findAll(IndexProperties search) {
    final List<IndexProperties> list = new ArrayList<>();
    final GetIndicesSettingsRequest.Builder builder = new GetIndicesSettingsRequest.Builder();
    try {
      final GetIndicesSettingsResponse response = this.elasticsearchClient.indices().getSettings(builder.build());
      for (final Map.Entry<String, IndexState> entry : response.result().entrySet()) {
        if (this.checkIndexName(entry.getKey())) {
          final IndexProperties indexProperties = new IndexProperties();
          indexProperties.setResId(entry.getKey());
          indexProperties.setSettings(this.toJson(entry.getValue().settings()));

          // Complete with mapping
          final GetMappingRequest.Builder mappingBuilder = new GetMappingRequest.Builder();
          mappingBuilder.index(entry.getKey());
          final GetMappingResponse mappingResponse = this.elasticsearchClient.indices().getMapping(mappingBuilder.build());
          IndexMappingRecord mappingRecord = mappingResponse.get(entry.getKey());
          indexProperties.setMapping(this.toJson(mappingRecord.mappings()));

          list.add(indexProperties);
        }
      }
      return list;
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Error while running search " + search + " on index " + this.indexPrefix, e);
    }
  }

  private String toJson(JsonpSerializable serializable) {
    StringBuilder sb = new StringBuilder();
    int maxLength = JsonpUtils.maxToStringLength();
    JsonpUtils.maxToStringLength(Integer.MAX_VALUE);
    JsonpUtils.toString(serializable, sb);
    JsonpUtils.maxToStringLength(maxLength); // reset value
    return sb.toString();
  }

  @Override
  public IndexProperties findOne(String indexName) {
    if (!this.checkIndexName(indexName)) {
      log.error("Wrong index name ({})", indexName);
      return null;
    }

    try {
      GetIndexRequest.Builder builder = new GetIndexRequest.Builder().index(indexName);
      GetIndexResponse response = this.elasticsearchClient.indices().get(builder.build());

      if (response.get(indexName) != null) {
        final IndexProperties indexProperties = new IndexProperties();
        indexProperties.setResId(indexName);
        indexProperties.setSettings(this.toJson(response.get(indexName).settings()));
        indexProperties.setMapping(this.toJson(response.get(indexName).mappings()));
        return indexProperties;
      } else {
        log.warn("Index '{}' does not exist", indexName);
        return null;
      }
    } catch (ElasticsearchException e) {
      if (HttpStatus.NOT_FOUND.value() == e.status()) {
        log.warn("Index '{}' does not exist", indexName);
        return null;
      }
      throw e;
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Error while searching index '" + indexName + "'", e);
    }
  }

  @Override
  public void init(IndexDefinitionList indexList) {
    for (IndexDefinition indexDefinition : indexList.getIndexList()) {
      if (indexDefinition.isUseAlias()) {
        this.createIndexAndAliasIfNotExist(indexDefinition);
      } else {
        this.createIndexIfNotExist(indexDefinition);
      }
    }
  }

  @Override
  public IndexProperties update(IndexProperties t) {
    throw new UnsupportedOperationException("Index Setting update");
  }

  private boolean checkIndexName(String indexName) {
    return indexName.startsWith(this.indexPrefix);
  }

  @Override
  public IndexProperties save(IndexProperties indexSettings) {
    String indexName = indexSettings.getResId();
    if (!this.checkIndexName(indexName)) {
      throw new SolidifyRuntimeException("Wrong index name (" + indexName + ")");
    }

    TypeMapping.Builder mappingBuilder = new TypeMapping.Builder()
            .withJson(new StringReader(JSONTool.convert2Json(indexSettings.getMapping()).toString()));

    IndexSettings.Builder indexSettingsBuilder = new IndexSettings.Builder()
            .withJson(new StringReader(JSONTool.convert2Json(indexSettings.getSettings()).toString()));
    indexSettingsBuilder.numberOfShards(String.valueOf(this.shardNumber));
    indexSettingsBuilder.numberOfReplicas(String.valueOf(this.replicaNumber));
    indexSettingsBuilder.mapping(limitBuilder -> limitBuilder.totalFields(totalFieldBuilder -> totalFieldBuilder.limit(this.fieldsLimit)));

    CreateIndexRequest.Builder requestBuilder = new CreateIndexRequest.Builder()
            .index(indexName)
            .mappings(mappingBuilder.build())
            .settings(indexSettingsBuilder.build());

    try {
      CreateIndexResponse response = this.elasticsearchClient.indices().create(requestBuilder.build());
      if (response.acknowledged()) {
        log.info("Index '{}' created", indexName);
        return indexSettings;
      } else {
        throw new SolidifyRuntimeException("Cannot create index '" + indexName + "' (response is not acknowledged)");
      }
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Cannot create index '" + indexName + "'", e);
    }
  }

  private IndexProperties createIndexIfNotExist(IndexDefinition indexDefinition) {
    String indexName = indexDefinition.getIndexName();
    IndexProperties existingIndexSettings = this.findOne(indexName);
    if (existingIndexSettings == null) {

      try {
        final IndexProperties indexSettings = new IndexProperties();
        indexSettings.setResId(indexName);

        // Load mapping config from file
        if (!StringTool.isNullOrEmpty(indexDefinition.getIndexMapping())) {
          String mappingJson = this.getMappingFileContent(indexDefinition);
          indexSettings.setMapping(mappingJson);
        }

        // Load settings from config file
        if (!StringTool.isNullOrEmpty(indexDefinition.getIndexSettings())) {
          String settingsJson = this.getSettingFileContent(indexDefinition);
          indexSettings.setSettings(settingsJson);
        }

        return this.save(indexSettings);
      } catch (IOException e) {
        final String message = "Cannot create index '" + indexName + "' from IndexDefinition";
        throw new SolidifyRuntimeException(message, e);
      }
    } else {
      log.info("Index '{}' already exists", indexName);
      return existingIndexSettings;
    }
  }

  private IndexProperties createIndexAndAliasIfNotExist(IndexDefinition indexDefinition) {
    String aliasName = indexDefinition.getIndexName();

    List<String> indexNames = this.findIndexNamesByAlias(aliasName);
    if (indexNames.isEmpty()) {
      // Alias for the index doesn't exist yet, so we consider that the corresponding index doesn't exist either.

      // Create the index behind the alias
      String indexName = aliasName + INDEX_CREATION_SUFFIX;
      IndexProperties newIndexProperties = this.createIndexIfNotExist(
              new IndexDefinition(indexName, indexDefinition.getIndexMapping(), indexDefinition.getIndexSettings()));

      // Add the alias on the newly created index
      this.createAlias(aliasName, indexName);

      return newIndexProperties;
    } else {
      log.info("Index alias '{}' already exists", aliasName);
      return this.findOne(indexNames.get(0));
    }
  }

  public List<String> findIndexNamesByAlias(String aliasName) {
    List<String> indexNames = new ArrayList<>();
    try {
      GetAliasRequest.Builder builder = new GetAliasRequest.Builder().index(aliasName);
      GetAliasResponse response = this.elasticsearchClient.indices().getAlias(builder.build());

      if (response.result().isEmpty()) {
        log.info("Index alias '{}' does not exist", aliasName);
      } else {
        for (Map.Entry<String, IndexAliases> aliasEntry : response.result().entrySet()) {
          String indexName = aliasEntry.getKey();
          IndexAliases indexAliases = aliasEntry.getValue();
          for (Map.Entry<String, AliasDefinition> aliasDefinitionEntry : indexAliases.aliases().entrySet()) {
            String foundAliasName = aliasDefinitionEntry.getKey();
            AliasDefinition aliasDefinition = aliasDefinitionEntry.getValue();
            if (foundAliasName.equals(aliasName)) {
              if (!Boolean.TRUE.equals(aliasDefinition.isWriteIndex())) {
                throw new SolidifyRuntimeException("Alias '" + aliasName + "' for index '" + indexName + "' is not writable");
              }
              indexNames.add(indexName);
            }
          }
        }
      }
    } catch (ElasticsearchException e) {
      if (HttpStatus.NOT_FOUND.value() == e.status()) {
        log.warn("Index alias '{}' does not exist", aliasName);
      } else {
        throw e;
      }
    } catch (IOException e) {
      throw new SolidifyRuntimeException("Error while searching index '" + aliasName + "'", e);
    }
    return indexNames;
  }

  public void createAlias(String aliasName, String indexName) {
    try {
      UpdateAliasesRequest.Builder updateBuilder = new UpdateAliasesRequest.Builder();
      updateBuilder.actions(actions -> actions
              .add(add -> add
                      .index(indexName)
                      .alias(aliasName)
                      .isWriteIndex(true)
              )
      );

      UpdateAliasesResponse response = this.elasticsearchClient.indices().updateAliases(updateBuilder.build());
      if (response.acknowledged()) {
        log.info("Index alias '{}' for index {} created", aliasName, indexName);
      } else {
        throw new SolidifyRuntimeException(
                "Index alias '" + aliasName + "' for index '" + indexName + "' could not be created (response is not acknowledged)");
      }
    } catch (IOException | ElasticsearchException e) {
      throw new SolidifyRuntimeException("Index alias '" + aliasName + "' for index '" + indexName + "' could not be created", e);
    }
  }

  private String getMappingFileContent(IndexDefinition indexDefinition) throws IOException {
    final ClassPathResource mappingResource = new ClassPathResource(MAPPING_HOME + "/" + indexDefinition.getIndexMapping());
    return FileTool.toString(mappingResource.getInputStream());
  }

  private String getSettingFileContent(IndexDefinition indexDefinition) throws IOException {
    final ClassPathResource mappingResource = new ClassPathResource(MAPPING_HOME + "/" + indexDefinition.getIndexSettings());
    return FileTool.toString(mappingResource.getInputStream());
  }
}
