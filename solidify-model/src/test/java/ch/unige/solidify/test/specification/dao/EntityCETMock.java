/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - EntityCETMock.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.specification.dao;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import ch.unige.solidify.rest.ResourceCET;

@Entity
@Table(name = "EntityCETMock")
public class EntityCETMock extends ResourceCET {

  @Id
  @Column(name = "id")
  private String entityId;

  private String label;

  @Override
  public String getEntityId() {
    return this.entityId;
  }

  public String getLabel() {
    return this.label;
  }

  @Override
  public void init() {

  }

  @Override
  public String managedBy() {
    return null;
  }

  public void setEntityId(String entityId) {
    this.entityId = entityId;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return "EntityCETMock{" +
            "entityId='" + this.entityId + '\'' +
            ", label='" + this.label + '\'' +
            ", createdby='" + super.getCreatedBy() + '\'' +
            ", createdAt='" + super.getCreatedAt() + '\'' +
            ", updatedby='" + super.getUpdatedBy() + '\'' +
            ", upatedAt='" + super.getUpdatedAt() + '\'' +
            ", creation='" + super.getCreation() + '\'' +
            ", upate='" + super.getLastUpdate() + '\'' +
            '}';
  }
}
