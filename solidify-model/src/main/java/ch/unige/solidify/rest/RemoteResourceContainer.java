/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - RemoteResourceContainer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Interface used by Resources containing other Resources that must be fetched by using REST requests.
 */
public interface RemoteResourceContainer {

  <T> boolean addItem(T t);

  /**
   * Return a list of classes that are embedded sub Resources
   *
   * @return
   */
  @JsonIgnore
  default List<Class<? extends Resource>> getEmbeddedResourceTypes() {
    return new ArrayList<>();
  }

  @JsonIgnore
  default List<String> getSubResourceIds() {
    final List<String> subResourceIds = new ArrayList<>();
    for (Class<? extends Resource> resourceType : this.getEmbeddedResourceTypes()) {
      subResourceIds.addAll(this.getSubResourceIds(resourceType));
    }
    return subResourceIds;
  }

  default <T> List<String> getSubResourceIds(Class<T> type) {
    return Collections.emptyList();
  }

  default <T> Page<String> getSubResourceIds(Class<T> type, Pageable pageable) {
    List<String> completeList = this.getSubResourceIds(type);
    long start = pageable.getOffset();
    long end = (start + pageable.getPageSize()) > completeList.size() ? completeList.size() : (start + pageable.getPageSize());
    if (start > end) {
      return new PageImpl<>(Collections.emptyList(), pageable, 0);
    }
    return new PageImpl<>(completeList.subList((int) start, (int) end), pageable, completeList.size());
  }

  <T> boolean removeItem(T t);

}
