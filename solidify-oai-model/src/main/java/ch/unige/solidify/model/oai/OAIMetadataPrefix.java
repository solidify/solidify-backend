/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIMetadataPrefix.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.oai;

import java.net.URL;
import java.util.Objects;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.ResourceNormalized;

@Schema(description = "The OAI metadata prefixes defined the metadata formats supported by OAI-PMH provider. The default metadata prefix is oai_dc, derived from Dublin Core.")
@Entity
@Table(name = "oaiMetadataPrefix")
public class OAIMetadataPrefix extends ResourceNormalized {

  private static final int DB_METADATA_LENGTH = 102400;

  @Schema(description = "The name of the OAI metadata prefix.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  @Column(unique = true)
  private String prefix;

  @Schema(description = "The XML schema URL of the OAI metadata prefix.")
  @NotNull
  private URL schemaUrl;

  @Schema(description = "The XML schema namespace of the OAI metadata prefix.")
  @NotNull
  private String schemaNamespace;

  @Schema(description = "The name of the OAI metadata prefix.")
  @NotNull
  @Size(min = 1, max = SolidifyConstants.DB_DEFAULT_STRING_LENGTH)
  @Column(unique = true)
  private String name;

  @Schema(description = "The description of the OAI metadata prefix.")
  @Size(min = 1, max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String description;

  @Schema(description = "If the OAI metadata prefix is enable.")
  @NotNull
  private Boolean enabled;

  @Schema(description = "If the OAI metadata prefix is the reference prefix.")
  @NotNull
  private Boolean reference;

  @Schema(description = "The XML class of the OAI metadata prefix..")
  private String xmlClass;

  @Schema(description = "The XML schema content of the OAI metadata prefix.")
  @Size(max = DB_METADATA_LENGTH) // Set column size (100KB) for H2 & documentation
  private String metadataSchema;

  @Schema(description = "The XML transformation from reference prefix to the OAI metadata prefix.")
  @Size(max = DB_METADATA_LENGTH) // Set column size (100KB) for H2 & documentation
  private String metadataXmlTransformation;

  public String getDescription() {
    return this.description;
  }

  public String getName() {
    return this.name;
  }

  public String getPrefix() {
    return this.prefix;
  }

  public URL getSchemaUrl() {
    return this.schemaUrl;
  }

  public String getSchemaNamespace() {
    return this.schemaNamespace;
  }

  public Boolean getEnabled() {
    return this.enabled;
  }

  public Boolean getReference() {
    return this.reference;
  }

  public String getMetadataSchema() {
    return this.metadataSchema;
  }

  public String getMetadataXmlTransformation() {
    return this.metadataXmlTransformation;
  }

  public String getXmlClass() {
    return this.xmlClass;
  }

  public void setXmlClass(String xmlClass) {
    this.xmlClass = xmlClass;
  }

  @Override
  @PrePersist
  @PreUpdate
  public void init() {
    if (this.enabled == null) {
      this.enabled = false;
    }
    if (this.reference == null) {
      this.reference = false;
    }
  }

  public boolean isEnabled() {
    return Objects.requireNonNullElse(this.enabled, Boolean.FALSE);
  }

  public boolean isReference() {
    return Objects.requireNonNullElse(this.reference, Boolean.FALSE);
  }

  @Override
  public String managedBy() {
    return OAIConstants.OAI_MODULE;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public void setReference(Boolean reference) {
    this.reference = reference;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public void setMetadataSchema(String metadataSchema) {
    this.metadataSchema = metadataSchema;
  }

  public void setMetadataXmlTransformation(String metadataXmlTransformation) {
    this.metadataXmlTransformation = metadataXmlTransformation;
  }

  public void setSchemaUrl(URL schemaUrl) {
    this.schemaUrl = schemaUrl;
  }

  public void setSchemaNamespace(String schemaNamespace) {
    this.schemaNamespace = schemaNamespace;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hash(this.description, this.enabled, this.metadataSchema, this.metadataXmlTransformation, this.name,
            this.prefix, this.reference,
            this.schemaNamespace, this.schemaUrl, this.xmlClass);
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (this.getClass() != obj.getClass())
      return false;
    OAIMetadataPrefix other = (OAIMetadataPrefix) obj;
    return Objects.equals(this.description, other.description) && Objects.equals(this.enabled, other.enabled)
            && Objects.equals(this.metadataSchema, other.metadataSchema)
            && Objects.equals(this.metadataXmlTransformation, other.metadataXmlTransformation)
            && Objects.equals(this.name, other.name) && Objects.equals(this.prefix, other.prefix)
            && Objects.equals(this.reference, other.reference)
            && Objects.equals(this.schemaNamespace, other.schemaNamespace) && Objects.equals(this.schemaUrl, other.schemaUrl)
            && Objects.equals(this.xmlClass, other.xmlClass);
  }

}
