/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Remote Search - IndexMetadataRemoteResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service.rest.abstractservice;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.service.SolidifyRestClientService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

public abstract class IndexMetadataRemoteResourceService<T extends IndexMetadata> {
  private static final Logger log = LoggerFactory.getLogger(IndexMetadataRemoteResourceService.class);

  private final String indexName;
  private final Class<T> indexResourceClass;
  protected final SolidifyRestClientService restClientService;

  protected IndexMetadataRemoteResourceService(
          Class<T> indexResourceClass,
          SolidifyRestClientService restClientService,
          String indexName) {
    this.indexResourceClass = indexResourceClass;
    this.restClientService = restClientService;
    this.indexName = indexName;
  }

  public String getIndexName() {
    return this.indexName;
  }

  public HttpStatusCode headIndexMetadata(String id) {
    String url = this.getIndexUrl() + "/" + id;
    return this.restClientService.headResource(url);
  }

  public T getIndexMetadata(String resId) {
    this.restClientService.checkResId(resId);
    String url = this.getIndexUrl() + "/" + resId;
    return this.restClientService.getResource(url, this.indexResourceClass);
  }

  public RestCollection<T> getIndexMetadataList(Map<String, Object> parameters, Pageable pageable) {
    String url = this.buildUrl(parameters);
    return this.getIndexMetadataListWithUrl(url, pageable);
  }

  public RestCollection<T> getIndexMetadataList(Pageable pageable) {
    return this.getIndexMetadataList(Collections.emptyMap(), pageable);
  }

  public RestCollection<T> getIndexMetadataList(String query, String field, String from, String to, String set, Pageable pageable) {
    HashMap<String, Object> parameters = this.createParameterMap(query, field, from, to, set);
    return this.getIndexMetadataList(parameters, pageable);
  }

  public RestCollection<T> getIndexMetadataList(String query, String set, Pageable pageable) {
    HashMap<String, Object> parameters = this.createParameterMap(query, null, null, null, set);
    return this.getIndexMetadataList(parameters, pageable);
  }

  public RestCollection<T> search(List<SearchCondition> searchConditions, Pageable pageable) {
    final String url = this.getIndexUrl() + "/" + ActionName.SEARCH;
    return this.restClientService.postResourcesList(url, searchConditions, this.indexResourceClass, pageable);
  }

  public boolean indexMetadata(List<T> metadataList) {
    boolean result = true;
    long count = 0;
    for (T metadata : metadataList) {
      log.debug("Creating index metadata document: {}) {} {}", ++count, metadata.getIndex(), metadata.getResId());
      result = result && this.indexMetadata(metadata);
    }
    return result;
  }

  public boolean indexMetadata(T metadata) {
    HttpStatusCode statusCode = this.headIndexMetadata(metadata.getResId());
    if (statusCode == HttpStatus.OK) {
      return this.updateIndexMetadata(metadata);
    } else {
      return this.createIndexMetadata(metadata);
    }
  }

  public boolean createIndexMetadata(T metadata) {
    try {
      IndexMetadata res = this.restClientService.postResource(this.getIndexUrl(), metadata, this.indexResourceClass);
      if (res == null) {
        return false;
      }

    } catch (SolidifyResourceNotFoundException | SolidifyRestException e) {
      log.error("Error in indexing (" + metadata.getResId() + ")", e);
      return false;
    }
    return true;
  }

  public boolean updateIndexMetadata(T metadata) {
    try {
      this.restClientService.patchResource(this.getIndexUrl() + "/" + metadata.getResId(), metadata, this.indexResourceClass);
    } catch (SolidifyResourceNotFoundException | SolidifyRestException e) {
      log.error("Error in updating index (" + metadata.getResId() + ")", e);
      return false;
    }
    return true;
  }

  public boolean deleteIndexMetadata(String id) {
    try {
      this.restClientService.deleteResource(this.getIndexUrl() + "/" + id);
    } catch (SolidifyResourceNotFoundException | SolidifyRestException e) {
      log.error("Error in deleting metadata index (" + id + ")", e);
      return false;
    }
    return true;
  }

  // Build index web service URL
  protected abstract String getIndexUrl();

  protected RestCollection<T> getIndexMetadataListWithUrl(String url, Pageable pageable) {
    try {
      String jsonString = this.restClientService.getResource(url, pageable);
      return new RestCollection<>(jsonString, this.indexResourceClass);
    } catch (SolidifyRestException e) {
      if (e.getCause() instanceof HttpClientErrorException.BadRequest) {
        throw new SolidifyValidationException(new ValidationError(IndexConstants.BAD_SEARCH_REQUEST));
      } else {
        throw e;
      }
    }

  }

  protected String buildUrl(Map<String, Object> parameters) {

    String url = this.getIndexUrl() + "/" + ActionName.SEARCH;
    UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(url);

    if (parameters != null) {
      this.completeUri(uriBuilder, parameters);
    }

    return uriBuilder.toUriString();
  }

  protected HashMap<String, Object> createParameterMap(String query, String field, String from, String to, String set) {
    HashMap<String, Object> parameters = new HashMap<>();
    if (!StringTool.isNullOrEmpty(query)) {
      parameters.put(IndexConstants.SEARCH_QUERY, query);
    }
    if (!StringTool.isNullOrEmpty(field)) {
      parameters.put(IndexConstants.SEARCH_FIELD, field);
    }
    if (!StringTool.isNullOrEmpty(from)) {
      parameters.put(IndexConstants.SEARCH_FROM, from);
    }
    if (!StringTool.isNullOrEmpty(to)) {
      parameters.put(IndexConstants.SEARCH_TO, to);
    }
    if (!StringTool.isNullOrEmpty(set)) {
      parameters.put(IndexConstants.SEARCH_SET, set);
    }
    return parameters;
  }

  protected void completeUri(UriComponentsBuilder uriBuilder, Map<String, Object> parameters) {
    if (!StringTool.isNullOrEmpty((String) parameters.get(IndexConstants.SEARCH_QUERY))) {
      uriBuilder.queryParam(IndexConstants.SEARCH_QUERY, parameters.get(IndexConstants.SEARCH_QUERY));
    }

    if (!StringTool.isNullOrEmpty((String) parameters.get(IndexConstants.SEARCH_FIELD))) {
      uriBuilder.queryParam(IndexConstants.SEARCH_FIELD, parameters.get(IndexConstants.SEARCH_FIELD));
    }

    if (!StringTool.isNullOrEmpty((String) parameters.get(IndexConstants.SEARCH_FROM))) {
      uriBuilder.queryParam(IndexConstants.SEARCH_FROM, parameters.get(IndexConstants.SEARCH_FROM));
    }

    if (!StringTool.isNullOrEmpty((String) parameters.get(IndexConstants.SEARCH_TO))) {
      uriBuilder.queryParam(IndexConstants.SEARCH_TO, parameters.get(IndexConstants.SEARCH_TO));
    }

    if (!StringTool.isNullOrEmpty((String) parameters.get(IndexConstants.SEARCH_SET))) {
      uriBuilder.queryParam(IndexConstants.SEARCH_SET, parameters.get(IndexConstants.SEARCH_SET));
    }
  }
}
