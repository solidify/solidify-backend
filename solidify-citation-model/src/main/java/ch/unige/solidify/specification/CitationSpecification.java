/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Citation Model - CitationSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import java.io.Serial;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.model.Citation;
import ch.unige.solidify.util.StringTool;

public class CitationSpecification extends SolidifySpecification<Citation> {
  @Serial
  private static final long serialVersionUID = 2597608881483815709L;

  public CitationSpecification(Citation criteria) {
    super(criteria);
  }

  @Override
  protected void completePredicatesList(Root<Citation> root, CriteriaQuery<?> query, CriteriaBuilder builder,
          List<Predicate> predicatesList) {
    if (this.criteria.getItemId() != null) {
      predicatesList.add(builder.equal(root.get("itemId"), this.criteria.getItemId()));
    }
    if (this.criteria.getMode() != null) {
      predicatesList.add(builder.equal(root.get("mode"), this.criteria.getMode()));
    }
    if (this.criteria.getLanguage() != null) {
      predicatesList.add(builder.equal(root.get("language"), this.criteria.getLanguage()));
    }
    if (this.criteria.getStyle() != null) {
      predicatesList.add(builder.equal(root.get("style"), this.criteria.getStyle()));
    }
    if (this.criteria.getOutputFormat() != null) {
      predicatesList.add(builder.equal(root.get("outputFormat"), this.criteria.getOutputFormat()));
    }
    if (this.criteria.getText() != null) {
      predicatesList.add(builder.like(root.get("text"), "%" + this.criteria.getText() + "%"));
    }
    if (!StringTool.isNullOrEmpty(this.criteria.getCreatedBy())) {
      predicatesList.add(builder.equal(root.get("creation").get("who"), this.criteria.getCreatedBy()));
    }
  }

}
