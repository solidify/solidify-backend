/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ApplicationRoleSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import ch.unige.solidify.model.SolidifyApplicationRole;

public class ApplicationRoleSpecification implements Specification<SolidifyApplicationRole> {
  @Serial
  private static final long serialVersionUID = -1235772724215350229L;

  private final transient SolidifyApplicationRole criteria;

  public ApplicationRoleSpecification(SolidifyApplicationRole criteria) {
    this.criteria = criteria;
  }

  @Override
  public Predicate toPredicate(Root<SolidifyApplicationRole> root, CriteriaQuery<?> query,
          CriteriaBuilder builder) {

    final List<Predicate> listPredicate = new ArrayList<>();

    if (this.criteria.getName() != null) {
      listPredicate.add(builder.like(root.get("name"), "%" + this.criteria.getName() + "%"));
    }

    return builder.and(listPredicate.toArray(new Predicate[listPredicate.size()]));
  }

}
