/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify BagIt - BagItTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import gov.loc.repository.bagit.domain.Bag;

import ch.unige.solidify.exception.SolidifyBagItCheckingException;
import ch.unige.solidify.util.BagItTool;
import ch.unige.solidify.util.FileTool;

@ExtendWith(SpringExtension.class)
class BagItTest {

  private final static String BAGIT_FOLDER = "bagit";
  private final static String BAG_FOLDER = "bag";
  private final static String BAGIT_PROFILE = "BagIt-Profile-Test.json";

  @AfterEach
  public void purgeData() throws IOException {
    FileTool.deleteFolder(Paths.get(new ClassPathResource(BAGIT_FOLDER).getURI()).resolve(BAG_FOLDER));
  }

  @Test
  void createTest() throws IOException {
    Path bagFolder = this.createBag();
    Bag bag = BagItTool.create(bagFolder, null);
    this.verifyBag(bag, null);
  }

  @Test
  void createBagOnBagTest() throws IOException {
    Path bagFolder = this.createBag();
    Bag bag = BagItTool.create(bagFolder, null);
    this.verifyBag(bag, null);
    Bag newBag = BagItTool.create(bagFolder.resolve("data"), null);
    this.verifyBag(newBag, null);
  }

  @Test
  void createBagOnBagByRenamingDataFolderTest() throws IOException {
    Path bagFolder = this.createBag();
    Bag bag = BagItTool.create(bagFolder, null);
    this.verifyBag(bag, null);
    FileTool.renameFile(bagFolder.resolve("data"), bagFolder.resolve("newData"), false);
    Bag newBag = BagItTool.create(bagFolder.resolve("newData"), null);
    this.verifyBag(newBag, null);
  }

  @Test
  void createWithMetadataTest() throws IOException {
    Path bagFolder = this.createBag();
    Bag bag = BagItTool.create(bagFolder, this.getMetadata());
    this.verifyBag(bag, null);
  }

  @Test
  void readTest() throws IOException {
    Path bagFolder = this.createBag();
    BagItTool.create(bagFolder, this.getMetadata());
    Bag bag = BagItTool.read(bagFolder.toString());
    this.verifyBag(bag, null);
  }

  @Test
  void checkTest() throws IOException {
    assertDoesNotThrow(() -> this.check(null, null));
  }

  @Test
  void checkWithMetadataTest() throws IOException {
    assertDoesNotThrow(this::getMetadata);
  }

  @Test
  void checkWithBagItProfileTest() {
    assertThrows(SolidifyBagItCheckingException.class, () -> this.check(BAGIT_PROFILE, null));
  }

  @Test
  void checkWithBagItProfileAndMetadataTest() throws IOException {
    assertDoesNotThrow(() -> this.check(BAGIT_PROFILE, this.getMetadata()));
  }

  @Test
  void checkWithBagItProfileAndWrongMetadataTest() {
    final MultiValueMap<String, String> wrongMetadata = this.getWrongMetadata();
    assertThrows(SolidifyBagItCheckingException.class, () -> this.check(BAGIT_PROFILE, wrongMetadata));
  }

  @Test
  void quickCheckTest() throws IOException {
    assertDoesNotThrow(() -> this.quickCheck(null, null));
  }

  @Test
  void quickCheckWithMetadataTest() throws IOException {
    assertDoesNotThrow(() -> this.quickCheck(null, this.getMetadata()));
  }

  @Test
  void quickCheckWithBagItProfileTest() {
    assertThrows(SolidifyBagItCheckingException.class, () -> this.quickCheck(BAGIT_PROFILE, null));
  }

  @Test
  void quickCheckWithBagItProfileAndMetadataTest() {
    assertDoesNotThrow(() -> this.quickCheck(BAGIT_PROFILE, this.getMetadata()));
  }

  @Test
  void quickCheckWithBagItProfileAndWrongMetadataTest() {
    final MultiValueMap<String, String> wrongMetadata = this.getWrongMetadata();
    assertThrows(SolidifyBagItCheckingException.class, () -> this.quickCheck(BAGIT_PROFILE, wrongMetadata));
  }

  private void check(String bagItProfile, MultiValueMap<String, String> metadata) throws IOException {
    Path bagFolder = this.createBag();
    Bag bag = BagItTool.create(bagFolder, metadata);
    BagItTool.check(bagItProfile, bag);
  }

  private void quickCheck(String bagItProfile, MultiValueMap<String, String> metadata) throws IOException {
    Path bagFolder = this.createBag();
    Bag bag = BagItTool.create(bagFolder, metadata);
    BagItTool.quickCheck(bagItProfile, bag);
  }

  private void verifyBag(Bag bag, String bagItProfile) {
    assertEquals("1.0", bag.getVersion().toString());
    assertEquals(2, BagItTool.getBagFileNumber(bag));
    assertNotEquals(0, FileTool.getSize(bag.getRootDir().resolve("tagmanifest-md5.txt")));
    assertDoesNotThrow(() -> BagItTool.quickCheck(bagItProfile, bag));
    assertDoesNotThrow(() -> BagItTool.check(bagItProfile, bag));
  }

  private MultiValueMap<String, String> getMetadata() {
    LinkedMultiValueMap<String, String> metadata = new LinkedMultiValueMap<>();
    metadata.add("Source-Organization", "Université de Genève");
    metadata.add("Organization-Address", "Switzerland");
    metadata.add("Id", "123456789");
    metadata.add("Author", "Franquin");
    metadata.add("Author", "Depuis");
    metadata.add("Title", "Le nid des marsupilanis");
    metadata.add("Description", "BD ++");
    return metadata;
  }

  private MultiValueMap<String, String> getWrongMetadata() {
    LinkedMultiValueMap<String, String> metadata = new LinkedMultiValueMap<>();
    metadata.add("Organization-Address", "France");
    metadata.add("Id", "123456789");
    metadata.add("Author", "Franquin");
    metadata.add("Title", "Le nid des marsupilanis");
    metadata.add("Description", "BD ++");
    return metadata;
  }

  private Path createBag() throws IOException {
    Path source = Paths.get(new ClassPathResource(BAGIT_FOLDER).getURI());
    Path destination = source.resolve(BAG_FOLDER);
    for (Path f : FileTool.findFiles(source)) {
      FileTool.copyFile(f, destination.resolve(f.getFileName()));
    }
    return destination;
  }

}
