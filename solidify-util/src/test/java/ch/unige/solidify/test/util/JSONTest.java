/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - JSONTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.everit.json.schema.ValidationException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import ch.unige.solidify.exception.SolidifyCheckingException;
import ch.unige.solidify.util.FileTool;
import ch.unige.solidify.util.JSONTool;

class JSONTest {

  private static class Person {
    private String firstName;
    private String lastName;
    private long zipCode;

    public Person() {
      this.firstName = "My First Name";
      this.lastName = "My Last Name";
      this.zipCode = 74160;
    }

    public Person(String suffix) {
      this.firstName = "My First Name " + suffix;
      this.lastName = "My Last Name " + suffix;
      this.zipCode = 74160;
    }

    public String getFirstName() {
      return this.firstName;
    }

    public String getLastName() {
      return this.lastName;
    }

    public long getZipCode() {
      return this.zipCode;
    }

    public void setFirstName(String firstName) {
      this.firstName = firstName;
    }

    public void setLastName(String lastName) {
      this.lastName = lastName;
    }

    public void setZipCode(long zipCode) {
      this.zipCode = zipCode;
    }

  }

  private static String[] JSON_FILES = { "arrays.json", "geo-location.json", "person.json" };

  private static String PREFIX = "json/";

  @Test
  void convert2JsonArrayTest() {

    // List<Person>
    final List<Person> parray = new ArrayList<>();
    parray.add(new Person("Astérix"));
    parray.add(new Person("Obélix"));
    parray.add(new Person("Idéfix"));
    final String personList = "["
            + "{\"firstName\":\"My First Name Astérix\",\"lastName\":\"My Last Name Astérix\",\"zipCode\":74160},"
            + "{\"firstName\":\"My First Name Obélix\",\"lastName\":\"My Last Name Obélix\",\"zipCode\":74160},"
            + "{\"firstName\":\"My First Name Idéfix\",\"lastName\":\"My Last Name Idéfix\",\"zipCode\":74160}"
            + "]";
    final JSONArray newValue = JSONTool.convert2JsonArray(parray);
    assertEquals(personList, newValue.toString());

  }

  @Test
  void convert2JsonObjectTest() {

    // Person object
    final Person person = new Person();
    final String personString = "{\"firstName\":\"My First Name\",\"lastName\":\"My Last Name\",\"zipCode\":74160}";
    final JSONObject newValue = JSONTool.convert2Json(person);
    assertEquals(personString, newValue.toString());
  }

  @Test
  void convert2JsonStringTest() {

    // Line separator
    final String lineSep = System.lineSeparator();

    // long
    final long lvalue = 10;
    String newValue = JSONTool.convert2JsonString(lvalue);
    assertEquals(String.valueOf(lvalue), newValue);

    // String
    final String svalue = "string";
    newValue = JSONTool.convert2JsonString(svalue);
    assertEquals("\"" + svalue + "\"", newValue);

    // String[]
    final String[] sarray = { "string1", "string2", "string3" };
    final String arrayString = "[ \"string1\", \"string2\", \"string3\" ]";
    newValue = JSONTool.convert2JsonString(sarray);
    assertEquals(arrayString, newValue);

    // List<String>
    final List<String> larray = new ArrayList<>();
    larray.add("string1");
    larray.add("string2");
    larray.add("string3");
    newValue = JSONTool.convert2JsonString(larray);
    assertEquals(arrayString, newValue);

    // Person object
    final Person person = new Person();
    final String personString = "{" + lineSep + "  \"firstName\" : \"My First Name\"," + lineSep
            + "  \"lastName\" : \"My Last Name\"," + lineSep + "  \"zipCode\" : 74160" + lineSep
            + "}";
    newValue = JSONTool.convert2JsonString(person);
    assertEquals(personString, newValue);

    // List<Person>
    final List<Person> parray = new ArrayList<>();
    parray.add(new Person("Astérix"));
    parray.add(new Person("Obélix"));
    parray.add(new Person("Idéfix"));
    final String personList = "[ {" + lineSep + "  \"firstName\" : \"My First Name Astérix\"," + lineSep
            + "  \"lastName\" : \"My Last Name Astérix\"," + lineSep + "  \"zipCode\" : 74160"
            + lineSep + "}, {" + lineSep + "  \"firstName\" : \"My First Name Obélix\"," + lineSep
            + "  \"lastName\" : \"My Last Name Obélix\"," + lineSep + "  \"zipCode\" : 74160"
            + lineSep + "}, {" + lineSep + "  \"firstName\" : \"My First Name Idéfix\"," + lineSep
            + "  \"lastName\" : \"My Last Name Idéfix\"," + lineSep + "  \"zipCode\" : 74160"
            + lineSep + "} ]";
    newValue = JSONTool.convert2JsonString(parray);
    assertEquals(personList, newValue);

  }

  @Test
  void invalidSchemaTest() {
    final List<Path> fileList = this.loadJsonFiles(PREFIX);
    int count = 0;
    for (final Path file : fileList) {
      try {
        JSONTool.validate(this.getSchema(file), this.getInvalidSchemaJson(file));
      } catch (final SolidifyCheckingException e) {
        count++;
        System.out.println(this.getInvalidSchemaJson(file).getFileName());
        System.out.println(e.getMessage());
        ((ValidationException) e.getCause()).getCausingExceptions().stream()
                .map(ValidationException::getMessage)
                .forEach(System.out::println);
      }
    }
    assertEquals(fileList.size(), count);
  }

  @Test
  void validSchemaTest() {
    final List<Path> fileList = this.loadJsonFiles(PREFIX);
    for (final Path file : fileList) {
      JSONTool.validate(this.getSchema(file), file);
    }
  }

  @Test
  void wellformedTest() {
    final List<Path> fileList = this.loadJsonFiles(PREFIX);
    for (final Path file : fileList) {
      assertDoesNotThrow(() -> JSONTool.wellformed(file));
      assertDoesNotThrow(() -> JSONTool.wellformed(this.getSchema(file)));
    }
  }

  @Test
  void malformedTest() throws IOException {
    final String fileAsString = FileTool.toString(this.getPath("json/jsonInvalid.json"));
    assertThrows(SolidifyCheckingException.class, () -> JSONTool.wellformed(fileAsString));
  }

  @Test
  void wellformedObjectTest() {
    try {
      final Path path = this.getPath("json/jsonObject.json");
      JSONTool.wellformedObject(FileTool.toString(path));
    } catch (final IOException e) {
      fail(e.getMessage());
    }
  }

  @Test()
  void malformedObjectTest() throws IOException {
    final String fileAsString = FileTool.toString(this.getPath("json/jsonArray.json"));
    assertThrows(SolidifyCheckingException.class, () -> JSONTool.wellformedObject(fileAsString));
  }

  @Test
  void wellformedArrayTest() {
    try {
      final Path path = this.getPath("json/jsonArray.json");
      JSONTool.wellformedArray(FileTool.toString(path));
    } catch (final IOException e) {
      fail(e.getMessage());
    }
  }

  @Test
  void malformedArrayTest() throws IOException {
    final String fileAsString = FileTool.toString(this.getPath("json/jsonObject.json"));
    assertThrows(SolidifyCheckingException.class, () -> JSONTool.wellformedArray(fileAsString));
  }

  private Path getInvalidSchemaJson(Path file) {
    return Paths.get(file.toString().replace(".json", "-invalid.json"));
  }

  private Path getPath(String f) {
    try {
      return new ClassPathResource(f).getFile().toPath();
    } catch (final IOException e) {
      return new FileSystemResource(f).getFile().toPath();
    }
  }

  private Path getSchema(Path file) {
    return Paths.get(file.toString().replace(".json", ".schema.json"));
  }

  private List<Path> loadJsonFiles(String prefix) {
    final List<Path> list = new ArrayList<>();
    for (final String file : JSON_FILES) {
      list.add(this.getPath(prefix + file));
    }
    return list;
  }
}
