/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SolidifyControllerAdvice.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static ch.unige.solidify.controller.SolidifyControllerAdvice.StackTraceDisplay.WITHOUT_STACK_TRACE;
import static ch.unige.solidify.controller.SolidifyControllerAdvice.StackTraceDisplay.WITH_STACK_TRACE;

import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;

import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyBulkActionException;
import ch.unige.solidify.exception.SolidifyError;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyMfaNeededException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyRestException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.service.MessageService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.validation.ValidationError;

@ControllerAdvice
public class SolidifyControllerAdvice {

  private static final String ERROR_CONTENT_TYPE = "application/hal+json";
  private static final Logger log = LoggerFactory.getLogger(SolidifyControllerAdvice.class);

  private final ErrorAttributes errorAttributes;
  private final HttpHeaders headers = new HttpHeaders();
  private final MessageService messageService;

  private final StackTraceDisplay badRequestStacktrace;
  private final StackTraceDisplay accessDeniedStacktrace;

  public enum StackTraceDisplay {
    WITH_STACK_TRACE, WITHOUT_STACK_TRACE
  }

  public SolidifyControllerAdvice(ErrorAttributes errorAttributes, MessageService messageService, SolidifyProperties solidifyProperties) {
    this.headers.add(HttpHeaders.CONTENT_TYPE, ERROR_CONTENT_TYPE);
    this.errorAttributes = errorAttributes;
    this.messageService = messageService;
    this.badRequestStacktrace = this.getStackTraceDisplay(solidifyProperties.getDebug().isBadRequestStacktrace());
    this.accessDeniedStacktrace = this.getStackTraceDisplay(solidifyProperties.getDebug().isAccessDeniedStacktrace());
  }

  /******************************/

  @ExceptionHandler(SolidifyHttpErrorException.class)
  public ResponseEntity<SolidifyError> handleException(SolidifyHttpErrorException exception, HttpServletRequest request) {
    SolidifyError solidifyError = exception.getSolidifyError();
    solidifyError.setPath(request.getRequestURI());
    return ResponseEntity.status(solidifyError.getStatus()).body(solidifyError);
  }

  @ExceptionHandler(BindException.class)
  public ResponseEntity<SolidifyError> handleException(BindException exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, this.messageService.get("error.illegal.parameter"), HttpStatus.BAD_REQUEST, WITH_STACK_TRACE);
  }

  @ExceptionHandler({ ConversionFailedException.class, MethodArgumentTypeMismatchException.class })
  public ResponseEntity<SolidifyError> handleException(RuntimeException exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, this.messageService.get("error.illegal.parameter"), HttpStatus.BAD_REQUEST, WITH_STACK_TRACE);
  }

  /**
   * Used to catch DataIntegrityViolationException
   *
   * @param exception
   * @param request
   * @return
   */
  @ExceptionHandler(DataIntegrityViolationException.class)
  public ResponseEntity<SolidifyError> handleException(DataIntegrityViolationException exception, HttpServletRequest request) {
    final SolidifyError solidifyError = new SolidifyError(this.errorAttributes, request);
    solidifyError.setStatus(HttpStatus.BAD_REQUEST);
    solidifyError.setError("Data integrity violation exception");
    String exceptionMessage = exception.getMostSpecificCause().getMessage();
    if (exceptionMessage.contains("\n")) {
      exceptionMessage = exceptionMessage.substring(0, exceptionMessage.indexOf('\n'));
    }
    solidifyError.setMessage(exceptionMessage);
    return this.buildBadRequestResponse(exception, request, solidifyError);
  }

  /**
   * Used to catch all exceptions not specifically catched by another ExceptionHandler
   *
   * @param exception
   * @param request   @returnlogException(exception, request)
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<SolidifyError> handleException(Exception exception, HttpServletRequest request) {

    this.logException(exception, null, request, WITH_STACK_TRACE);
    final SolidifyError error = new SolidifyError(this.errorAttributes, request);
    error.setMessage(exception.getMessage());
    return ResponseEntity.status(error.getStatus()).headers(this.headers).body(error);
  }

  /**
   * Used to catch requests errors (such as invalid JSON received)
   *
   * @param exception
   * @param request
   * @return
   */
  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ResponseEntity<SolidifyError> handleException(HttpMessageNotReadableException exception, HttpServletRequest request) {

    this.logException(exception, null, request, WITH_STACK_TRACE);

    String message = this.messageService.get("error.request.notreadable");

    final Throwable nestedException = exception.getCause();
    if (nestedException instanceof JsonParseException) {
      message = this.messageService.get("error.request.json.malformed");
    }

    final SolidifyError error = new SolidifyError(this.errorAttributes, request);
    error.setMessage(message);
    error.setError(exception.getMessage());
    error.setStatus(HttpStatus.BAD_REQUEST);

    return ResponseEntity.badRequest().headers(this.headers).body(error);
  }

  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  public ResponseEntity<SolidifyError> handleException(HttpRequestMethodNotSupportedException exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, this.messageService.get("error.illegal.http.method"), HttpStatus.METHOD_NOT_ALLOWED,
            WITHOUT_STACK_TRACE);
  }

  @ExceptionHandler(IllegalStateException.class)
  public ResponseEntity<SolidifyError> handleException(IllegalStateException exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, this.messageService.get("error.illegal.state"), HttpStatus.BAD_REQUEST, WITH_STACK_TRACE);
  }

  /**
   * Used to catch Spring constraints validation errors (rules defined as annotations on entities)
   *
   * @param exception
   * @param request
   * @return
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<SolidifyError> handleException(MethodArgumentNotValidException exception, HttpServletRequest request) {
    final BindingResult errors = exception.getBindingResult();
    final ValidationError validationError = new ValidationError(errors);
    return this.buildBadRequestResponse(exception, request, validationError);
  }

  /**
   * Used to catch ValidationError set at the service level (such as linked resources not found)
   *
   * @param exception
   * @param request
   * @return
   */
  @ExceptionHandler(SolidifyValidationException.class)
  public ResponseEntity<SolidifyError> handleException(SolidifyValidationException exception, HttpServletRequest request) {
    return this.buildBadRequestResponse(exception, request, exception.getValidationError());
  }

  @ExceptionHandler({ AccessDeniedException.class })
  public ResponseEntity<SolidifyError> handleAccessDeniedException(Exception exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, this.messageService.get("error.request.forbidden"), HttpStatus.FORBIDDEN,
            this.accessDeniedStacktrace);
  }

  @ExceptionHandler({ SolidifyUnmodifiableException.class, SolidifyUndeletableException.class })
  public ResponseEntity<SolidifyError> handleBadRequestException(Exception exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, this.messageService.get("error.request.badrequest"), HttpStatus.BAD_REQUEST,
            this.badRequestStacktrace);
  }

  @ExceptionHandler({ SolidifyBulkActionException.class })
  public ResponseEntity<SolidifyError> handleBulkActionException(SolidifyBulkActionException exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, exception.getDetailedMessage(), HttpStatus.BAD_REQUEST,
            this.badRequestStacktrace);
  }

  @ExceptionHandler(HttpClientErrorException.class)
  public ResponseEntity<SolidifyError> handleHttpClientErrorException(HttpClientErrorException exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, exception.getMessage(), exception.getStatusCode(), WITHOUT_STACK_TRACE);
  }

  @ExceptionHandler(SolidifyRestException.class)
  public ResponseEntity<SolidifyError> handleHttpRestClientException(SolidifyRestException exception, HttpServletRequest request) {
    this.logException(exception, null, request, WITH_STACK_TRACE);
    SolidifyError error = exception.getSourceError();
    if (error == null) {
      error = new SolidifyError(this.errorAttributes, request);
    }
    return ResponseEntity.status(error.getStatus()).headers(this.headers).body(error);
  }

  @ExceptionHandler(HttpMediaTypeException.class)
  public ResponseEntity<SolidifyError> handleHttpMediaTypeException(HttpMediaTypeException exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, exception.getMessage(), HttpStatus.UNSUPPORTED_MEDIA_TYPE, WITHOUT_STACK_TRACE);
  }

  @ExceptionHandler(MultipartException.class)
  public ResponseEntity<SolidifyError> handleMultipartException(MultipartException exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, exception.getMessage(), HttpStatus.BAD_REQUEST, WITHOUT_STACK_TRACE);
  }

  @ExceptionHandler({ MissingServletRequestParameterException.class, MissingServletRequestPartException.class })
  public ResponseEntity<SolidifyError> handleMissingServletRequestParameterException(ServletException exception,
          HttpServletRequest request) {
    return this.buildResponse(exception, request, exception.getMessage(), HttpStatus.BAD_REQUEST, WITHOUT_STACK_TRACE);
  }

  /**
   * Used to catch NoSuchElementException/EmptyResultDataAccessException
   *
   * @param exception
   * @param request
   * @return
   */
  @ExceptionHandler({ NoSuchElementException.class, EmptyResultDataAccessException.class, SolidifyResourceNotFoundException.class,
          NoResourceFoundException.class })
  public ResponseEntity<SolidifyError> handleNotFoundException(Exception exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, this.messageService.get("error.request.notfound"), HttpStatus.NOT_FOUND, WITHOUT_STACK_TRACE);
  }

  @ExceptionHandler(SolidifyMfaNeededException.class)
  public ResponseEntity<SolidifyError> handleSolidifyMfaNeededException(Exception exception, HttpServletRequest request) {
    return this.buildResponse(exception, request, "MFA needed", HttpStatus.UNAUTHORIZED, WITHOUT_STACK_TRACE);
  }

  /**
   * Use to catch ResponseStatusException and build response based on it's params
   *
   * @param ex
   * @param request
   * @return
   */
  @ExceptionHandler(ResponseStatusException.class)
  public ResponseEntity<SolidifyError> handleResponseStatusException(ResponseStatusException ex, HttpServletRequest request) {
    return this.buildResponse(ex, request, ex.getReason(), ex.getStatusCode(), WITHOUT_STACK_TRACE);
  }

  protected ResponseEntity<SolidifyError> buildBadRequestResponse(Exception exception, HttpServletRequest request, SolidifyError solidifyError) {
    this.logException(exception, null, request, this.badRequestStacktrace);
    this.logErrorAsJson(solidifyError);
    return ResponseEntity.badRequest().headers(this.headers).body(solidifyError);
  }

  protected ResponseEntity<SolidifyError> buildBadRequestResponse(Exception exception, HttpServletRequest request,
          ValidationError validationError) {

    final SolidifyError solidifyError = new SolidifyError(this.errorAttributes, request);
    solidifyError.setValidationError(validationError);
    solidifyError.setMessage(exception.getMessage());
    solidifyError.setStatus(HttpStatus.BAD_REQUEST);

    return this.buildBadRequestResponse(exception, request, solidifyError);
  }

  protected ResponseEntity<SolidifyError> buildResponse(Exception exception, HttpServletRequest request, String technicalMessage,
          HttpStatusCode httpStatusCode, StackTraceDisplay withStackTrace) {
    this.logException(exception, technicalMessage, request, withStackTrace);

    final SolidifyError error = new SolidifyError(this.errorAttributes, request);
    error.setMessage(exception.getMessage()); // Message for user (i18n)
    error.setError(technicalMessage); // Technical message for internal use
    error.setStatus(HttpStatus.valueOf(httpStatusCode.value()));

    return ResponseEntity.status(httpStatusCode).headers(this.headers).body(error);
  }

  protected void logErrorAsJson(Object errorObject) {
    try {
      final String errorMessage = new ObjectMapper().writeValueAsString(errorObject);
      log.error(errorMessage);
    } catch (final JsonProcessingException e) {
      log.error("unable to log validation error", e);
    }
  }

  protected void logException(Exception exception, String technicalMessage, HttpServletRequest request, StackTraceDisplay stackTraceDisplay) {
    final String headerXFF = request.getHeader("X-FORWARDED-FOR");
    String message = "Request url {} {} from remote address {} with X-FORWARDED-FOR {} has generated an exception caught by controller advice:";
    if (stackTraceDisplay == WITHOUT_STACK_TRACE) {
      message += " {}";
      log.warn(message, request.getMethod(), request.getRequestURL(), request.getRemoteAddr(), headerXFF, exception.toString());
    } else {
      log.error(message, request.getMethod(), request.getRequestURL(), request.getRemoteAddr(), headerXFF, exception);
    }

    // Log technical information
    if (!StringTool.isNullOrEmpty(technicalMessage)) {
      log.error("Exception error details: {}", technicalMessage);
    }
  }

  private StackTraceDisplay getStackTraceDisplay(boolean withStackTrace) {
    if (withStackTrace) {
      return WITH_STACK_TRACE;
    } else {
      return WITHOUT_STACK_TRACE;
    }
  }

}
