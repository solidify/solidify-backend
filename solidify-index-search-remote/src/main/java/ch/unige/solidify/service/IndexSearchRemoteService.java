/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Remote Search - IndexSearchRemoteService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.service.rest.abstractservice.IndexMetadataRemoteResourceService;

public abstract class IndexSearchRemoteService<T extends IndexMetadata> extends AbstractResourceService {

  protected final IndexMetadataRemoteResourceService<T> indexMetadataRemoteResourceService;

  protected IndexSearchRemoteService(IndexMetadataRemoteResourceService<T> indexMetadataRemoteResourceService) {
    this.indexMetadataRemoteResourceService = indexMetadataRemoteResourceService;
  }

  public List<T> findAll(T search) {
    Page<T> page = this.findAll(search, PageRequest.of(0, RestCollectionPage.MAX_SIZE_PAGE));
    return page.getContent();
  }

  public Page<T> findAll(T search, Pageable pageable) {
    final RestCollection<T> result = this.indexMetadataRemoteResourceService.search(null, pageable);
    return new PageImpl<>(result.getData(), pageable, result.getPage().getTotalItems());
  }

  public T findOne(String id) {
    return this.indexMetadataRemoteResourceService.getIndexMetadata(id);
  }

  public RestCollection<T> search(List<SearchCondition> conditions, Pageable pageable) {
    return this.indexMetadataRemoteResourceService.search(conditions, pageable);
  }

  // Define Database name ~ index name
  public String getIndexName() {
    return this.indexMetadataRemoteResourceService.getIndexName();
  }

  public List<String> getIndexList() {
    return Arrays.asList(this.getIndexName().split(IndexConstants.INDEX_SEPARATOR));
  }

}
