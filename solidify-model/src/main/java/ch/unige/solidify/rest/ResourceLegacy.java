/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ResourceLegacy.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import jakarta.persistence.AttributeOverride;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.Valid;

import ch.unige.solidify.listener.CacheListener;
import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.model.RegularChangeInfo;

@MappedSuperclass
@EntityListeners({ CacheListener.class })
public abstract class ResourceLegacy extends ResourceDiverse {

  @AttributeOverride(name = "when", column = @Column(name = "d_cre"))
  @AttributeOverride(name = "who", column = @Column(name = "id_cre"))
  @Embedded
  @Valid
  protected RegularChangeInfo creation = new RegularChangeInfo();

  @AttributeOverride(name = "when", column = @Column(name = "d_maj"))
  @AttributeOverride(name = "who", column = @Column(name = "id_maj"))
  @Embedded
  @Valid
  protected RegularChangeInfo lastUpdate = new RegularChangeInfo();

  @Override
  public ChangeInfo getCreation() {
    return this.creation;
  }

  @Override
  public ChangeInfo getLastUpdate() {
    return this.lastUpdate;
  }

}
