/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - AbstractUserSynchronizationService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import ch.unige.solidify.auth.model.AuthUserDto;
import ch.unige.solidify.auth.model.PersonInfo;
import ch.unige.solidify.auth.model.UserInfo;
import ch.unige.solidify.auth.service.UserInfoLocalService;
import ch.unige.solidify.util.StringTool;
import ch.unige.solidify.util.TrustedRestClientTool;

public abstract class AbstractUserSynchronizationService<T extends UserInfo> {
  private static final String USERS_PATH = "/users/";
  private static final String CHANGE_ORCID_ENDPOINT = "/change-orcid";
  private static final Logger log = LoggerFactory.getLogger(AbstractUserSynchronizationService.class);
  private final UserInfoLocalService<T> userInfoLocalService;
  private final TrustedRestClientTool trustedRestClientTool;
  private final TrustedRestClientService trustedRestClientService;
  private final String authModuleUrl;

  protected AbstractUserSynchronizationService(UserInfoLocalService<T> userInfoLocalService, TrustedRestClientTool trustedRestClientTool,
          TrustedRestClientService trustedRestClientService, String authModuleUrl) {
    this.userInfoLocalService = userInfoLocalService;
    this.trustedRestClientTool = trustedRestClientTool;
    this.trustedRestClientService = trustedRestClientService;
    this.authModuleUrl = authModuleUrl;
  }

  @Scheduled(cron = "${solidify.userSynchroCron: 0 59 23 * * *}")
  @Transactional
  public void synchronizeUsersWithAuthServer() {
    for (UserInfo userInfo : this.userInfoLocalService.findAll()) {
      try {
        final AuthUserDto authUserDto = this.getAuthenticatedUser(userInfo.getExternalUid());
        this.userInfoLocalService.saveAuthUserDto(userInfo, authUserDto);
        this.synchronizeOrcidWithAuthorizationServer(userInfo, authUserDto);
      } catch (RuntimeException e) {
        log.error("User information can't be updated to authorization server for user {} : {}", userInfo.getResId(), e.getMessage());
        log.debug("Exception in method synchronizeUsersWithAuthServer", e);
      }
    }
  }

  public void synchronizeOrcidWithAuthorizationServer(UserInfo userInfo, AuthUserDto authUserDto) {
    final PersonInfo personInfo = userInfo.getPerson();
    if (this.isOrcidPresentInAdminAndAbsentInAuthServer(personInfo, authUserDto) ||
            this.isOrcidVerifiedInAdminAndNotVerifiedInAuthServer(personInfo, authUserDto)) {
      AuthUserDto orcidInfo = new AuthUserDto();
      orcidInfo.setOrcid(personInfo.getOrcid());
      orcidInfo.setVerifiedOrcid(personInfo.isVerifiedOrcid());
      this.updateUserOrcidOnAuthServer(authUserDto.getExternalUid(), orcidInfo);
      log.info("User {}: ORCID updated with {}", authUserDto.getExternalUid(), personInfo.getOrcid());
    } else if (this.isOrcidAbsentInAdminAndPresentInAuthServer(personInfo, authUserDto) ||
            this.isOrcidNotVerifiedInAdminAndVerifiedInAuthServer(personInfo, authUserDto)) {
      personInfo.setOrcid(authUserDto.getOrcid());
      personInfo.setVerifiedOrcid(authUserDto.isVerifiedOrcid());
      this.userInfoLocalService.saveUserPersonInfo(userInfo, personInfo);
    }
    // Consistency verification
    if (this.isOrcidDifferentAndVerified(personInfo, authUserDto)) {
      log.warn("The person: '{}' has a verified ORCID {} in administration module and a verified ORCID {} in authorization server.",
              personInfo.getFullName(), personInfo.getOrcid(), authUserDto.getOrcid());
    }
    if (this.isOrcidDifferentAndNotVerified(personInfo, authUserDto)) {
      log.warn("The person '{}' has a not verified ORCID {} in administration module and a not verified ORCID {} in authorization server.",
              personInfo.getFullName(), personInfo.getOrcid(), authUserDto.getOrcid());
    }

  }

  public AuthUserDto getAuthenticatedUser(String externalUid) {
    return this.trustedRestClientService.getResource(this.authModuleUrl + USERS_PATH + externalUid, AuthUserDto.class);
  }

  private void updateUserOrcidOnAuthServer(String externalUid, AuthUserDto orcidInfo) {
    RestTemplate restTemplate = this.trustedRestClientTool.getClient();
    AuthUserDto authUserDto = restTemplate.postForObject(this.authModuleUrl + USERS_PATH + externalUid + CHANGE_ORCID_ENDPOINT, orcidInfo,
            AuthUserDto.class);
    if (authUserDto == null) {
      throw new IllegalStateException();
    }
  }

  private boolean isOrcidPresentInAdminAndAbsentInAuthServer(PersonInfo personInfo, AuthUserDto authUserDto) {
    return !StringTool.isNullOrEmpty(personInfo.getOrcid()) && StringTool.isNullOrEmpty(authUserDto.getOrcid());
  }

  private boolean isOrcidAbsentInAdminAndPresentInAuthServer(PersonInfo personInfo, AuthUserDto authUserDto) {
    return StringTool.isNullOrEmpty(personInfo.getOrcid()) && !StringTool.isNullOrEmpty(authUserDto.getOrcid());
  }

  private boolean isOrcidVerifiedInAdminAndNotVerifiedInAuthServer(PersonInfo personInfo, AuthUserDto authUserDto) {
    if (StringTool.isNullOrEmpty(personInfo.getOrcid()) || StringTool.isNullOrEmpty(authUserDto.getOrcid())) {
      return false;
    }
    return personInfo.isVerifiedOrcid() && !authUserDto.isVerifiedOrcid();
  }

  private boolean isOrcidNotVerifiedInAdminAndVerifiedInAuthServer(PersonInfo personInfo, AuthUserDto authUserDto) {
    if (StringTool.isNullOrEmpty(personInfo.getOrcid()) || StringTool.isNullOrEmpty(authUserDto.getOrcid())) {
      return false;
    }
    return !personInfo.isVerifiedOrcid() && authUserDto.isVerifiedOrcid();
  }

  private boolean isOrcidDifferentAndNotVerified(PersonInfo personInfo, AuthUserDto authUserDto) {
    if (StringTool.isNullOrEmpty(personInfo.getOrcid()) || StringTool.isNullOrEmpty(authUserDto.getOrcid())) {
      return false;
    }
    return !personInfo.isVerifiedOrcid() && !authUserDto.isVerifiedOrcid() && !personInfo.getOrcid().equals(authUserDto.getOrcid());
  }

  private boolean isOrcidDifferentAndVerified(PersonInfo personInfo, AuthUserDto authUserDto) {
    if (StringTool.isNullOrEmpty(personInfo.getOrcid()) || StringTool.isNullOrEmpty(authUserDto.getOrcid())) {
      return false;
    }
    return personInfo.isVerifiedOrcid() && authUserDto.isVerifiedOrcid() && !personInfo.getOrcid().equals(authUserDto.getOrcid());
  }
}
