/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - IndexSettingsDeserializer.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.index;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import ch.unige.solidify.SolidifyConstants;

public class IndexPropertiesDeserializer extends JsonDeserializer<IndexProperties> {

  @Override
  public IndexProperties deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
    final JsonNode node = jp.getCodec().readTree(jp);
    final IndexProperties ai = new IndexProperties();
    ai.setResId(node.get(SolidifyConstants.DB_RES_ID).asText());
    if (node.has("settings")) {
      ai.setSettings(node.findValue("settings").toString());
    }
    return ai;
  }
}
