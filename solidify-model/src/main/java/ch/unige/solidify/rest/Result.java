/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - Result.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        The result is the result of REST action (HTTP POST) with the status:
        - EXECUTED => the action was executed successfully
        - NOT_EXEUTED => the action failed and the message details the reason
        - NON_APPLICABLE => the action cannot be executed and the message details the reason
        """)
public class Result extends RepresentationModel<Result> {

  public enum ActionStatus {
    EXECUTED, NON_APPLICABLE, NOT_EXECUTED
  }

  @Schema(description = "The detailed message on what happens")
  private String message;

  @Schema(description = "The ID of the object")
  private final String resId;

  @Schema(description = "The status of the launched action")
  private ActionStatus status;

  public Result() {
    this.resId = null;
    this.status = ActionStatus.NON_APPLICABLE;
    this.message = null;
  }

  public Result(String id) {
    this.resId = id;
    this.status = ActionStatus.NON_APPLICABLE;
    this.message = null;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof Result result)) {
      return false;
    }
    return super.equals(result) && Objects.equals(this.resId, result.getResId())
            && Objects.equals(this.status, result.getStatus())
            && Objects.equals(this.message, result.getMessage());
  }

  public String getMessage() {
    return this.message;
  }

  public String getResId() {
    return this.resId;
  }

  public ActionStatus getStatus() {
    return this.status;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.resId, this.status, this.message);
  }

  public void setMesssage(String m) {
    this.message = m;
  }

  public void setStatus(ActionStatus status) {
    this.status = status;
  }

}
