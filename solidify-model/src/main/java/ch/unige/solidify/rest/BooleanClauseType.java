/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - BooleanClauseType.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

public enum BooleanClauseType {
  MUST,     // document must match conditions. Matching values are used to calculate score.
  FILTER,   // document must match conditions. Matching values are not used to calculate score.
  SHOULD,   // document should match conditions. Matching values are used to calculate score.
  MUST_NOT  // document are discarded if they match conditions.
}
