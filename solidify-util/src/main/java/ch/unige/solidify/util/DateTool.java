/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - DateTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;

import ch.unige.solidify.SolidifyConstants;

public class DateTool {

  public static OffsetDateTime swissDateTime2offSetDate(LocalDateTime input) {
    if (input == null) {
      return null;
    }
    return input.atZone(ZoneId.of("Europe/Zurich")).toOffsetDateTime();
  }

  /**
   * This constructor should not be called
   */
  private DateTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }
}
