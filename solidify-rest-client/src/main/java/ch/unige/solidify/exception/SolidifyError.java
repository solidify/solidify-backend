/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - SolidifyError.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.exception;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

import jakarta.servlet.http.HttpServletRequest;

import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.ServletWebRequest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import ch.unige.solidify.validation.FieldValidationError;
import ch.unige.solidify.validation.ValidationError;

public class SolidifyError implements Serializable {
  @Serial
  private static final long serialVersionUID = 8308030061661426307L;
  private String error; // Technical/internal message
  private String message; // End-user message
  private String path;
  private HttpStatus status;
  private int statusCode;
  private String timeStamp;

  @JsonIgnore
  private ValidationError validationError;

  private List<FieldValidationError> validationErrors;

  public SolidifyError() {
  }

  public SolidifyError(ErrorAttributes errorAttributes, HttpServletRequest request) {
    final Map<String, Object> mapErrorAttributes = errorAttributes.getErrorAttributes(new ServletWebRequest(request),
            ErrorAttributeOptions.defaults());
    if (mapErrorAttributes.containsKey("path")) {
      this.setPath((String) mapErrorAttributes.get("path"));
    } else {
      this.setPath(request.getRequestURL().toString());
    }
    this.setStatus(this.getStatus(mapErrorAttributes.get("status")));
    this.setError((String) mapErrorAttributes.get("error"));
    this.setMessage((String) mapErrorAttributes.get("message"));
    this.setTimeStamp(mapErrorAttributes.get("timestamp").toString());
    this.statusCode = this.status.value();
    if (this.validationError != null) {
      this.validationErrors = this.validationError.getErrors();
    }
  }

  public String getError() {
    return this.error;
  }

  public String getMessage() {
    return this.message;
  }

  public String getPath() {
    return this.path;
  }

  public HttpStatus getStatus() {
    return this.status;
  }

  public int getStatusCode() {
    return this.statusCode;
  }

  public String getTimeStamp() {
    return this.timeStamp;
  }

  public ValidationError getValidationError() {
    return this.validationError;
  }

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  public List<FieldValidationError> getValidationErrors() {
    return this.validationErrors;
  }

  public void setError(String error) {
    this.error = error;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public void setStatus(HttpStatus status) {
    this.status = status;
    if (this.status != null) {
      this.statusCode = this.status.value();
    }
  }

  public void setStatusCode(int statusCode) {
    this.statusCode = statusCode;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public void setValidationError(ValidationError validationError) {
    this.validationError = validationError;
    if (this.validationError != null) {
      this.validationErrors = this.validationError.getErrors();
      if (this.validationError.getMainErrorMessage() != null) {
        this.error = this.validationError.getMainErrorMessage();
      }
    } else {
      this.validationErrors = null;
    }
  }

  public void setValidationErrors(List<FieldValidationError> validationErrors) {
    this.validationErrors = validationErrors;
  }

  private HttpStatus getStatus(Object status) {
    if (status instanceof Integer integer) {
      try {
        return HttpStatus.valueOf(integer);
      } catch (final IllegalArgumentException e) {
        // Unknown status code
        return HttpStatus.INTERNAL_SERVER_ERROR;
      }
    } else {
      return HttpStatus.INTERNAL_SERVER_ERROR;
    }
  }

}
