/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - ValidationTool.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.unige.solidify.SolidifyConstants;

public class ValidationTool {

  private static final String DEWEY_CODE_REGEX = "^\\d{1,5}((\\.|-|\\ -\\ |/|\\ /\\ |\\ /)\\d{1,5}){0,8}$";
  private static final String ORCID_ID_REGEX = "^\\d{4}-\\d{4}-\\d{4}-\\d{3}[\\dX]$";
  private static final String ARXIVID_REGEX = "^[\\w./-]+$";
  private static final String URN_REGEX = "^urn:[a-z0-9][a-z0-9-]{0,31}:[a-z0-9()+,\\-.:=@;$_!*'%/?#]+$";
  private static final String ISBN_REGEX = "^[0-9\\-]+[xX]*$";
  private static final String ISSN_REGEX = "^\\d{4}\\-\\d{3}[\\dxX]$";
  private static final String DOI_REGEX = "^10.\\d{4,9}/[-._;()<>/:A-Za-z0-9]+$";
  private static final String RORID_REGEX = "^0[\\d|a-z&&[^ilou]]{6}\\d{2}$";
  private static final String ARK_V18_REGEX = "^ark:/\\d{5}/[a-zA-Z0-9=#*+@_$%-]+$";
  private static final String ARK_V37_REGEX = "^ark:/?\\d{5}/[bcdfghjkmnpqrstvwxz]+\\d{1}[0123456789bcdfghjkmnpqrstvwxz=~*+@_$%-]+$";
  private static final String UUID_REGEX = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$";

  private static final int ARXIVID_MAX_LENGTH = 25;

  /**
   * This constructor should not be called
   */
  private ValidationTool() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  /**
   * Valid ISSN is made of: 4 number, 1 dash, 3 numbers, 1 number or X (1234-5678 or 0004-567X)
   *
   * @param issn The ISSN code to validate
   * @return
   */
  public static boolean isValidISSN(String issn) {
    return issn.matches(ISSN_REGEX);
  }

  /**
   * Valid DOI is made of: prefix/suffix where prefix is build as 10.NNNN where NNNN are at least four digits greater or equal to 1000.
   *
   * @param doi The DOI to validate
   * @return
   */
  public static boolean isValidDOI(String doi) {
    if (!StringTool.isNullOrEmpty(doi)) {
      return doi.matches(DOI_REGEX);
    }
    return false;
  }

  /**
   * Valid ARK is made of: 'ark:/NAAN/Name'
   * NAAN=Name Assigning Authority Number
   *
   * @param ark The ARK to validate
   * @return
   */
  public static boolean isValidARKv18(String ark) {
    if (!StringTool.isNullOrEmpty(ark)) {
      return ark.matches(ARK_V18_REGEX);
    }
    return false;
  }

  /**
   * Valid ARK is made of: 'ark:NAAN/<shoulder><blade>'
   * NAAN=Name Assigning Authority Number
   *
   * @param ark The ARK to validate
   * @return
   */
  public static boolean isValidARKv37(String ark) {
    if (!StringTool.isNullOrEmpty(ark)) {
      return ark.matches(ARK_V37_REGEX);
    }
    return false;
  }

  /**
   * Valid UUDI
   *
   * @param uuid The UUID to validate
   * @return
   */
  public static boolean isValidUUID(String uuid) {
    if (!StringTool.isNullOrEmpty(uuid)) {
      return uuid.matches(UUID_REGEX);
    }
    return false;
  }

  /**
   * ISBN contains only digit, dashes and a eventual trailing X (for ISBN-10)
   *
   * @param isbn The ISBN code to validate
   * @return
   */
  public static boolean isValidISBN(String isbn) {
    return isbn.matches(ISBN_REGEX);
  }

  /**
   * Different formats for dates are valid: yyyy or given date format (at the moment: yyyy-MM-dd and yyyy-MM)
   *
   * @param dateStr
   * @param validDateFormats
   * @return True if the date string is valid according to the format
   */
  public static boolean isValidDate(String dateStr, String[] validDateFormats) {
    if (StringTool.isNullOrEmpty(dateStr)) {
      return false;
    }
    int currentYear = LocalDate.now().getYear();
    int maxYear = currentYear + 10;

    if (dateStr.length() == 4) {
      // case of year only
      if (dateStr.matches("\\d+")) {
        try {
          int year = Integer.parseInt(dateStr);
          return (year <= maxYear);
        } catch (NumberFormatException e) {
          return false;
        }
      } else {
        return false;
      }
    } else {
      // case of full dates
      if (validDateFormats == null) {
        return false;
      }

      DateTimeFormatter formatter = null;
      for (String pattern : validDateFormats) {
        try {
          formatter = new DateTimeFormatterBuilder()
                  .appendPattern(pattern)
                  .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                  .toFormatter();
          LocalDate date = LocalDate.parse(dateStr, formatter);
          if (date.getYear() <= maxYear) {
            return true;
          }
        } catch (DateTimeParseException e) {
          continue;
        }
      }
      return false;
    }
  }

  public static boolean isValidPMID(String pmidString) {
    BigInteger result;
    if (pmidString.matches("\\d+")) {
      try {
        result = new BigInteger(pmidString);
        return !result.equals(BigInteger.ZERO);
      } catch (NumberFormatException e) {
        return false;
      }
    } else {
      return false;
    }
  }

  public static boolean isValidDeweyCode(String dewey) {
    if (!StringTool.isNullOrEmpty(dewey)) {
      return dewey.matches(DEWEY_CODE_REGEX);
    }
    return false;
  }

  public static boolean isValidOrcid(String orcId) {
    if (!StringTool.isNullOrEmpty(orcId)) {
      return orcId.matches(ORCID_ID_REGEX);
    }
    return false;
  }

  public static boolean isValidArxivId(String arxivId) {
    // Very relax validation, to implement a strict one see:
    // https://arxiv.org/help/arxiv_identifier
    if (arxivId == null || arxivId.length() > ARXIVID_MAX_LENGTH) {
      return false;
    }
    Pattern p = Pattern.compile(ARXIVID_REGEX);
    Matcher m = p.matcher(arxivId);
    return m.matches();
  }

  public static boolean isValidURN(String urn) {
    if (urn == null) {
      return false;
    }
    Pattern p = Pattern.compile(URN_REGEX, Pattern.CASE_INSENSITIVE);
    Matcher m = p.matcher(urn);
    return m.matches();
  }

  public static boolean isValidRorId(String rorId) {
    if (rorId == null) {
      return false;
    }
    Pattern p = Pattern.compile(RORID_REGEX, Pattern.CASE_INSENSITIVE);
    Matcher m = p.matcher(rorId);
    return m.matches();
  }
}
