/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - CommandToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import ch.unige.solidify.util.CommandTool;
import ch.unige.solidify.util.OSTool;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CommandToolTest {

  private Path testFolder = Paths.get("src", "test", "resources").toAbsolutePath();

  @Order(1)
  @Test
  void runJavaTest() throws IOException, InterruptedException {
    assertEquals(0, CommandTool.run("java", "-version"));
  }

  @Order(2)
  @Test
  void runPythonTest() throws InterruptedException, IOException {
    try {
      assertEquals(0, CommandTool.run("python", "--version"));
    } catch (IOException e) {
      assertEquals(0, CommandTool.run("python3", "--version"));
    }
  }

  @Order(3)
  @Test
  void runUnknownCommandTest() {
    assertThrows(IOException.class, () -> CommandTool.run("dlahdjlkhasjk"));
  }

  @Order(10)
  @Test
  void runPythonScriptTest() throws IOException, InterruptedException {
    try {
      assertEquals(0, CommandTool.run("python", this.testFolder.resolve("hello-world.py").toString()));
    } catch (IOException e) {
      assertEquals(0, CommandTool.run("python3", this.testFolder.resolve("hello-world.py").toString()));
    }
  }

  @Order(11)
  @Test
  void runPythonScriptWithWorkingFolderTest() throws IOException, InterruptedException {
    try {
      assertEquals(0, CommandTool.runWithWorkingFolder(this.testFolder.toString(), "python", "hello-world.py"));
    } catch (IOException e) {
      assertEquals(0, CommandTool.runWithWorkingFolder(this.testFolder.toString(), "python3", "hello-world.py"));
    }
  }

  @Order(20)
  @Test
  void runShellScriptWithWorkingFolderTest() throws IOException, InterruptedException {
    if (OSTool.isUnix() || OSTool.isMac()) {
      assertEquals(0, CommandTool.runWithWorkingFolder(this.testFolder.toString(), "sh", "hello-world.sh"));
    } else if (OSTool.isWindows()) {
      assertEquals(0, CommandTool.runWithWorkingFolder(this.testFolder.toString(), "cmd.exe", "hello-world.cmd"));
    } else {
      fail("Unknown OS");
    }
  }

  @Order(20)
  @Test
  void runShellScriptTest() throws IOException, InterruptedException {
    if (OSTool.isUnix() || OSTool.isMac()) {
      assertEquals(0, CommandTool.run("sh", this.testFolder.resolve("hello-world.sh").toString()));
    } else if (OSTool.isWindows()) {
      assertEquals(0, CommandTool.run("cmd.exe", this.testFolder.resolve("hello-world.cmd").toString()));
    } else {
      fail("Unknown OS");
    }
  }

}
