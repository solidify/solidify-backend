/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - IndexDefinition.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.index;

/**
 * Contains index values defined in application properties.
 * <p>
 * It contains the name of the index, the name of the file containing the fields mapping and the name of the file containing the index settings.
 */
public class IndexDefinition {

  private String indexName;
  private String indexMapping;
  private String indexSettings;

  private boolean useAlias;

  public IndexDefinition(String name, String mapping) {
    this(name, mapping, null);
  }

  public IndexDefinition(String name, String mapping, String settings) {
    this(name, mapping, settings, false);
  }

  public IndexDefinition(String name, String mapping, String settings, boolean useAlias) {
    this.indexName = name;
    this.indexMapping = mapping;
    this.indexSettings = settings;
    this.useAlias = useAlias;
  }

  public String getIndexName() {
    return this.indexName;
  }

  public String getIndexMapping() {
    return this.indexMapping;
  }

  public String getIndexSettings() {
    return this.indexSettings;
  }

  public boolean isUseAlias() {
    return this.useAlias;
  }
}
