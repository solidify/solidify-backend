/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - FileToolTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import ch.unige.solidify.util.FileTool;

class FileToolTest {

  private final Path pathAB = Path.of("a", "b");
  private final Path pathAE = Path.of("a", "e");
  private final Path pathABC = this.pathAB.resolve(Path.of("c"));
  private final Path pathABD = this.pathAB.resolve(Path.of("d"));
  private final Path pathAEF = this.pathAE.resolve(Path.of("f"));

  @Test
  void copyTest() throws IOException {
    final Path sourcePath = new ClassPathResource("xslt/datacite2oai_dc.xsl").getFile().toPath();
    final Path destPath = Paths.get(System.getProperty("java.io.tmpdir"), "tmpFile");
    FileTool.copyFile(sourcePath, destPath);
    assertArrayEquals(Files.readAllBytes(sourcePath), Files.readAllBytes(destPath));
  }

  @Test
  void deleteRecursivelyEmptyDirectoriesWithFiles() throws IOException {
    final Path testDir = this.createTestDirectories();
    final Path newFile = Files.createFile(testDir.resolve(this.pathABC.resolve("test.txt")));
    FileTool.deleteRecursivelyEmptyDirectories(testDir);
    assertTrue(Files.exists(testDir.resolve(this.pathAB)));
    assertTrue(Files.exists(testDir.resolve(this.pathABC)));
    assertTrue(Files.exists(newFile));
    assertFalse(Files.exists(testDir.resolve(this.pathAE)));
    assertFalse(Files.exists(testDir.resolve(this.pathABD)));
    assertFalse(Files.exists(testDir.resolve(this.pathAEF)));
  }

  @Test
  void deleteRecursivelyEmptyDirectoriesWithoutFiles() throws IOException {
    Path testDir = this.createTestDirectories();
    FileTool.deleteRecursivelyEmptyDirectories(testDir);
    assertFalse(Files.exists(testDir.resolve(testDir)));
  }

  @Test
  void sortFolderList() throws IOException {
    Path testDir = this.createTestDirectories();
    List<Path> listFiles = new ArrayList<>();
    listFiles.addAll(FileTool.readFolder(testDir.resolve(this.pathAB)));
    Collections.sort(listFiles);
    assertTrue(listFiles.isEmpty());
  }

  private Path createTestDirectories() throws IOException {
    final Path testDir = Paths.get(System.getProperty("java.io.tmpdir") + File.separator + UUID.randomUUID());
    Files.createDirectories(testDir.resolve(this.pathABC));
    Files.createDirectories(testDir.resolve(this.pathABD));
    Files.createDirectories(testDir.resolve(this.pathAEF));
    return testDir;
  }
}
