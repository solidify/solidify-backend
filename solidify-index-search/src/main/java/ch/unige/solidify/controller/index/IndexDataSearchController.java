/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Search - IndexDataSearchController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller.index;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import ch.unige.solidify.IndexConstants;
import ch.unige.solidify.index.indexing.IndexingService;
import ch.unige.solidify.model.index.IndexFieldAlias;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.SearchCondition;
import ch.unige.solidify.rest.SearchConditionType;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.service.IndexFieldAliasInfoProvider;
import ch.unige.solidify.util.SearchConditionTool;
import ch.unige.solidify.util.StringTool;

@RootPermissions
public abstract class IndexDataSearchController<T extends IndexMetadata> extends IndexDataReadOnlyController<T> {

  private static final Logger log = LoggerFactory.getLogger(IndexDataSearchController.class);

  protected final IndexFieldAliasInfoProvider indexFieldAliasInfoProvider;

  protected IndexDataSearchController(IndexingService<T> indexResourceService, IndexFieldAliasInfoProvider indexFieldAliasInfoProvider) {
    super(indexResourceService);
    this.indexFieldAliasInfoProvider = indexFieldAliasInfoProvider;
  }

  @GetMapping("/" + ActionName.SEARCH)
  public HttpEntity<RestCollection<T>> search(
          @RequestParam(required = false) String query,
          Pageable pageable) {
    final List<SearchCondition> searchConditions = new ArrayList<>();
    // Add query condition if given
    SearchConditionTool.addQueryCondition(searchConditions, query);
    return this.searchPost(searchConditions, pageable);
  }

  @PostMapping(value = "/" + ActionName.SEARCH, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public HttpEntity<RestCollection<T>> searchPost(@RequestBody MultiValueMap<String, String> params) {
    log.trace("Search parameters: {}", params);

    // Pageable
    final Pageable pageable = this.initPageable(params);

    // Search conditions
    final List<SearchCondition> searchConditions = SearchConditionTool
            .generateFromString(this.getParameter(params, IndexConstants.SEARCH_CONDITIONS));

    return this.searchPost(searchConditions, pageable);
  }

  @PostMapping(value = "/" + ActionName.SEARCH)
  public HttpEntity<RestCollection<T>> searchPost(
          @RequestBody(required = false) List<SearchCondition> searchConditions,
          Pageable pageable) {

    // Replace aliases by real index field names
    searchConditions = this.replaceAliasesByIndexFieldName(searchConditions);

    // Eventual sorted field may be an index field alias as well
    pageable = this.replaceAliasByIndexFieldName(pageable);

    // callback method
    searchConditions = this.beforeSearch(searchConditions);

    // Get facets to return
    List<FacetRequest> facetsList = this.indexFieldAliasInfoProvider.getFacetRequestsListForIndex(this.indexResourceService.getIndexName());

    // Get fields to include/exclude from search response
    FieldsRequest fieldsRequest = this.getDefaultFieldsRequest();

    // Perform the search
    final FacetPage<T> results = this.indexResourceService.search(searchConditions, facetsList, pageable, fieldsRequest);
    return this.page2Collection(results, pageable);
  }

  private Pageable initPageable(MultiValueMap<String, String> params) {
    int size = RestCollectionPage.DEFAULT_SIZE_PAGE;
    int page = 0;
    Sort sort = Sort.unsorted();
    if (this.getParameter(params, ActionName.SIZE) != null) {
      size = Integer.parseInt(this.getParameter(params, ActionName.SIZE));
    }
    if (this.getParameter(params, ActionName.PAGE) != null) {
      page = Integer.parseInt(this.getParameter(params, ActionName.PAGE));
    }
    if (this.getParameter(params, ActionName.SORT) != null) {
      String value = this.getParameter(params, ActionName.SORT);
      String sortValue = value.substring(0, value.indexOf(','));
      String direction = value.substring(value.indexOf(',') + 1);
      sort = Sort.by(Sort.Direction.fromString(direction), sortValue);
    }

    return PageRequest.of(page, size, sort);
  }

  /**
   * Replaces searched conditions fields names by their real name in index if they correspond to an IndexFieldAlias
   *
   * @param searchConditions
   * @return
   */
  protected List<SearchCondition> replaceAliasesByIndexFieldName(List<SearchCondition> searchConditions) {
    for (SearchCondition searchCondition : searchConditions) {
      if (searchCondition.getType() == SearchConditionType.NESTED_BOOLEAN) {
        this.replaceAliasesByIndexFieldName(searchCondition.getNestedConditions());
      } else {
        IndexFieldAlias fieldAlias = this.indexFieldAliasInfoProvider.findByIndexNameAndAlias(
                this.indexResourceService.getIndexName(),
                searchCondition.getField());
        if (fieldAlias != null) {
          searchCondition.setField(fieldAlias.getField());
        }
      }
    }
    return searchConditions;
  }

  /**
   * If pageable is sorted, replace the sorted field name by its real name if it exists as an IndexFieldAlias
   *
   * @param pageable
   * @return
   */
  protected Pageable replaceAliasByIndexFieldName(Pageable pageable) {
    if (!pageable.getSort().isEmpty() && pageable.getSort().isSorted()) {
      pageable = PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), this.adaptSortProperties(pageable.getSort()));
    }
    return pageable;
  }

  protected Sort adaptSortProperties(Sort sort) {
    // create a copy from the sort since the list of orders are not modifiable.
    List<org.springframework.data.domain.Sort.Order> newOrderList = new ArrayList<>(sort.toList());
    for (Sort.Order order : sort.toList()) {
      String newNameProperty = this.getOrderField(order.getProperty());
      if (!StringTool.isNullOrEmpty(newNameProperty)) {
        newOrderList.remove(order);
        newOrderList.add(new Sort.Order(order.getDirection(), newNameProperty));
      }
    }
    return Sort.by(newOrderList);
  }

  protected String getOrderField(String alias) {
    IndexFieldAlias indexFieldAlias = this.indexFieldAliasInfoProvider.findByIndexNameAndAlias(
            this.indexResourceService.getIndexName(),
            alias);
    if (indexFieldAlias != null) {
      return indexFieldAlias.getField();
    }
    return null;
  }

  /**
   * Return a FieldsRequest allowing to indicate fields that must be included/excluded from the search response.
   * <p>
   * By default, it returns an empty FieldsRequest. Override at project level to add specific content.
   *
   * @return
   */
  protected FieldsRequest getDefaultFieldsRequest() {
    return new FieldsRequest();
  }

  /**
   * Override to implement custom logic in search conditions before searching the index
   *
   * @param searchConditions
   */
  protected List<SearchCondition> beforeSearch(List<SearchCondition> searchConditions) {
    // do nothing by default
    return searchConditions;
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    w.add(linkTo(methodOn(this.getClass()).searchPost(null, null)).withRel(ActionName.SEARCH));
  }
}
