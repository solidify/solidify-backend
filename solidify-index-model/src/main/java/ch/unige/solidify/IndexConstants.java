/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Model - IndexConstants.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify;

public class IndexConstants {

  private IndexConstants() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

  // Resources
  public static final String INDEX_MODULE = "index";
  public static final String INDEX_SEPARATOR = ",";

  public static final String INDEX_FIELD_ALIAS = "index-field-aliases";
  public static final String SETTING = "settings";
  public static final String INDEX = "indexes";
  public static final String METADATA = "metadata";

  // Actions
  public static final String DELETE_ALL = "delete-all";

  // Default Values
  public static final int ORDER_INCREMENT = 10;

  // Database column name
  public static final String DB_INDEX_FIELD_ALIAS_ID = "indexFieldAliasId";

  // Message
  public static final String BAD_SEARCH_REQUEST = "Bad search request";

  // Search
  public static final String SEARCH_QUERY = "query";
  public static final String SEARCH_FIELD = "field";
  public static final String SEARCH_FROM = "from";
  public static final String SEARCH_TO = "to";
  public static final String SEARCH_SET = "set";
  public static final String SEARCH_CONDITIONS = "conditions";
  public static final String FACET_ORDER_FIELD = "facetOrder";
}
