/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ValidationError.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.validation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import com.fasterxml.jackson.annotation.JsonInclude;

import ch.unige.solidify.util.StringTool;

public class ValidationError {

  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private final List<FieldValidationError> fieldErrors = new ArrayList<>();

  private String mainErrorMessage;

  /****************/

  public ValidationError() {

  }

  public ValidationError(BindingResult errors) {

    final String errorMessage = "Validation failed on " + errors.getObjectName() + ": "
            + errors.getErrorCount() + " error(s)";
    this.setMainErrorMessage(errorMessage);

    this.addErrors(errors);
  }

  public ValidationError(String errorMessage) {
    this.setMainErrorMessage(errorMessage);
  }

  public ValidationError(String errorMessage, BindingResult errors) {
    this.setMainErrorMessage(errorMessage);
    this.addErrors(errors);
  }

  public void addError(ObjectError error) {

    final String fieldName = error instanceof FieldError ? ((FieldError) error).getField() : error.getObjectName();
    final String errorMessage = error.getDefaultMessage();
    final String code = error.getCode();

    if (StringTool.isNullOrEmpty(code) ) {
      this.addError(fieldName, errorMessage);
    } else {
      this.addError(fieldName, errorMessage, code);
    }
  }

  public void addError(String fieldName, String errorMessage) {

    FieldValidationError fieldError = this.getFieldValidationError(fieldName);
    if (fieldError == null) {

      fieldError = new FieldValidationError(fieldName, errorMessage);
      this.fieldErrors.add(fieldError);

    } else {
      fieldError.addErrorMessage(errorMessage);
    }
  }

  public void addError(String fieldName, String errorMessage, String code) {

    FieldValidationError fieldError = this.getFieldValidationError(fieldName);
    if (fieldError == null) {

      fieldError = new FieldValidationError(fieldName, errorMessage, code);
      this.fieldErrors.add(fieldError);

    } else {
      fieldError.addErrorMessage(errorMessage, code);
    }
  }

  /****************/

  public void addErrors(BindingResult errors) {

    final List<ObjectError> errorsList = errors.getAllErrors();

    for (final ObjectError error : errorsList) {
      this.addError(error);
    }
  }

  public void clearErrors() {
    this.fieldErrors.clear();
  }

  public List<FieldValidationError> getErrors() {
    return this.fieldErrors;
  }

  public FieldValidationError getFieldValidationError(String fieldname) {

    for (final FieldValidationError fieldError : this.fieldErrors) {
      if (fieldError.getFieldName().equals(fieldname)) {
        return fieldError;
      }
    }

    return null;
  }

  public String getMainErrorMessage() {
    return this.mainErrorMessage;
  }

  public boolean hasError() {
    return !this.fieldErrors.isEmpty();
  }

  public void setMainErrorMessage(String mainErrorMessage) {
    this.mainErrorMessage = mainErrorMessage;
  }
}
