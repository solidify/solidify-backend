/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SubResourceController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Tool;
import ch.unige.solidify.service.ResourceService;
import ch.unige.solidify.specification.SolidifySpecification;

public abstract class SubResourceController<T extends Resource, V extends Resource> extends SolidifyController {

  @Autowired
  protected ResourceService<T> resourceService;

  @Autowired
  protected ResourceService<V> subResourceService;

  protected HttpEntity<RestCollection<V>> getSubResourcesCollectionAsResponseEntity(String parentId, SolidifySpecification<V> specification,
          Pageable pageable) {
    final Page<V> list = this.subResourceService.findAll(specification, pageable);

    for (final V v : list) {
      this.addLinks(parentId, v);
    }
    final RestCollection<V> collection = this.processPageAndLinks(list, parentId, pageable);
    return new ResponseEntity<>(collection, HttpStatus.OK);
  }

  protected RestCollection<V> processPageAndLinks(Page<V> list, String parentId, Pageable pageable) {
    final RestCollection<V> collection = new RestCollection<>(list, pageable);
    collection.add(linkTo(this.getClass(), parentId).withSelfRel());
    collection.add((Tool.parentLink((linkTo(this.getClass(), parentId)).toUriComponentsBuilder()))
            .withRel(ActionName.PARENT));
    this.addSortLinks(linkTo(this.getClass(), parentId, pageable), collection);
    this.addPageLinks(linkTo(this.getClass(), parentId), collection, pageable);
    this.addOthersLinks(parentId, collection);
    return collection;
  }

  protected <W extends RepresentationModel<W>> void addOthersLinks(String parentId, W w) {
    // Do nothing
    // Could be overridden by subclass
  }

  protected abstract void addLinks(String parentid, V v);
}
