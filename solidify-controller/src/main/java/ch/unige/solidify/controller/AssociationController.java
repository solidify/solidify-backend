/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - AssociationController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyHttpErrorException;
import ch.unige.solidify.exception.SolidifyResourceNotFoundException;
import ch.unige.solidify.exception.SolidifyValidationException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.security.RootPermissions;
import ch.unige.solidify.specification.JoinedTableInValues;
import ch.unige.solidify.specification.SolidifySpecification;
import ch.unige.solidify.validation.ValidationError;

@RootPermissions
public abstract class AssociationController<T extends Resource, V extends Resource> extends SubResourceController<T, V> {

  @GetMapping
  public HttpEntity<RestCollection<V>> list(@PathVariable String parentid, @ModelAttribute V filterItem, Pageable pageable) {

    // Ensure parent exists
    this.resourceService.existsByIdOrThrowException(parentid);

    SolidifySpecification<V> specification = this.subResourceService.getSpecification(filterItem);

    // add inner join to parent
    specification.addJoinTableInValues(new JoinedTableInValues(this.getParentFieldName(), SolidifyConstants.DB_RES_ID, Arrays.asList(parentid)));

    return this.getSubResourcesCollectionAsResponseEntity(parentid, specification, pageable);
  }

  @GetMapping(SolidifyConstants.URL_ID)
  public HttpEntity<V> get(@PathVariable String parentid, @PathVariable String id) {
    V subResource = this.findSubResource(parentid, id);
    this.addLinks(parentid, subResource);
    return new ResponseEntity<>(subResource, HttpStatus.OK);
  }

  @PostMapping
  public HttpEntity<List<V>> create(@PathVariable String parentid, @RequestBody String[] ids) {
    final T parentItem = this.resourceService.findOne(parentid);

    final List<V> list = new ArrayList<>();
    for (final String id : ids) {
      final V childItem = this.subResourceService.findOne(id);

      // Check that the relation doesn't already exist
      Optional<V> subResourceOpt = this.findOptionalSubResource(parentid, id);
      if (subResourceOpt.isPresent()) {
        throw new SolidifyValidationException(new ValidationError("There is already a relation between " + parentid + " and " + id));
      }

      this.addChildOnParent(parentItem, childItem);
      list.add(childItem);
    }

    this.resourceService.save(parentItem);
    return new ResponseEntity<>(list, HttpStatus.CREATED);
  }

  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<V> update(@PathVariable String parentid, @PathVariable String id, @RequestBody V v2) {
    return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
  }

  @DeleteMapping(SolidifyConstants.URL_ID)
  public ResponseEntity<Void> delete(@PathVariable String parentid, @PathVariable String id) {
    return this.deleteList(parentid, new String[] { id });
  }

  @DeleteMapping
  public ResponseEntity<Void> deleteList(@PathVariable String parentid, @RequestBody String[] ids) {
    boolean noError = true;
    final T parentItem = this.resourceService.findOne(parentid);

    for (final String id : ids) {
      V subResource = this.findSubResource(parentid, id);
      noError &= this.removeChildFromParent(parentItem, subResource);
    }

    if (noError) {
      this.resourceService.save(parentItem);
      return new ResponseEntity<>(HttpStatus.OK);
    }
    throw new SolidifyHttpErrorException(HttpStatus.GONE, "");
  }

  @Override
  protected void addLinks(String parentid, V v) {
    v.removeLinks();
    v.addLinks(linkTo(methodOn(this.getClass()).list(parentid, null, null)), false, false);
  }

  private V findSubResource(String parentId, String id) {
    return this.findOptionalSubResource(parentId, id)
            .orElseThrow(() -> new SolidifyResourceNotFoundException("Child item with id " + id + " not found"));
  }

  private Optional<V> findOptionalSubResource(String parentId, String id) {
    // First check if sub resource exists
    this.subResourceService.existsByIdOrThrowException(id);

    // Then check if it is linked with parent resource
    V emptySubResource = this.getEmptyChildResourceObject();
    emptySubResource.setResId(id);
    SolidifySpecification<V> childSpecification = this.subResourceService.getSpecification(emptySubResource);
    // generate a where clause with resId = id
    childSpecification.setFilterOnRootResId(true);
    // add inner join to parent
    childSpecification.addJoinTableInValues(
            new JoinedTableInValues(this.getParentFieldName(), SolidifyConstants.DB_RES_ID, Arrays.asList(parentId)));

    final Page<V> list = this.subResourceService.findAll(childSpecification, Pageable.unpaged());
    if (list.getTotalElements() == 1) {
      return Optional.of(list.getContent().get(0));
    } else {
      return Optional.empty();
    }
  }

  protected abstract String getParentFieldName();

  public abstract V getEmptyChildResourceObject();

  protected abstract boolean addChildOnParent(T parentResource, V childResource);

  protected abstract boolean removeChildFromParent(T parentResource, V childResource);
}
