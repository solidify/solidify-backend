/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - CryptoConverter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

import ch.unige.solidify.model.security.CipherPasswordProvider;
import ch.unige.solidify.util.StringCipherTools;

@Converter
public class CryptoConverter implements AttributeConverter<String, String> {

  private final CipherPasswordProvider cipherPasswordProvider;

  public CryptoConverter(CipherPasswordProvider cipherPasswordProvider) {
    this.cipherPasswordProvider = cipherPasswordProvider;
  }

  @Override
  public String convertToDatabaseColumn(String attribute) {
    return StringCipherTools.encrypt(attribute, this.cipherPasswordProvider.getCipherPassword());
  }

  @Override
  public String convertToEntityAttribute(String dbData) {
    return StringCipherTools.decrypt(dbData, this.cipherPasswordProvider.getCipherPassword());
  }
}
