/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Client - ResourceClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.lang.reflect.Array;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.model.SolidifyIdentifierType;
import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.RestCollectionPage;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.util.AbstractRestClientTool;
import ch.unige.solidify.util.CollectionTool;
import ch.unige.solidify.util.StringTool;

public abstract class ResourceClientService<T extends Resource>
        extends ResourceBaseClientService<T>
        implements ResourceClientServiceInterface<T> {
  protected static final String ID_LABEL = SolidifyConstants.DB_RES_ID;
  protected static final String ID_PARAM = "{" + ID_LABEL + "}";

  protected ResourceClientService(
          String application,
          String module,
          String resourcePath,
          Class<T> resourceClass,
          AbstractRestClientTool restClientTool,
          Environment env) {
    super(application, module, resourcePath, resourceClass, restClientTool, env);
  }

  protected ResourceClientService(
          String application,
          String module,
          String resourcePath,
          Class<T> resourceClass,
          AbstractRestClientTool restClientTool,
          Environment env,
          boolean isResourceServer) {
    super(application, module, resourcePath, resourceClass, restClientTool, env);
    this.setIsResourceServer(isResourceServer);
  }

  /******************/
  /** CRUD methods **/
  /******************/

  /**
   * CREATE
   **/
  @Override
  public T create(T resource) {
    final String url = this.getResourceUrl();
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    return restTemplate.postForObject(url, resource, this.getResourceClass());
  }

  /**
   * GET
   **/
  @Override
  public T findOne(String resId) {
    final String url = this.getResourceUrl() + "/" + ID_PARAM;
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    return this.getObjectFromUrl(url, params);
  }

  public <V> V findByIdentifierType(String url, SolidifyIdentifierType identifierType, String identifierValue, Class<V> clazz) {
    StringBuilder urlBuilder = new StringBuilder(url);
    urlBuilder.append("/")
            .append(ID_PARAM)
            .append("?")
            .append(SolidifyConstants.IDENTIFIER_TYPE_PARAM)
            .append("={")
            .append(SolidifyConstants.IDENTIFIER_TYPE_PARAM)
            .append("}");
    final Map<String, String> params = new HashMap<>();
    params.put(SolidifyConstants.DB_RES_ID, identifierValue);
    params.put(SolidifyConstants.IDENTIFIER_TYPE_PARAM, identifierType.getName());
    return this.getObjectFromUrl(urlBuilder.toString(), params, clazz);
  }

  /**
   * LIST
   **/
  protected List<T> findAll(Class<T> itemClass) {
    final String url = this.getResourceUrl() + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, itemClass);
  }

  protected List<T> findAllWithParameters(Class<T> itemClass, MultiValueMap<String, String> parameters) {
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(this.getResourceUrl());
    if (!parameters.containsKey(SIZE_LABEL)) {
      parameters.add(SIZE_LABEL, String.valueOf(RestCollectionPage.MAX_SIZE_PAGE));
    }
    uriBuilder.queryParams(parameters);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(uriBuilder.toUriString(), String.class);
    return CollectionTool.getList(jsonString, itemClass);
  }

  protected List<T> findAllWithPagination(Class<T> itemClass) {
    final String url = this.getResourceUrl() + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    RestCollectionPage page = CollectionTool.getPage(jsonString);
    List<T> listOfAllItems = CollectionTool.getList(jsonString, itemClass);
    for (int i = 1; i <= page.getTotalPages() - 1; i++) {
      String urlWithPage = this.getResourceUrl() + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE + "&page=" + i;
      String forObject = restTemplate.getForObject(urlWithPage, String.class);
      listOfAllItems.addAll(CollectionTool.getList(forObject, itemClass));
    }
    return listOfAllItems;
  }

  /**
   * UPDATE
   **/
  @Override
  public T update(String resId, T resource) {
    final String url = this.getResourceUrl() + "/" + ID_PARAM;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    return restTemplate.patchForObject(url, new HttpEntity<>(resource), this.getResourceClass(), params);
  }

  @Override
  public T update(String resId, T resource, List<String> propertiesToUpdate) {
    final String url = this.getResourceUrl() + "/" + ID_PARAM;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);

    // send given properties only
    Map<String, Object> newProperties = this.objectToMap(resource, propertiesToUpdate);
    return restTemplate.patchForObject(url, new HttpEntity<>(newProperties), this.getResourceClass(), params);
  }

  /**
   * DELETE
   **/
  @Override
  public void delete(String resId) {
    final String url = this.getResourceUrl() + "/" + ID_PARAM;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    restTemplate.delete(url, params);
  }

  @Override
  public void deleteList(String[] ids) {
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.exchange(this.getResourceUrl(), HttpMethod.DELETE, new HttpEntity<>(ids), String.class);
  }

  /**
   * SEARCH
   **/
  protected List<T> searchByProperties(Map<String, String> params, Class<T> itemClass) {
    StringBuilder urlBuilder = new StringBuilder();
    urlBuilder.append(this.getResourceUrl())
            .append(SIZE_PARAM)
            .append(RestCollectionPage.MAX_SIZE_PAGE);
    for (final Map.Entry<String, String> entry : params.entrySet()) {
      urlBuilder.append("&")
              .append(entry.getKey())
              .append("=")
              .append(entry.getValue());
    }
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(urlBuilder.toString(), String.class);
    return CollectionTool.getList(jsonString, itemClass);
  }

  protected List<T> searchByProperties(String url, Map<String, String> params, Class<T> itemClass) {
    StringBuilder urlBuilder = new StringBuilder(url);
    for (final Map.Entry<String, String> entry : params.entrySet()) {
      urlBuilder.append("&")
              .append(entry.getKey())
              .append("=")
              .append(entry.getValue());
    }
    final RestTemplate restTemplate = this.restClientTool.getClient();
    final String jsonString = restTemplate.getForObject(urlBuilder.toString(), String.class);
    return CollectionTool.getList(jsonString, itemClass);
  }

  protected List<T> search(String search, String matchType, Class<T> itemClass) {
    String url = this.getResourceUrl() + "/search?search=" + search;
    if (!StringTool.isNullOrEmpty(matchType)) {
      url += "&match=" + matchType;
    }
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, itemClass);
  }

  /************************************/
  /** CRUD methods for sub-resources **/
  /************************************/

  /**
   * CREATE
   **/
  protected <U extends Resource> void addItem(String resId, String itemTypeLabel, Class<U> itemClass, String... ids) {
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.postForObject(this.getAssociationUrl(itemTypeLabel), ids, itemClass, params);
  }

  protected <U extends Resource> void addItem(String resId, String itemTypeLabel, String itemId, Class<U> itemClass) {
    @SuppressWarnings("unchecked")
    final Class<? extends Resource[]> arrayType = (Class<? extends Resource[]>) Array.newInstance(itemClass, 0).getClass();
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.postForObject(this.getAssociationUrl(itemTypeLabel), new String[] { itemId }, arrayType, params);
  }

  protected <U extends Resource> U addItem(String resId, String itemTypeLabel, U item, Class<U> itemClass) {
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    return restTemplate.postForObject(this.getAssociationUrl(itemTypeLabel), item, itemClass, resId);
  }

  protected <U extends Resource> void addRelationItem(String resId, String itemTypeLabel, Class<U> itemClass, String... ids) {
    @SuppressWarnings("unchecked")
    final Class<? extends Resource[]> arrayType = (Class<? extends Resource[]>) Array.newInstance(itemClass, 0).getClass();
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.postForObject(this.getAssociationUrl(itemTypeLabel), ids, arrayType, params);
  }

  protected <U extends Resource> void addRelationItem(String resId, String itemTypeLabel, Class<U> itemClass, String itemId, Object joinResource) {
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.postForObject(this.getAssociationUrl(itemTypeLabel) + "/" + itemId, joinResource, itemClass, params);
  }

  protected <U extends Resource> void updateRelationItem(String resId, String itemTypeLabel, Class<U> itemClass, JoinResource<?> joinResource) {
    final Class<? extends Resource[]> arrayType = (Class<? extends Resource[]>) Array.newInstance(itemClass, 0).getClass();
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.patchForObject(this.getAssociationUrl(itemTypeLabel), joinResource, arrayType, params);
  }

  /**
   * GET
   **/
  protected <U> U findOneLinkedResource(String resId, String itemTypeLabel, Class<U> itemClass, String childId) {
    final String url = this.getResourceUrl()
            + "/" + ID_PARAM
            + "/" + itemTypeLabel
            + "/" + childId;
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    final RestTemplate restTemplate = this.restClientTool.getClient();
    return restTemplate.getForObject(url, itemClass, params);
  }

  /**
   * LIST
   **/
  protected <U> List<U> findAllLinkedResources(String resId, String itemTypeLabel, Class<U> itemClass) {
    final String url = this.getResourceUrl() + "/" + ID_PARAM + "/" + itemTypeLabel + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE;
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class, params);
    return CollectionTool.getList(jsonString, itemClass);
  }

  /**
   * UPDATE
   **/
  protected <U extends Resource> U updateSubItem(String resId, String subItemTypeLabel, Class<U> subItemClass, String subItemId,
          Map<String, Object> mapProperties) {
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    return restTemplate.patchForObject(
            this.getAssociationUrl(subItemTypeLabel) + "/" + subItemId,
            new HttpEntity<>(mapProperties),
            subItemClass,
            params);
  }

  /**
   * DELETE
   **/
  protected void removeItem(String parentId, String itemTypeLabel, String itemId) {
    final String url = this.getResourceUrl()
            + "/" + parentId
            + "/" + itemTypeLabel
            + "/" + itemId;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    restTemplate.delete(url);
  }

  protected void removeItem(String parentId, String itemTypeLabel, String itemId, String... subItemIds) {
    if (subItemIds.length == 0) {
      this.removeItem(parentId, itemTypeLabel, itemId);
    } else {
      final Map<String, String> params = new HashMap<>();
      params.put(ID_LABEL, parentId);
      final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
      restTemplate.exchange(
              this.getAssociationUrl(itemTypeLabel + "/" + itemId),
              HttpMethod.DELETE,
              new HttpEntity<>(subItemIds),
              String.class,
              params);
    }
  }

  protected T uploadFile(String resId, String uploadAction, org.springframework.core.io.Resource file) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + uploadAction;
    return this.uploadFile(url, file, this.getResourceClass());
  }

  protected T uploadFile(String resId, String uploadAction, org.springframework.core.io.Resource file, String mimeType) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + uploadAction + "?" + SolidifyConstants.MIME_TYPE_PARAM + "=" + mimeType;
    return this.uploadFile(url, file, this.getResourceClass());
  }

  protected void downloadFile(String resId, String downloadAction, Path path) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + downloadAction;
    this.download(url, path);
  }

  protected void deleteAction(String resId, String deleteAction) {
    final String url = this.getResourceUrl() + "/" + resId + "/" + deleteAction;
    final RestTemplate restTemplate = this.restClientTool.getClient();
    restTemplate.delete(url);
  }

  /********************/
  /** Action methods **/
  /********************/
  protected T doAction(String action) {
    final String url = this.getActionUrl(action);
    return this.getObjectFromUrl(url, new HashMap<>());
  }

  protected Result postAction(String resId, String action) {
    String url = this.getResourceUrl()
            + "/" + resId
            + "/" + action;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    return restTemplate.postForObject(url, null, Result.class);
  }

  protected <U> U postAction(String resId, String action, Class<U> itemClass) {
    String url = this.getResourceUrl()
            + "/" + resId
            + "/" + action;
    final RestTemplate restTemplate = this.restClientTool.getClient(this.getResourceUrl());
    return restTemplate.postForObject(url, null, itemClass);
  }

  protected List<T> doActionOnList(String action, Class<T> itemClass) {
    final String url = this.getActionUrl(action) + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, itemClass);
  }

  protected List<T> docActionOnListWithParameter(String action, Class<T> itemClass, String parameterName, String parameterValue) {
    final String url = this.getActionUrl(action) + SIZE_PARAM + RestCollectionPage.MAX_SIZE_PAGE + "&" + parameterName + "=" + parameterValue;
    final RestTemplate restTemplate = this.getRestClientTool().getClient(this.getResourceUrl());
    final String jsonString = restTemplate.getForObject(url, String.class);
    return CollectionTool.getList(jsonString, itemClass);
  }

  protected T doActionOnId(String resId, String action) {
    final String url = this.getAssociationUrl(action);
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    return this.getObjectFromUrl(url, params);
  }

  protected T doActionWithId(String resId, String action) {
    final String url = this.getActionUrlWithId(action);
    final Map<String, String> params = new HashMap<>();
    params.put(ID_LABEL, resId);
    return this.getObjectFromUrl(url, params);
  }

  /***********************/
  /** Protected methods **/
  /***********************/
  protected String getAssociationUrl(String resourceLabel) {
    return this.getResourceUrl() + "/" + ID_PARAM + "/" + resourceLabel;
  }

  /*********************/
  /** Private methods **/
  /*********************/
  private String getActionUrl(String actionLabel) {
    return this.getResourceUrl() + "/" + actionLabel;
  }

  private String getActionUrlWithId(String actionLabel) {
    return this.getResourceUrl() + "/" + actionLabel + "/{" + ID_LABEL + "}";
  }

}
