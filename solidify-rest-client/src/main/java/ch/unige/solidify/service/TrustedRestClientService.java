/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - TrustedRestClientService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Profile;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResponseErrorHandler;

import ch.unige.solidify.rest.RestCollection;
import ch.unige.solidify.rest.Result;
import ch.unige.solidify.security.TokenUsage;
import ch.unige.solidify.util.TrustedRestClientTool;

@Profile({ "client-backend", "test" })
@Service
public class TrustedRestClientService extends SolidifyRestClientService {
  private static final String MAX_ATTEMPTS = "${trustedClient.retry.maxAttempts:4}";
  private static final String BACKOFF_DELAY = "${trustedClient.retry.backoffDelay:1000}";
  private static final String BACKOFF_MULTIPLIER = "${trustedClient.retry.backoffDelay:2.5}";

  public TrustedRestClientService(TrustedRestClientTool restClientTool, ResponseErrorHandler responseErrorHandler) {
    super(restClientTool, responseErrorHandler);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public void deleteResource(String url) {
    super.deleteResource(url);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public void deleteResourceList(String url, List<String> ids) {
    super.deleteResourceList(url, ids);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public String getResource(String url) {
    return super.getResource(url);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T> T getResource(String url, Class<T> clazz) {
    return super.getResource(url, clazz);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public String getResource(String url, Pageable pageable) {
    return super.getResource(url, pageable);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T> RestCollection<T> getResourceList(String url, Class<T> clazz, Pageable pageable) {
    return super.getResourceList(url, clazz, pageable);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T> List<T> getAllResources(String url, Class<T> clazz) {
    return super.getAllResources(url, clazz);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public HttpStatusCode headResource(String url) {
    return super.headResource(url);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public long headContentLength(String url, TokenUsage withToken) {
    return super.headContentLength(url, withToken);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T> T patchResource(String url, T updatedResource, Class<T> clazz) {
    return super.patchResource(url, updatedResource, clazz);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T> T patchResource(String url, Map<String, Object> updatedResource, Class<T> clazz) {
    return super.patchResource(url, updatedResource, clazz);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T> T postResource(String url, Class<T> clazz) {
    return super.postResource(url, clazz);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T> T postResource(String url, T newResource, Class<T> clazz) {
    return super.postResource(url, newResource, clazz);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T> T postObject(String url, Object request, Class<T> responseType) {
    return super.postObject(url, request, responseType);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public Result postResourceAction(String url) {
    return super.postResourceAction(url);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T, V> T postResourceOther(String url, V otherResource, Class<T> clazz) {
    return super.postResourceOther(url, otherResource, clazz);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public <T> T uploadContent(String url, Resource resource, String parameterName, Class<T> clazz) {
    return super.uploadContent(url, resource, parameterName, clazz);
  }

  @Override
  @Retryable(value = HttpClientErrorException.Unauthorized.class, maxAttemptsExpression = MAX_ATTEMPTS, backoff = @Backoff(delayExpression = BACKOFF_DELAY, multiplierExpression = BACKOFF_MULTIPLIER))
  public void downloadContent(String url, Path destData, TokenUsage tokenUsage) {
    super.downloadContent(url, destData, tokenUsage);
  }

}
