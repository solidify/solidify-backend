/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIException.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.exception;

import java.io.Serial;

import ch.unige.solidify.model.xml.oai.v2.OAIPMHerrorType;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHerrorcodeType;

public class OAIException extends SolidifyRuntimeException {

  @Serial
  private static final long serialVersionUID = 5594617811163794690L;

  private final OAIPMHerrorType error = new OAIPMHerrorType();

  public OAIException(OAIPMHerrorcodeType code, String message) {
    super(message);
    this.error.setCode(code);
    this.error.setValue(message);
  }

  public OAIPMHerrorType getError() {
    return this.error;
  }

}
