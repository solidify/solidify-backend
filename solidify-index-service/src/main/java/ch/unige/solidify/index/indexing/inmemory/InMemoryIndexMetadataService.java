/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Index Service - InMemoryIndexMetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.index.indexing.inmemory;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ch.unige.solidify.IndexConfigProperties;
import ch.unige.solidify.index.settings.IndexingSettingsService;
import ch.unige.solidify.model.index.IndexMetadata;
import ch.unige.solidify.model.index.IndexProperties;
import ch.unige.solidify.rest.FacetPage;
import ch.unige.solidify.rest.FacetRequest;
import ch.unige.solidify.rest.FieldsRequest;
import ch.unige.solidify.rest.SearchCondition;

public abstract class InMemoryIndexMetadataService<T extends IndexMetadata> extends InMemoryService<T> {

  private final String indexName;

  protected InMemoryIndexMetadataService(
          IndexConfigProperties indexConfigProperties,
          IndexingSettingsService<IndexProperties> settingsService,
          IndexData indexData,
          String indexName) {
    super(indexConfigProperties, settingsService, indexData);
    this.indexName = indexName;
  }

  @Override
  public FacetPage<T> search(List<SearchCondition> conditions, List<FacetRequest> facetRequests, Pageable pageable,
          FieldsRequest fieldsRequest) {
    Page<T> page = this.findAll(null, pageable);
    return new FacetPage<>(page.getContent(), null, page.getPageable(), page.getTotalElements());
  }

  @Override
  public String getIndexName() {
    return this.indexName;
  }

}
