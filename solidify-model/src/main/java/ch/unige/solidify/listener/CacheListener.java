/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - CacheListener.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.listener;

import org.springframework.context.ApplicationEventPublisher;

import jakarta.persistence.PostPersist;
import jakarta.persistence.PostRemove;
import jakarta.persistence.PostUpdate;

import ch.unige.solidify.config.SolidifyEventPublisher;
import ch.unige.solidify.message.CacheMessage;
import ch.unige.solidify.rest.JoinResource;
import ch.unige.solidify.rest.Resource;

public class CacheListener {

  /**
   * Publish a CacheMessage event that may be used to invalidate cache when a new relations between entities is created. A message is sent for
   * all objects that are JoinResource.
   * <p>
   * For simple resources, a message may be sent based on the value returned by the sendCacheMessageOnCreation() method.
   *
   * @param object
   */
  @PostPersist
  private void notifyPersist(Object object) {
    final ApplicationEventPublisher publisher = SolidifyEventPublisher.getPublisher();

    // publisher may be null when creating resources during initialization
    if (publisher != null) {
      CacheMessage cacheMessage = null;
      if (object instanceof JoinResource<?> joinResource) {
        cacheMessage = joinResource.getCacheMessage();
      } else if (object instanceof Resource resource && resource.sendCacheMessageOnCreation()) {
        cacheMessage = resource.getCacheMessage();
      }
      if (cacheMessage != null) {
        publisher.publishEvent(cacheMessage);
      }
    }
  }

  /**
   * Publish a CacheMessage event that may be used to invalidate cache when object is updated or deleted. The message is sent if the object is a
   * simple Resource or a JoinResource as it may worth to invalidate cache in both cases.
   *
   * @param object
   */
  @PostUpdate
  @PostRemove
  private void notifyUpdate(Object object) {
    final ApplicationEventPublisher publisher = SolidifyEventPublisher.getPublisher();

    // publisher may be null when creating resources during initialization
    if (publisher != null) {
      CacheMessage cacheMessage = null;
      if (object instanceof Resource resource) {
        cacheMessage = resource.getCacheMessage();
      } else if (object instanceof JoinResource<?> joinResource) {
        cacheMessage = joinResource.getCacheMessage();
      }
      if (cacheMessage != null) {
        publisher.publishEvent(cacheMessage);
      }
    }
  }
}
