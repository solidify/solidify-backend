/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Documentation - FileUtils.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.helper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileUtils {
  private static final String FILE_SEPARATOR = "/"; // Should be always slash,
  // do not use
  // File.separator constant
  // because it's equals to
  // anti-slash on windows.

  public static void generateFile(String content, String outputFilePath) throws IOException {
    createDirectory(outputFilePath);
    try (FileWriter fileOutput = new FileWriter(outputFilePath)) {
      fileOutput.write(content);
    }
  }

  private static void createDirectory(String outputFilePath) {
    final File directory = new File(getDirectoryPath(outputFilePath));
    if (!directory.exists()) {
      directory.mkdirs();
    }
  }

  private static String getDirectoryPath(String outputFilePath) {
    if (outputFilePath.endsWith(FILE_SEPARATOR)) {
      return outputFilePath;
    }
    final int lastIndexOfSeparator = outputFilePath.lastIndexOf(FILE_SEPARATOR);
    return outputFilePath.substring(0, lastIndexOfSeparator);
  }
}
