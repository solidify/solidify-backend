/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - Label.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.rest.EmbeddableEntity;

@Embeddable
@MappedSuperclass
public class Label implements EmbeddableEntity {

  @NotNull
  @Size(max = SolidifyConstants.DB_LONG_STRING_LENGTH)
  @Column(length = SolidifyConstants.DB_LONG_STRING_LENGTH)
  private String text;

  @NotNull
  @ManyToOne
  @JoinColumn(name = SolidifyConstants.DB_LANGUAGE_ID, referencedColumnName = SolidifyConstants.DB_RES_ID)
  private Language language;

  public String getText() {
    return this.text;
  }

  public Language getLanguage() {
    return this.language;
  }

  public void setText(String text) {
    this.text = text;
  }

  public void setLanguage(Language language) {
    this.language = language;
  }

}
