/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - SolidifyTime.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.util.concurrent.TimeUnit;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;

public class SolidifyTime {

  public static void waitInMilliSeconds(int delay) {
    try {
      TimeUnit.MILLISECONDS.sleep(delay);
    } catch (final InterruptedException e) {
      // Some useful details
      // See http://www.yegor256.com/2015/10/20/interrupted-exception.html
      // See https://rules.sonarsource.com/java/tag/multi-threading/RSPEC-2142
      Thread.currentThread().interrupt();
      throw new SolidifyRuntimeException("Interrupted exception", e);
    }
  }

  public static void waitInSeconds(int delay) {
    waitInMilliSeconds(1000 * delay);
  }

  public static void waitOneSecond() {
    waitInSeconds(1);
  }

  /**
   * This constructor should not be called
   */
  private SolidifyTime() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
