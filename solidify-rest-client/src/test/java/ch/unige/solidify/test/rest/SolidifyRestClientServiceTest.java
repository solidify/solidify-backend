/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify REST Client - SolidifyRestClientServiceTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class SolidifyRestClientServiceTest {

  private final String baseUrl = "http://localhost:8080/test";

  private MockSolidifyRestClientService restService;

  @Test
  void completeBaseUrlWithNullPagination() {
    assertEquals(this.baseUrl, this.restService.completeUrlWithPagination(this.baseUrl, null));
  }

  @Test
  void completeBaseUrlWithNullPaginationAndPageParameter() {
    final String url = this.baseUrl + "?page=45";
    assertEquals(url, this.restService.completeUrlWithPagination(url, null));
  }

  @Test
  void completeUlrWithEncodedCharacter() {
    final String urlWithSpecialChars = this.baseUrl + "?a=%20";
    assertEquals(urlWithSpecialChars, this.restService.completeUrlWithPagination(urlWithSpecialChars, null));
  }

  @Test
  void completeUlrWithExistingPaginationParameters() {
    final String url = this.baseUrl + "?page=0&size=10";
    final Pageable pageable = PageRequest.of(2, 5);
    final String pageableString = "?page=2&size=5";
    assertEquals(this.baseUrl + pageableString, this.restService.completeUrlWithPagination(url, pageable));
  }

  @Test
  void completeUlrWithPlusCharacter() {
    final String urlWithPlusChar = this.baseUrl + "?a=toto+tot";
    assertEquals(urlWithPlusChar, this.restService.completeUrlWithPagination(urlWithPlusChar, null));
  }

  @Test
  void completeUrlTest() {
    final String expectedResult = this.baseUrl + "?page=0&size=10";
    final Pageable pageable = PageRequest.of(0, 10);
    assertEquals(expectedResult, this.restService.completeUrlWithPagination(this.baseUrl, pageable));
  }

  @Test
  void completeUrlWithParametersTest() {
    final String url = this.baseUrl + "?search=toto";
    final String expectedResult = url + "&page=0&size=10";
    final Pageable pageable = PageRequest.of(0, 10);
    assertEquals(expectedResult, this.restService.completeUrlWithPagination(url, pageable));
  }

  @BeforeEach
  void setup() {
    this.restService = new MockSolidifyRestClientService(null, null);
  }

}
