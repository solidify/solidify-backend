/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - RestCollectionPage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class RestCollectionPage {
  public static final int DEFAULT_SIZE_PAGE = 20;

  public static final int MAX_SIZE_PAGE = 2000;
  private static final Logger logger = LoggerFactory.getLogger(RestCollectionPage.class);
  private final long currentPage;
  private final long sizePage;

  private final long totalItems;
  private final long totalPages;

  public RestCollectionPage() {
    this.currentPage = 0;
    this.sizePage = DEFAULT_SIZE_PAGE;
    this.totalPages = 0;
    this.totalItems = 0;
  }

  public RestCollectionPage(long nbItems) {
    this.currentPage = 0;
    this.sizePage = DEFAULT_SIZE_PAGE;
    this.totalPages = this.getPages(nbItems, DEFAULT_SIZE_PAGE);
    this.totalItems = nbItems;
  }

  public RestCollectionPage(long pageNum, long pageSize, long totalPages, long totalItems) {
    this.currentPage = pageNum;
    if (pageSize <= MAX_SIZE_PAGE) {
      this.sizePage = pageSize;
    } else {
      logger.warn("The max value of page size must be under {}, not {}", MAX_SIZE_PAGE, pageSize);
      this.sizePage = MAX_SIZE_PAGE;
    }
    this.totalPages = totalPages;
    this.totalItems = totalItems;
  }

  public <T> RestCollectionPage(Page<T> page, Pageable pageable) {
    this.currentPage = pageable.getPageNumber();
    this.sizePage = pageable.getPageSize();
    this.totalPages = page.getTotalPages();
    this.totalItems = page.getTotalElements();
  }

  @JsonIgnore
  public long getCurrentItems() {
    if (this.totalItems < this.sizePage) {
      return this.totalItems;
    } else {
      if (this.currentPage + 1 == this.totalPages) {
        return this.totalItems - this.currentPage * this.sizePage;
      } else {
        return this.sizePage;
      }
    }
  }

  public long getCurrentPage() {
    return this.currentPage;
  }

  @JsonIgnore
  public long getPreviousItems() {
    return this.currentPage * this.sizePage;
  }

  public long getSizePage() {
    return this.sizePage;
  }

  public long getTotalItems() {
    return this.totalItems;
  }

  public long getTotalPages() {
    return this.totalPages;
  }

  @JsonIgnore
  public boolean hasNext() {
    return (this.totalItems != 0 && this.currentPage + 1 != this.totalPages);
  }

  @JsonIgnore
  public boolean hasPrevious() {
    return (this.totalItems != 0 && this.currentPage != 0);
  }

  private long getPages(long nbItems, long pageSize) {
    if (nbItems % pageSize == 0) {
      return nbItems / pageSize;
    } else {
      return nbItems / pageSize + 1;
    }
  }
}
