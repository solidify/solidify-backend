/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Authorization Model - ApplicationRoleListService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.auth.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import ch.unige.solidify.auth.model.AuthApplicationRole;

public interface ApplicationRoleListService {

  default boolean isRootOrTrustedRole() {
    final List<String> authorizedApplicationRoleNames = Arrays.asList(AuthApplicationRole.ROOT_ID, AuthApplicationRole.TRUSTED_CLIENT_ID);
    return this.isInApplicationRoleList(authorizedApplicationRoleNames);
  }

  default boolean isRootOrTrustedOrAdminRole() {
    final List<String> authorizedApplicationRoleNames = Arrays.asList(AuthApplicationRole.ROOT_ID, AuthApplicationRole.TRUSTED_CLIENT_ID,
            AuthApplicationRole.ADMIN_ID);
    return this.isInApplicationRoleList(authorizedApplicationRoleNames);
  }

  default boolean isRootRole() {
    final List<String> authorizedApplicationRoleNames = Arrays.asList(AuthApplicationRole.ROOT_ID);
    return this.isInApplicationRoleList(authorizedApplicationRoleNames);
  }

  default boolean isAdminRole() {
    final List<String> authorizedApplicationRoleNames = Arrays.asList(AuthApplicationRole.ADMIN_ID);
    return this.isInApplicationRoleList(authorizedApplicationRoleNames);
  }

  default boolean isUserRole() {
    final List<String> authorizedApplicationRoleNames = Arrays.asList(AuthApplicationRole.USER_ID);
    return this.isInApplicationRoleList(authorizedApplicationRoleNames);
  }

  default boolean isInApplicationRoleList(List<String> roleList) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null) {
      return false;
    }
    final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
    for (GrantedAuthority authority : authorities) {
      if (roleList.contains(authority.getAuthority())) {
        return true;
      }
    }
    return false;
  }
}
