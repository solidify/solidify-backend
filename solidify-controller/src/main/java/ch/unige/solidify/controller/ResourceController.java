/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - ResourceController.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.controller;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.hateoas.RepresentationModel;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyBulkActionException;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUndeletableException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.security.RootPermissions;

@RootPermissions
public abstract class ResourceController<T extends Resource> extends ResourceReadOnlyController<T> {

  @PostMapping
  public HttpEntity<T> create(@RequestBody T t) {

    if (t.getResId() != null && this.itemService.existsById(t.getResId())) {
      throw new SolidifyRuntimeException(this.messageService.get("validation.resource.alreadyexist",
              new Object[] { t.getResId() }));
    }

    final T item = this.itemService.save(t);
    this.addLinks(item);
    return new ResponseEntity<>(item, HttpStatus.CREATED);
  }

  @DeleteMapping(SolidifyConstants.URL_ID)
  public ResponseEntity<Void> delete(@PathVariable String id) {
    final T t = this.itemService.findOne(id);

    if (!t.isDeletable()) {
      throw new SolidifyUndeletableException(
              this.messageService.get("validation.resource.undeletable",
                      new Object[] { t.getClass().getSimpleName() + ": " + id }));
    }

    this.itemService.delete(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @DeleteMapping
  public ResponseEntity<Void> deleteList(@RequestBody String[] ids) {
    Map<String, String> errorList = new HashMap<>();
    // Remove each item
    for (final String id : ids) {
      try {
        final T t = this.itemService.findOne(id);
        if (!t.isDeletable()) {
          throw new SolidifyUndeletableException(
                  this.messageService.get("validation.resource.undeletable",
                          new Object[] { t.getClass().getSimpleName() + ": " + id }));
        }
        this.itemService.delete(id);
      } catch (RuntimeException e) {
        errorList.put(id, e.getMessage());
      }
    }
    // Check if there is error
    if (!errorList.isEmpty()) {
      throw new SolidifyBulkActionException(this.messageService.get("validation.resource.bulkdelete"), errorList);
    }
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @PatchMapping(SolidifyConstants.URL_ID)
  public HttpEntity<T> update(@PathVariable String id, @RequestBody Map<String, Object> updateMap) {

    T item = this.itemService.findOne(id);

    if (!item.isModifiable()) {
      throw new SolidifyUnmodifiableException(
              this.messageService.get("validation.resource.unmodifiable",
                      new Object[] { item.getClass().getSimpleName() + ": " + id }));
    }
    this.itemService.patchResource(item, updateMap);
    final T savedItem = this.itemService.save(item);
    this.addLinks(savedItem);
    return new ResponseEntity<>(savedItem, HttpStatus.OK);
  }

  @Override
  protected void addLinks(T t) {
    t.removeLinks();
    t.addLinks(linkTo(this.getClass()), true, true);
  }

  @Override
  protected <W extends RepresentationModel<W>> void addOthersLinks(W w) {
    // Do nothing
    // Could be overridden by subclass
  }

}
