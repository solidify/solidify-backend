/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Email - EmailParameters.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmailParameters {
  String from;
  String replyTo;
  List<String> toList = new ArrayList<>();
  List<String> ccList = new ArrayList<>();
  List<String> bccList = new ArrayList<>();
  String subject;
  String body;
  String template;
  Map<String, Object> templateParameters = new HashMap<>();

  public String getFrom() {
    return this.from;
  }

  public EmailParameters setFrom(String from) {
    this.from = from;
    return this;
  }

  public String getReplyTo() {
    return this.replyTo;
  }

  public EmailParameters setReplyTo(String replyTo) {
    this.replyTo = replyTo;
    return this;
  }

  public List<String> getToList() {
    return this.toList;
  }

  public EmailParameters setToList(List<String> toList) {
    this.toList = toList;
    return this;
  }

  public List<String> getCcList() {
    return this.ccList;
  }

  public EmailParameters addCc(List<String> ccList) {
    if (ccList != null) {
      this.ccList.addAll(ccList);
    }
    return this;
  }

  public EmailParameters setCcList(List<String> ccList) {
    this.ccList = ccList;
    return this;
  }

  public List<String> getBccList() {
    return this.bccList;
  }

  public EmailParameters addBcc(List<String> bccList) {
    if (bccList != null) {
      this.bccList.addAll(bccList);
    }
    return this;
  }

  public EmailParameters setBccList(List<String> bccList) {
    this.bccList = bccList;
    return this;
  }

  public String getSubject() {
    return this.subject;
  }

  public EmailParameters setSubject(String subject) {
    this.subject = subject;
    return this;
  }

  public String getBody() {
    return this.body;
  }

  public EmailParameters setBody(String body) {
    this.body = body;
    return this;
  }

  public String getTemplate() {
    return this.template;
  }

  public EmailParameters setTemplate(String template) {
    this.template = template;
    return this;
  }

  public Map<String, Object> getTemplateParameters() {
    return this.templateParameters;
  }

  public EmailParameters setTemplateParameters(Map<String, Object> templateParameters) {
    this.templateParameters = templateParameters;
    return this;
  }
}
