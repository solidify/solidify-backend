/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - FrenchBooleanConverter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.converter;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter
public class FrenchBooleanConverter implements AttributeConverter<Boolean, String> {
  public static final String NON = "N";
  public static final String OUI = "O";

  @Override
  public String convertToDatabaseColumn(Boolean aBoolean) {
    if (aBoolean == null) {
      return null;
    }
    return (aBoolean) ? "O" : "N";
  }

  @Override
  public Boolean convertToEntityAttribute(String s) {
    if (s == null) {
      return null;
    }
    if (s.equalsIgnoreCase(OUI)) {
      return true;
    }
    if (s.equalsIgnoreCase(NON)) {
      return false;
    }

    throw new IllegalArgumentException(s);
  }
}
