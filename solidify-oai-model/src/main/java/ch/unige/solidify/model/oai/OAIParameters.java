/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIParameters.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.oai;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = """
        OAI-PMH parameters:
        - from => Start date of date range for searching
        - until => End date of date range for searching
        - set => OAI set to filter
        - metadaPrefix => Metadata schema to get
        """)
public class OAIParameters {
  private String from;
  private String metadataPrefix;
  private OAISet set;
  private String until;

  public String getFrom() {
    return this.from;
  }

  public String getMetadataPrefix() {
    return this.metadataPrefix;
  }

  public OAISet getSet() {
    return this.set;
  }

  public String getUntil() {
    return this.until;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public void setMetadataPrefix(String metadataPrefix) {
    this.metadataPrefix = metadataPrefix;
  }

  public void setSet(OAISet set) {
    this.set = set;
  }

  public void setUntil(String until) {
    this.until = until;
  }

}
