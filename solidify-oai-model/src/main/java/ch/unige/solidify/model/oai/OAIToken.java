/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIToken.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.model.oai;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;

import io.swagger.v3.oas.annotations.media.Schema;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.OAIException;
import ch.unige.solidify.model.xml.oai.v2.OAIPMHerrorcodeType;
import ch.unige.solidify.model.xml.oai.v2.ResumptionTokenType;
import ch.unige.solidify.rest.RestCollectionPage;

@Schema(description = "OAI token for pagination.")
public class OAIToken {
  private BigInteger currentItems;
  private OffsetDateTime expirationDate;
  private OAIParameters oaiParameters = new OAIParameters();
  private Pageable pageable;
  private BigInteger totalItems;

  public OAIToken() {
    this.pageable = PageRequest.of(0, RestCollectionPage.DEFAULT_SIZE_PAGE);
    this.setExpirationDate(5);
  }

  public OAIToken(int oaiPmhPageSize, int oaiPmhTokenLifeTime) {
    this.pageable = PageRequest.of(0, oaiPmhPageSize);
    this.setExpirationDate(oaiPmhTokenLifeTime);
  }

  public OAIToken(int oaiPmhPageSize, int oaiPmhTokenLifeTime, Sort sort) {
    this.pageable = PageRequest.of(0, oaiPmhPageSize, sort);
    this.setExpirationDate(oaiPmhTokenLifeTime);
  }

  public BigInteger getCurrentItems() {
    return this.currentItems;
  }

  public OffsetDateTime getExpirationDate() {
    return this.expirationDate;
  }

  public OAIParameters getOaiParameters() {
    return this.oaiParameters;
  }

  public Pageable getPageable() {
    return this.pageable;
  }

  public ResumptionTokenType getToken(long totalItems, long previousItems, int oaiPmhTokenLifeTime, boolean lastPage) {
    final ResumptionTokenType resumptionToken = new ResumptionTokenType();
    // set total items
    this.setTotalItems(BigInteger.valueOf(totalItems));
    resumptionToken.setCompleteListSize(this.getTotalItems());
    // set cursor = items already read
    this.setCurrentItems(BigInteger.valueOf(previousItems));
    resumptionToken.setCursor(this.getCurrentItems());
    if (!lastPage) {
      // If token is not on the last page, fill its value
      this.setExpirationDate(oaiPmhTokenLifeTime);
      resumptionToken.setExpirationDate(this.getExpirationDate());
      resumptionToken.setValue(this.encodeResumptionToken());
    }
    return resumptionToken;
  }

  public BigInteger getTotalItems() {
    return this.totalItems;
  }

  public void setCurrentItems(BigInteger currentItems) {
    this.currentItems = currentItems;
  }

  public void setExpirationDate(OffsetDateTime expirationDate) {
    this.expirationDate = expirationDate;
  }

  public void setOaiParameters(OAIParameters oaiParameters) {
    this.oaiParameters = oaiParameters;
  }

  public void setToken(String token) {
    this.setParameters(new String(Base64Utils.decodeFromString(token), StandardCharsets.UTF_8));
    if (OffsetDateTime.now(ZoneOffset.UTC).isAfter(this.getExpirationDate())) {
      throw new OAIException(OAIPMHerrorcodeType.BAD_RESUMPTION_TOKEN, "Resumption token expired");
    }

    // set current page
    int currentPage = this.getCurrentItems().intValue() / this.getPageable().getPageSize();
    this.pageable = PageRequest.of(currentPage, this.getPageable().getPageSize(), this.getPageable().getSort());

    // Go to next page
    this.pageable = this.getPageable().next();
  }

  public void setTotalItems(BigInteger totalItems) {
    this.totalItems = totalItems;
  }

  private String encodeResumptionToken() {
    final String parameters = this.getParameters();
    return Base64Utils.encodeToString(parameters.getBytes(StandardCharsets.UTF_8));
  }

  private String getParameters() {
    String params = this.getExpirationDate().toInstant().toEpochMilli() + SolidifyConstants.FIELD_SEP;
    params += this.getTotalItems() + SolidifyConstants.FIELD_SEP;
    params += this.getCurrentItems() + SolidifyConstants.FIELD_SEP;
    if (this.getOaiParameters().getMetadataPrefix() != null) {
      params += this.getOaiParameters().getMetadataPrefix() + SolidifyConstants.FIELD_SEP;
    } else {
      params += SolidifyConstants.FIELD_SEP;
    }
    if (this.getOaiParameters().getFrom() != null) {
      params += this.getOaiParameters().getFrom() + SolidifyConstants.FIELD_SEP;
    } else {
      params += SolidifyConstants.FIELD_SEP;
    }
    if (this.getOaiParameters().getUntil() != null) {
      params += this.getOaiParameters().getUntil() + SolidifyConstants.FIELD_SEP;
    } else {
      params += SolidifyConstants.FIELD_SEP;
    }
    if (this.getOaiParameters().getSet() != null) {
      params += this.getOaiParameters().getSet().getSpec();
    }
    return params;
  }

  private void setExpirationDate(int lifeTimeMinutes) {
    this.expirationDate = OffsetDateTime.now(ZoneOffset.UTC).plusMinutes(lifeTimeMinutes);
  }

  private void setParameters(String parameters) {

    final String[] params = StringUtils.delimitedListToStringArray(parameters, SolidifyConstants.FIELD_SEP);
    if (params.length == 0 || params.length != 7) {
      throw new OAIException(OAIPMHerrorcodeType.BAD_RESUMPTION_TOKEN, "Resumption token invalid");
    }
    this.setExpirationDate(OffsetDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(params[0])), ZoneOffset.UTC));
    this.setTotalItems(new BigInteger(params[1]));
    this.setCurrentItems(new BigInteger(params[2]));
    this.getOaiParameters().setMetadataPrefix(params[3]);
    this.getOaiParameters().setFrom(params[4]);
    this.getOaiParameters().setUntil(params[5]);
    final OAISet set = new OAISet();
    set.setSpec(params[6]);
    this.getOaiParameters().setSet(set);
  }

}
