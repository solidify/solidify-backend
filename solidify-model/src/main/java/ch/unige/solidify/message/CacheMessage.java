/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - CacheMessage.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.message;

import java.io.Serial;
import java.io.Serializable;
import java.util.HashMap;

public class CacheMessage implements Serializable {

  @Serial
  private static final long serialVersionUID = -15518034654214571L;

  protected HashMap<String, Serializable> otherProperties = new HashMap<>();

  public void addOtherProperty(String key, Serializable value) {
    this.otherProperties.put(key, value);
  }

  public Serializable getOtherProperty(String key) {
    return this.otherProperties.get(key);
  }

  public boolean hasOtherProperty(String key) {
    return this.otherProperties.containsKey(key);
  }

}
