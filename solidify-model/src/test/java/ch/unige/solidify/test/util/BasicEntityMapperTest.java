/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - BasicEntityMapperTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.util;

import static ch.unige.solidify.SolidifyConstants.RES_ID_FIELD;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.exception.SolidifyUnmodifiableException;
import ch.unige.solidify.rest.EmbeddableEntity;
import ch.unige.solidify.rest.Resource;
import ch.unige.solidify.rest.ResourceNormalized;
import ch.unige.solidify.util.BasicEntityMapper;

class BasicEntityMapperTest {

  private static final String ORIGINAL_NAME = "Original Name";
  private static final String NEW_NAME = "New Name";
  private static final String ORIGINAL_EMBEDDED_NAME = "Original Embedded Name";
  private static final String NEW_EMBEDDED_NAME = "New Embedded Name";
  private static final String ORIGINAL_DESCRIPTION = "Original Description";
  private static final String NEW_DESCRIPTION = "New Description";
  private static final String NEW_RES_ID = "0000-0000-0001";
  private static final String SUB_RESOURCE_TEST_FIELD = "subResourceTest";
  private static final String NON_UPDATABLE_SUB_RESOURCE_TEST_FIELD = "nonUpdatableSubResourceTest";

  private static final BasicEntityMapper basicEntityMapper = new BasicEntityMapper();

  public static class EmbeddedEntity implements EmbeddableEntity {
    private String embeddedName = ORIGINAL_EMBEDDED_NAME;
    private DayOfWeek embeddedDayOfWeek = DayOfWeek.TUESDAY;

    public String getEmbeddedName() {
      return this.embeddedName;
    }

    public void setEmbeddedName(String embeddedName) {
      this.embeddedName = embeddedName;
    }

    public DayOfWeek getEmbeddedDayOfWeek() {
      return this.embeddedDayOfWeek;
    }

    public void setEmbeddedDayOfWeek(DayOfWeek embeddedDayOfWeek) {
      this.embeddedDayOfWeek = embeddedDayOfWeek;
    }
  }

  public static class ResourceTest extends ResourceNormalized {
    private String name = ORIGINAL_NAME;
    private EmbeddedEntity embeddableEntity = new EmbeddedEntity();
    private DayOfWeek dayOfWeek = DayOfWeek.MONDAY;
    private SubResourceTest subResourceTest = new SubResourceTest();
    private SubResourceTest nonUpdatableSubResourceTest = new SubResourceTest();
    private List<SubResourceTest> subResourceTests = new ArrayList<>();
    private List<String> keywords = new ArrayList<>();
    private List<String> patchableSubresources = new ArrayList<>();
    private String noSetterValue = "readonly";

    @Override
    public void init() {
    }

    @Override
    public List<String> getPatchableSubResources() {
      return this.patchableSubresources;
    }

    @Override
    public String managedBy() {
      return null;
    }

    public String getName() {
      return this.name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public EmbeddedEntity getEmbeddableEntity() {
      return this.embeddableEntity;
    }

    public void setEmbeddableEntity(EmbeddedEntity embeddableEntity) {
      this.embeddableEntity = embeddableEntity;
    }

    public DayOfWeek getDayOfWeek() {
      return this.dayOfWeek;
    }

    public void setDayOfWeek(DayOfWeek dayOfWeek) {
      this.dayOfWeek = dayOfWeek;
    }

    public SubResourceTest getSubResourceTest() {
      return this.subResourceTest;
    }

    public SubResourceTest getNonUpdatableSubResourceTest() {
      return this.nonUpdatableSubResourceTest;
    }

    public void setSubResourceTest(SubResourceTest subResourceTest) {
      this.subResourceTest = subResourceTest;
    }

    public void setNonUpdatableSubResourceTest(SubResourceTest subResourceTest) {
      this.subResourceTest = subResourceTest;
    }

    public void setSubResourceTests(List<SubResourceTest> subResourceTests) {
      this.subResourceTests = subResourceTests;
    }

    public List<SubResourceTest> getSubResourceTests() {
      return this.subResourceTests;
    }

    public List<String> getKeywords() {
      return this.keywords;
    }

    public void setKeywords(List<String> keywords) {
      this.keywords = keywords;
    }

    public String getNoSetterValue() {
      return this.noSetterValue;
    }

    @Override
    public List<String> getNonUpdatableFields() {
      return Arrays.asList(RES_ID_FIELD, NON_UPDATABLE_SUB_RESOURCE_TEST_FIELD);
    }
  }

  public static class SubResourceTest extends ResourceNormalized {

    private String description = ORIGINAL_DESCRIPTION;

    public String getDescription() {
      return this.description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    @Override
    public void init() {
      // no-op
    }

    @Override
    public String managedBy() {
      return null;
    }
  }

  /***************************************************************************************************/

  @Test
  void emptyMapTest() {
    final ResourceTest resourceTest = new ResourceTest();
    this.basicEntityMapper.patchResource(resourceTest, Map.of());
    assertEquals(ORIGINAL_NAME, resourceTest.getName(), "Field name shouldn't have changed");
  }

  @Test
  void nullResourceTest() {
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource((Resource) null, Map.of()));
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource((EmbeddableEntity) null, Map.of()));
  }

  @Test
  void nullMapTest() {
    final ResourceTest resourceTest = new ResourceTest();
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, null));
  }

  @Test
  void nullFieldTest() {
    final ResourceTest resourceTest = new ResourceTest();
    final Map<String, Object> updateMap = new HashMap<>();
    updateMap.put("name", null);
    updateMap.put("embeddableEntity", null);
    updateMap.put("dayOfWeek", null);
    this.basicEntityMapper.patchResource(resourceTest, updateMap);
    assertEquals(null, resourceTest.getName(), "Field name should be null");
    assertEquals(null, resourceTest.getEmbeddableEntity(), "Field embeddableEntity should be null");
    assertEquals(null, resourceTest.getDayOfWeek(), "Field dayOfWeek should be null");
  }

  @Test
  void embeddedNullFieldTest() {
    final ResourceTest resourceTest = new ResourceTest();
    final Map<String, Object> updateMap = new HashMap<>();
    final Map<String, Object> embeddedUpdateMap = new HashMap<>();
    updateMap.put("embeddableEntity", embeddedUpdateMap);
    embeddedUpdateMap.put("embeddedName", null);
    embeddedUpdateMap.put("embeddedDayOfWeek", null);
    this.basicEntityMapper.patchResource(resourceTest, updateMap);
    assertEquals(null, resourceTest.getEmbeddableEntity().getEmbeddedName(), "Embedded field name should be null");
    assertEquals(null, resourceTest.getEmbeddableEntity().getEmbeddedDayOfWeek(), "Embedded field dayOfWeek should be null");
  }

  @Test
  void embeddedNullTest() {
    final ResourceTest resourceTest = new ResourceTest();
    final Map<String, Object> updateMap = new HashMap<>();
    updateMap.put("embeddableEntity", null);
    this.basicEntityMapper.patchResource(resourceTest, updateMap);
    assertEquals(null, resourceTest.getEmbeddableEntity(), "Embedded entity should be null");
  }

  @Test
  void resIdTest() {
    final ResourceTest resourceTest = new ResourceTest();
    final String originalResId = resourceTest.getResId();
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, Map.of("resId", NEW_RES_ID)),
            "Trying to change resId should be silently ignored");
    assertEquals(originalResId, resourceTest.getResId(), "resId should not have changed");
  }

  @Test
  void updateWhenNullResIdTest() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.setResId(null);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, Map.of("resId", "000000")));
    assertEquals(null, resourceTest.getResId(), "resId should not have changed");
  }

  @Test
  void stringFieldTest() {
    final ResourceTest resourceTest = new ResourceTest();
    this.basicEntityMapper.patchResource(resourceTest, Map.of("name", NEW_NAME));
    assertEquals(NEW_NAME, resourceTest.getName(), "Field name should have changed");
  }

  @Test
  void enumFieldTest() {
    final ResourceTest resourceTest = new ResourceTest();
    this.basicEntityMapper.patchResource(resourceTest, Map.of("dayOfWeek", "SUNDAY"));
    assertEquals(DayOfWeek.SUNDAY, resourceTest.getDayOfWeek(), "Field name should have changed");
  }

  @Test
  void stringInEmbeddableEntityTest() {
    final ResourceTest resourceTest = new ResourceTest();
    this.basicEntityMapper.patchResource(resourceTest, Map.of("embeddableEntity", Map.of("embeddedName", NEW_EMBEDDED_NAME)));
    assertEquals(NEW_EMBEDDED_NAME, resourceTest.getEmbeddableEntity().getEmbeddedName(), "Field name should have changed");
  }

  @Test
  void enumInEmbeddableEntityFieldTest() {
    final ResourceTest resourceTest = new ResourceTest();
    this.basicEntityMapper.patchResource(resourceTest, Map.of("embeddableEntity", Map.of("embeddedDayOfWeek", "SUNDAY")));
    assertEquals(DayOfWeek.SUNDAY, resourceTest.getEmbeddableEntity().getEmbeddedDayOfWeek(), "Field name should have changed");
  }

  @Test
  void useMapInsteadOfStringTest() {
    final ResourceTest resourceTest = new ResourceTest();
    Map updateMap = Map.of("name", Map.of());
    assertThrows(SolidifyRuntimeException.class, () -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
  }

  @Test
  void useStringInsteadOfMapTest() {
    final ResourceTest resourceTest = new ResourceTest();
    Map updateMap = Map.of("embeddableEntity", "ab");
    assertThrows(SolidifyRuntimeException.class, () -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
  }

  @Test
  void unknownFieldTest() {
    final ResourceTest resourceTest = new ResourceTest();
    final Map updateMap = Map.of("unknownField", "value");
    assertDoesNotThrow(() -> basicEntityMapper.patchResource(resourceTest, updateMap));
    assertEquals(ORIGINAL_NAME, resourceTest.getName(), "Field name should not have changed");
    assertEquals(ORIGINAL_EMBEDDED_NAME, resourceTest.getEmbeddableEntity().getEmbeddedName(),
            "Field embeddableEntity name should not have changed");
    assertEquals(DayOfWeek.MONDAY, resourceTest.getDayOfWeek(), "Field dayOfWeek should not have changed");
  }

  @Test
  void unknownEmbeddedFieldTest() {
    final ResourceTest resourceTest = new ResourceTest();
    final Map updateMap = Map.of("embeddableEntity", Map.of("unknownField", "value"));
    assertDoesNotThrow(() -> basicEntityMapper.patchResource(resourceTest, updateMap));
  }

  @Test
  void unknownEnumValueTest() {
    final ResourceTest resourceTest = new ResourceTest();
    final Map<String, Object> updateMap = Map.of("dayOfWeek", "SUN");
    assertThrows(SolidifyRuntimeException.class, () -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
  }

  @Test
  void updateSubResourceFieldDisabledByDefault() {
    final ResourceTest resourceTest = new ResourceTest();
    final Map<String, Object> updateMap = Map.of(SUB_RESOURCE_TEST_FIELD, Map.of("description", NEW_DESCRIPTION));
    assertThrows(SolidifyRuntimeException.class, () -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
  }

  @Test
  void updateSubResourceFieldEnabled() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.getPatchableSubResources().add(SUB_RESOURCE_TEST_FIELD);
    this.basicEntityMapper.patchResource(resourceTest, Map.of(SUB_RESOURCE_TEST_FIELD, Map.of("description", NEW_DESCRIPTION)));
    assertEquals(NEW_DESCRIPTION, resourceTest.getSubResourceTest().getDescription(), "Field description should have changed");
  }

  @Test
  void updateSubResourceWithNullField() {
    final ResourceTest resourceTest = new ResourceTest();
    Map<String, Object> updateMap = new HashMap<>();
    updateMap.put(SUB_RESOURCE_TEST_FIELD, null);
    resourceTest.getPatchableSubResources().add(SUB_RESOURCE_TEST_FIELD);
    this.basicEntityMapper.patchResource(resourceTest, updateMap);
    assertNull(resourceTest.getSubResourceTest(), "Field subResourceTest should be null");
  }

  @Test
  void updateSubResourceWhenNullField() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.setSubResourceTest(null);
    Map<String, Object> updateMap = new HashMap<>();
    updateMap.put(SUB_RESOURCE_TEST_FIELD, Map.of("description", NEW_DESCRIPTION));
    resourceTest.getPatchableSubResources().add(SUB_RESOURCE_TEST_FIELD);
    this.basicEntityMapper.patchResource(resourceTest, updateMap);
    assertEquals(NEW_DESCRIPTION, resourceTest.getSubResourceTest().getDescription(), "Field description should have changed");
  }

  @Test
  void updateSubResourceWhenNullWithNullField() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.setSubResourceTest(null);
    Map<String, Object> updateMap = new HashMap<>();
    updateMap.put(SUB_RESOURCE_TEST_FIELD, null);
    resourceTest.getPatchableSubResources().add(SUB_RESOURCE_TEST_FIELD);
    this.basicEntityMapper.patchResource(resourceTest, updateMap);
    assertNull(resourceTest.getSubResourceTest(), "Field subResourceTest should be null");
  }

  @Test
  void updateSubResourceResId() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.getPatchableSubResources().add(SUB_RESOURCE_TEST_FIELD);
    this.basicEntityMapper.patchResource(resourceTest, Map.of(SUB_RESOURCE_TEST_FIELD, Map.of("resId", NEW_RES_ID)));
    assertEquals(NEW_RES_ID, resourceTest.getSubResourceTest().getResId(), "resId of subResource should have changed");
  }

  @Test
  void updateSubResourceResIdWhenNonUpdatable() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.getPatchableSubResources().add(NON_UPDATABLE_SUB_RESOURCE_TEST_FIELD);

    String subResourceResId = resourceTest.getNonUpdatableSubResourceTest().getResId();
    Map updateMap = Map.of(NON_UPDATABLE_SUB_RESOURCE_TEST_FIELD, Map.of("resId", NEW_RES_ID));
    assertThrows(SolidifyUnmodifiableException.class, () -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertEquals(subResourceResId, resourceTest.getNonUpdatableSubResourceTest().getResId(), "resId of subResource should not have changed");
  }

  @Test
  void updateSubResourceDescriptionWhenNonUpdatable() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.getPatchableSubResources().add(NON_UPDATABLE_SUB_RESOURCE_TEST_FIELD);

    String subResourceResId = resourceTest.getSubResourceTest().getResId();
    String description = resourceTest.getSubResourceTest().getDescription();
    Map updateMap = Map.of(NON_UPDATABLE_SUB_RESOURCE_TEST_FIELD, Map.of("description", NEW_DESCRIPTION));
    assertThrows(SolidifyUnmodifiableException.class, () -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertEquals(subResourceResId, resourceTest.getSubResourceTest().getResId(), "resId of subResource should not have changed");
    assertEquals(description, resourceTest.getSubResourceTest().getDescription(), "description of subResource should not have changed");
  }

  /**********************************************************************************************/

  @Test
  void updateNonEmptyListWithNonEmptyListOfSubResources() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.getSubResourceTests().add(new SubResourceTest());
    resourceTest.getSubResourceTests().add(new SubResourceTest());
    List<String> subResourceResIds = resourceTest.getSubResourceTests().stream().map(sr -> sr.getResId()).collect(Collectors.toList());

    List<Map> subResourceTests = new ArrayList<>();
    subResourceTests.add(Map.of("resId", "123-456-123-456"));
    Map updateMap = Map.of("subResourceTests", subResourceTests);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertEquals(2, resourceTest.getSubResourceTests().size());
    assertEquals(subResourceResIds.get(0), resourceTest.getSubResourceTests().get(0).getResId());
    assertEquals(subResourceResIds.get(1), resourceTest.getSubResourceTests().get(1).getResId());
  }

  @Test
  void updateEmptyListWithEmptyListOfSubResources() {
    final ResourceTest resourceTest = new ResourceTest();
    assertEquals(0, resourceTest.getSubResourceTests().size());

    List<Map> subResourceTests = new ArrayList<>();
    Map updateMap = Map.of("subResourceTests", subResourceTests);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertTrue(resourceTest.getSubResourceTests().isEmpty());
  }

  @Test
  void updateNonEmptyListWithNullListOfSubResources() {
    final ResourceTest resourceTest = new ResourceTest();
    SubResourceTest subResourceTest = new SubResourceTest();
    resourceTest.getSubResourceTests().add(subResourceTest);
    assertEquals(1, resourceTest.getSubResourceTests().size());

    Map updateMap = new HashMap();
    updateMap.put("subResourceTests", null);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertEquals(1, resourceTest.getSubResourceTests().size());
    assertEquals(subResourceTest.getResId(), resourceTest.getSubResourceTests().get(0).getResId());
  }

  @Test
  void updateEmptyListWithNullListOfSubResources() {
    final ResourceTest resourceTest = new ResourceTest();
    assertEquals(0, resourceTest.getSubResourceTests().size());

    Map updateMap = new HashMap();
    updateMap.put("subResourceTests", null);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertTrue(resourceTest.getSubResourceTests().isEmpty());
  }

  @Test
  void updateNullListWithNonEmptyListOfSubResources() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.setSubResourceTests(null);

    List<Map> subResourceTests = new ArrayList<>();
    subResourceTests.add(Map.of("resId", "123-456-123-456"));
    Map updateMap = Map.of("subResourceTests", subResourceTests);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertNull(resourceTest.getSubResourceTests());
  }

  @Test
  void updateNullListWithEmptyListOfSubResources() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.setSubResourceTests(null);

    List<Map> subResourceTests = new ArrayList<>();
    Map updateMap = Map.of("subResourceTests", subResourceTests);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertNull(resourceTest.getSubResourceTests());
  }

  @Test
  void updateNullListWithNullListOfSubResources() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.setSubResourceTests(null);

    Map updateMap = new HashMap();
    updateMap.put("subResourceTests", null);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertNull(resourceTest.getSubResourceTests());
  }

  /**********************************************************************************************/

  @Test
  void updateNonEmptyListWithNonEmptyListOfStrings() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.getKeywords().add("keyword1");
    resourceTest.getKeywords().add("keyword2");

    List<String> keywords = new ArrayList<>();
    keywords.add("update1");
    keywords.add("update2");
    keywords.add("update3");
    Map updateMap = Map.of("keywords", keywords);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertEquals(keywords.size(), resourceTest.getKeywords().size());
    assertEquals(keywords.get(0), resourceTest.getKeywords().get(0));
    assertEquals(keywords.get(1), resourceTest.getKeywords().get(1));
    assertEquals(keywords.get(2), resourceTest.getKeywords().get(2));
  }

  @Test
  void updateEmptyListWithEmptyListOfStrings() {
    final ResourceTest resourceTest = new ResourceTest();
    assertEquals(0, resourceTest.getKeywords().size());

    List<String> keywords = new ArrayList<>();
    Map updateMap = Map.of("keywords", keywords);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertTrue(resourceTest.getKeywords().isEmpty());
  }

  @Test
  void updateNonEmptyListWithNullListOfStrings() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.getKeywords().add("keyword1");
    resourceTest.getKeywords().add("keyword2");

    Map updateMap = new HashMap();
    updateMap.put("keywords", null);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertNull(resourceTest.getKeywords());
  }

  @Test
  void updateNonEmptyListWithEmptyListOfStrings() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.getKeywords().add("keyword1");
    resourceTest.getKeywords().add("keyword2");

    Map updateMap = new HashMap();
    List<String> keywords = new ArrayList<>();
    updateMap.put("keywords", keywords);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertTrue(resourceTest.getKeywords().isEmpty());
  }

  @Test
  void updateEmptyListWithNullListOfStrings() {
    final ResourceTest resourceTest = new ResourceTest();
    assertTrue(resourceTest.getKeywords().isEmpty());

    Map updateMap = new HashMap();
    updateMap.put("keywords", null);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    // Note: as both list are null or empty, patchResource ignore the field --> it doesn't get null
    assertTrue(resourceTest.getKeywords().isEmpty());
  }

  @Test
  void updateNullListWithNonEmptyListOfStrings() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.setKeywords(null);

    List<String> keywords = new ArrayList<>();
    keywords.add("update1");
    keywords.add("update2");
    keywords.add("update3");
    Map updateMap = Map.of("keywords", keywords);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertNotNull(resourceTest.getKeywords());
    assertEquals(keywords.size(), resourceTest.getKeywords().size());
    assertEquals(keywords.get(0), resourceTest.getKeywords().get(0));
    assertEquals(keywords.get(1), resourceTest.getKeywords().get(1));
    assertEquals(keywords.get(2), resourceTest.getKeywords().get(2));
  }

  @Test
  void updateNullListWithEmptyListOfStrings() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.setKeywords(null);

    List<String> keywords = new ArrayList<>();
    Map updateMap = Map.of("keywords", keywords);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertNull(resourceTest.getKeywords());
  }

  @Test
  void updateNullListWithNullListOfStrings() {
    final ResourceTest resourceTest = new ResourceTest();
    resourceTest.setKeywords(null);

    Map updateMap = new HashMap();
    updateMap.put("keywords", null);
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertNull(resourceTest.getKeywords());
  }

  @Test
  void noUpdateIfNoSetter() {
    final ResourceTest resourceTest = new ResourceTest();
    String noSetterValue = resourceTest.getNoSetterValue();

    Map updateMap = new HashMap();
    updateMap.put("noSetterValue", "updated");
    assertDoesNotThrow(() -> this.basicEntityMapper.patchResource(resourceTest, updateMap));
    assertEquals(noSetterValue, resourceTest.getNoSetterValue());
  }
}
