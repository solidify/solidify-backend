/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - Join3TiersSpecification.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.specification;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Path;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.rest.JoinResource;

/**
 * Specification allowing to manage a 3-tiers relationship
 *
 * @param <J> Type of the join resource entity between the 3 entity types linked together by a relation table
 */
public abstract class Join3TiersSpecification<J extends JoinResource<?>> extends JoinNTiersSpecification<J> {

  protected final String pathToGrandChild;

  protected boolean filterOnGrandChildResId = false;

  protected Join3TiersSpecification(J joinCriteria, String pathToParent, String pathToChild, String pathToGrandChild) {
    super(joinCriteria, pathToParent, pathToChild);
    this.pathToGrandChild = pathToGrandChild;
  }

  @Override
  public Predicate toPredicate(Root<J> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    final List<Predicate> predicatesList = new ArrayList<>();

    // ensure only children of parent are found
    this.completeParentIdPredicate(root, builder, predicatesList);

    if (!this.filterOnChildResId && !this.filterOnGrandChildResId) {
      // return a filtered list of children
      this.completeChildPredicatesList(root, query, builder, predicatesList);

    } else if (this.filterOnChildResId && !this.filterOnGrandChildResId) {
      // select specific child
      this.completeChildIdPredicate(root, builder, predicatesList);

    } else if (this.filterOnChildResId && this.filterOnGrandChildResId) {
      // return a specific relation resource
      this.completeChildIdPredicate(root, builder, predicatesList);
      this.completeGrandChildIdPredicate(root, query, builder, predicatesList);

    } else if (!this.filterOnChildResId && this.filterOnGrandChildResId) {
      throw new SolidifyRuntimeException("searching on specific grandchild id without child id criteria is not supported");
    }

    // filter on join table properties
    this.completeJoinPredicatesList(root, query, builder, predicatesList);

    return builder.and(predicatesList.toArray(new Predicate[predicatesList.size()]));
  }

  public void setFilterOnGrandChildResId(boolean filterOnGrandChildResId) {
    this.filterOnGrandChildResId = filterOnGrandChildResId;
  }

  protected void completeGrandChildIdPredicate(Root<J> root, CriteriaQuery<?> query, CriteriaBuilder builder, List<Predicate> predicatesList) {
    predicatesList.add(builder.equal(this.getGrandChildPath(root).get(SolidifyConstants.DB_RES_ID), this.getGrandChildId()));
  }

  protected Path<?> getGrandChildPath(Root<J> root) {
    return this.getPathFromExpression(root, this.pathToGrandChild);
  }

  protected abstract String getGrandChildId();

}
