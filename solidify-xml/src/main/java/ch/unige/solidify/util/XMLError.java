/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify XML - XMLError.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class XMLError implements ErrorHandler {

  private static final Logger log = LoggerFactory.getLogger(XMLError.class);

  private final boolean msg;
  private final boolean x7;

  public XMLError() {
    this.msg = false;
    this.x7 = true;
  }

  public XMLError(boolean m, boolean e) {
    this.msg = m;
    this.x7 = e;
  }

  @Override
  public void error(SAXParseException e) throws SAXException {
    if (this.msg) {
      log.error(e.getMessage());
    }
    if (this.x7) {
      throw e;
    }
  }

  @Override
  public void fatalError(SAXParseException e) throws SAXException {
    if (this.msg) {
      log.error(e.getMessage());
    }
    if (this.x7) {
      throw e;
    }
  }

  @Override
  public void warning(SAXParseException e) throws SAXException {
    if (this.msg) {
      log.warn(e.getMessage());
    }
    if (this.x7) {
      throw e;
    }
  }
}
