/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify OAI-PMH Model - OAIMetadataService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;

import ch.unige.solidify.OAIConstants;
import ch.unige.solidify.model.oai.OAIRecord;

public interface OAIMetadataService {

  List<Class<?>> getOaiXmlClasses();

  Object getOAIMetadata(String referenceMetadata) throws JAXBException;

  Marshaller getMarshaller(JAXBContext jaxbContext) throws JAXBException;

  default Map<String, String> getXsltParameters(OAIRecord oaiRecord) {
    final Map<String, String> params = new HashMap<>();
    params.put(OAIConstants.XSL_PARAM_RES_ID, oaiRecord.getResId());
    params.put(OAIConstants.XSL_PARAM_OAI_ID, oaiRecord.getOaiId());
    return params;
  }
}
