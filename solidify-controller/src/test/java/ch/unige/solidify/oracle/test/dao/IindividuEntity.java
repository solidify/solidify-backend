/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - IindividuEntity.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.oracle.test.dao;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import org.springframework.data.jpa.domain.Specification;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.unige.solidify.model.ChangeInfo;
import ch.unige.solidify.rest.ResourceDiverse;
import ch.unige.solidify.rest.Searchable;
import ch.unige.solidify.util.SearchCriteria;

@Entity
@Table(name = "IINDIVIDU")
public class IindividuEntity extends ResourceDiverse implements Searchable<IindividuEntity> {

  @Id
  @Column(name = "CN_INDIVIDU", nullable = false)
  private String entityId;

  @Column(name = "PRENOMS", nullable = false)
  private String firstname;

  @Column(name = "NOM", nullable = false)
  private String lastname;

  @Column(name = "RECH_NOMS")
  private String namesForSearch;

  @JsonProperty
  @JsonInclude(JsonInclude.Include.NON_NULL)
  @Transient
  private List<SearchCriteria> searchCriterias;

  @Override
  public ChangeInfo getCreation() {
    return null;
  }

  @Override
  public String getEntityId() {
    return this.entityId;
  }

  public String getFirstname() {
    return this.firstname;
  }

  public String getLastname() {
    return this.lastname;
  }

  @Override
  public ChangeInfo getLastUpdate() {
    return null;
  }

  public String getNamesForSearch() {
    return this.namesForSearch;
  }

  @Override
  @java.beans.Transient
  @JsonIgnore
  public Specification<IindividuEntity> getSearchSpecification(SearchCriteria criteria) {
    return new IindividuSpecification(criteria);
  }

  @Override
  @JsonIgnore
  public List<SearchCriteria> getSearchCriterias() {
    return this.searchCriterias;
  }

  @Override
  @JsonProperty
  public void setSearchCriterias(List<SearchCriteria> searchCriterias) {
    this.searchCriterias = searchCriterias;
  }

  @Override
  public void init() {

  }

  @Override
  public String managedBy() {
    return null;
  }

  public void setEntityId(String entityId) {
    this.entityId = entityId;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public void setNamesForSearch(String namesForSearch) {
    this.namesForSearch = namesForSearch;
  }
}
