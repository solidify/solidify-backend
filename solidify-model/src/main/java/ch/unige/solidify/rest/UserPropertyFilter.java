/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - UserPropertyFilter.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.PropertyWriter;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;

import ch.unige.solidify.auth.model.RoleView;
import ch.unige.solidify.auth.model.UserInfo;
import ch.unige.solidify.model.ChangeInfo;

public class UserPropertyFilter extends SimpleBeanPropertyFilter {

  private static final String EXTERNAL_UID_FIELD_NAME = "externalUid";
  private static final String WHO_FIELD_NAME = "who";
  private static final String LAST_LOGIN_IP_ADDRESS_FIELD_NAME = "lastLoginIpAddress";
  private static final String LAST_LOGIN_TIME_FIELD_NAME = "lastLoginTime";
  private static final String APPLICATION_ROLE_FIELD_NAME = "applicationRole";

  private final Class<?> jsonViewClass;
  private final String externalUid;

  public UserPropertyFilter(String externalUid, Class<?> jsonViewClass) {
    this.jsonViewClass = jsonViewClass;
    this.externalUid = externalUid;
  }

  @Override
  public void serializeAsField(Object pojo, JsonGenerator jsonGenerator, SerializerProvider provider, PropertyWriter propertyWriter) throws Exception {
    if (this.isAnonymousOrUser()) {
      if (!this.isFilteredField(propertyWriter) || this.isMyUserInfo(pojo) || this.isMyChangeInfo(pojo)) {
        propertyWriter.serializeAsField(pojo, jsonGenerator, provider);
      }
    } else {
      propertyWriter.serializeAsField(pojo, jsonGenerator, provider);
    }
  }

  private boolean isMyUserInfo(Object pojo) {
    if (!(pojo instanceof UserInfo)) {
      return false;
    }
    final String externalUidToSerialize = ((UserInfo) pojo).getExternalUid();
    return externalUidToSerialize != null && externalUidToSerialize.equals(this.externalUid);
  }

  private boolean isMyChangeInfo(Object pojo) {
    if (!(pojo instanceof ChangeInfo)) {
      return false;
    }
    final String externalUidToSerialize = ((ChangeInfo) pojo).getWho();
    return externalUidToSerialize != null && externalUidToSerialize.equals(this.externalUid);
  }

  private boolean isFilteredField(PropertyWriter propertyWriter) {
    return propertyWriter.getName().equals(EXTERNAL_UID_FIELD_NAME) ||
           propertyWriter.getName().equals(WHO_FIELD_NAME) ||
            propertyWriter.getName().equals(LAST_LOGIN_IP_ADDRESS_FIELD_NAME) ||
            propertyWriter.getName().equals(LAST_LOGIN_TIME_FIELD_NAME) ||
            propertyWriter.getName().equals(APPLICATION_ROLE_FIELD_NAME);
  }

  private boolean isAnonymousOrUser() {
    return this.jsonViewClass == null || this.jsonViewClass.equals(RoleView.User.class);
  }
}
