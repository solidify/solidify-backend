/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Email - EmailService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.exception.SolidifyRuntimeException;
import ch.unige.solidify.model.EmailParameters;
import ch.unige.solidify.util.StringTool;

@Profile("email-service")
@Service
public class EmailService {
  private static final Logger log = LoggerFactory.getLogger(EmailService.class);

  private final JavaMailSender emailSender;
  private final SpringTemplateEngine templateEngine;
  private final String senderAddress;
  private List<String> defaultCcList = new ArrayList<>();
  private List<String> defaultBccList = new ArrayList<>();

  public EmailService(SolidifyProperties config, JavaMailSender emailSender, SpringTemplateEngine templateEngine) {
    // Inject beans
    this.emailSender = emailSender;
    this.templateEngine = templateEngine;
    // Sender email
    if (StringTool.isNullOrEmpty(config.getEmail().getSenderAddress())) {
      throw new SolidifyRuntimeException("Missing sender address ('solidify.email.sender-address' parameter)");
    }
    this.senderAddress = config.getEmail().getSenderAddress();
    // List (CC & BCC) to copy
    if (config.getEmail().getCcList().length > 0) {
      this.defaultCcList = Arrays.asList(config.getEmail().getCcList());
    }
    if (config.getEmail().getBccList().length > 0) {
      this.defaultBccList = Arrays.asList(config.getEmail().getBccList());
    }
  }

  // ********************
  // ** Public methods **
  // ********************

  // HTML emails

  /**
   * @param template
   * @param toList
   * @param subject
   * @param parameters
   * @throws MessagingException
   * @deprecated Use method with EmailParameters instead
   */
  @Deprecated
  public void sendEmailWithTemplate(String template, List<String> toList, String subject, Map<String, Object> parameters)
          throws MessagingException {
    this.sendEmailWithTemplate(template, toList, null, null, subject, parameters);
  }

  /**
   * @param template
   * @param toList
   * @param ccList
   * @param bccList
   * @param subject
   * @param templateParameters
   * @throws MessagingException
   * @deprecated Use method with EmailParameters instead
   */
  @Deprecated
  public void sendEmailWithTemplate(String template, List<String> toList, List<String> ccList, List<String> bccList, String subject,
          Map<String, Object> templateParameters)
          throws MessagingException {
    EmailParameters emailParameters = new EmailParameters().setFrom(this.senderAddress).setToList(toList).setSubject(subject)
            .setTemplate(template).setTemplateParameters(templateParameters).addCc(ccList).addBcc(bccList);
    this.sendEmailWithTemplate(emailParameters);
  }

  public void sendEmailWithTemplate(EmailParameters parameters) throws MessagingException {
    if (StringTool.isNullOrEmpty(parameters.getFrom())) {
      parameters.setFrom(this.senderAddress);
    }
    if (StringTool.isNullOrEmpty(parameters.getTemplate())) {
      throw new SolidifyRuntimeException("email template is undefined");
    }
    String htmlBody = this.formatTemplate(parameters.getTemplate(), parameters.getTemplateParameters());
    parameters.setBody(htmlBody);
    this.sendHtmlEmail(parameters);
  }

  private String formatTemplate(String template, Map<String, Object> templateParameters) {
    final Context thymeleafContext = new Context();
    thymeleafContext.setVariables(templateParameters);
    return this.templateEngine.process(template + SolidifyConstants.HTML_EXT, thymeleafContext);
  }

  // Text email

  /**
   * @param toList
   * @param subject
   * @param body
   * @deprecated Use method with EmailParameters instead
   */
  @Deprecated
  public void sendEmail(List<String> toList, String subject, String body) {
    this.sendEmail(toList, null, null, subject, body);
  }

  /**
   * @param toList
   * @param ccList
   * @param bccList
   * @param subject
   * @param body
   * @deprecated Use method with EmailParameters instead
   */
  @Deprecated
  public void sendEmail(List<String> toList, List<String> ccList, List<String> bccList, String subject, String body) {
    EmailParameters emailParameters = new EmailParameters().setFrom(this.senderAddress).setToList(toList).setSubject(subject).setBody(body)
            .addCc(ccList).addBcc(bccList);
    this.sendEmail(emailParameters);
  }

  public void sendEmail(EmailParameters parameters) {
    if (StringTool.isNullOrEmpty(parameters.getFrom())) {
      parameters.setFrom(this.senderAddress);
    }
    this.sendTextEmail(parameters);
  }

  // *********************
  // ** Private methods **
  // *********************
  private void sendTextEmail(EmailParameters parameters) {
    try {
      SimpleMailMessage email = new SimpleMailMessage();
      // From
      email.setFrom(parameters.getFrom());
      // ReplyTo
      if (!StringTool.isNullOrEmpty(parameters.getReplyTo())) {
        email.setReplyTo(parameters.getReplyTo());
      }
      // To
      email.setTo(this.toArray(parameters.getToList()));
      // CC
      List<String> ccList = this.completeList(parameters.getCcList(), this.defaultCcList);
      if (!ccList.isEmpty()) {
        email.setCc(this.toArray(ccList));
      }
      // BCC
      List<String> bccList = this.completeList(parameters.getBccList(), this.defaultBccList);
      if (!bccList.isEmpty()) {
        email.setBcc(this.toArray(bccList));
      }
      // Subject
      email.setSubject(parameters.getSubject());
      // Body
      email.setText(parameters.getBody());
      // Send email
      this.emailSender.send(email);
      log.info("Email sent: {}", email);
    } catch (MailException e) {
      log.error("Email sending error: {}", e.getMessage(), e);
    }
  }

  private void sendHtmlEmail(EmailParameters parameters) throws MessagingException {
    MimeMessage email = this.emailSender.createMimeMessage();
    MimeMessageHelper emailHelper = new MimeMessageHelper(email, true, "UTF-8");
    // From
    emailHelper.setFrom(parameters.getFrom());
    // ReplyTo
    if (!StringTool.isNullOrEmpty(parameters.getReplyTo())) {
      emailHelper.setReplyTo(parameters.getReplyTo());
    }
    // To
    emailHelper.setTo(this.toArray(parameters.getToList()));
    // CC
    List<String> ccList = this.completeList(parameters.getCcList(), this.defaultCcList);
    if (!ccList.isEmpty()) {
      emailHelper.setCc(this.toArray(ccList));
    }
    // BCC
    List<String> bccList = this.completeList(parameters.getBccList(), this.defaultBccList);
    if (!bccList.isEmpty()) {
      emailHelper.setBcc(this.toArray(bccList));
    }
    // Subject
    emailHelper.setSubject(parameters.getSubject());
    // Convert HTML in text
    String plainBody = this.toPlainText(parameters.getBody());
    // Add body in text & HTML
    emailHelper.setText(plainBody, parameters.getBody());
    // Send email
    this.emailSender.send(email);
  }

  private String toPlainText(String htmlText) {
    String plainText = Jsoup.parse(htmlText).wholeText();
    StringBuilder sb = new StringBuilder();
    for (String line : plainText.split("\n")) {
      if (!StringTool.isNullOrEmpty(line.strip())) {
        sb.append(line.strip());
        sb.append("\n");
      }
    }
    return sb.toString();
  }

  private String[] toArray(List<String> list) {
    if (list == null) {
      list = new ArrayList<>();
    }
    return list.toArray(new String[list.size()]);
  }

  private List<String> completeList(List<String> list, List<String> copyList) {
    if (list == null) {
      list = new ArrayList<>();
    }
    if (!copyList.isEmpty()) {
      list.addAll(copyList);
    }
    return list;
  }
}
