/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - SolidifyOpenApiConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springdoc.core.customizers.OpenApiCustomizer;
import org.springdoc.core.models.GroupedOpenApi;
import org.springdoc.core.properties.SwaggerUiOAuthProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.Scopes;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.security.SecurityScheme.Type;
import io.swagger.v3.oas.models.tags.Tag;

import ch.unige.solidify.auth.model.AuthApplicationRole;

public abstract class SolidifyOpenApiConfig {

  protected SolidifyOpenApiConfig(SwaggerUiOAuthProperties swaggerUiOAuthProperties) {
    swaggerUiOAuthProperties.setUseBasicAuthenticationWithAccessCodeGrant(true);
  }

  protected abstract String getAuthorizationServerUrl();

  protected abstract String getApplicationName();

  protected abstract String getApplicationDescription();

  protected abstract String getApplicationVersion();

  protected abstract String getApplicationScope();

  protected abstract String getContactEmail();

  protected abstract String getApplicationTagName();

  @Bean
  public OpenAPI applicationOpenAPI() {
    return new OpenAPI()
            .info(new Info()
                    .title(this.getApplicationName() + " API")
                    .version(this.getApplicationVersion())
                    .description(this.getApplicationDescription())
                    .contact(new Contact()
                            .email(this.getContactEmail()))
                    .license(new License()
                            .name("GNU General Public License v2.0 or later")
                            .identifier("GPL-2.0-or-later")))
            .components(new Components()
                    .addSecuritySchemes(AuthApplicationRole.TOKEN_AUTH, this.tokenSecurityScheme())
                    .addSecuritySchemes(AuthApplicationRole.AUTH, this.oauth2SecurityScheme()))
            .addTagsItem(new Tag().name(this.getApplicationTagName()));
  }

  @Bean
  public OpenApiCustomizer addModuleTagOpenAPICustomiser() {
    return openApi -> openApi.getPaths().entrySet().stream()
            .forEach(path -> {
              final PathItem pathItem = path.getValue();
              // Update Tags
              pathItem.readOperations().stream()
                      .forEach(operation -> this.updateTags(path.getKey(), operation));
              // Update operationId
              this.updateOperationId(path.getKey(), pathItem);
            });
  }

  private SecurityScheme oauth2SecurityScheme() {
    return new SecurityScheme()
            .type(Type.OAUTH2)
            .flows(new OAuthFlows()
                    .authorizationCode(new OAuthFlow()
                            .authorizationUrl(this.getAuthorizationServerUrl() + "/oauth/authorize")
                            .tokenUrl(this.getAuthorizationServerUrl() + "/oauth/token")
                            .scopes(new Scopes()
                                    .addString(this.getApplicationScope(), this.getApplicationName()))));
  }

  protected SecurityScheme tokenSecurityScheme() {
    return new SecurityScheme()
            .type(Type.HTTP)
            .scheme("bearer")
            .bearerFormat("JWT");
  }

  protected GroupedOpenApi moduleGroup(String moduleName, String... pathsToMatch) {
    List<String> pathList = new ArrayList<>(Arrays.asList(pathsToMatch));
    pathList.add("/" + moduleName + "/**");
    return GroupedOpenApi.builder()
            .group(moduleName)
            .pathsToMatch(pathList.toArray(new String[0]))
            .addOpenApiCustomizer(this.addModuleTagOpenAPICustomiser())
            .build();
  }

  private void updateTags(String path, Operation operation) {
    operation.getTags().clear();
    String[] pathDetails = path.split("/");
    if (pathDetails.length > 2) {
      operation.addTagsItem(pathDetails[1] + "/" + pathDetails[2]);
    } else if (pathDetails.length > 1) {
      operation.addTagsItem(pathDetails[1]);
    } else {
      operation.addTagsItem(this.getApplicationTagName());
    }
  }

  private void updateOperationId(String path, PathItem pathItem) {
    if (pathItem.getGet() != null) {
      pathItem.getGet().setOperationId(this.getOperationId(HttpMethod.GET, path, pathItem.getGet().getOperationId()));
    }
    if (pathItem.getPost() != null) {
      pathItem.getPost().setOperationId(this.getOperationId(HttpMethod.POST, path, pathItem.getPost().getOperationId()));
    }
    if (pathItem.getPut() != null) {
      pathItem.getPut().setOperationId(this.getOperationId(HttpMethod.PUT, path, pathItem.getPut().getOperationId()));
    }
    if (pathItem.getPatch() != null) {
      pathItem.getPatch().setOperationId(this.getOperationId(HttpMethod.PATCH, path, pathItem.getPatch().getOperationId()));
    }
    if (pathItem.getDelete() != null) {
      pathItem.getDelete().setOperationId(this.getOperationId(HttpMethod.DELETE, path, pathItem.getDelete().getOperationId()));
    }
    if (pathItem.getOptions() != null) {
      pathItem.getOptions().setOperationId(this.getOperationId(HttpMethod.OPTIONS, path, pathItem.getOptions().getOperationId()));
    }
    if (pathItem.getHead() != null) {
      pathItem.getHead().setOperationId(this.getOperationId(HttpMethod.HEAD, path, pathItem.getHead().getOperationId()));
    }
    if (pathItem.getTrace() != null) {
      pathItem.getTrace().setOperationId(this.getOperationId(HttpMethod.TRACE, path, pathItem.getTrace().getOperationId()));
    }
  }

  public String getOperationId(HttpMethod httpMethod, String endpointContext, String originalOperationId) {
    String endpointAction = endpointContext.substring(endpointContext.lastIndexOf("/") + 1);
    String newOperationId = endpointContext.substring(1);
    if (this.isDifferentAction(endpointAction, originalOperationId)) {
      if (originalOperationId.contains("_")) {
        newOperationId += "_" + originalOperationId.substring(0, originalOperationId.indexOf('_'));
      } else {
        newOperationId += "_" + originalOperationId;
      }
    }
    if (newOperationId.endsWith("advancedSearch")
            || newOperationId.endsWith("search")) {
      newOperationId += "_" + httpMethod.name().toLowerCase();
    }
    newOperationId = newOperationId.replace("/**", "");
    newOperationId = newOperationId.replace('/', '_');
    newOperationId = newOperationId.replaceAll("\\{[A-Za-z0-9-_]+\\}", "by_id");
    return newOperationId;
  }

  private boolean isDifferentAction(String pathAction, String oldOperationId) {
    if (pathAction.length() == oldOperationId.length()) {
      return !pathAction.equals(oldOperationId);
    }
    String action = pathAction.substring(0, this.findFirstSeparatorCharacter(pathAction));
    String operation = oldOperationId.substring(0, this.findFirstSeparatorCharacter(oldOperationId));
    if (action.length() < operation.length()) {
      operation = operation.substring(0, action.length());
    } else {
      action = action.substring(0, operation.length());
    }
    return !action.equals(operation);
  }

  private int findFirstSeparatorCharacter(String str) {
    for (int i = 0; i < str.length(); i++) {
      if (Character.isUpperCase(str.charAt(i))
              || str.charAt(i) == '_'
              || str.charAt(i) == '/') {
        return i;
      }
    }
    return str.length();
  }
}
