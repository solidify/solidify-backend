/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - NoSqlResourceService.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

public abstract class NoSqlResourceService<T> extends AbstractResourceService {

  public Page<T> findAll(T search, Pageable pageable) {
    final List<T> list = this.findAll(search);
    final long start = pageable.getOffset();
    final long end = (start + pageable.getPageSize()) > list.size() ? list.size()
            : (start + pageable.getPageSize());
    return new PageImpl<>(list.subList((int) start, (int) end), pageable, list.size());
  }

  public abstract T findOne(String id);

  public abstract List<T> findAll(T search);

  public abstract T save(T t);

  public abstract T update(T t);

  public abstract boolean delete(T t);

}
