/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - SearchSpecificationTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.specification;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsIn.in;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;

import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.ContextConfiguration;

import jakarta.annotation.Resource;

import ch.unige.solidify.test.config.MockUserJpaConfig;
import ch.unige.solidify.test.specification.dao.MockUser;
import ch.unige.solidify.test.specification.dao.MockUserInfo;
import ch.unige.solidify.test.specification.dao.MockUserInfoRepository;
import ch.unige.solidify.test.specification.dao.MockUserRepository;
import ch.unige.solidify.test.specification.dao.MockUserSearchSpecification;
import ch.unige.solidify.util.SearchCriteria;
import ch.unige.solidify.util.SearchOperation;

@DataJpaTest
@ContextConfiguration(classes = { MockUserJpaConfig.class })
class SearchSpecificationTest {

  @Resource
  private MockUserInfoRepository mockUserInfoRepository;
  @Resource
  private MockUserRepository userRepository;

  LocalDate baseDate = LocalDate.parse("2016-08-16");
  LocalDateTime baseDateTime = LocalDateTime.parse("2015-12-03T10:15:30");
  OffsetDateTime baseOffsetTime = OffsetDateTime.parse("2007-12-03T10:15:30+01:00");
  MockUser user1;
  MockUser user2;
  MockUser user3;

  // Test Greater Than and less Than
  @Test
  void ageSearch_ByGreaterLess_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("i", "age", SearchOperation.LESS_THAN, 30);
    final SearchCriteria criteria2 = new SearchCriteria("age", SearchOperation.GREATER_THAN, 20);
    final List<MockUser> results = this.userRepository.findAll(
            Specification.where(new MockUserSearchSpecification(criteria1))
                    .and(new MockUserSearchSpecification(criteria2)));
    assertEquals(1, results.size());
    final MockUser userFound = results.get(0);
    assertEquals("john", userFound.getName());
  }

  // Test EQUALITY with BigDecimal field type
  @Test
  void BigDecimalSearch_equals_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("salary", SearchOperation.EQUALITY, "200.58");
    final List<MockUser> results = this.userRepository.findAll(Specification.where(new MockUserSearchSpecification(criteria1)));
    assertEquals(1, results.size());
    final MockUser userFound = results.get(0);
    assertEquals("john", userFound.getName());
  }

  //
  @Test
  void compare_2Criterias_equal() {
    final SearchCriteria criteria1 = new SearchCriteria("name", SearchOperation.STARTS_WITH, "jo");
    final SearchCriteria criteria2 = new SearchCriteria("name", SearchOperation.STARTS_WITH, "jo");
    assertEquals(criteria2, criteria1);
    criteria2.setCaseType("i");
    assertNotEquals(criteria2, criteria1);
    criteria2.setCaseType("c");
    criteria2.setOperation("~");
    assertNotEquals(criteria2, criteria1);
  }

  /**
   * Test contains and case insensitive
   */
  @Test
  void givenNameSearch_ByContainsCaseInsensitive_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("i", "name", SearchOperation.CONTAINS, "O");
    final List<MockUser> results = this.userRepository.findAll(Specification.where(new MockUserSearchSpecification(criteria1)));
    assertEquals(3, results.size());
    final SearchCriteria criteria2 = new SearchCriteria("name", SearchOperation.CONTAINS, "O");
    final List<MockUser> results2 = this.userRepository.findAll(
            Specification.where(new MockUserSearchSpecification(criteria2)));
    assertEquals(0, results2.size());
  }

  @Test
  void givenNameSearch_ByEqual_thenCorrect() {
    final SearchCriteria criteria = new SearchCriteria("name", SearchOperation.EQUALITY, "john");
    final Specification<MockUser> spec = new MockUserSearchSpecification(criteria);
    final List<MockUser> results = this.userRepository.findAll(spec);
    MatcherAssert.assertThat(this.user1, is(in(results)));
  }

  /**
   * Test starts with and not
   */
  @Test
  void givenNamesearch_ByStartsWithNot_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("name", SearchOperation.STARTS_WITH, "jo");
    final SearchCriteria criteria2 = new SearchCriteria("name", SearchOperation.NEGATION, "jo");
    final List<MockUser> results = this.userRepository.findAll(
            Specification.where(new MockUserSearchSpecification(criteria1))
                    .and(new MockUserSearchSpecification(criteria2)));
    final MockUser userFound = results.get(0);
    assertEquals("john", userFound.getName());
    assertNotEquals("jo", userFound.getName());
  }

  // Test BETWEEN with Integer field type
  @Test
  void IntegerSearch_between_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("childNumber", SearchOperation.BETWEEN, "4_8");
    final List<MockUser> results = this.userRepository.findAll(Specification.where(new MockUserSearchSpecification(criteria1)));
    assertEquals(1, results.size());
    final MockUser userFound = results.get(0);
    assertEquals("jo", userFound.getName());
  }

  // Test LESS THAN OR EQUAL with Localedate field type
  @Test
  void LocaleDateSearch_lessEquals_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("birthday", SearchOperation.GREATER_EQUAL, "1997-01-30");
    final List<MockUser> results = this.userRepository.findAll(Specification.where(new MockUserSearchSpecification(criteria1)));
    assertEquals(1, results.size());
    final MockUser userFound = results.get(0);
    assertEquals("john", userFound.getName());
  }

  // Test BETWEEN with LocaledateTime field type
  @Test
  void LocaledateTimeSearch_between_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("marriageTime", SearchOperation.BETWEEN,
            "2003-10-03T10:15:30_2016-02-03T12:15:30");
    final List<MockUser> results = this.userRepository.findAll(Specification.where(new MockUserSearchSpecification(criteria1)));
    assertEquals(2, results.size());
    final MockUser userFound = results.get(0);
    assertEquals("john", userFound.getName());
  }

  // Test GREATER THAN OR EQUAL with Long field type
  @Test
  void LongSearch_greaterEquals_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("carKilos", SearchOperation.GREATER_EQUAL, "3000");
    final List<MockUser> results = this.userRepository.findAll(Specification.where(new MockUserSearchSpecification(criteria1)));
    assertEquals(2, results.size());
    final MockUser userFound = results.get(0);
    assertEquals("doe", userFound.getName());
  }

  // TEST equal with OFfsetDateTime baseOffsetTime
  @Test
  void OffsetdateTimeSearch_equals_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("meetingTime", SearchOperation.EQUALITY,
            "2007-12-03T10:15:30+01:00");
    final List<MockUser> results = this.userRepository.findAll(Specification.where(new MockUserSearchSpecification(criteria1)));
    assertEquals(3, results.size());
  }

  @Test
  void searchOne2OneEntity_ByBooleanValue_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("userInfo.adminuser", SearchOperation.EQUALITY,
            "true");
    final List<MockUser> results = this.userRepository.findAll(Specification.where(new MockUserSearchSpecification(criteria1)));
    assertEquals(2, results.size());
    final MockUser userFound = results.get(0);
    assertEquals(true, userFound.getUserInfo().isAdminuser());
  }

  @Test
  void searchOne2OneEntity_ByContains_thenCorrect() {
    final SearchCriteria criteria1 = new SearchCriteria("userInfo.info", SearchOperation.CONTAINS,
            "Doe");
    final List<MockUser> results = this.userRepository.findAll(Specification.where(new MockUserSearchSpecification(criteria1)));
    assertEquals(1, results.size());
    final MockUser userFound = results.get(0);
    assertEquals("Doe info", userFound.getUserInfo().getInfo());
  }

  @BeforeEach
  void setup() {
    final MockUserInfo info = new MockUserInfo("1", "John info", true);
    final MockUserInfo info2 = new MockUserInfo("2", "Doe info", false);
    final MockUserInfo info3 = new MockUserInfo("3", "Jo info", true);
    this.mockUserInfoRepository.save(info);
    this.mockUserInfoRepository.save(info2);
    this.mockUserInfoRepository.save(info3);
    this.mockUserInfoRepository.flush();

    this.user1 = new MockUser("john", 22, 3, 2000L, new BigDecimal("200.58"),
            this.baseDate.minusYears(10), this.baseDateTime.minusYears(5).minusMonths(2), this.baseOffsetTime);
    this.user2 = new MockUser("doe", 15, 1, 4000L, new BigDecimal("300.25"),
            this.baseDate.minusYears(20), this.baseDateTime.minusYears(10).minusMonths(2), this.baseOffsetTime);
    this.user3 = new MockUser("jo", 40, 5, 3000L, new BigDecimal("400.13"),
            this.baseDate.minusYears(30), this.baseDateTime.minusYears(15).minusMonths(2), this.baseOffsetTime);
    this.user1.setUserInfo(info);
    this.user2.setUserInfo(info2);
    this.user3.setUserInfo(info3);

    this.userRepository.save(this.user1);
    this.userRepository.save(this.user2);
    this.userRepository.save(this.user3);
    this.user1 = this.userRepository.findByName("john");
    this.user2 = this.userRepository.findByName("doe");
    this.user3 = this.userRepository.findByName("jo");
  }

}
