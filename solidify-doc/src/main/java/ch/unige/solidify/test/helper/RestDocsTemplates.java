/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Documentation - RestDocsTemplates.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test.helper;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.hamcrest.Matcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.restdocs.hypermedia.LinkDescriptor;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.snippet.Attributes.Attribute;
import org.springframework.stereotype.Service;

import ch.unige.solidify.SolidifyConstants;
import ch.unige.solidify.config.SolidifyProperties;
import ch.unige.solidify.rest.ActionName;
import ch.unige.solidify.test.exception.SolidifyTestRuntimeException;
import ch.unige.solidify.util.StringTool;

/**
 * Helper retrieving the default REST fields
 */
@Service
public class RestDocsTemplates {

  public static final String CREATION_FIELD = "creation";
  public static final String DATA_FIELD = "_data";
  public static final String HREF_FIELD = "href";
  public static final String LAST_UPDATE_FIELD = "lastUpdate";
  public static final String LINKS_FIELD = "_links";
  public static final String NAME_FIELD = "name";
  public static final String PAGE_FIELD = "_page";
  public static final String RES_SRV = SolidifyConstants.RES_SRV;
  public static final String RESOURCE_ID_FIELD = SolidifyConstants.DB_RES_ID;
  public static final String RESOURCES_FIELD = "resources";
  public static final String SELF_FIELD = "self";
  public static final String TOOLS_FIELD = "tools";

  private static final String RESOURCE_TEXT = "_ resource";
  private static final String THE_TEXT = " the _";

  private static String getResourceName(String resource) {
    return resource.substring(resource.lastIndexOf('/') + 1);
  }

  private static String getToolName(String tool) {
    return tool.substring(tool.lastIndexOf('=') + 1);
  }
  // ================================
  // Application
  // ================================

  @Value("${server.servlet.context-path}")
  protected String applicationContext;

  @Autowired
  protected SolidifyProperties config;

  /**
   * Application fields description template
   */
  public List<FieldDescriptor> getApplicationFieldsTemplate() {
    return Arrays.asList(subsectionWithPath(NAME_FIELD).description("The name of the application"),
            subsectionWithPath(LINKS_FIELD).description("The _links_ list of the application"));
  }

  /**
   * Application _links description template
   */
  public List<LinkDescriptor> getApplicationLinkDescriptors(List<String> modules) {
    final List<LinkDescriptor> linkDescriptorList = new ArrayList<>();
    assertNotNull(modules);
    this.getCommonApplicationLinksFields().forEach(action -> {
      if (action.equals(SELF_FIELD)) {
        linkDescriptorList.add(linkWithRel(action).description("<<application, Links>> to application itself"));
      } else {
        linkDescriptorList.add(linkWithRel(action).description("<<" + action + ", Links>> to _" + action + RESOURCE_TEXT));
      }
    });
    modules.forEach(
            action -> linkDescriptorList.add(linkWithRel(action).description("<<" + action + ", Links>> to _" + action + RESOURCE_TEXT)));
    return linkDescriptorList;
  }

  /**
   * Aplication test template
   */
  public Map<String, Matcher<?>> getApplicationTestTemplate(List<String> modules) {
    final Map<String, Matcher<?>> linksMap = new LinkedHashMap<>();
    linksMap.put("$.name", allOf(is(notNullValue()), is(this.config.getServerDisplayName())));
    linksMap.put("$._links.*", hasSize(this.getBaseApplicationFieldsTemplate().size() + modules.size()));
    this.getBaseApplicationFieldsTemplate().forEach((attribute, uri) -> linksMap.put("$._links." + attribute + "." + HREF_FIELD,
            allOf(is(notNullValue()), anyOf(endsWith(uri.getPath())))));
    for (final String module : modules) {
      linksMap.put("$._links." + module + "." + HREF_FIELD, allOf(is(notNullValue()), endsWith(this.applicationContext + "/" + module)));
    }
    return linksMap;
  }

  public List<String> getCommonApplicationFields() {
    return Arrays.asList(NAME_FIELD, LINKS_FIELD);
  }

  public List<String> getCommonModuleFields() {
    return List.of(NAME_FIELD, LINKS_FIELD);
  }

  // ================================
  // Module
  // ================================

  public List<String> getCommonResourceListFields() {
    return List.of(DATA_FIELD, PAGE_FIELD, LINKS_FIELD);
  }

  /**
   * Module fields description template
   */
  public List<FieldDescriptor> getModuleFieldsDescriptor(String module) {
    return Arrays.asList(fieldWithPath(NAME_FIELD).description("The name of the module, i.e. _" + module + "_")
            .attributes(new Attribute(AbstractResourceDoc.MANDATORY_VALUE, String.valueOf(true))),
            subsectionWithPath(LINKS_FIELD).description("The _links_ list of the module, included its REST resources"));
  }

  /**
   * Module _links description template
   */
  public List<LinkDescriptor> getModulesLinkDescriptors(String module, List<String> resources, List<String> tools) {
    final List<LinkDescriptor> linkDescriptorList = new ArrayList<>();
    assertNotNull(resources);
    this.getCommonModuleLinksFields()
            .forEach(attribute -> linkDescriptorList.add(linkWithRel(attribute).description("<<" + module + ", Links>> to module itself")));

    final StringBuilder stringBuilder = new StringBuilder();
    resources.forEach(resource -> {
      final String resourceName = getResourceName(resource);
      stringBuilder.append("<<" + module + "-" + resourceName + ", REST Resource>> named _" + resourceName + "_");
      stringBuilder.append("\n{empty} +\n");
    });
    linkDescriptorList.add(linkWithRel(RESOURCES_FIELD).description(stringBuilder.toString()));
    if (module.equals(RES_SRV)) {
      final StringBuilder toolDescription = new StringBuilder();
      tools.forEach(tool -> {
        final String toolName = getToolName(tool);
        toolDescription.append("<<tool>> named _" + StringTool.decodeUrl(toolName) + "_");
        toolDescription.append("\n{empty} +\n");
      });
      linkDescriptorList.add(linkWithRel(TOOLS_FIELD).description(toolDescription.toString()));
    }
    return linkDescriptorList;
  }

  /**
   * Resource fields description template
   */
  public List<FieldDescriptor> getResourceCollectionFieldDescriptors() {
    return Arrays.asList(subsectionWithPath(DATA_FIELD).description("List the collection of the resources"),
            subsectionWithPath(PAGE_FIELD).description("Gives the total pages and the total available items of one resource"),
            subsectionWithPath(LINKS_FIELD).description("All links available on the resource"));
  }

  public List<LinkDescriptor> getResourceDataLinkDescriptors(String resource, List<String> specificLinks) {
    final List<LinkDescriptor> linkDescriptorList = new ArrayList<>();
    final String resourceName = getResourceName(resource);
    linkDescriptorList.add(linkWithRel(ActionName.SELF).description("Link to the _" + resourceName + RESOURCE_TEXT));
    linkDescriptorList.add(linkWithRel(ActionName.LIST).description("Link to the collection of _" + resourceName + RESOURCE_TEXT));
    linkDescriptorList.add(linkWithRel(ActionName.MODULE).description("Link to the parent's resource"));
    linkDescriptorList.addAll(this.getSpecificLinks(resourceName, specificLinks));
    return linkDescriptorList;
  }

  /**
   * Module fields description template
   */
  public List<FieldDescriptor> getResourceFieldDescriptors(String resource, List<FieldDescriptor> resFields, List<String> commonFieldList) {
    final String resourceName = getResourceName(resource);
    List<FieldDescriptor> resultList = new ArrayList<>();
    for (String commonField : commonFieldList) {
      resultList.add(this.getFieldDescription(resourceName, commonField));
    }
    resultList = Stream.concat(resultList.stream(), resFields.stream()).collect(Collectors.toList());
    return resultList;
  }

  private FieldDescriptor getFieldDescription(String resourceName, String commonField) {
    return switch (commonField) {
      case RESOURCE_ID_FIELD -> subsectionWithPath(RESOURCE_ID_FIELD)
              .description(
                      "The _" + resourceName + "_ resource identifier [https://en.wikipedia.org/wiki/Universally_unique_identifier[UUID]]");
      case CREATION_FIELD ->
              subsectionWithPath(CREATION_FIELD).description("The creation info of the _" + resourceName + "_ resource (see <<change-info>>)");
      case LAST_UPDATE_FIELD -> subsectionWithPath(LAST_UPDATE_FIELD)
              .description("The last updated info of the _" + resourceName + "_ resource (see <<change-info>>)");
      case LINKS_FIELD -> subsectionWithPath(LINKS_FIELD).description("The _links_ list of the _" + resourceName + RESOURCE_TEXT);
      default -> throw new SolidifyTestRuntimeException("Wrong common field " + commonField + " for " + resourceName + " resource");
    };
  }

  /**
   * Template description for resource links fields
   */
  public List<LinkDescriptor> getResourceLinkDescriptors(String module, String resource, List<String> specificLinks) {
    final List<LinkDescriptor> linkDescriptorList = new ArrayList<>();
    final String LINK_PREFIX = "Link to ";
    final String resourceName = getResourceName(resource);
    linkDescriptorList.add(linkWithRel(ActionName.SELF).description(LINK_PREFIX + THE_TEXT + resourceName + "_ collection"));
    linkDescriptorList.add(linkWithRel(ActionName.MODULE).description(LINK_PREFIX + THE_TEXT + module + "_ module"));
    linkDescriptorList.add(linkWithRel(ActionName.LAST_CREATED)
            .description(LINK_PREFIX + THE_TEXT + resourceName + "_ collection sorted by creation date"));
    linkDescriptorList.add(linkWithRel(ActionName.LAST_UPDATED)
            .description(LINK_PREFIX + THE_TEXT + resourceName + "_ collection sorted by last updated date"));
    linkDescriptorList.addAll(this.getSpecificLinks(resourceName, specificLinks));
    return linkDescriptorList;
  }

  /**
   * Application default _links fields
   */

  private Map<String, URI> getBaseApplicationFieldsTemplate() {
    final Map<String, URI> baseAttributes = new LinkedHashMap<>();
    try {
      baseAttributes.put(ActionName.SELF, new URI(this.applicationContext + "/"));
      baseAttributes.put(ActionName.DASHBOARD, new URI(this.applicationContext + "/" + ActionName.DASHBOARD));
    } catch (final URISyntaxException e) {
      throw new SolidifyTestRuntimeException("Invalid URL.", e);
    }
    return baseAttributes;
  }

  private List<String> getCommonApplicationLinksFields() {
    return Arrays.asList(ActionName.SELF, ActionName.DASHBOARD);
  }

  private List<String> getCommonModuleLinksFields() {
    return List.of(ActionName.SELF, ActionName.RESOURCES);
  }

  private List<LinkDescriptor> getSpecificLinks(String resourceName, List<String> specificLinks) {
    final List<LinkDescriptor> linkDescriptorList = new ArrayList<>();
    if (!specificLinks.isEmpty()) {
      specificLinks.stream().forEach(link -> linkDescriptorList
              .add(linkWithRel(link).description("Link to _" + link + "_ action on the current _" + resourceName + RESOURCE_TEXT)));
    }
    return linkDescriptorList;
  }

}
