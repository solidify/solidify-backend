/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Model - ResourceBase.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.rest;

import static ch.unige.solidify.SolidifyConstants.RES_ID_FIELD;

import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.hateoas.RepresentationModel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Transient;

import ch.unige.solidify.model.ChangeInfo;

public abstract class ResourceBase extends RepresentationModel<ResourceBase> {
  /**
   * Indicates whether any resources contained in the resource should be updated when the resource itself is updated by the BasicEntityMapper.
   * Note: this has no effect on the resId of the contained resources so that the relationship between the entity and its children can always be
   * modified
   */

  protected ResourceBase() {}

  @JsonIgnore
  @Transient
  public List<String> getNonUpdatableFields() {
    return Arrays.asList(RES_ID_FIELD);
  }

  @JsonIgnore
  @Transient
  public List<String> getPatchableSubResources() {
    return List.of();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof ResourceBase resourceBase)) {
      return false;
    }
    if (this.getCreationTime() != null && this.getUpdateTime() != null) {
      return super.equals(obj)
              && Objects.equals(this.getCreationTime(), resourceBase.getCreationTime())
              && Objects.equals(this.getUpdateTime(), resourceBase.getUpdateTime());
    } else if (this.getCreationTime() != null) {
      return super.equals(obj)
              && Objects.equals(this.getCreationTime(), resourceBase.getCreationTime());
    } else if (this.getUpdateTime() != null) {
      return super.equals(obj)
              && Objects.equals(this.getUpdateTime(), resourceBase.getUpdateTime());
    }
    return super.equals(obj);
  }

  @JsonIgnore
  public abstract String getCreatedBy();

  @JsonIgnore
  public abstract String getCreatorName();

  @JsonIgnore
  public abstract String getEditorName();

  public abstract ChangeInfo getCreation();

  @JsonIgnore
  @Transient
  public abstract OffsetDateTime getCreationTime();

  public abstract ChangeInfo getLastUpdate();

  @JsonIgnore
  public abstract String getUpdatedBy();

  @JsonIgnore
  public abstract OffsetDateTime getUpdateTime();

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), this.getCreationTime(), this.getUpdateTime());
  }

  public abstract void setCreatedBy(String createdBy);

  public abstract void setCreatorName(String creatorName);

  public abstract void setUpdatedBy(String updatedBy);

  public abstract void setEditorName(String editorName);

  public abstract void updateTime();

}
