/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Documentation - TestObjectBuilderTest.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.test;

import static ch.unige.solidify.test.helper.AbstractResourceDoc.TEST_STRING_VALUE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.snippet.Attributes.Attribute;

import ch.unige.solidify.test.helper.TestObjectBuilder;

class TestObjectBuilderTest {

  private List<FieldDescriptor> fieldDescriptorList;

  @BeforeEach
  void setUp() {
    // @formatter:off
    this.fieldDescriptorList = Arrays.asList(
            fieldWithPath("a.b").attributes(new Attribute(TEST_STRING_VALUE, "1")),

            fieldWithPath("a.d.b").attributes(new Attribute(TEST_STRING_VALUE, "2")),

            fieldWithPath("c.d.b").attributes(new Attribute(TEST_STRING_VALUE, "3")),

            fieldWithPath("c.d.c").attributes(new Attribute(TEST_STRING_VALUE, "4")));

    // @formatter:on
  }

  @Test
  void testEmptyObject() {
    final JSONObject jsonObject = TestObjectBuilder.buildJsonObject(Collections.emptyList());
    assertEquals("{}", jsonObject.toString());
  }

  @Test
  void testJsonBuild() {
    final JSONObject jsonObject = TestObjectBuilder.buildJsonObject(this.fieldDescriptorList);
    assertEquals("1", jsonObject.getJSONObject("a").get("b"));
    assertEquals("2", jsonObject.getJSONObject("a").getJSONObject("d").get("b"));
    assertEquals("3", jsonObject.getJSONObject("c").getJSONObject("d").get("b"));
    assertEquals("4", jsonObject.getJSONObject("c").getJSONObject("d").get("c"));
  }
}
