/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Controller - JmsConfig.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.config;

import jakarta.jms.ConnectionFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

@Configuration
public class JmsConfig {

  // Value in milliseconds set to one week
  private static final long JMS_MESSAGE_TIME_TO_LIVE = (long) 1000 * 60 * 60 * 24 * 7;

  @Value("${solidify.queue.transacted-session:false}")
  private boolean queueTransactedSession;

  @Value("${solidify.topic.transacted-session:false}")
  private boolean topicTransactedSession;

  @Bean
  @Primary
  public JmsTemplate queueJmsTemplate(ConnectionFactory connectionFactory) {
    final JmsTemplate jmsTemplate = new JmsTemplate();
    jmsTemplate.setConnectionFactory(connectionFactory);
    jmsTemplate.setExplicitQosEnabled(true);
    jmsTemplate.setTimeToLive(JMS_MESSAGE_TIME_TO_LIVE);
    jmsTemplate.setSessionTransacted(this.queueTransactedSession);

    // send messages to Queues
    jmsTemplate.setPubSubDomain(false);

    return jmsTemplate;
  }

  @Bean
  public JmsTemplate topicJmsTemplate(ConnectionFactory connectionFactory) {

    final JmsTemplate jmsTemplate = new JmsTemplate();
    jmsTemplate.setConnectionFactory(connectionFactory);
    jmsTemplate.setExplicitQosEnabled(true);
    jmsTemplate.setTimeToLive(JMS_MESSAGE_TIME_TO_LIVE);
    jmsTemplate.setSessionTransacted(this.topicTransactedSession);

    // send messages to Topics
    jmsTemplate.setPubSubDomain(true);

    return jmsTemplate;
  }

  @Bean
  public DefaultJmsListenerContainerFactory topicListenerFactory(
          ConnectionFactory connectionFactory,
          DefaultJmsListenerContainerFactoryConfigurer configurer) {

    final DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

    // This provides all boot's default to this factory, including the message converter
    configurer.configure(factory, connectionFactory);

    // listen to Topics
    factory.setPubSubDomain(true);

    return factory;
  }
}
