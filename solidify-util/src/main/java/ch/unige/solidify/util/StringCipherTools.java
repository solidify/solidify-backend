/*-
 * %%----------------------------------------------------------------------------------------------
 * Solidify Framework - Solidify Util - StringCipherTools.java
 * SPDX-License-Identifier: GPL-2.0-or-later
 * %----------------------------------------------------------------------------------------------%
 * Copyright (C) 2017 - 2024 University of Geneva
 * %----------------------------------------------------------------------------------------------%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * ----------------------------------------------------------------------------------------------%%
 */

package ch.unige.solidify.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.springframework.security.crypto.encrypt.BouncyCastleAesCbcBytesEncryptor;

import jakarta.xml.bind.DatatypeConverter;

import ch.unige.solidify.SolidifyConstants;

/**
 * This class provides a way to encrypt or decrypt a string. The encrypted text in Hexdecimal
 * format. Encryption and decryption use BouncyCastleAesCbcBytesEncryptor
 **/
public class StringCipherTools {

  // According to RFC 8018 salt should be at least 64 bits
  private static final int SALT_LENGTH = 8;

  /**
   * Method to decrypt text
   *
   * @param cipherText
   * encrypted text
   * @param password
   * password
   * @return decrypted clear text
   */
  public static String decrypt(String cipherText, String password) {
    return decrypt(cipherText, password, SALT_LENGTH);
  }

  /**
   * Method to decrypt text
   *
   * @param cipherText
   * encrypted text
   * @param password
   * password
   * @param saltlength
   * length of salt
   * @return decrypted clear text
   */
  public static String decrypt(String cipherText, String password, int saltlength) {
    try (ByteArrayInputStream is = new ByteArrayInputStream(DatatypeConverter.parseHexBinary(cipherText))) {
      final byte[] salt = new byte[saltlength];
      final int cipherTextRealSize = is.available() - saltlength;
      final byte[] cipherTextPart = new byte[cipherTextRealSize];
      is.read(salt, 0, saltlength);
      is.read(cipherTextPart, 0, cipherTextRealSize);
      final BouncyCastleAesCbcBytesEncryptor encryptor = new BouncyCastleAesCbcBytesEncryptor(password,
              DatatypeConverter.printHexBinary(salt));
      return new String(encryptor.decrypt(cipherTextPart), StandardCharsets.UTF_8);
    } catch (final IOException e) {
      throw new IllegalStateException("Could not read buffer during decryption", e);
    }
  }

  /**
   * Method to encrypt plain text String
   *
   * @param plainText
   * clear text to be encrypted
   * @param password
   * password
   * @return encrypted text in Hexdecimal format
   */
  public static String encrypt(String plainText, String password) {
    return encrypt(plainText, password, SALT_LENGTH);
  }

  /**
   * Method to encrypt plain text String
   *
   * @param plainText
   * clear text to be encrypted
   * @param password
   * password
   * @param salt
   * salt
   * @return encrypted text in Hexdecimal format
   */
  public static String encrypt(String plainText, String password, byte[] salt) {

    if (salt == null || salt.length == 0) {
      throw new IllegalArgumentException("Salt must not be empty");
    }

    final int saltLength = salt.length;
    final BouncyCastleAesCbcBytesEncryptor encryptor = new BouncyCastleAesCbcBytesEncryptor(password, DatatypeConverter.printHexBinary(salt));
    final byte[] result = encryptor.encrypt(plainText.getBytes(StandardCharsets.UTF_8));
    final byte[] ciphertext = new byte[saltLength + result.length];
    System.arraycopy(salt, 0, ciphertext, 0, saltLength);
    System.arraycopy(result, 0, ciphertext, saltLength, result.length);
    return DatatypeConverter.printHexBinary(ciphertext);

  }

  /**
   * Method to encrypt plain text String
   *
   * @param plainText
   * clear text to be encrypted
   * @param password
   * password
   * @param saltlength
   * length of salt
   * @return encrypted text in Hexdecimal format
   */
  public static String encrypt(String plainText, String password, int saltlength) {

    final byte[] salt = new byte[saltlength];
    // Generate the random salt
    SecureRandom secureRandom;
    try {
      secureRandom = SecureRandom.getInstance("SHA1PRNG");
    } catch (final NoSuchAlgorithmException e) {
      throw new IllegalStateException("Algorithm SHA1PRNG not available", e);
    }
    secureRandom.nextBytes(salt);
    return encrypt(plainText, password, salt);
  }

  /**
   * This constructor should not be called
   */
  private StringCipherTools() {
    throw new IllegalStateException(SolidifyConstants.TOOL_CLASS);
  }

}
